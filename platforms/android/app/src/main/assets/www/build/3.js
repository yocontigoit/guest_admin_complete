webpackJsonp([3],{

/***/ 772:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Modalstatus_cancelacionPageModule", function() { return Modalstatus_cancelacionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modalstatus_cancelacion__ = __webpack_require__(815);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Modalstatus_cancelacionPageModule = /** @class */ (function () {
    function Modalstatus_cancelacionPageModule() {
    }
    Modalstatus_cancelacionPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__modalstatus_cancelacion__["a" /* Modalstatus_cancelacionPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__modalstatus_cancelacion__["a" /* Modalstatus_cancelacionPage */]),
            ],
        })
    ], Modalstatus_cancelacionPageModule);
    return Modalstatus_cancelacionPageModule;
}());

//# sourceMappingURL=modalstatus_cancelacion.module.js.map

/***/ }),

/***/ 815:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Modalstatus_cancelacionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_reservacion_reservacion__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Modalstatus_cancelacionPage = /** @class */ (function () {
    function Modalstatus_cancelacionPage(navCtrl, alertCtrl, fb, _providerReserva, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.fb = fb;
        this._providerReserva = _providerReserva;
        this.navParams = navParams;
        this.data = {};
        this.idReserv = this.navParams.get("idReserv");
        console.log("Id Reserv: ", this.idReserv);
        this.idSucursal = this.navParams.get("idSucursal");
        console.log("Id Sucursal: ", this.idSucursal);
        this.myForm = this.fb.group({
            motivo: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            status: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]]
        });
        console.log("Motivo seleccionada: ", this.data.motivo);
        console.log("Estatus seleccionada: ", this.data.status);
    }
    Modalstatus_cancelacionPage.prototype.ionViewDidLoad = function () {
        // this.getMesas(this.idSucursal);
    };
    Modalstatus_cancelacionPage.prototype.saveData = function () {
        console.log(this.myForm.value);
        console.log('saveDataMotivo: ', this.data.motivo);
        console.log('saveDataStatus: ', this.data.status);
        var info = {
            motivo: this.data.motivo,
            status: this.data.status,
            otro: "xxx",
        };
        this._providerReserva.cancelarStatus(this.idReserv, info).then(function (respuesta) {
            console.log("Respuesta: ", respuesta);
        });
        this.closeModal();
    };
    // getMesas(idx) {
    //   this._providerReserva.getMesas2(idx).subscribe(mesas => {
    //     console.log("Mesas2", mesas);
    //     this.mesas = mesas;
    //   });
    // }
    Modalstatus_cancelacionPage.prototype.closeModal = function () {
        this.navCtrl.pop();
    };
    Modalstatus_cancelacionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-modalstatus_cancelacion',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\modalstatus_cancelacion\modalstatus_cancelacion.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Cancelar reservacion</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="fondo2">\n\n    <div class="divBtn">\n\n        <button class="btnClose" ion-button icon-only (click)="closeModal()">\n\n            <ion-icon item-right name="ios-close-outline"></ion-icon>\n\n        </button>\n\n    </div>\n\n    \n\n    <form [formGroup]="myForm" novalidate class="card_atras" (ngSubmit)="saveData()">\n\n        <div class="style1">\n\n            <ion-row>\n\n                <ion-col text-center>\n\n                    Motivo\n\n                </ion-col>\n\n                <ion-col text-center class="color2">\n\n                    <ion-item >\n\n                        <ion-select [(ngModel)]="data.motivo"   name="motivo" formControlName="motivo" >\n\n                            <ion-option>Mesa no disponible</ion-option>                                \n\n                            <ion-option>Nos reservamos derecho de admisión</ion-option>\n\n                            <ion-option>Estamos buscando en ticketmaster</ion-option>\n\n                            <ion-option>Se agotaron los espacios para reservar</ion-option>\n\n                        </ion-select>\n\n                    </ion-item>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>    \n\n\n\n        <div class="style1" >\n\n            <ion-row>\n\n                <ion-col text-center>\n\n                    Estatus\n\n                </ion-col>\n\n                <ion-col text-center>\n\n                    <ion-item>\n\n                        <ion-label>Cancelado</ion-label>\n\n                        <ion-input type="hidden" [(ngModel)]="data.status"  name="status" formControlName="status" value="Cancelado"></ion-input>                      \n\n                    </ion-item>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n\n\n        <button ion-button class="btnModal" type="submit" color="light" [disabled]="!myForm.valid">Actualizar</button>\n\n      </form>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\modalstatus_cancelacion\modalstatus_cancelacion.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_reservacion_reservacion__["a" /* ReservacionProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], Modalstatus_cancelacionPage);
    return Modalstatus_cancelacionPage;
}());

//# sourceMappingURL=modalstatus_cancelacion.js.map

/***/ })

});
//# sourceMappingURL=3.js.map