webpackJsonp([2],{

/***/ 776:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModiareazonaPageModule", function() { return ModiareazonaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modiareazona__ = __webpack_require__(816);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ModiareazonaPageModule = /** @class */ (function () {
    function ModiareazonaPageModule() {
    }
    ModiareazonaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__modiareazona__["a" /* ModiareazonaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__modiareazona__["a" /* ModiareazonaPage */]),
            ],
        })
    ], ModiareazonaPageModule);
    return ModiareazonaPageModule;
}());

//# sourceMappingURL=modiareazona.module.js.map

/***/ }),

/***/ 816:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModiareazonaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_reservacion_reservacion__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ModiareazonaPage = /** @class */ (function () {
    function ModiareazonaPage(navCtrl, alertCtrl, fb, _providerReserva, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.fb = fb;
        this._providerReserva = _providerReserva;
        this.navParams = navParams;
        this.data = {};
        this.idReserv = this.navParams.get("idReserv");
        console.log("Id Reserv: ", this.idReserv);
        this.idSucursal = this.navParams.get("idSucursal");
        console.log("Id Sucursal: ", this.idSucursal);
        this.idArea = this.navParams.get("idArea");
        console.log("Area: ", this.idArea);
        this.idZona = this.navParams.get("idZona");
        console.log("Zona: ", this.idZona);
        this.myForm = this.fb.group({
            area: [" ", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            zona: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]]
        });
        console.log("Area seleccionada: ", this.data.area);
    }
    ModiareazonaPage.prototype.ionViewDidLoad = function () {
        this.getAreas(this.idSucursal);
    };
    ModiareazonaPage.prototype.getAreas = function (idx) {
        var _this = this;
        this._providerReserva.getAreas(idx).subscribe(function (areas) {
            console.log("areas", areas);
            _this.areas = areas;
        });
    };
    ModiareazonaPage.prototype.getZonas = function () {
        var _this = this;
        var idx = this.idSucursal;
        var area = this.data.area;
        this._providerReserva.getZonas(idx, area).subscribe(function (zonas) {
            console.log("zona", zonas);
            _this.zonas = zonas;
        });
    };
    ModiareazonaPage.prototype.alertConsumo = function () {
        var zona = this.data.zona;
        var alertMesas = this.alertCtrl.create({
            message: "<div text-center> Esta zona cuenta con un consumo sugerido de " +
                "<br><br>" +
                "<b>" +
                150 +
                "</b>" +
                "</div>",
            buttons: [
                {
                    text: "Aceptar",
                    handler: function () {
                        console.log("Buy clicked");
                    }
                }
            ]
        });
        alertMesas.present();
    };
    ModiareazonaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-modiareazona',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\modiareazona\modiareazona.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <!-- <ion-title>Modificar Área y Zona</ion-title> -->\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="fondo2">\n\n    <form [formGroup]="myForm" novalidate class="card_atras">\n\n        <div class="style1">\n\n            <ion-row>\n\n                <ion-col text-center>\n\n                    Área\n\n                </ion-col>\n\n                <ion-col text-center>\n\n                    <ion-item>\n\n                        <ion-label>Área</ion-label>\n\n                        <ion-select [(ngModel)]="data.area" multiple="false" cancelText="Cancelar" okText="Aceptar" name="area" formControlName="area" (ionChange)=getZonas()>\n\n                            <div>\n\n                                <ion-option *ngFor="let area of areas" value="{{area.$key}}">{{area.nombre}}</ion-option>\n\n                            </div>\n\n                        </ion-select>\n\n                    </ion-item>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n        <div class="style1">\n\n            <ion-row>\n\n                <ion-col text-center>\n\n                    Zona\n\n                </ion-col>\n\n                <ion-col text-center>\n\n                    <ion-item>\n\n                        <ion-label>Zona</ion-label>\n\n                        <ion-select [(ngModel)]="data.zona" multiple="false" cancelText="Cancelar" okText="Aceptar" name="zona" formControlName="zona" (ionChange)=alertConsumo()>\n\n                            <div>\n\n                                <ion-option *ngFor="let zona of zonas" value="{{zona.$key}}">{{ zona.nombre }}\n\n                                </ion-option>\n\n                            </div>\n\n                        </ion-select>\n\n                    </ion-item>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n    </form>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\modiareazona\modiareazona.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_reservacion_reservacion__["a" /* ReservacionProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], ModiareazonaPage);
    return ModiareazonaPage;
}());

//# sourceMappingURL=modiareazona.js.map

/***/ })

});
//# sourceMappingURL=2.js.map