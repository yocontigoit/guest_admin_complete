webpackJsonp([4],{

/***/ 770:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalstatusPageModule", function() { return ModalstatusPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modalstatus__ = __webpack_require__(814);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ModalstatusPageModule = /** @class */ (function () {
    function ModalstatusPageModule() {
    }
    ModalstatusPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__modalstatus__["a" /* ModalstatusPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__modalstatus__["a" /* ModalstatusPage */]),
            ],
        })
    ], ModalstatusPageModule);
    return ModalstatusPageModule;
}());

//# sourceMappingURL=modalstatus.module.js.map

/***/ }),

/***/ 814:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalstatusPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_reservacion_reservacion__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ModalstatusPage = /** @class */ (function () {
    function ModalstatusPage(navCtrl, alertCtrl, fb, _providerReserva, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.fb = fb;
        this._providerReserva = _providerReserva;
        this.navParams = navParams;
        this.data = {};
        this.idReserv = this.navParams.get("idReserv");
        console.log("Id Reserv: ", this.idReserv);
        this.idSucursal = this.navParams.get("idSucursal");
        console.log("Id Sucursal: ", this.idSucursal);
        // this.idArea = this.navParams.get("idArea");
        // console.log("Area: ", this.idArea);
        // this.idZona = this.navParams.get("idZona");
        // console.log("Zona: ", this.idZona);
        this.myForm = this.fb.group({
            mesa: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            status: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]]
        });
        console.log("Mesa seleccionada: ", this.data.mesa);
        console.log("Estatus seleccionada: ", this.data.status);
    }
    ModalstatusPage.prototype.ionViewDidLoad = function () {
        // this.getAreas(this.idSucursal);
        this.getMesas(this.idSucursal);
    };
    ModalstatusPage.prototype.saveData = function () {
        console.log(this.myForm.value);
        var info = {
            mesa: this.data.mesa,
            status: this.data.status,
        };
        this._providerReserva
            .updateStatus(this.idReserv, info)
            .then(function (respuesta) {
            console.log("Respuesta: ", respuesta);
        });
    };
    ModalstatusPage.prototype.getMesas = function (idx) {
        var _this = this;
        this._providerReserva.getMesas2(idx).subscribe(function (mesas) {
            console.log("Mesas2", mesas);
            _this.mesas = mesas;
        });
    };
    // loadReservacion(idx) {
    // }
    // getAreas(idx) {
    //   this._providerReserva.getAreas(idx).subscribe(areas => {
    //     console.log("areas", areas);
    //     this.areas = areas;
    //   });
    // }
    // getZonas() {
    //   const idx = this.idSucursal;
    //   const area = this.data.area;
    //   this._providerReserva.getZonas(idx, area).subscribe(zonas => {
    //     console.log("zona", zonas);
    //     this.zonas = zonas;
    //   });
    // }
    // alertConsumo() {
    //   const zona = this.data.zona;
    //     let alertMesas = this.alertCtrl.create({
    //       message:
    //         "<div text-center> Esta zona cuenta con un consumo sugerido de " +
    //         "<br><br>" +
    //         "<b>" +
    //         150 +
    //         "</b>" +
    //         "</div>",
    //       buttons: [
    //         {
    //           text: "Aceptar",
    //           handler: () => {
    //             console.log("Buy clicked");
    //           }
    //         }
    //       ]
    //     });
    //     alertMesas.present();
    // }
    ModalstatusPage.prototype.closeModal = function () {
        this.navCtrl.pop();
    };
    ModalstatusPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-modalstatus',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\modalstatus\modalstatus.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <!-- <ion-title>Modificar Área y Zona</ion-title> -->\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="fondo2">\n\n    <div class="divBtn">\n\n        <button class="btnClose" ion-button icon-only (click)="closeModal()">\n\n            <ion-icon item-right name="ios-close-outline"></ion-icon>\n\n        </button>\n\n    </div>\n\n    \n\n    <form [formGroup]="myForm" novalidate class="card_atras" (ngSubmit)="saveData()">\n\n        <!-- <form [formGroup]="myForm" ></form> -->\n\n        <!-- <form [formGroup]="myForm" (ngSubmit)="saveData()"></form> -->\n\n        <div class="style1">\n\n            <ion-row>\n\n                <ion-col text-center>\n\n                    Estatus\n\n                </ion-col>\n\n                <ion-col text-center class="color2">\n\n                    <ion-item>\n\n                        <ion-label>Estatus</ion-label>\n\n                        <!-- <ion-select color="red" [(ngModel)]="data.area" multiple="false" cancelText="Cancelar" okText="Aceptar" name="area" formControlName="area" (ionChange)=getZonas()> -->\n\n                            <ion-select [(ngModel)]="data.status" multiple="false" cancelText="Cancelar" okText="Aceptar" name="status" formControlName="status" >\n\n                            <div >\n\n                                <!-- <ion-option  *ngFor="let area of areas" value="{{area.$key}}">{{area.nombre}}</ion-option> -->\n\n                                \n\n                                <ion-option   value="Creando" >Creando</ion-option>                                \n\n                                <ion-option   value="CreadaCompartida" >Creada Compartida</ion-option>  \n\n                                <ion-option  value="Modificado">Modificado</ion-option>\n\n                                <ion-option   value="Compartida" >Compartida</ion-option>\n\n                                <ion-option  value="Pagando">Pagando</ion-option>\n\n                                <ion-option   value="Aceptado" >Aceptado</ion-option>\n\n                                <ion-option   value="Terminado" >Terminado</ion-option>\n\n                            </div>\n\n                        </ion-select>\n\n                    </ion-item>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n        <div class="style1" >\n\n            <ion-row>\n\n                <ion-col text-center>\n\n                    No Mesa\n\n                </ion-col>\n\n                <ion-col text-center>\n\n                    <ion-item>\n\n                        \n\n                        <!-- <ion-label>Mesa</ion-label> -->\n\n                        <ion-input type="number" [(ngModel)]="data.mesa" name="mesa" formControlName="mesa" placeholder="Numero de mesa"></ion-input>\n\n                        <!-- <ion-select [(ngModel)]="data.mesa" multiple="false" cancelText="Cancelar" okText="Aceptar" name="mesa" formControlName="mesa" > -->\n\n                            <!-- <div> -->\n\n                                <!-- <ion-option *ngFor="let mesa of mesas" value="{{mesa.uid}}">No Mesa {{ mesa.noMesa }}</ion-option> -->\n\n                                <!-- <ion-option value="0iCJvbe9uGvw4Q0zX5yT">1</ion-option> -->\n\n                                <!-- <ion-option *ngFor="let mesa of mesas" value="{{mesa.uid}}" >No Mesa {{mesa.noMesa}}</ion-option> -->\n\n                            <!-- </div> -->\n\n                        <!-- </ion-select> -->\n\n\n\n                    </ion-item>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n\n\n        <button ion-button class="btnModal" type="submit" color="light" [disabled]="!myForm.valid">Actualizar</button>\n\n        <!-- <button ion-button class="btnModal" type="submit" color="light" (click)="saveData(myForm)">Actualizar</button> -->\n\n        <!-- <button [disabled]="myForm.invalid" ion-button type="submit">Connexion</button> -->\n\n        <!-- <button ion-button type="submit" block>Add Todo</button> -->\n\n    </form>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\modalstatus\modalstatus.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_reservacion_reservacion__["a" /* ReservacionProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], ModalstatusPage);
    return ModalstatusPage;
}());

//# sourceMappingURL=modalstatus.js.map

/***/ })

});
//# sourceMappingURL=4.js.map