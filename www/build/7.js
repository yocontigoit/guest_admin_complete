webpackJsonp([7],{

/***/ 753:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminmesasPageModule", function() { return AdminmesasPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__adminmesas__ = __webpack_require__(811);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AdminmesasPageModule = /** @class */ (function () {
    function AdminmesasPageModule() {
    }
    AdminmesasPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__adminmesas__["a" /* AdminmesasPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__adminmesas__["a" /* AdminmesasPage */]),
            ],
        })
    ], AdminmesasPageModule);
    return AdminmesasPageModule;
}());

//# sourceMappingURL=adminmesas.module.js.map

/***/ }),

/***/ 811:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminmesasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminmesasPage = /** @class */ (function () {
    function AdminmesasPage(navCtrl, navParams, alertCtrl, SucProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.SucProv = SucProv;
        this.area = {};
        this.zona = {};
        this.idArea = this.navParams.get('idArea');
        this.idZona = this.navParams.get('idZona');
    }
    AdminmesasPage.prototype.ionViewDidLoad = function () {
        this.consultaArea();
        this.consultaZona();
        this.getMesas();
    };
    AdminmesasPage.prototype.consultaArea = function () {
        var _this = this;
        this.SucProv.getArea(this.idArea).subscribe(function (s) {
            _this.area = s;
            console.log('Area', s);
        });
    };
    AdminmesasPage.prototype.consultaZona = function () {
        var _this = this;
        this.SucProv.getZona(this.idZona).subscribe(function (z) {
            _this.zona = z;
            console.log('Zona', z);
        });
    };
    AdminmesasPage.prototype.getMesas = function () {
        var _this = this;
        this.SucProv.getMesas(this.idZona).subscribe(function (mesas) {
            _this.mesas = mesas;
            var longitud = _this.mesas.length;
            console.log("Esta es la longitud: ", longitud);
            console.log("mesas JAJA: ", _this.mesas);
        });
    };
    AdminmesasPage.prototype.modificaMesa = function (idMesa, numMesa, numPersonas) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Modificar Mesa',
            inputs: [
                {
                    name: 'numMesa',
                    value: numMesa,
                    type: 'number'
                },
                {
                    name: 'numPersonas',
                    value: numPersonas,
                    type: 'number'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Eliminar',
                    handler: function (data) {
                        console.log('Cancel clicked');
                        _this.eliminarMesa(idMesa);
                        // this.navCtrl.setPages([{page:'AdminSucursalPerfilPage'}]);
                    }
                },
                {
                    text: 'Actualizar',
                    handler: function (data) {
                        console.log('Actualizar clicked');
                        _this.modificarMesa(data.numMesa, data.numPersonas, idMesa);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminmesasPage.prototype.modificarMesa = function (numMesa, numPersonas, idMesa) {
        if (numMesa == '') {
            this.alertCtrl.create({
                title: 'El número de mesa es obligatorio',
                buttons: ['Aceptar']
            }).present();
        }
        else if (numPersonas == '') {
            this.SucProv.alertCtrl.create({
                title: 'El número de personas por mesa es obligatorio',
                buttons: ['Aceptar']
            }).present();
        }
        else {
            this.SucProv.modificarMesa(numMesa, numPersonas, idMesa);
        }
    };
    AdminmesasPage.prototype.eliminarMesa = function (idMesa) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Eliminar Mesa',
            subTitle: 'Esta seguro que desea eliminar la mesa',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Aceptar',
                    handler: function (data) {
                        _this.SucProv.eliminarMesa(idMesa);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminmesasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-adminmesas',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\adminmesas\adminmesas.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <!-- <ion-title>Mesas</ion-title> -->\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="fondo2">\n\n\n\n    <ion-list>\n\n        <ion-list-header>\n\n            Administrar Mesas\n\n        </ion-list-header>\n\n        <ion-item>\n\n            <h2>{{area.nombre}}</h2>\n\n            <p>Área</p>\n\n        </ion-item>\n\n        <ion-item>\n\n            <h2>{{zona.nombre}}</h2>\n\n            <p>Zona</p>\n\n        </ion-item>\n\n    </ion-list>\n\n    <ion-item-group>\n\n        <ion-row class="grid-3 grid-example">\n\n            <ion-col width-50 class="grid-item" *ngFor="let mesa of mesas">\n\n                <div class="grid-item-wrapper">\n\n                    <div class="grid-item-inner" (click)="modificaMesa(mesa.id, mesa.mesa, mesa.numPersonas)">\n\n                        <ion-icon class="icono"><img src="../assets/imgs/icons/mesa.png" /></ion-icon>\n\n                        <div class="grid-item-inner" class="color1">\n\n                            {{mesa.numPersonas}}\n\n                        </div>\n\n                    </div>\n\n                    <div class="numMesas" text-center>\n\n                        {{mesa.mesa}}\n\n                    </div>\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-item-group>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\adminmesas\adminmesas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */]])
    ], AdminmesasPage);
    return AdminmesasPage;
}());

//# sourceMappingURL=adminmesas.js.map

/***/ })

});
//# sourceMappingURL=7.js.map