webpackJsonp([55],{

/***/ 118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_admin_evento_home_admin_evento_home__ = __webpack_require__(460);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_admin_carta_home_admin_carta_home__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__admin_users_list_admin_users_list__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__admin_menu_reservacion_admin_menu_reservacion__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__admin_sucursal_perfil_admin_sucursal_perfil__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_monitoreo_reservas_monitoreo_reservas__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_push_noti_push_noti__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__admin_perfil_empleado_admin_perfil_empleado__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__admin_monitear_reserv_admin_monitear_reserv__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__admin_reservaciones_curso_admin_reservaciones_curso__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__admin_sucursal_list_admin_sucursal_list__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__admin_login_admin_login__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__admin_users_guest_admin_users_guest__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__corte_historial_corte_historial__ = __webpack_require__(125);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








//import { AdminUsersPage } from "../../pages/admin-users/admin-users";




//import * as firebase from 'firebase/app';










var AdminHomePage = /** @class */ (function () {
    function AdminHomePage(navCtrl, menuCtrl, navParams, authProvider, sucProv, firebase, _providerPushNoti, afs, monRes) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.navParams = navParams;
        this.authProvider = authProvider;
        this.sucProv = sucProv;
        this.firebase = firebase;
        this._providerPushNoti = _providerPushNoti;
        this.afs = afs;
        this.monRes = monRes;
        this._sucursal = {};
        this._userEmpleado = {};
        this.reservaciones = "reserv";
        this.sucursal = this.firebase.auth.currentUser;
        console.log("Esta es la sucursal: ", this.sucursal);
        this.menu = "menu";
        if (this.sucursal != null) {
            //this.uid = localStorage.getItem('uidSucursal');
            this.uid = this.sucursal.uid;
            this.email = this.sucursal.email;
            console.log('sucursalid', this.uid);
            // photoURL = sucursal.photoURL;
            // uid = sucursal.uid;
            // Recibiendo información del usuario=sucursal
            this.authProvider.getUserBd(this.uid).subscribe(function (s) {
                _this._sucursal = s;
                console.log('sucursal', _this._sucursal);
            });
            // Traemos la información del empleado
            this.authProvider.getUserAdmins(this.uid).subscribe(function (s) {
                _this._userEmpleado = s;
                console.log('empleado uid', _this.uid);
                console.log('empleado info', _this._userEmpleado);
            });
            //obtener fecha actual
            var dateObj = new Date();
            var anio = dateObj.getFullYear().toString();
            var mes = dateObj.getMonth().toString();
            var dia = dateObj.getDate();
            var mesArray = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
            if (dia >= 1 && dia <= 9) {
                var diaCero = '0' + dia;
                this.formatoFecha = anio + '-' + mesArray[mes] + '-' + diaCero;
            }
            else {
                this.formatoFecha = anio + '-' + mesArray[mes] + '-' + dia;
            }
            console.log("fecha actual");
            console.log(this.formatoFecha);
            //reservaciones proximas estatus aceptado
            this.monRes.getReservaciones(this.uid, this.formatoFecha).subscribe(function (res) {
                _this.resSucursal = res;
                //reservaciones  proximas estatus aceptadocompartida
                _this.monRes.getReservacionesAcepCom(_this.uid, _this.formatoFecha).subscribe(function (res3) {
                    _this.resSucursalAcepCom = res3;
                    _this.totalAcepCom = _this.resSucursalAcepCom.length;
                    _this.totalReservaciones = _this.resSucursal.length + _this.totalAcepCom;
                    console.log('total reservaciones acep', _this.totalReservaciones);
                });
            });
            //reservaciones en curso
            this.monRes.getReservacionesCurso(this.uid, this.formatoFecha).subscribe(function (res2) {
                _this.resSucursal2 = res2;
                _this.totalReservaciones2 = _this.resSucursal2.length;
                console.log('reservaciones', _this.resSucursal2);
                console.log('total reservaciones', _this.totalReservaciones2);
            });
            this.afs.collection('users', function (ref) { return ref.where('uid', '==', _this.uid); }).valueChanges().subscribe(function (data) {
                _this.sucursales = data;
                _this.sucursales.forEach(function (element) {
                    var uidSucursal = element.uidSucursal;
                    //reservaciones proximas
                    _this.monRes.getReservaciones(uidSucursal, _this.formatoFecha).subscribe(function (res) {
                        _this.resSucursal = res;
                        _this.totalReservaciones = _this.resSucursal.length;
                        console.log('reservaciones', _this.resSucursal);
                        console.log('total reservaciones', _this.totalReservaciones);
                    });
                    //reservaciones en curso
                    _this.monRes.getReservacionesCurso(uidSucursal, _this.formatoFecha).subscribe(function (res2) {
                        _this.resSucursal2 = res2;
                        _this.totalReservaciones2 = _this.resSucursal2.length;
                        console.log('reservaciones', _this.resSucursal2);
                        console.log('total reservaciones', _this.totalReservaciones2);
                    });
                });
            });
        }
        console.log('Este es el UID de la sucursal ' + this.uid);
        console.log('Este es el correo de la sucursal ' + this.email);
        //total de Sucursales
        this.afs.collection('sucursales', function (ref) { return ref.where('status', '==', 'activo'); }).valueChanges().subscribe(function (data) {
            _this.resultadoSucursales = data;
            console.log('sucursales activas ' + _this.resultadoSucursales);
            _this.totalSucursales = _this.resultadoSucursales.length;
        });
        //total de Usuarios empleados
        this.afs.collection('users', function (ref) { return ref.where('type', '==', 'e'); })
            .valueChanges().subscribe(function (u) {
            _this.admins = u;
            console.log('admins', _this.admins);
            _this.totalEmpleados = _this.admins.length;
        });
    }
    AdminHomePage.prototype.goMonitorear = function (menu) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__admin_monitear_reserv_admin_monitear_reserv__["a" /* AdminMonitearReservPage */], { menu: menu });
    };
    AdminHomePage.prototype.goMonitorearCurso = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__admin_reservaciones_curso_admin_reservaciones_curso__["a" /* AdminReservacionesCursoPage */]);
    };
    AdminHomePage.prototype.ionViewDidLoad = function () {
        console.log('HOME PAGE');
        this._providerPushNoti.init_push_noti();
    };
    AdminHomePage.prototype.logout = function () {
        this.authProvider.logout();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
    };
    AdminHomePage.prototype.goUsers = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__admin_users_list_admin_users_list__["a" /* AdminUsersListPage */]);
    };
    AdminHomePage.prototype.goEventos = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_admin_evento_home_admin_evento_home__["a" /* AdminEventoHomePage */]);
    };
    AdminHomePage.prototype.goCarta = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_admin_carta_home_admin_carta_home__["a" /* AdminCartaHomePage */]);
    };
    AdminHomePage.prototype.goReservacion = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__admin_menu_reservacion_admin_menu_reservacion__["a" /* AdminMenuReservacionPage */]);
    };
    AdminHomePage.prototype.goPerfilEmpleado = function (uid) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__admin_perfil_empleado_admin_perfil_empleado__["a" /* AdminPerfilEmpleadoPage */], { uid: uid });
    };
    AdminHomePage.prototype.goUsuarioHis = function () {
    };
    AdminHomePage.prototype.goPerfilSucursal = function (uid) {
        console.log('aqui uid', uid);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__admin_sucursal_perfil_admin_sucursal_perfil__["a" /* AdminSucursalPerfilPage */], { uid: uid });
    };
    AdminHomePage.prototype.goSucursal = function () {
        this.usertipo = 'master';
        console.log(this.usertipo);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__admin_sucursal_list_admin_sucursal_list__["a" /* AdminSucursalListPage */], { usertipo: this.usertipo });
    };
    AdminHomePage.prototype.goListaUsuariosEmpleados = function (home) {
        console.log('aqui home', home);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_19__admin_users_guest_admin_users_guest__["a" /* AdminUsersGuestPage */], { home: home });
    };
    AdminHomePage.prototype.goCorteHistorial = function (x) {
        console.log('Historial-Corte', x);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_20__corte_historial_corte_historial__["a" /* CorteHistorialPage */], this.sucProv.selectedSucursalItem = Object.assign({}, x));
    };
    AdminHomePage.prototype.salir = function () {
        localStorage.setItem("isLogin", 'false');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_18__admin_login_admin_login__["a" /* AdminLoginPage */]);
        this.menuCtrl.close();
    };
    AdminHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-home',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-home\admin-home.html"*/'<ion-header>\n\n\n\n    <ion-navbar class="borde">\n\n        <ion-row>\n\n            <ion-col col-1 align="center">\n\n                <img src="./assets/content/line2.png" class="imgLi" alt="">\n\n            </ion-col>\n\n            <ion-col col-8>\n\n\n\n                <div *ngIf="_sucursal?.type == \'a\'">\n\n                    <div class="AdminEvento" (click)="goPerfilSucursal(this.sucursal.uid)">\n\n                        {{ _sucursal?.displayName }}\n\n                    </div>\n\n                    <div class="sucursal" (click)="goPerfilSucursal(this.sucursal.uid)"> Ver perfil</div>\n\n                </div>\n\n\n\n                <div *ngIf="_userEmpleado?.type == \'e\' || _userEmpleado?.type == \'master\' ">\n\n                    <div class="AdminEvento" (click)="goPerfilEmpleado(_userEmpleado.uid)">\n\n                        {{ _userEmpleado?.displayName }}\n\n                    </div>\n\n                    <div class="sucursal" (click)="goPerfilEmpleado(_userEmpleado.uid)"> Ver perfil</div>\n\n                </div>\n\n\n\n                <div class="centrart" *ngIf="_userEmpleado?.type == \'e\'">\n\n                    <!-- <h3>Bienvenido</h3> -->\n\n                    <p>{{ _userEmpleado?.displayName }} </p>\n\n                </div>\n\n                <div class="centrart" *ngIf="_userEmpleado?.type == \'coordinacion\'">\n\n                    <!-- <h3>Bienvenido</h3> -->\n\n                    <p>{{ _userEmpleado?.displayName }} </p>\n\n                </div>\n\n                <div class="centrart" *ngIf="_userEmpleado?.type == \'rp\'">\n\n                    <!-- <h3>Bienvenido</h3> -->\n\n                    <p>{{ _userEmpleado?.displayName }} </p>\n\n                </div>\n\n                <div class="centrart" *ngIf="_userEmpleado?.type == \'capitan_mesero\'">\n\n                    <!-- <h3>Bienvenido</h3> -->\n\n                    <p>{{ _userEmpleado?.displayName }} </p>\n\n                </div>\n\n\n\n                <!-- <div class="AdminEvento">{{ _cap.selectedEventoItem.titulo }}</div>\n\n                <div class="sucursal">Sucursal</div> -->\n\n            </ion-col>\n\n            <ion-col>\n\n                <div align=\'end\'>\n\n                    <img src="./assets/content/corona.png" style="width: 40%;" alt="">\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content class="fondo_admin card-background-page">\n\n\n\n    <br>\n\n\n\n    <ion-row>\n\n        <ion-col></ion-col>\n\n        <ion-col>\n\n            <img src="./assets/content/logo.png" alt="">\n\n        </ion-col>\n\n        <ion-col></ion-col>\n\n    </ion-row>\n\n\n\n    <div class="bienvenido" *ngIf="_userEmpleado?.type == \'e\'">\n\n        <h3>¡Bienvenido!</h3>\n\n    </div>\n\n    <div class="bienvenido" *ngIf="_userEmpleado?.type == \'coordinacion\'">\n\n        <h3>¡Bienvenido!</h3>\n\n    </div>\n\n    <div class="bienvenido" *ngIf="_userEmpleado?.type == \'rp\'">\n\n        <h3>¡Bienvenido!</h3>\n\n    </div>\n\n    <div class="bienvenido" *ngIf="_userEmpleado?.type == \'capitan_mesero\'">\n\n        <h3>¡Bienvenido!</h3>\n\n    </div>\n\n\n\n    <br>\n\n\n\n    <!-- (listo) -->\n\n    <ng-container *ngIf="_sucursal">\n\n\n\n        <ion-row>\n\n            <ion-col align="center">\n\n                <ion-row>\n\n                    <ion-col>\n\n                        <img src="./assets/content/icono1.png" (click)="goMonitorear(menu)" class="imgRes" alt="">\n\n                    </ion-col>\n\n                    <ion-col>\n\n                        <ion-card class="nombreCar" (click)="goMonitorear(menu)">\n\n                            <p style="margin-top: -14px;">&nbsp;</p>\n\n                            Reservaciones próximas\n\n                        </ion-card>\n\n                        <!-- <button ion-button class="nombreRes"> Reservaciones próximas </button> -->\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-col>\n\n            <ion-col align="center">\n\n                <ion-row>\n\n                    <ion-col>\n\n                        <img src="./assets/content/icono4.png" (click)="goMonitorearCurso()" class="imgRes2" alt="">\n\n                    </ion-col>\n\n                    <ion-col>\n\n                        <ion-card class="nombreCar" (click)="goMonitorearCurso()">\n\n                            <p style="margin-top: -14px;">&nbsp;</p>\n\n                            &nbsp;&nbsp;&nbsp;&nbsp;Reservaciones en curso\n\n                        </ion-card>\n\n                        <!-- <button ion-button class="nombreRes"> Reservaciones en curso </button> -->\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <br> <br> <br>\n\n\n\n        <ion-row>\n\n            <ion-col></ion-col>\n\n            <ion-col align="center">\n\n                <p style="color: #fff;">{{this.totalReservaciones2}} EN CURSO</p>\n\n                <img src="../assets/content/curso.png">\n\n            </ion-col>\n\n            <ion-col></ion-col>\n\n        </ion-row>\n\n\n\n    </ng-container>\n\n\n\n    <br>\n\n\n\n    <!-- Tablero de reservaciones para los usuarios -->\n\n    <!-- <ng-container *ngIf="_sucursal">\n\n        <div padding>\n\n            <ion-segment [(ngModel)]="reservaciones">\n\n                <ion-segment-button active value="reserv">\n\n                    Reservaciones próximas\n\n                </ion-segment-button>\n\n                <ion-segment-button value="encur">\n\n                    Reservaciones en curso\n\n                </ion-segment-button>\n\n            </ion-segment>\n\n        </div>\n\n        <div [ngSwitch]="reservaciones">\n\n            <ion-list class="cl" *ngSwitchCase="\'reserv\'">\n\n                <ion-item class="cl">\n\n                    <button class="cl" ion-item (click)="goMonitorear(menu)">\n\n                        <ion-thumbnail item-start>\n\n                            <img src="../assets/imgs/icons/calendario.png">\n\n                        </ion-thumbnail>\n\n                        <h1>{{this.totalReservaciones}} PRÓXIMAS</h1>\n\n                    </button>\n\n                </ion-item>\n\n            </ion-list>\n\n            <ion-list class="cl" *ngSwitchCase="\'encur\'">\n\n                <ion-item class="cl">\n\n                    <button class="cl" ion-item (click)="goMonitorearCurso()">\n\n                        <ion-thumbnail item-start>\n\n                            <img src="../assets/imgs/icons/admin-evento.png">\n\n                        </ion-thumbnail>\n\n                        <h1>{{this.totalReservaciones2}} EN CURSO</h1>\n\n                    </button>\n\n                </ion-item>\n\n            </ion-list>\n\n        </div>\n\n    </ng-container> -->\n\n\n\n\n\n    <!-- Tablero de reservaciones para los usuarios  corrdinacion (listo)-->\n\n    <ng-container *ngIf="_userEmpleado?.type == \'coordinacion\' || _userEmpleado?.type == \'rp\'">\n\n        <ion-row>\n\n            <ion-col align="center">\n\n                <ion-row>\n\n                    <ion-col>\n\n                        <img src="./assets/content/icono1.png" (click)="goMonitorear(menu)" class="imgRes" alt="">\n\n                    </ion-col>\n\n                    <ion-col>\n\n                        <ion-card class="nombreCar" (click)="goMonitorear(menu)">\n\n                            <p style="margin-top: -14px;">&nbsp;</p>\n\n                            Reservaciones próximas\n\n                        </ion-card>\n\n                        <!-- <button ion-button class="nombreRes"> Reservaciones próximas </button> -->\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-col>\n\n            <ion-col align="center">\n\n                <ion-row>\n\n                    <ion-col>\n\n                        <img src="./assets/content/icono4.png" (click)="goMonitorearCurso()" class="imgRes2" alt="">\n\n                    </ion-col>\n\n                    <ion-col>\n\n                        <ion-card class="nombreCar" (click)="goMonitorearCurso()">\n\n                            <p style="margin-top: -14px;">&nbsp;</p>\n\n                            &nbsp;&nbsp;&nbsp;&nbsp;Reservaciones en curso\n\n                        </ion-card>\n\n                        <!-- <button ion-button class="nombreRes"> Reservaciones en curso </button> -->\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <br> <br> <br>\n\n\n\n        <ion-row>\n\n            <ion-col></ion-col>\n\n            <ion-col align="center">\n\n                <p style="color: #fff;">{{this.totalReservaciones2}} EN CURSO</p>\n\n                <img src="../assets/content/curso.png">\n\n            </ion-col>\n\n            <ion-col></ion-col>\n\n        </ion-row>\n\n\n\n    </ng-container>\n\n\n\n\n\n    <!-- Solo para el usuario master -->\n\n    <!-- Tablero de reservaciones para los usuarios  corrdinacion-->\n\n    <!-- <ng-container *ngIf="_userEmpleado?.type == \'coordinacion\' || _userEmpleado?.type == \'rp\'">\n\n        <div padding>\n\n            <ion-segment [(ngModel)]="reservaciones">\n\n                <ion-segment-button active value="reserv">\n\n                    Reservaciones próximas\n\n                </ion-segment-button>\n\n                <ion-segment-button value="encur">\n\n                    Reservaciones en curso\n\n                </ion-segment-button>\n\n            </ion-segment>\n\n        </div>\n\n        <div [ngSwitch]="reservaciones">\n\n            <ion-list class="cl" *ngSwitchCase="\'reserv\'">\n\n                <ion-item class="cl">\n\n                    <button class="cl" ion-item (click)="goMonitorear(menu)">\n\n                        <ion-thumbnail item-start>\n\n                            <img src="../assets/imgs/icons/calendario.png">\n\n                        </ion-thumbnail>\n\n                        <h1>{{this.totalReservaciones}} PRÓXIMAS</h1>\n\n                    </button>\n\n                </ion-item>\n\n            </ion-list>\n\n            <ion-list class="cl" *ngSwitchCase="\'encur\'">\n\n                <ion-item class="cl">\n\n                    <button class="cl" ion-item (click)="goMonitorearCurso()">\n\n                        <ion-thumbnail item-start>\n\n                            <img src="../assets/imgs/icons/admin-evento.png">\n\n                        </ion-thumbnail>\n\n                        <h1>{{this.totalReservaciones2}} EN CURSO</h1>\n\n                    </button>\n\n                </ion-item>\n\n            </ion-list>\n\n        </div>\n\n    </ng-container> -->\n\n\n\n\n\n\n\n    <!-- <ion-row>\n\n        <ion-col>\n\n            <img src="./assets/content/icono2.png" class="imgRes3" alt="">\n\n        </ion-col>\n\n        <ion-col>\n\n            <button ion-button full class="nombreCar3" (click)="goReservacion()">Administrar reservaciones</button>\n\n        </ion-col>\n\n    </ion-row> -->\n\n\n\n    <ion-grid>\n\n        <ion-list-header class="accesoA" no-lines>\n\n            Accesos para administrar\n\n        </ion-list-header>\n\n    </ion-grid>\n\n\n\n    <br><br>\n\n    <!-- Solo para el usuario coordinacion (listo) -->\n\n    <!-- <div *ngIf="_userEmpleado?.type == \'coordinacion\'"> -->\n\n    <div *ngIf="_userEmpleado?.type == \'master\'">\n\n        <ion-grid>\n\n            <ion-row>\n\n                <ion-col>\n\n                    <div>\n\n                        <ion-row>\n\n                            <ion-col>\n\n                                <img src="./assets/content/icono1.png" class="imgRes3" alt="">\n\n                            </ion-col>\n\n                            <ion-col>\n\n                                <button ion-button full class="nombreCar3" (click)="goSucursal()">\n\n                                    Sucursales registradas\n\n                                </button>\n\n                            </ion-col>\n\n                        </ion-row>\n\n                        <!-- <button class="cl" ion-item (click)="goSucursal()">\n\n                            <ion-icon><img style="home" class="home" src="../assets/imgs/icons/sucursal.png">\n\n                            </ion-icon>\n\n                            <h4>Sucursales registradas</h4>\n\n                            <h3>{{totalSucursales}}</h3>\n\n                        </button> -->\n\n                    </div>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <div>\n\n                        <ion-row>\n\n                            <ion-col>\n\n                                <img src="./assets/content/icono2.png" class="imgRes3" alt="">\n\n                            </ion-col>\n\n                            <ion-col>\n\n                                <button ion-button full class="nombreCar3" (click)="goListaUsuariosEmpleados(\'home\')">\n\n                                    Usuarios registrados\n\n                                </button>\n\n                            </ion-col>\n\n                        </ion-row>\n\n                        <!-- <button class="cl" ion-item (click)="goListaUsuariosEmpleados(\'home\')">\n\n                            <ion-icon><img style="home" class="home" src="../assets/imgs/icons/admin-user.png">\n\n                            </ion-icon>\n\n                            <h4>Usuarios registrados</h4>\n\n                            <h3>{{totalEmpleados}}</h3>\n\n                        </button> -->\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-grid>\n\n    </div>\n\n    <!--Termina. Solo para el usuario master -->\n\n\n\n\n\n\n\n    <ion-grid>\n\n        <!-- <ion-list-header class="accesoA" no-lines>\n\n            Accesos para administrar\n\n        </ion-list-header> -->\n\n\n\n        <ion-row *ngIf="_sucursal?.type == \'a\'">\n\n            <ion-col>\n\n                <div>\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <img src="./assets/content/icono3.png" class="imgRes3" alt="">\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <button ion-button full class="nombreCar3" (click)="goUsers()">\n\n                                Administración de usuarios</button>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <!-- <button class="cl" ion-item (click)="goUsers()">\n\n                        <ion-icon><img class="home" src="../assets/imgs/icons/admin-user.png"> </ion-icon>\n\n                        <h4>Administración de usuarios</h4>\n\n                    </button> -->\n\n                </div>\n\n            </ion-col>\n\n            <ion-col>\n\n                <div>\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <img src="./assets/content/icono3.png" class="imgRes3" alt="">\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <button ion-button full class="nombreCar3" (click)="goCarta()">\n\n                                Administrar carta móvil</button>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <!-- <button class="cl" ion-item (click)="goCarta()">\n\n                        <ion-icon><img class="home" src="../assets/imgs/icons/admin-carta.png"> </ion-icon>\n\n                        <h4>Administrar carta móvil</h4>\n\n                    </button> -->\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n        <!--ACESSOS PARA USUARIO RP (listo)-->\n\n\n\n        <ion-row *ngIf="_userEmpleado?.type == \'rp\'">\n\n            <ion-col>\n\n                <div>\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <img src="./assets/content/icono3.png" class="imgRes3" alt="">\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <button ion-button full class="nombreCar3" (click)="goUsers()">\n\n                                Administración de usuarios\n\n                            </button>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <!-- <button class="cl" ion-item (click)="goUsers()">\n\n                        <ion-icon><img class="home" src="../assets/imgs/icons/admin-user.png"> </ion-icon>\n\n                        <h4>Administración de usuarios</h4>\n\n                    </button> -->\n\n                </div>\n\n            </ion-col>\n\n            <ion-col>\n\n                <div>\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <img src="./assets/content/icono3.png" class="imgRes3" alt="">\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <button ion-button full class="nombreCar3" (click)="goCarta()">\n\n                                Administrar carta móvil\n\n                            </button>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <!-- <button class="cl" ion-item (click)="goCarta()">\n\n                        <ion-icon><img class="home" src="../assets/imgs/icons/admin-carta.png"> </ion-icon>\n\n                        <h4>Administrar carta móvil</h4>\n\n                    </button> -->\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row *ngIf="_sucursal?.type == \'a\'">\n\n            <ion-col>\n\n                <div>\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <img src="./assets/content/icono3.png" class="imgRes3" alt="">\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <button ion-button full class="nombreCar3" (click)="goEventos()">\n\n                                Administración de usuarios\n\n                            </button>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <!-- <button class="cl" ion-item (click)="goEventos()">\n\n                        <ion-icon><img class="home" src="../assets/imgs/icons/admin-evento.png"> </ion-icon>\n\n                        <h4>Administrar eventos </h4>\n\n                    </button> -->\n\n                </div>\n\n            </ion-col>\n\n            <!--<ion-col>\n\n                <div>\n\n                    <button class="cl" ion-item (click)="goUsuarioHis()">\n\n                          <ion-icon><img class="home" src="../assets/imgs/icons/admin-historial.png"> </ion-icon>\n\n                         <h4>Historial de usuarios </h4>\n\n                    </button>\n\n                </div>\n\n            </ion-col>-->\n\n        </ion-row>\n\n        <!--ACCESOS USUARIO COORDINACION (listo)-->\n\n        <ion-row *ngIf="_userEmpleado?.type == \'coordinacion\'">\n\n            <ion-row>\n\n                <ion-col></ion-col>\n\n                <ion-col align="center">\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <img src="./assets/content/icono3.png" class="imgRes" alt="">\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <button ion-button full class="nombreCar2" (click)="goEventos()">Administrar\n\n                                eventos</button>\n\n                            <!-- <ion-card class="nombreCar2" (click)="goEventos()">\n\n                                <p style="margin-top: -12px">&nbsp;</p>\n\n                                Administrar eventos \n\n                            </ion-card> -->\n\n                            <!-- <button ion-button class="nombreRes"> Reservaciones próximas </button> -->\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </ion-col>\n\n                <ion-col></ion-col>\n\n            </ion-row>\n\n\n\n            <!-- <ion-col>\n\n                <div>\n\n                    <button class="cl" ion-item (click)="goEventos()">\n\n                        <ion-icon><img class="home" src="../assets/imgs/icons/admin-evento.png"> </ion-icon>\n\n                        <h4>Administrar eventos </h4>\n\n                    </button>\n\n                </div>\n\n            </ion-col> -->\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-col>\n\n                <div>\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <img src="./assets/content/icono2.png" class="imgRes3" alt="">\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <button ion-button full class="nombreCar3" (click)="goReservacion()">Administrar\n\n                                reservaciones</button>\n\n                        </ion-col>\n\n                    </ion-row>\n\n\n\n                    <!-- <button class="cl" ion-item (click)="goReservacion()">\n\n                        <ion-icon><img class="home" src="../assets/imgs/icons/corona.png"> </ion-icon>\n\n                        <h4>Administrar reservaciones</h4>\n\n                    </button> -->\n\n                </div>\n\n            </ion-col>\n\n            <ion-col *ngIf="_sucursal?.type == \'a\'">\n\n                <div>\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <img src="./assets/content/icono3.png" class="imgRes3" alt="">\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <button ion-button full class="nombreCar3" href="https://guestreservation-8b24b.web.app">\n\n                                Administrar croquis\n\n                            </button>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </div>\n\n                <!-- <div>\n\n                    <a href="https://guestreservation-8b24b.web.app" target="_blank">\n\n                        <button class="cl" ion-item>\n\n                            <ion-icon><img class="home" src="../assets/imgs/icons/corona.png"> </ion-icon>\n\n                            <h4>Administrar croquis</h4>\n\n                        </button>\n\n                    </a>\n\n                </div> -->\n\n            </ion-col>\n\n            <!--ACESSOS PARA USUARIO RP-->\n\n            <ion-col *ngIf="_userEmpleado?.type == \'rp\'">\n\n                <div>\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <img src="./assets/content/icono3.png" class="imgRes3" alt="">\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <button ion-button full class="nombreCar3" href="https://guestreservation-8b24b.web.app">\n\n                                Administrar croquis\n\n                            </button>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </div>\n\n                <!-- <div>\n\n                    <a href="https://guestreservation-8b24b.web.app" target="_blank">\n\n                        <button class="cl" ion-item>\n\n                            <ion-icon><img class="home" src="../assets/imgs/icons/corona.png"> </ion-icon>\n\n                            <h4>Administrar croquis</h4>\n\n                        </button>\n\n                    </a>\n\n                </div> -->\n\n            </ion-col>\n\n            <!-- <ion-col>\n\n                    <div>\n\n                        <button class="atras" ion-item (click)="goUsuarioHis()" icon-start>\n\n                              <ion-icon item-start><img class="home" src="../assets/imgs/icons/admin-historial.png"> </ion-icon>\n\n                              Historial de usuarios\n\n                        </button>\n\n                    </div>\n\n                </ion-col> -->\n\n\n\n        </ion-row>\n\n\n\n        <ion-row *ngIf="_sucursal?.type == \'a\'">\n\n            <ion-col>\n\n                <div>\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <img src="./assets/content/icono3.png" class="imgRes3" alt="">\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            <button ion-button full class="nombreCar3" (click)="goCorteHistorial(this.sucursal)">\n\n                                Cortes {{ this.sucursal.type }}</button>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <!-- <button class="cl" ion-item (click)="goCorteHistorial(this.sucursal)">\n\n                        <ion-icon><img class="home" src="../assets/imgs/icons/corona.png"> </ion-icon>\n\n                        <h4>Cortes {{ this.sucursal.type }}</h4>\n\n                    </button> -->\n\n                </div>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n    </ion-grid>\n\n    <br><br><br>\n\n    <ion-item class="atras" text-center no-lines>\n\n        <button class="blancos" ion-button round style="min-width: 200px; padding: 10px 16px; height: 85%;"\n\n            (click)="salir()">Cerrar\n\n            Sesión</button>\n\n    </ion-item>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-home\admin-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */],
            __WEBPACK_IMPORTED_MODULE_11_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_12__providers_push_noti_push_noti__["a" /* PushNotiProvider */],
            __WEBPACK_IMPORTED_MODULE_13__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_10__providers_monitoreo_reservas_monitoreo_reservas__["a" /* MonitoreoReservasProvider */]])
    ], AdminHomePage);
    return AdminHomePage;
}());

//# sourceMappingURL=admin-home.js.map

/***/ }),

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__home_home__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__eventos_eventos__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__reservacion_1_reservacion_1__ = __webpack_require__(121);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//Paginas que conforman el tabs



//import { Observable } from 'rxjs/Observable';
//import { AngularFireDatabase } from 'angularfire2/database';
var TabsPage = /** @class */ (function () {
    function TabsPage() {
        // this.users = this.DB.list('users').valueChanges();
        // console.log(this.users);
        // users: Observable<any[]>;
        this.tab1 = __WEBPACK_IMPORTED_MODULE_1__home_home__["a" /* HomePage */];
        this.tab2 = __WEBPACK_IMPORTED_MODULE_2__eventos_eventos__["a" /* EventosPage */];
        this.tab3 = __WEBPACK_IMPORTED_MODULE_3__reservacion_1_reservacion_1__["a" /* Reservacion_1Page */];
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        //console.log(this.users);
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\tabs\tabs.html"*/'<ion-tabs color="dark" selectedIndex="0">\n\n    <!-- <ion-tab *ngIf="users.active != true " tabIcon="home" tabTitle="Home" [root]="tab1"></ion-tab> -->\n\n    <ion-tab color="gold" tabIcon="custom-corona" tabTitle="Eventos" [root]="tab2"></ion-tab>\n\n    <ion-tab color="gold" tabIcon="custom-relog" tabTitle="Reservacion" [root]="tab3"></ion-tab>\n\n</ion-tabs>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\tabs\tabs.html"*/,
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 120:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_evento_detalle_evento_detalle__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_fire_database__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_usuario_usuario__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_push_noti_push_noti__ = __webpack_require__(176);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//Plugins


// import { Observable } from 'rxjs/Observable';
//import { CargaArchivoProvider } from "../../providers/carga-archivo/carga-archivo";



var EventosPage = /** @class */ (function () {
    function EventosPage(//private _cap: CargaArchivoProvider,
        socialSharing, navCtrl, afDB, navParams, _cap, afs, _providerPushNoti) {
        var _this = this;
        this.socialSharing = socialSharing;
        this.navCtrl = navCtrl;
        this.afDB = afDB;
        this.navParams = navParams;
        this._cap = _cap;
        this.afs = afs;
        this._providerPushNoti = _providerPushNoti;
        //hayMas:boolean= true;
        this.eventos = [];
        this.afs.collection('evento').valueChanges().subscribe(function (data) {
            _this.eventos = data;
        });
        //  afDB.list('evento').valueChanges().subscribe( e =>  {
        //    this.eventos = e;
        //  })   
    }
    EventosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventosPage');
        this._providerPushNoti.init_push_noti();
    };
    EventosPage.prototype.compartir = function (evento) {
        this.socialSharing.shareViaFacebook(evento.titulo, null, evento.img)
            .then(function () { }) // se pudo compartir
            .catch(function () { }); // si sucede un error
    };
    EventosPage.prototype.compartirInsta = function (evento) {
        this.socialSharing.shareViaInstagram(evento.titulo, evento.img)
            .then(function () { }) // se pudo compartir
            .catch(function () { }); // si sucede un error
    };
    EventosPage.prototype.verDetalle = function (uid, sucursalID) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_evento_detalle_evento_detalle__["a" /* EventoDetallePage */], {
            uid: uid,
            sucursalID: sucursalID
        });
    };
    EventosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-eventos',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\eventos\eventos.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Eventos</ion-title>\n\n    </ion-navbar>\n\n    <ion-item text-right class="card_atras">\n\n        <ion-label>\n\n            <ion-icon ios="ios-list" md="md-list"></ion-icon>\n\n        </ion-label>\n\n        <ion-select [(ngModel)]="filterPost" interface="popover">\n\n            <ion-option value="Deportivo">Deportivo</ion-option>\n\n            <ion-option value="Cultural">Cultural</ion-option>\n\n            <ion-option value="Académico">Académico</ion-option>\n\n            <ion-option value="Recreativo">Recreativo</ion-option>\n\n            <ion-option value="Social">Social</ion-option>\n\n            <ion-option value="Festival">Festival</ion-option>\n\n            <ion-option value="Homenaje">Homenaje</ion-option>\n\n            <ion-option value="Reconocimiento">Reconocimiento</ion-option>\n\n            <ion-option value="Feria">Feria</ion-option>\n\n            <ion-option value="Cumpleaños">Cumpleaños</ion-option>\n\n            <ion-option value="Familiy">Familiy Day</ion-option>\n\n            <ion-option value="Simposio">Simposio</ion-option>\n\n            <ion-option value="Otro">Otro</ion-option>\n\n            <ion-option value="">Todos</ion-option>\n\n        </ion-select>\n\n    </ion-item>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="card-background-page">\n\n    <!-- pipesFilterEvento -->\n\n    <ion-card class="card_atras" *ngFor="let evento of eventos | pipesFilterEvento: filterPost">\n\n        <img [src]="evento.img" />\n\n        <ion-row>\n\n            <ion-title color="light">{{ evento.titulo }}</ion-title>\n\n            <ion-row>\n\n                <ion-col mr-5 text-right>\n\n                    <!-- \n\n                     Compartir\n\n                  </button> -->\n\n                    <ion-fab class="btnshare">\n\n                        <button ion-fab color="dark"><ion-icon name="ios-share-alt"></ion-icon></button>\n\n                        <ion-fab-list side="top">\n\n                            <button class="fabbtn" ion-fab (click)="compartir(evento)">\n\n                            <img  src="../assets/imgs/icons/facebook.png"></button>\n\n                            <!-- <button class="fabbtn" ion-fab (click)="compartirInsta(evento)">\n\n                                <img  src="../assets/imgs/icons/insta.png"></button> -->\n\n                        </ion-fab-list>\n\n                    </ion-fab>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-col text-right>\n\n                <button (click)="verDetalle(evento.uid, evento.uidSucursal)" text-align ion-button clear small color="light" icon-left>\n\n              <img class="icon" src="../assets/imgs/icons/mas.png">\n\n                Más\n\n          </button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\eventos\eventos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_fire_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_usuario_usuario__["a" /* UsuarioProvider */],
            __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_7__providers_push_noti_push_noti__["a" /* PushNotiProvider */]])
    ], EventosPage);
    return EventosPage;
}());

//# sourceMappingURL=eventos.js.map

/***/ }),

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Reservacion_1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_database__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__reservaciones_reservaciones__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { Observable } from 'rxjs-compat';




var Reservacion_1Page = /** @class */ (function () {
    function Reservacion_1Page(navCtrl, navParams, afDB, afAuth, afs) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDB = afDB;
        this.afAuth = afAuth;
        this.afs = afs;
        // sucursales: Observable<any[]>;
        this.sucursales = [];
        this.reservacion = {};
        // this.sucursales = afDB.list('sucursales').valueChanges();
        this.uid = this.afAuth.auth.currentUser.uid;
        this.afs.collection('sucursales').valueChanges().subscribe(function (s) {
            _this.sucursales = s;
            console.log('sucursale', _this.sucursales);
            // afDB.list('sucursales').valueChanges().subscribe( s => {
            //   this.sucursales = s;
            //   console.log('sucursale', this.sucursales);      
        });
    }
    Reservacion_1Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Reservacion_1Page');
        // console.log('filtro', this.filterPost);
    };
    Reservacion_1Page.prototype.reservar = function (idSucursal) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__reservaciones_reservaciones__["a" /* ReservacionesPage */], { 'idSucursal': idSucursal });
    };
    Reservacion_1Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reservacion-1',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\reservacion-1\reservacion-1.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Guest Reservations</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-item text-right class="card_atras">\n\n        <ion-label>\n\n            <ion-icon ios="ios-list" md="md-list"></ion-icon>\n\n        </ion-label>\n\n        <ion-select [(ngModel)]="filterPost" interface="popover">\n\n            <ion-option value="bar">Bares</ion-option>\n\n            <ion-option value="antro">Antros</ion-option>\n\n            <ion-option value="restaurante">Restaurantes</ion-option>\n\n            <ion-option value="">Todos</ion-option>\n\n        </ion-select>\n\n    </ion-item>\n\n    <div class="warning1">\n\n        <div style="text-align: center">\n\n            <img height="43px" width="43px" src="../assets/imgs/icons/corona.png" item-start><br>\n\n        </div>\n\n        <br> Selecciona el lugar de tu reservación.\n\n    </div>\n\n    <!-- <ion-list class="card_atras">\n\n        <ion-item *ngFor="let sucursal of sucursales | async" class="card_atras">\n\n            <ion-thumbnail item-start>\n\n                <img [src]="sucursal.photoURL" />\n\n            </ion-thumbnail>\n\n            <h2> {{sucursal.displayName}} </h2>\n\n            <p> {{sucursal.direccion}} </p>\n\n            <button ion-button clear item-end (click)="reservar(sucursal.uid)">Seleccionar</button>\n\n        </ion-item>\n\n    </ion-list> -->\n\n\n\n    <ion-card *ngFor="let sucursal of sucursales | pipesCategoria:filterPost" class="card_atras">\n\n        <!-- {{ sucursal.tipo }} -->\n\n        <div>\n\n            <div class="colo">\n\n                <img class="ima" [src]="sucursal.photoURL" />\n\n            </div>\n\n            <ion-card-content>\n\n                <ion-card-title text-center color="light">\n\n                    {{sucursal.displayName}}\n\n                </ion-card-title>\n\n                <p text-center style="color: #ecf0f1;">\n\n                    {{sucursal.direccion}}\n\n                </p>\n\n                <div hidden>\n\n                    {{ sucursal.tipo }}\n\n                </div>\n\n            </ion-card-content>\n\n            <ion-row no-padding>\n\n                <ion-col>\n\n                    <button ion-button clear small color="light" (click)="reservar(sucursal.uid)" icon-start>\n\n                    <ion-icon ios="ios-checkbox-outline" md="md-checkbox-outline"></ion-icon>\n\n                      Seleccionar\n\n                    </button>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n    </ion-card>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\reservacion-1\reservacion-1.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], Reservacion_1Page);
    return Reservacion_1Page;
}());

//# sourceMappingURL=reservacion-1.js.map

/***/ }),

/***/ 122:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminUsersGuestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__admin_users_admin_users__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_user__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { Observable } from 'rxjs/Observable';





var AdminUsersGuestPage = /** @class */ (function () {
    function AdminUsersGuestPage(navCtrl, navParams, DB, modalCtrl, actionSheet, _up, afs, firebase) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DB = DB;
        this.modalCtrl = modalCtrl;
        this.actionSheet = actionSheet;
        this._up = _up;
        this.afs = afs;
        this.firebase = firebase;
        this.user = 'empleado';
        this.home = this.navParams.get("home");
        console.log("vengo del home", this.home);
        this.sucursal = this.firebase.auth.currentUser;
        if (this.sucursal != null) {
            this.uid = this.sucursal.uid;
            //Cuando es un usuario se saca el id de la sucursal ala que pertenece
            this.afs.collection('users', function (ref) { return ref.where('uid', '==', _this.uid); }).valueChanges().subscribe(function (data) {
                _this.sucursales = data;
                _this.sucursales.forEach(function (element) {
                    var uidSucursal = element.uidSucursal;
                    _this.afs.collection('users', function (ref) { return ref.where('uidSucursal', '==', uidSucursal); })
                        .valueChanges().subscribe(function (u) {
                        _this.admins = u;
                        _this.empleados = u;
                        console.log('admins', _this.admins);
                    });
                });
            });
            this.afs.collection('users', function (ref) { return ref.where('uidSucursal', '==', _this.uid); })
                .valueChanges().subscribe(function (u) {
                _this.admins = u;
                _this.empleados = u;
                console.log('admins', _this.admins);
            });
        }
        if (this.home == 'home') {
            this.afs.collection('users', function (ref) { return ref.where('type', '==', 'e'); })
                .valueChanges().subscribe(function (u2) {
                _this.admins2 = u2;
                _this.empleados2 = u2;
            });
        }
        // this.admins = this.DB.list('users').valueChanges();
        //var admins = firebase.database().ref('/users').orderByChild('type').equalTo('a');
    }
    // Esto es para el buscador
    AdminUsersGuestPage.prototype.initializeItems2 = function () {
        this.admins2 = this.empleados2;
    };
    AdminUsersGuestPage.prototype.getItems2 = function (evt) {
        this.initializeItems2();
        var searchTerm = evt.srcElement.value;
        if (!searchTerm) {
            return;
        }
        this.admins2 = this.admins2.filter(function (admin2) {
            if (admin2.displayName && searchTerm) {
                if (admin2.displayName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            }
        });
    };
    // Esto es para el buscador
    AdminUsersGuestPage.prototype.initializeItems = function () {
        this.admins = this.empleados;
    };
    AdminUsersGuestPage.prototype.getItems = function (evt) {
        this.initializeItems();
        var searchTerm = evt.srcElement.value;
        if (!searchTerm) {
            return;
        }
        this.admins = this.admins.filter(function (admin) {
            if (admin.displayName && searchTerm) {
                if (admin.displayName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            }
        });
    };
    //Termina. Esto es para el buscador
    AdminUsersGuestPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminUsersGuestPage');
    };
    AdminUsersGuestPage.prototype.mostrar_modal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__admin_users_admin_users__["a" /* AdminUsersPage */]);
        modal.present();
    };
    AdminUsersGuestPage.prototype.selectUsuario = function (uid, active) {
        var _this = this;
        this.actionSheet.create({
            title: 'Acciones',
            buttons: [
                // {
                //   text: 'Editar',
                //   handler:()=>{
                //     //Aqui va el codigo
                //   }
                // },
                {
                    text: 'Inhabilitar/hablitar cuenta',
                    role: 'destructive',
                    handler: function () {
                        var coma = active == 'true' ? 'Inhabilitado' : 'Habilitado';
                        if (confirm('Cambiara el estado a ' + coma + ' de este usuario')) {
                            // Si el usuario está activo y se quiere inhablitar entra a esta función
                            if (active == 'true') {
                                _this._up.inhabilitar_user(uid);
                                console.log('Se inhablito cuenta');
                                // Si el usuario está desactivo y se quiere hablitar entra a esta función
                            }
                            else if (active == 'false') {
                                _this._up.habilitar_user(uid);
                                console.log('Se hablito cuenta');
                            }
                        }
                    }
                },
                {
                    text: 'Eliminar',
                    role: 'destructive',
                    handler: function () {
                        if (confirm('¿Estas seguro de eliminar este usuario?')) {
                            _this._up.delete_user(uid);
                            console.log('Se elimino');
                        }
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log("Cancelo");
                    }
                }
            ]
        }).present();
    };
    AdminUsersGuestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-users-guest',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-users-guest\admin-users-guest.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Usuarios</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="fondo_admin">\n\n  <div *ngIf="home != \'home\' ">\n\n    <div class="fondo">\n\n        <ion-searchbar placeholder="Buscar" [animated]="true" (ionInput)="getItems($event)"></ion-searchbar>\n\n        <ion-segment selectedIndex="1" [(ngModel)]="user">\n\n            <!-- <ion-segment-button value="administrador">\n\n                <h5>Administradores</h5>\n\n            </ion-segment-button> -->\n\n            <ion-segment-button value="empleado">\n\n                <h5>Empleados</h5>\n\n            </ion-segment-button>\n\n        </ion-segment>\n\n    </div>\n\n    </div>\n\n    <div *ngIf="home == \'home\' ">\n\n    <div class="fondo">\n\n        <ion-searchbar [animated]="true" (ionInput)="getItems2($event)"></ion-searchbar>\n\n        <ion-segment selectedIndex="1" [(ngModel)]="user">\n\n            <!-- <ion-segment-button value="administrador">\n\n                <h5>Administradores</h5>\n\n            </ion-segment-button> -->\n\n            <ion-segment-button value="empleado">\n\n                <h5>Empleados</h5>\n\n            </ion-segment-button>\n\n        </ion-segment>\n\n    </div>\n\n   </div>\n\n    <div [ngSwitch]="user">\n\n        <div *ngIf="home != \'home\'">\n\n        <div *ngFor="let admin of admins">\n\n            <ion-list class="list_blanco_transp" *ngSwitchCase="\'empleado\'">\n\n                <ion-item *ngIf="admin.type == \'e\' || admin.type == \'coordinacion\' || admin.type == \'rp\' || admin.type == \'capitan_mesero\'" (click)="selectUsuario(admin.uid, admin.active)">\n\n                    <ion-thumbnail item-start>\n\n                        <img src="../assets/imgs/icons/profile.png">\n\n                    </ion-thumbnail>\n\n                    <h6>\n\n                        Nombre: {{ admin.displayName }}\n\n                    </h6>\n\n                    <h6>\n\n                        <div *ngIf="admin.type == \'e\'">Tipo: Empleado</div>\n\n                        <div *ngIf="admin.type == \'coordinacion\'">Tipo: Coordinación</div>\n\n                        <div *ngIf="admin.type == \'rp\'">Tipo: RP</div>\n\n                        <div *ngIf="admin.type == \'capitan_mesero\'">Tipo: Capitán de meseros</div>\n\n\n\n                    </h6>\n\n                    <!--<h6>\n\n                        ID\n\n                        <p class="p">{{ admin.uid }}</p>\n\n                    </h6>-->\n\n                    <ion-badge *ngIf="admin.active == \'false\' " class="fondo" item-end>Inhabilitado</ion-badge>\n\n                </ion-item>\n\n            </ion-list>\n\n        </div>\n\n        </div>\n\n        <div *ngIf="home == \'home\' ">\n\n        <div *ngFor="let admin of admins2">\n\n            <ion-list class="list_blanco_transp" *ngSwitchCase="\'empleado\'">\n\n                <ion-item *ngIf="admin.type == \'e\' || admin.type == \'coordinacion\' || admin.type == \'rp\' || admin.type == \'capitan_mesero\'" (click)="selectUsuario(admin.uid, admin.active)">\n\n                    <ion-thumbnail item-start>\n\n                        <img src="../assets/imgs/icons/profile.png">\n\n                    </ion-thumbnail>\n\n                    <h6>\n\n                        Nombre\n\n                        <p class="p">{{ admin.displayName }}</p>\n\n                    </h6>\n\n                    <h6>\n\n                        Tipo\n\n                        <div *ngIf="admin.type == \'e\'"><p class="p">Empleado</p></div>\n\n                        <div *ngIf="admin.type == \'coordinacion\'"><p class="p">Coordinación</p></div>\n\n                        <div *ngIf="admin.type == \'rp\'"><p class="p">RP</p></div>\n\n                        <div *ngIf="admin.type == \'capitan_mesero\'"><p class="p">Capitán de meseros</p></div>\n\n                    </h6>\n\n                    <h6>\n\n                        ID\n\n                        <p class="p">{{ admin.uid }}</p>\n\n                    </h6>\n\n                    <ion-badge *ngIf="admin.active == \'false\' " class="fondo" item-end>Inhabilitado</ion-badge>\n\n                </ion-item>\n\n            </ion-list>\n\n        </div>\n\n        </div>\n\n        <!-- <div *ngFor="let admin of admins">\n\n            <ion-list class="list_blanco_transp" *ngSwitchCase="\'administrador\'">\n\n                <ion-item *ngIf="admin.type == \'a\' " (click)="selectUsuario(admin.uid)">\n\n                    <ion-thumbnail item-start>\n\n                        <img class="border" src="../assets/imgs/icons/profile.png">\n\n                    </ion-thumbnail>\n\n                    <h6>\n\n                        Nombre\n\n                        <p class="p">{{ admin.displayName }}</p>\n\n                    </h6>\n\n\n\n                    <h6>\n\n                        ID\n\n                        <p class="p">{{ admin.uid }}</p>\n\n                    </h6>\n\n                </ion-item>\n\n            </ion-list>\n\n        </div> -->\n\n    </div>\n\n    <div *ngIf="home != \'home\' ">\n\n    <ion-fab left bottom>\n\n        <button ion-fab color="light"><ion-icon name="contacts"></ion-icon></button>\n\n        <ion-fab-list side="right">\n\n            <button ion-fab (click)="mostrar_modal()">\n\n              <ion-icon name="md-person-add"></ion-icon>\n\n              </button>\n\n        </ion-fab-list>\n\n    </ion-fab>\n\n</div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-users-guest\admin-users-guest.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["AngularFireAuth"]])
    ], AdminUsersGuestPage);
    return AdminUsersGuestPage;
}());

//# sourceMappingURL=admin-users-guest.js.map

/***/ }),

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminMenuReservacionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_sucursal_list_admin_sucursal_list__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__administrar_reservaciones_administrar_reservaciones__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__admin_monitear_reserv_admin_monitear_reserv__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__admin_historial_reservaciones_admin_historial_reservaciones__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__cupones_sucursal_cupones_sucursal__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ciudad_establecimiento_ciudad_establecimiento__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__admin_login_admin_login__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__cuentas_cuentas__ = __webpack_require__(224);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { AdminSucursalSubirPage } from '../admin-sucursal-subir/admin-sucursal-subir';









var AdminMenuReservacionPage = /** @class */ (function () {
    function AdminMenuReservacionPage(navCtrl, firebase, navParams, menuCtrl, authProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.firebase = firebase;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.authProvider = authProvider;
        this._sucursal = {};
        this._userAdmin = {};
        this._sucursalEmpleado = {};
        this.sucursal = this.firebase.auth.currentUser;
        //this.sucursal= '6J7V17w785B8ez4vdvIt';
        if (this.sucursal != null) {
            this.uid = this.sucursal.uid; //'a7btYItzhhhtUzRphc2EWuqGzE12';
            this.email = this.sucursal.email;
            // photoURL = sucursal.photoURL;
            // uid = sucursal.uid;
            // Recibiendo información del usuario/sucursal
            console.log("uid sucursal", this.uid);
            this.authProvider.getUserBd(this.uid).subscribe(function (s) {
                console.log('S:', s);
                _this._sucursal = s;
                console.log('_sucursal', _this._sucursal);
            });
            // Traemos el perfil del empleado de la tabla users
            this.authProvider.getUserAdmins(this.uid).subscribe(function (s) {
                _this._userAdmin = s;
                //this._userAdmin='a7btYItzhhhtUzRphc2EWuqGzE12';
                console.log('_userAdmin', _this._userAdmin);
                // Extraemos el uid de su sucursal
                var uidSucursal = _this.uid;
                console.log('sucursal empleado', uidSucursal);
                // Traemos la informacion de la sucursal del empleado
                _this.authProvider.getUserBd(uidSucursal).subscribe(function (s) {
                    _this._sucursalEmpleado = s;
                    console.log('_sucursalEmpleado', _this._sucursalEmpleado);
                    // Guardamos el uidSucursal del empleado en el localStorage
                    localStorage.setItem('uidSucursal', uidSucursal);
                });
            });
            console.log("_sucursal", this._sucursal);
        }
    }
    AdminMenuReservacionPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad AdminMenuReservacionPage");
        this.getUserAdmins();
    };
    AdminMenuReservacionPage.prototype.getUserAdmins = function () {
        var _this = this;
        // Traemos el perfil del empleado de la tabla users
        this.authProvider.getUserAdmins(this.uid).subscribe(function (s) {
            _this._userAdmin = s;
            //this._userAdmin= 'a7btYItzhhhtUzRphc2EWuqGzE12';
            var uidSucursal = _this.uid;
            console.log("_userAdmin", _this._userAdmin);
            // Extraemos el uid de su sucursala
            console.log("sucursal empleado", uidSucursal);
            // Traemos la informacion de la sucursal del empleado
            _this.authProvider.getUserBd(uidSucursal).subscribe(function (s) {
                _this._sucursalEmpleado = s;
                console.log("_sucursalEmpleado", _this._sucursalEmpleado);
            });
        });
    };
    AdminMenuReservacionPage.prototype.adminReservaciones = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__administrar_reservaciones_administrar_reservaciones__["a" /* AdministrarReservacionesPage */], {
            uidSucursal: this.sucursal.uid
        });
    };
    AdminMenuReservacionPage.prototype.goMonitorear = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__admin_monitear_reserv_admin_monitear_reserv__["a" /* AdminMonitearReservPage */]);
    };
    AdminMenuReservacionPage.prototype.goSucursal = function () {
        this.usertipo = 'master';
        console.log(this.usertipo);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__admin_sucursal_list_admin_sucursal_list__["a" /* AdminSucursalListPage */], { usertipo: this.usertipo });
    };
    //ir a pagina de cupones
    AdminMenuReservacionPage.prototype.goCupones = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__cupones_sucursal_cupones_sucursal__["a" /* CuponesSucursalPage */]);
    };
    //ir a pagina de ciudades
    AdminMenuReservacionPage.prototype.goCiudades = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__ciudad_establecimiento_ciudad_establecimiento__["a" /* CiudadEstablecimientoPage */]);
    };
    //ir a pagina de historial
    AdminMenuReservacionPage.prototype.goReservacionHistorial = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__admin_historial_reservaciones_admin_historial_reservaciones__["a" /* AdminHistorialReservacionesPage */]);
    };
    AdminMenuReservacionPage.prototype.goConsumo = function () {
        console.log("consumo sucUID", this.sucursal.uid);
        var obj = [{ uid: this.sucursal.uid }];
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__cuentas_cuentas__["a" /* CuentasPage */], { uidSucursal: this.sucursal.uid });
    };
    AdminMenuReservacionPage.prototype.salir = function () {
        localStorage.setItem("isLogin", 'false');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__admin_login_admin_login__["a" /* AdminLoginPage */]);
        this.menuCtrl.close();
    };
    AdminMenuReservacionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-admin-menu-reservacion",template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-menu-reservacion\admin-menu-reservacion.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title *ngIf="_sucursal?.type == \'a\'">Control de {{ _sucursal?.displayName }} </ion-title>\n\n        <!--Inicia. Esta información solo la muestra a usuarios=empleados -->\n\n        <ion-title *ngIf="_userAdmin?.type == \'e\'">Control de la sucursal </ion-title>\n\n        <!--Termina. Esta información solo la muestra a usuarios=empleados -->\n\n        <!--Inicia. Esta información solo la muestra a usuarios=maestro -->\n\n        <ion-title *ngIf="_userAdmin?.type == \'master\'">Control de las sucursales </ion-title>\n\n        <ion-title *ngIf="_userAdmin?.type == \'coordinacion\' || _userAdmin?.type == \'rp\' || _userAdmin?.type == \'capitan_mesero\'">Administración</ion-title>\n\n        <!--Termina. Esta información solo la muestra a usuarios=maestro -->\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="fondo_admin">\n\n    <div class="centrar">\n\n        <ion-row>\n\n            <ion-col *ngIf="_sucursal?.type == \'a\'" class="text-center">\n\n                <img style="border-radius:65px;" height="150" width="150" [src]="_sucursal?.photoURL">\n\n            </ion-col>\n\n        </ion-row>\n\n    </div>\n\n    <!-- Inicia imagen para usuario=master -->\n\n    <div class="centrar">\n\n        <ion-row>\n\n            <ion-col *ngIf="_userAdmin?.type == \'master\'" class="text-center">\n\n                <img height="150" width="auto" src="../assets/imgs/Logoblanco.png">\n\n            </ion-col>\n\n        </ion-row>\n\n    </div>\n\n    <!-- Termina para imagen para usuario=master -->\n\n    <!--Inicia. Esta información solo la muestra a usuarios=empleados -->\n\n    <div class="centrar">\n\n        <p>{{ _sucursalEmpleado?.displayName }} </p>\n\n    </div>\n\n\n\n    <div *ngIf="_userAdmin?.type == \'e\'" class="centrar">\n\n        <ion-row>\n\n            <ion-col class="text-center">\n\n                <img style="border-radius:65px;" height="150" width="150" [src]="_sucursalEmpleado?.photoURL">\n\n            </ion-col>\n\n        </ion-row>\n\n    </div>\n\n    <!--Termina. Esta información solo la muestra a usuarios=empleados -->\n\n    <ion-grid>\n\n        <ion-row>\n\n            <ion-col *ngIf="_userAdmin?.type == \'master\'">\n\n                <div>\n\n                    <button class="cl" ion-item (click)="goSucursal()">\n\n                      <ion-icon ><img class="iconmenu" src="../assets/imgs/icons/admin-user.png"> </ion-icon>\n\n                     <h4>Administrar sucursales</h4>\n\n                    </button>\n\n                </div>\n\n            </ion-col>\n\n            <!--<ion-col *ngIf="_sucursal?.type == \'a\'">\n\n                <div>\n\n                    <button class="cl" ion-item (click)="go3()">\n\n                  <ion-icon><img class="home" src="../assets/imgs/icons/admin-carta.png"> </ion-icon>\n\n                  <h4>Cupones</h4>\n\n                </button>\n\n                </div>\n\n            </ion-col>-->\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-col *ngIf="_userAdmin?.type == \'master\'">\n\n                <div>\n\n                    <button class="cl" ion-item (click)="goCupones()">\n\n                      <ion-icon ><img class="iconmenu" src="../assets/imgs/icons/billete.png"> </ion-icon>\n\n                     <h4>Administrar cupones</h4>\n\n                    </button>\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-col *ngIf="_userAdmin?.type == \'master\'">\n\n                <div>\n\n                    <button class="cl" ion-item (click)="goCiudades()">\n\n                      <ion-icon ><img class="iconmenu" src="../assets/imgs/icons/antro.png"></ion-icon>\n\n                      <h4>Administrar ciudades</h4>\n\n                    </button>\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row *ngIf="_sucursal?.type == \'a\' || _userAdmin?.type == \'e\' || _userAdmin?.type == \'rp\'">\n\n            <ion-col>\n\n                <div>\n\n                <button class="cl" ion-item (click)="goMonitorear()">\n\n               <ion-icon><img class="home" src="../assets/imgs/icons/admin-evento.png"> </ion-icon>\n\n                <h4>Monitoreo de reservaciones</h4>\n\n               </button>\n\n                </div>\n\n            </ion-col>\n\n            <ion-col>\n\n                <div>\n\n                    <button class="cl" ion-item (click)="adminReservaciones()">\n\n                        <ion-icon><img class="home" src="../assets/imgs/icons/admin-historial.png"> </ion-icon>\n\n                       <h4>Gestión de reservaciones </h4>\n\n                  </button>\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row *ngIf="_userAdmin?.type == \'capitan_mesero\'">\n\n            <ion-col>\n\n                <div>\n\n                <button class="cl" ion-item (click)="goMonitorear()">\n\n               <ion-icon><img class="home" src="../assets/imgs/icons/admin-evento.png"> </ion-icon>\n\n                <h4>Monitoreo de reservaciones</h4>\n\n               </button>\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row *ngIf="_sucursal?.type == \'a\'">\n\n            <ion-col>\n\n                <div>\n\n                    <button class="cl" ion-item (click)="goReservacionHistorial()">\n\n                   <ion-icon><img class="home" src="../assets/imgs/icons/corona.png"> </ion-icon>\n\n                      <h4>Historial de reservaciones</h4>\n\n                  </button>\n\n                </div>\n\n            </ion-col>\n\n            <!--<ion-col>\n\n                <div>\n\n                    <button class="cl" ion-item (click)="goReservacion()">\n\n                 <ion-icon><img class="home" src="../assets/imgs/icons/sucursal.png"> </ion-icon>\n\n                    <h4>Consumos</h4>\n\n                </button>\n\n                </div>\n\n            </ion-col>-->\n\n        </ion-row>\n\n        <!--ACCESOS PARA USUARIO COORDINACION RP-->\n\n        <ion-row *ngIf="_userAdmin?.type == \'coordinacion\' || _userAdmin?.type == \'rp\'">\n\n            <!-- <ion-col>\n\n                <div>\n\n                    <button class="cl" ion-item (click)="goReservacionHistorial()">\n\n                   <ion-icon><img class="home" src="../assets/imgs/icons/corona.png"> </ion-icon>\n\n                      <h4>Historial de reservaciones</h4>\n\n                  </button>\n\n                </div>\n\n            </ion-col> -->\n\n        </ion-row>\n\n\n\n        <ion-row *ngIf="_sucursal?.type == \'a\'">\n\n            <ion-col>\n\n                <div>\n\n                    <button class="cl" ion-item (click)="goConsumo()">\n\n                 <ion-icon><img class="home" src="../assets/imgs/icons/sucursal.png"> </ion-icon>\n\n                    <h4>Consumos</h4> \n\n                </button>\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </ion-grid>\n\n    <!-- <ion-list class="atras">\n\n        <button class="atras" ion-item (click)="goSucursal()" icon-start>\n\n    <ion-icon item-start><img class="home" src="../assets/imgs/icons/admin-user.png"> </ion-icon>\n\n    Administrar sucursales\n\n  </button>\n\n        <button class="atras" ion-item (click)="goCarta()" icon-start>\n\n    <ion-icon item-start><img class="home" src="../assets/imgs/icons/admin-carta.png"> </ion-icon>\n\n   Cupones\n\n  </button>\n\n        <button class="atras" ion-item (click)="goEventos()" icon-start>\n\n    <ion-icon item-start><img class="home" src="../assets/imgs/icons/admin-evento.png"> </ion-icon>\n\n    Monitoreo de reservaciones\n\n  </button>\n\n        <button class="atras" ion-item (click)="openNavDetailsPage(item)" icon-start>\n\n    <ion-icon item-start><img class="home" src="../assets/imgs/icons/book.png"> </ion-icon>\n\n    Consulta de reservaciones\n\n  </button>\n\n        <button class="atras" ion-item (click)="openNavDetailsPage(item)" icon-start>\n\n    <ion-icon item-start><img class="home" src="../assets/imgs/icons/admin-historial.png"> </ion-icon>\n\n    Historial de consumos\n\n  </button> -->\n\n    <!--  <button class="atras" ion-item (click)="goReservacion()" icon-start>\n\n    <ion-icon item-start><img class="home" src="../assets/imgs/icons/corona.png"> </ion-icon>\n\n    Administrar Reservaciones\n\n  </button>\n\n      <button class="atras" ion-item (click)="logout()" icon-start>\n\n    <ion-icon item-start><img class="home" src="../assets/imgs/icons/turn-off.png"> </ion-icon>\n\n    Cerrar Sesión\n\n  </button> -->\n\n    <!-- </ion-list> -->\n\n\n\n    <ion-item class="atras" text-center>\n\n        <button class="blanco" ion-button round style="min-width: 200px; padding: 10px 16px" (click) = "salir()">Cerrar Sesión</button>\n\n    </ion-item>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-menu-reservacion\admin-menu-reservacion.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */]])
    ], AdminMenuReservacionPage);
    return AdminMenuReservacionPage;
}());

//# sourceMappingURL=admin-menu-reservacion.js.map

/***/ }),

/***/ 124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminSucursalPerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__admin_sucursal_editperfil_admin_sucursal_editperfil__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__admin_sucursal_editperfil_imagen_admin_sucursal_editperfil_imagen__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__admin_sucursal_croquis_admin_sucursal_croquis__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__imagencroquis_imagencroquis__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__corte_venta_corte_venta__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__corte_historial_corte_historial__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__admin_sucursal_list_admin_sucursal_list__ = __webpack_require__(93);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













// import { AdminmesasPage } from '../adminmesas/adminmesas';
var AdminSucursalPerfilPage = /** @class */ (function () {
    function AdminSucursalPerfilPage(navCtrl, navParams, SucProv, authProvider, afDB, alertCtrl, loadinCtl, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.SucProv = SucProv;
        this.authProvider = authProvider;
        this.afDB = afDB;
        this.alertCtrl = alertCtrl;
        this.loadinCtl = loadinCtl;
        this.toastCtrl = toastCtrl;
        this.zona = {};
        this.area = {};
        this.sucursal = this.navParams.get('uid');
        console.log('uid sucursal', this.sucursal);
        this.usertipo = this.navParams.get('usertipo');
        console.log('user tipo en perfil', this.usertipo);
        SucProv.getSucursal(this.sucursal).subscribe(function (s) {
            _this.sucursales = s;
            console.log('sucursal', s);
        });
        SucProv.getImagenSucursal(this.sucursal).subscribe(function (sucur) {
            _this.sucursal_croquis = sucur;
            console.log('sucursal croquis', _this.sucursal_croquis);
        });
        // this.areas = afDB.list('areas/'+this.sucursal+'/').snapshotChanges().map(data => {
        //   return data.map(s => ({ key: s.payload.key, ...s.payload.val() }));
        // });
        // this.zonas = afDB.list('zonas/').snapshotChanges().map(data => {
        //   return data.map(s => ({ key: s.payload.key, ...s.payload.val() }));
        // });
    }
    AdminSucursalPerfilPage.prototype.ionViewDidLoad = function () {
        console.log(this.sucursal);
        // console.log(this.sucursalItem);
        // console.log('ionViewDidLoad AdminSucursalPerfilPage');
        this.getAreas();
        this.getZonas();
    };
    AdminSucursalPerfilPage.prototype.logout = function () {
        this.authProvider.logout();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
    };
    AdminSucursalPerfilPage.prototype.goEditPerfil = function (sucursalItem) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__admin_sucursal_editperfil_admin_sucursal_editperfil__["a" /* AdminSucursalEditperfilPage */], this.SucProv.selectedSucursalItem = Object.assign({}, sucursalItem));
        console.log('sucursal item', sucursalItem);
    };
    AdminSucursalPerfilPage.prototype.goCorteVentas = function (sucursalItem) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__corte_venta_corte_venta__["a" /* CorteVentaPage */], this.SucProv.selectedSucursalItem = Object.assign({}, sucursalItem));
        //console.log('sucursal item',sucursalItem);
    };
    AdminSucursalPerfilPage.prototype.goCorteHistorial = function (sucursalItem) {
        console.log('Historial-Corte');
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__corte_historial_corte_historial__["a" /* CorteHistorialPage */], this.SucProv.selectedSucursalItem = Object.assign({}, sucursalItem));
    };
    AdminSucursalPerfilPage.prototype.goEditArquitectura = function (idSucursal) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__admin_sucursal_croquis_admin_sucursal_croquis__["a" /* AdminSucursalCroquisPage */], { idSucursal: idSucursal });
    };
    AdminSucursalPerfilPage.prototype.goEditPerfilImagen = function (uid) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__admin_sucursal_editperfil_imagen_admin_sucursal_editperfil_imagen__["a" /* AdminSucursalEditperfilImagenPage */], { uid: uid });
    };
    AdminSucursalPerfilPage.prototype.goEditImagenCroquis = function (idSucursal) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__imagencroquis_imagencroquis__["a" /* ImagencroquisPage */], { idSucursal: idSucursal });
    };
    AdminSucursalPerfilPage.prototype.agregarArea = function (idSucursal) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Agregar Área',
            inputs: [
                {
                    name: 'area',
                    placeholder: 'Area'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Registrar',
                    handler: function (data) {
                        _this.registrarArea(data.area, idSucursal);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminSucursalPerfilPage.prototype.registrarArea = function (area, idSucursal) {
        if (area == '') {
            this.alertCtrl.create({
                title: 'El nombre del área es obligatorio',
                buttons: ['Aceptar']
            }).present();
        }
        else {
            this.SucProv.agregarArea(area, idSucursal);
        }
    };
    AdminSucursalPerfilPage.prototype.getAreas = function () {
        var _this = this;
        this.SucProv.getAreas(this.sucursal).subscribe(function (areas) {
            _this.areas = areas;
            var longitud = _this.areas.length;
            console.log("Esta es la longitud: ", longitud);
            console.log("Areas JAJA: ", _this.areas);
        });
    };
    AdminSucursalPerfilPage.prototype.getZonas = function () {
        var _this = this;
        this.SucProv.getZonas(this.sucursal).subscribe(function (zonas) {
            _this.zonas = zonas;
            console.log("Zonas: ", _this.zonas);
        });
    };
    AdminSucursalPerfilPage.prototype.agregarZona = function (idArea, idSucursal) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Agregar Zona',
            inputs: [
                {
                    name: 'zona',
                    placeholder: 'Zona'
                },
                {
                    name: 'consumoMin',
                    placeholder: 'Consumo Mínimo',
                    type: 'number'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Registrar',
                    handler: function (data) {
                        _this.registrarZona(data.zona, data.consumoMin, idArea, idSucursal);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminSucursalPerfilPage.prototype.agregarMesa = function (idSucursal, idArea, idZona) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Agregar Mesas',
            inputs: [
                {
                    name: 'numMesas',
                    placeholder: 'Número de mesas',
                    type: 'Number'
                },
                {
                    name: 'numPersonas',
                    placeholder: 'Número de personas por mesa',
                    type: 'Number'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Registrar',
                    handler: function (data) {
                        _this.registrarMesa(idSucursal, idArea, idZona, data.numMesas, data.numPersonas);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminSucursalPerfilPage.prototype.registrarZona = function (zona, consumoMin, idArea, idSucursal) {
        if (zona == '') {
            this.alertCtrl.create({
                title: 'El nombre de la zona es obligatorio',
                buttons: ['Aceptar']
            }).present();
        }
        else if (consumoMin == '') {
            this.alertCtrl.create({
                title: 'El consumo mínimo es obligatorio',
                buttons: ['Aceptar']
            }).present();
        }
        else {
            this.SucProv.agregarZona(zona, consumoMin, idArea, idSucursal);
        }
    };
    AdminSucursalPerfilPage.prototype.registrarMesa = function (idSucursal, idArea, idZona, numMesas, numPersonas) {
        if (numMesas == '') {
            this.alertCtrl.create({
                title: 'El número de mesas es obligatorio',
                buttons: ['Aceptar']
            }).present();
        }
        else if (numPersonas == '') {
            this.alertCtrl.create({
                title: 'El número de personas por mesa es obligatorio',
                buttons: ['Aceptar']
            }).present();
        }
        else {
            this.SucProv.verificarConsecutivo(idSucursal, idArea, idZona, numMesas, numPersonas);
        }
    };
    AdminSucursalPerfilPage.prototype.eliminarZona = function (idZona) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Eliminar Zona',
            subTitle: 'Esta seguro que desea eliminar la zona',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Aceptar',
                    handler: function (data) {
                        _this.SucProv.eliminarZona(idZona);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminSucursalPerfilPage.prototype.eliminarArea = function (idArea) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Eliminar Área',
            subTitle: 'Esta seguro que desea eliminar el área',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                        // this.navCtrl.setPages([{page:'AdminSucursalPerfilPage'}]);
                    }
                },
                {
                    text: 'Aceptar',
                    handler: function (data) {
                        _this.SucProv.eliminarArea(idArea);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminSucursalPerfilPage.prototype.eliminarSucursal = function (idSucursal) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Eliminar sucursal',
            subTitle: 'Esta seguro que desea eliminar la sucursal',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                        // this.navCtrl.setPages([{page:'AdminSucursalPerfilPage'}]);
                    }
                },
                {
                    text: 'Aceptar',
                    handler: function (data) {
                        _this.SucProv.eliminarSucursal(idSucursal);
                    }
                }
            ]
        });
        alert.present();
        //this.navCtrl.setPages([{page:'AdminSucursalListPage'}]);
        //  this.navCtrl.push(AdminSucursalListPage);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_12__admin_sucursal_list_admin_sucursal_list__["a" /* AdminSucursalListPage */]);
    };
    AdminSucursalPerfilPage.prototype.modificaZona = function (idZona, zona, consumo) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Modificar Zona',
            inputs: [
                {
                    name: 'zonaName',
                    value: zona
                },
                {
                    name: 'consumoMin',
                    value: consumo,
                    type: 'number'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                        // this.navCtrl.setPages([{page:'AdminSucursalPerfilPage'}]);
                    }
                },
                {
                    text: 'Actualizar',
                    handler: function (data) {
                        _this.modificarZona(data.zonaName, data.consumoMin, idZona);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminSucursalPerfilPage.prototype.modificaArea = function (idArea, area) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Modificar Area',
            inputs: [
                {
                    name: 'areaName',
                    value: area
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Actualizar',
                    handler: function (data) {
                        _this.modificarArea(data.areaName, idArea);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminSucursalPerfilPage.prototype.modificarZona = function (zona, consumoMin, idZona) {
        if (zona == '') {
            this.alertCtrl.create({
                title: 'El nombre de la zona es obligatorio',
                buttons: ['Aceptar']
            }).present();
        }
        else if (consumoMin == '') {
            this.SucProv.alertCtrl.create({
                title: 'El consumo mínimo es obligatorio',
                buttons: ['Aceptar']
            }).present();
        }
        else {
            this.SucProv.modificarZona(zona, consumoMin, idZona);
        }
    };
    AdminSucursalPerfilPage.prototype.modificarArea = function (area, idArea) {
        if (area == '') {
            this.alertCtrl.create({
                title: 'El nombre del area es obligatorio',
                buttons: ['Aceptar']
            }).present();
        }
        else {
            this.SucProv.modificarArea(area, idArea);
        }
    };
    AdminSucursalPerfilPage.prototype.adminMesas = function (idArea, idZona) {
        // console.log('Id del Area', idArea ,'Id de la zona: ',idZona);
        // this.navCtrl.push(AdminSucursalPerfilPage, {uid:uid});
        this.navCtrl.push('AdminmesasPage', { idArea: idArea, idZona: idZona });
    };
    AdminSucursalPerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-sucursal-perfil',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-perfil\admin-sucursal-perfil.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Perfil</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content class="fondo_admin" *ngFor="let sucursal of sucursales">\n\n    <ion-item>\n\n        <!-- <ion-badge (click)="goEditArquitectura(sucursal.uid)" class="fon3" item-end>EDITAR ARQUITECTURA</ion-badge> -->\n\n    </ion-item>\n\n    <ion-item class="he">\n\n      <ion-badge *ngIf="usertipo != \'master\'" (click)="goEditPerfil(sucursal)" class="fon3" item-end>EDITAR MI INFORMACIÓN DE PERFÍL</ion-badge>\n\n    </ion-item>\n\n    <ion-item class="he">\n\n        <ion-badge (click)="goCorteVentas(sucursal)" class="fon3" item-end>Corte</ion-badge>\n\n    </ion-item>\n\n    <ion-item class="he">\n\n        <ion-badge (click)="goCorteHistorial(sucursal)" class="fon3" item-end>Historial Corte</ion-badge>\n\n    </ion-item>\n\n\n\n    <h3 text-center class="blanco"> Información de la sucursal</h3>\n\n    <ion-grid class="margen">\n\n\n\n        <ion-row>\n\n            <ion-col>\n\n                <img src="./assets/imgs/Logoblanco.png">\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n\n\n    <ion-list no-lines class="margen_2">\n\n        <ion-item text-center>\n\n            <!--Filtro de bandera de usuario-->\n\n            <!-- <div class="filt">\n\n                <img class="cate" src="../assets/imgs/icons/gold_guest.png" item-start>\n\n            </div> -->\n\n            <!--Imagen del usuario-->\n\n            <ion-item *ngIf="sucursal.status == \'activo\'">\n\n                <h4>Estado: </h4>\n\n                <ion-badge class="fon" item-end>Activo</ion-badge>\n\n            </ion-item>\n\n            <ion-item *ngIf="sucursal.status == \'falso\'">\n\n                <h4>Estado: </h4>\n\n                <ion-badge class="fon2" item-end>Inhabilitado</ion-badge>\n\n\n\n            </ion-item>\n\n            <div class="">\n\n                <img class="imagen_perfil" [src]="sucursal.photoURL">\n\n                <ion-badge (click)="goEditPerfilImagen(sucursal.uid)" class="edit_pho">\n\n                    <ion-icon name="md-brush" ios="ios-outline" md="md-brush"></ion-icon>\n\n                </ion-badge>\n\n            </div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/profile.png" item-start>\n\n            <div class="margen3">\n\n                <h4> Nombre:</h4> {{sucursal.displayName}}</div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/correo.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Correo:</h4>{{ sucursal.email}}</div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/contacto.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Contacto:</h4>{{ sucursal.contacto }}</div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/telefono.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Teléfono:</h4> {{ sucursal.tel }} </div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/home.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Dirección:</h4>{{ sucursal.direccion }}</div>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <img *ngIf="sucursal.tipo == \'antro\' " class="icon" src="../assets/imgs/icons/antro.png" item-start>\n\n            <img *ngIf="sucursal.tipo == \'bar\' " class="icon" src="../assets/imgs/icons/bar.png" item-start>\n\n            <img *ngIf="sucursal.tipo == \'restaurante\' " class="icon" src="../assets/imgs/icons/restaurante.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Giro</h4>{{ sucursal.tipo }}\n\n            </div>\n\n        </ion-item>\n\n        <br><br><br>\n\n        <h4 text-center class="blanco_2"> Croquis de la sucursal</h4>\n\n        <ion-card class="card_atras">\n\n            <ion-card-content class="card_atras">\n\n              <div *ngFor="let sucur of sucursal_croquis">\n\n                <div class="warning">\n\n                    <img *ngIf="sucur.imagenes != null" class="imagen_croquis" [src]="sucur.imagenes" (click)="goEditImagenCroquis(sucursal.uid)">\n\n                    <!-- <ion-badge (click)="goEditImagenCroquis(sucursal.uid)" class="edit_pho">\n\n                        <ion-icon name="md-brush" ios="ios-outline" md="md-brush"></ion-icon>\n\n                    </ion-badge> -->\n\n                </div>\n\n              </div>\n\n\n\n                <ion-card-title>\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <h4 class="blanco_2">Áreas</h4>\n\n                        </ion-col>\n\n                        <ion-col col-2>\n\n                            <img class="icon" src="../assets/imgs/icons/mas.png" item-start (click)="agregarArea(sucursal.uid)">\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </ion-card-title>\n\n                <div *ngFor="let area of areas">\n\n                    <!-- {{area | json}} -->\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <p class="blanco_2">\n\n                                {{area.nombre}}\n\n                            </p>\n\n                        </ion-col>\n\n                        <ion-col col-1>\n\n                            <!-- <img class="icon1" src="../assets/imgs/icons/mas.png" item-start (click)="agregarZona(area.key)"> -->\n\n                            <ion-icon name="md-add" (click)="agregarZona(area.id, sucursal.uid)"></ion-icon>\n\n                        </ion-col>\n\n                        <ion-col col-1>\n\n                            <ion-icon name="md-brush" (click)="modificaArea(area.id, area.nombre)"></ion-icon>\n\n                        </ion-col>\n\n                        <ion-col col-1>\n\n                            <ion-icon name="md-trash" (click)="eliminarArea(area.id)"></ion-icon>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <div *ngFor="let zona of zonas">\n\n                        <div *ngIf="area.id==zona.uidArea">\n\n                            <ion-item-sliding class="card_atras">\n\n                                <ion-item class="card_atras">\n\n                                    <img class="icon1" src="../assets/imgs/icons/home.png" item-start>\n\n                                    <p class="blanco_2">{{zona.nombre}} - {{zona.consumo}}</p>\n\n                                </ion-item>\n\n                                <ion-item-options class="card_atras" side="right">\n\n                                    <button ion-button color="dark" (click)="agregarMesa(sucursal.uid,area.id,zona.id)">\n\n                                        <ion-icon name="md-add"></ion-icon>\n\n                                        Agregar\n\n                                    </button>\n\n                                    <button ion-button color="dark" (click)="eliminarZona(zona.id)">\n\n                                        <ion-icon name="md-trash"></ion-icon>\n\n                                        Eliminar\n\n                                    </button>\n\n                                    <button ion-button color="dark" (click)="modificaZona(zona.id, zona.nombre, zona.consumo)">\n\n                                        <ion-icon name="md-brush"></ion-icon>\n\n                                        Editar\n\n                                    </button>\n\n                                </ion-item-options>\n\n                            </ion-item-sliding>\n\n                            <div>\n\n                                <ion-item class="card_atras" (click)="adminMesas(area.id, zona.id)">\n\n                                    <img class="icon2" src="../assets/imgs/mesa.png" item-start>\n\n                                    <p class="blanco_2">Administrar Mesas</p>\n\n                                </ion-item>\n\n                            </div>\n\n                        </div>\n\n                    </div>\n\n                    <br><br>\n\n\n\n                </div>\n\n            </ion-card-content>\n\n        </ion-card>\n\n        <ion-item footer>\n\n          <button *ngIf="usertipo == \'master\'" ion-button color="dark" (click)="eliminarSucursal(sucursal.uid)">\n\n              Eliminar Sucursal\n\n          </button>\n\n          <!--  <button ion-button (click)="logout()" color="dark" block>\n\n        Cerrar Sesión\n\n      </button>-->\n\n        </ion-item>\n\n\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-perfil\admin-sucursal-perfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_9_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */]])
    ], AdminSucursalPerfilPage);
    return AdminSucursalPerfilPage;
}());

//# sourceMappingURL=admin-sucursal-perfil.js.map

/***/ }),

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CorteHistorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reservacion_reservacion__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CorteHistorialPage = /** @class */ (function () {
    function CorteHistorialPage(navCtrl, navParams, sucProv, _providerCorte, authProvider, firebase) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sucProv = sucProv;
        this._providerCorte = _providerCorte;
        this.authProvider = authProvider;
        this.firebase = firebase;
        this.type = 'n';
        this.user = this.firebase.auth.currentUser;
        console.log("Esta es User: ", this.user.uid);
        console.log('ionViewDidLoad CorteHistorialPage');
        console.log('itemHistorialCorte', this.sucProv.selectedSucursalItem);
        this.idSucursal = this.sucProv.selectedSucursalItem.uid;
        //  this.type=this.sucProv.selectedSucursalItem.type;
        console.log('idSucursal CorteHistorico', this.idSucursal);
        //si es sucursal no tare nada
        this.authProvider.getUserAdmins(this.user.uid).subscribe(function (s) {
            console.log('s', s);
            if (s != null) {
                _this.type = s.type;
                // console.log('admin', this.admin);  
                console.log('type', _this.type);
            }
        });
        this.getCortes(this.idSucursal);
    }
    CorteHistorialPage.prototype.ionViewDidLoad = function () {
    };
    CorteHistorialPage.prototype.getCortes = function (idx) {
        var _this = this;
        console.log("funcion cortes: ", idx);
        this._providerCorte.getCortes(idx).subscribe(function (res) {
            console.log("Este es el resultado de corte historico: ", res);
            _this.corteHistorico = res;
        });
    };
    CorteHistorialPage.prototype.deleteCorte = function (id) {
        console.log('corte eliminado: ' + id);
        this._providerCorte.delete_corte(id);
    };
    CorteHistorialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-corte-historial',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\corte-historial\corte-historial.html"*/'\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Corte-Historial</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-card class="card_title">\n\n        <ion-card-header class="card_title" text-center>\n\n            Historial de Corte\n\n        </ion-card-header>\n\n    </ion-card>\n\n\n\n    <ion-card class="card_atras">\n\n        <ion-card-content class="card_atras" *ngFor="let corte of corteHistorico">\n\n            <ion-card-title>\n\n                <h4 class="blanco_1">Corte {{corte.folio}}\n\n                    <button *ngIf="type == \'master\'" ion-button clear (click)="deleteCorte(corte.uid)">\n\n                        <ion-icon name="trash"></ion-icon>\n\n                      </button>\n\n                     </h4>\n\n            </ion-card-title>\n\n            \n\n            <ion-item class="card_atras"  >\n\n                <img class="icon" src="../assets/imgs/icons/calendario.png" item-start>\n\n                <p class="blanco_2">Fecha de Inicio</p>\n\n                <ion-badge class="negro" item-end> {{corte.fecha_Inicio}} </ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/calendario.png" item-start>\n\n                <a class="blanco_2">Fecha fin</a>\n\n                <ion-badge class="negro" item-end> {{corte.fecha_Fin}} </ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                <a class="blanco_2">Total</a>\n\n                <ion-badge item-end> {{corte.totalCorte | currency}}</ion-badge>\n\n            </ion-item>\n\n            \n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                <a class="blanco_2">Propina</a>\n\n                <ion-badge item-end>  {{corte.propina | currency}} </ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                <a class="blanco_2">Total + Propina</a>\n\n                <ion-badge item-end>  {{corte.propina + corte.totalCorte | currency}} </ion-badge>\n\n            </ion-item>\n\n\n\n        </ion-card-content>\n\n    </ion-card>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\corte-historial\corte-historial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_reservacion_reservacion__["a" /* ReservacionProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__["AngularFireAuth"]])
    ], CorteHistorialPage);
    return CorteHistorialPage;
}());

//# sourceMappingURL=corte-historial.js.map

/***/ }),

/***/ 126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminMonitearReservPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_monitoreo_reservas_monitoreo_reservas__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__admin_lee_qr_admin_lee_qr__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__admin_reservacion_detalle_admin_reservacion_detalle__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AdminMonitearReservPage = /** @class */ (function () {
    function AdminMonitearReservPage(navCtrl, navParams, afs, monRes, barcodeScanner) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afs = afs;
        this.monRes = monRes;
        this.barcodeScanner = barcodeScanner;
        this.encodText = '';
        this.encodeData = {};
        this.scannedData = {};
        this.reservaciones = [];
        //recibe parametro de la reservacion
        this.menu = this.navParams.get("menu");
        console.log(this.menu);
    }
    AdminMonitearReservPage.prototype.ionViewDidLoad = function () {
        this.getAllReservacione();
        this.getClientes();
        this.getEvento();
    };
    AdminMonitearReservPage.prototype.getAllReservacione = function () {
        var _this = this;
        var dateObj = new Date();
        var anio = dateObj.getFullYear().toString();
        var mes = dateObj.getMonth().toString();
        var dia = dateObj.getDate();
        var mesArray = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        if (dia >= 1 && dia <= 9) {
            var diaCero = '0' + dia;
            this.formatoFecha = anio + '-' + mesArray[mes] + '-' + diaCero;
        }
        else {
            this.formatoFecha = anio + '-' + mesArray[mes] + '-' + dia;
        }
        console.log("fecha actual");
        console.log(this.formatoFecha);
        var id = localStorage.getItem('uidSucursal');
        //Cuando es un usuario se saca el id de la sucursal ala que pertenece
        this.afs.collection('users', function (ref) { return ref.where('uid', '==', id); }).valueChanges().subscribe(function (data) {
            _this.sucursales = data;
            _this.sucursales.forEach(function (element) {
                var uidSucursal = element.uidSucursal;
                _this.monRes.getReservaciones(uidSucursal, _this.formatoFecha).subscribe(function (reser) {
                    _this.reservaciones = reser;
                    console.log('reservaciones', reser);
                });
                _this.monRes.getReservacionesAcepCom(uidSucursal, _this.formatoFecha).subscribe(function (reser) {
                    _this.reservacionesAcep = reser;
                    console.log('reservaciones', reser);
                });
            });
        });
        this.monRes.getReservaciones(id, this.formatoFecha).subscribe(function (reser) {
            _this.reservaciones = reser;
            console.log('reservaciones', reser);
        });
        this.monRes.getReservacionesAcepCom(id, this.formatoFecha).subscribe(function (reser) {
            _this.reservacionesAcep = reser;
            console.log('reservaciones', reser);
        });
    };
    AdminMonitearReservPage.prototype.getClientes = function () {
        var _this = this;
        this.monRes.getAllClientes("users").then(function (c) {
            _this.clientes = c;
            console.log("Estos son los clientes: ", _this.clientes);
        });
    };
    AdminMonitearReservPage.prototype.goDetalle = function (idReservacion) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__admin_reservacion_detalle_admin_reservacion_detalle__["a" /* AdminReservacionDetallePage */], { idReservacion: idReservacion });
    };
    //aceptarReserv(idReservacion) {
    //this.navCtrl.push(AdminLeeQrPage, {idReservacion:idReservacion});
    //}
    AdminMonitearReservPage.prototype.getEvento = function () {
        var _this = this;
        this.afs.collection("evento").valueChanges().subscribe(function (data12) {
            _this.infoEvento = data12;
            console.log("evento", _this.infoEvento);
        });
    };
    AdminMonitearReservPage.prototype.scan = function () {
        var _this = this;
        this.options = {
            prompt: 'Procesando Reservación'
        };
        this.barcodeScanner.scan(this.options).then(function (data) {
            _this.scannedData = data;
            _this.datosQr = data.text;
            //console.log('this.datosQr');
            //console.log(this.datosQr);
            //obtener los datos del QR del cliente y sacarlo de uno por uno en la variable que manda el scanner
            //const dataCode = JSON.parse(this.datosQr);
            //console.log('dataCode: ', dataCode.idReservacion);
            //console.log('dataCode: ', dataCode.total);
            //console.log('dataCode: ', dataCode.idUsuario);
            //console.log('dataCode: ', dataCode.tarjeta);
            var datosQr = _this.datosQr;
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__admin_lee_qr_admin_lee_qr__["a" /* AdminLeeQrPage */], { datosQr: datosQr });
            //alert('La reservación cambio de estatus');
        }, function (err) {
            console.log('Error', err);
        });
    };
    AdminMonitearReservPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-monitear-reserv',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-monitear-reserv\admin-monitear-reserv.html"*/'<ion-header>\n\n\n\n    <ion-navbar class="borde">\n\n        <ion-row>\n\n            <ion-col col-1 align="center">\n\n                <img src="./assets/content/line2.png" class="imgLi" alt="">\n\n            </ion-col>\n\n            <ion-col col-8>\n\n                <div class="AdminEvento">Reservaciones próximas</div>\n\n                <div class="sucursal">Sucursal</div>\n\n            </ion-col>\n\n            <ion-col>\n\n                <div align=\'end\'>\n\n                    <img src="./assets/content/corona.png" style="width: 40%;" alt="">\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-row>\n\n        <ion-col col-1></ion-col>\n\n        <ion-col>\n\n            <p class="lugar">\n\n                Próximas\n\n                <br>\n\n                <img src="./assets/content/line-hori.png" style="width: 40%;" alt="">\n\n            </p>\n\n            <p class="descrip">\n\n                Sucursal\n\n            </p>\n\n        </ion-col>\n\n        <ion-col col-1></ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n        <ion-col col-1></ion-col>\n\n        <ion-col>\n\n            <ion-card class="cuadro">\n\n                <div class="proximas">PRÓXIMAS</div>\n\n            </ion-card>\n\n            <ion-card class="descripcion">\n\n                <br>\n\n                <ion-item *ngFor="let reservacion of reservaciones" class="descripcion" no-lines>\n\n                    <ion-row>\n\n                        <ion-col col-3>\n\n                            <div *ngFor="let event of infoEvento">\n\n                                <div *ngIf="event.uid == reservacion.idevento">\n\n                                    <img [src]="event.img" class="imgEvent">\n\n                                    <p class="tituloE">{{event.titulo}}</p>\n\n                                </div>\n\n                            </div>\n\n                        </ion-col>\n\n                        <ion-col col-1>\n\n                            <img src="./assets/content/line3.png" class="imgL" alt="">\n\n                        </ion-col>\n\n                        <ion-col col-6>\n\n                            <ng-container *ngFor="let cliente of clientes">\n\n                                <h2 *ngIf="cliente.uid == reservacion.idUsuario">{{ cliente.displayName }} </h2>\n\n                            </ng-container>\n\n                            <p class="fechaR">Fecha {{ reservacion.fechaR}} </p>\n\n                            <button ion-button class="btnDetalle" round (click)="goDetalle(reservacion.idReservacion)">\n\n                                Detalle\n\n                            </button>\n\n                        </ion-col>\n\n                        <ion-col col-2>\n\n                            <p class="hora">{{ reservacion.hora }} </p>\n\n                            <p style="margin-top: 5px;"></p>\n\n                            <div align="center"><img src="./assets/content/Ellipse43.png" class="imgElli" alt=""></div>\n\n                            <p style="margin-top: 5px;"></p>\n\n                            <p class="status">{{ reservacion.estatus }} </p>\n\n                            <!-- <p class="aceptado">Aceptado </p> -->\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <!-- <ion-avatar item-start>\n\n                        <img src="../assets/imgs/icons/book.png">\n\n                    </ion-avatar>\n\n                    <ng-container *ngFor="let cliente of clientes">\n\n                        <h2 *ngIf="cliente.uid == reservacion.idUsuario">{{ cliente.displayName }} </h2>\n\n                    </ng-container>\n\n                    <p>Folio:{{ reservacion.folio }} </p>\n\n                    <p>{{ reservacion.estatus }} </p>\n\n                    <p>{{ reservacion.fechaR}} </p>\n\n                    <ion-note item-end>{{ reservacion.hora }} </ion-note>\n\n                    <button ion-button color="dark" round (click)="goDetalle(reservacion.idReservacion)">\n\n                        Detalle\n\n                    </button> -->\n\n                </ion-item>\n\n                <br>\n\n                <ion-item *ngFor="let reservaciona of reservacionesAcep" class="descripcion" no-lines>\n\n                    <ion-row>\n\n                        <ion-col col-3>\n\n                            <div *ngFor="let event of infoEvento">\n\n                                <div *ngIf="event.uid == reservaciona.idevento">\n\n                                    <img [src]="event.img" class="imgEvent">\n\n                                    <p class="tituloE">{{event.titulo}}</p>\n\n                                </div>\n\n                            </div>\n\n                        </ion-col>\n\n                        <ion-col col-1>\n\n                            <img src="./assets/content/line3.png" class="imgL" alt="">\n\n                        </ion-col>\n\n                        <ion-col col-6>\n\n                            <ng-container *ngFor="let cliente of clientes">\n\n                                <h2 *ngIf="cliente.uid == reservaciona.idUsuario">{{ cliente.displayName }} </h2>\n\n                            </ng-container>\n\n                            <p class="fechaR">Fecha {{ reservaciona.fechaR}} </p>\n\n                            <button ion-button class="btnDetalle" round (click)="goDetalle(reservaciona.idReservacion)">\n\n                                Detalle\n\n                            </button>\n\n                        </ion-col>\n\n                        <ion-col col-2>\n\n                            <p class="hora">{{ reservaciona.hora }} </p>\n\n                            <p style="margin-top: 5px;"></p>\n\n                            <div align="center"><img src="./assets/content/Ellipse44.png" class="imgElli" alt=""></div>\n\n                            <p style="margin-top: 5px;"></p>\n\n                            <p class="status">STATUS </p>\n\n                            <p class="aceptado">Aceptado </p>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                    <!-- <ion-avatar item-start>\n\n                        <div *ngFor="let event of infoEvento">\n\n                            <div *ngIf="event.uid == reservaciona.idevento">\n\n                                <img [src]="event.img">\n\n                            </div>\n\n                        </div>\n\n                    </ion-avatar>\n\n                    <ng-container *ngFor="let cliente of clientes">\n\n                        <h2 *ngIf="cliente.uid == reservaciona.idUsuario">{{ cliente.displayName }} </h2>\n\n                    </ng-container>\n\n                    <p>Folio:{{ reservaciona.folio }} </p>\n\n                    <p>Aceptado </p>\n\n                    <p>{{ reservaciona.fechaR}} </p>\n\n                    <ion-note item-end>{{ reservaciona.hora }} </ion-note>\n\n                    <button ion-button color="dark" round (click)="goDetalle(reservaciona.idReservacion)">\n\n                        Detalle\n\n                    </button> -->\n\n                </ion-item>\n\n            </ion-card>\n\n        </ion-col>\n\n        <ion-col col-1></ion-col>\n\n    </ion-row>\n\n\n\n\n\n    <!-- <ion-searchbar [animated]="true" (ionInput)="getItems($event)"></ion-searchbar> -->\n\n    <!--  <ion-list *ngIf="reservaciones.length != 0">-->\n\n    <!-- <ion-list-header>Próximas</ion-list-header>\n\n    <ion-item *ngFor="let reservacion of reservaciones">\n\n        <ion-avatar item-start>\n\n            <img src="../assets/imgs/icons/book.png">\n\n        </ion-avatar>\n\n        <ng-container *ngFor="let cliente of clientes">\n\n            <h2 *ngIf="cliente.uid == reservacion.idUsuario">{{ cliente.displayName }} </h2>\n\n        </ng-container>\n\n        <p>Folio:{{ reservacion.folio }} </p>\n\n        <p>{{ reservacion.estatus }} </p>\n\n        <p>{{ reservacion.fechaR}} </p>\n\n        <ion-note item-end>{{ reservacion.hora }} </ion-note>\n\n        <button ion-button color="dark" round (click)="goDetalle(reservacion.idReservacion)">\n\n            <-- <ion-icon ios="ios-more" md="md-more"></ion-icon> --\n\n            Detalle\n\n        </button>\n\n        <--<button ion-button color="dark" round (click)="aceptarReserv(reservacion.idReservacion)">\n\n                  <-- <ion-icon ios="ios-checkmark-circle-outline" md="md-checkmark-circle-outline"></ion-icon>  -->\n\n        <!--  Aceptar\n\n                </button>--\n\n    </ion-item> -->\n\n    <!-- <ion-item *ngFor="let reservaciona of reservacionesAcep">\n\n        <ion-avatar item-start>\n\n            <img src="../assets/imgs/icons/book.png">\n\n        </ion-avatar>\n\n        <ng-container *ngFor="let cliente of clientes">\n\n            <h2 *ngIf="cliente.uid == reservaciona.idUsuario">{{ cliente.displayName }} </h2>\n\n        </ng-container>\n\n        <p>Folio:{{ reservaciona.folio }} </p>\n\n        <p>Aceptado </p>\n\n        <p>{{ reservaciona.fechaR}} </p>\n\n        <ion-note item-end>{{ reservaciona.hora }} </ion-note>\n\n        <button ion-button color="dark" round (click)="goDetalle(reservaciona.idReservacion)">\n\n            <-- <ion-icon ios="ios-more" md="md-more"></ion-icon> --\n\n            Detalle\n\n        </button>\n\n        <--<button ion-button color="dark" round (click)="aceptarReserv(reservacion.idReservacion)">\n\n                  <-- <ion-icon ios="ios-checkmark-circle-outline" md="md-checkmark-circle-outline"></ion-icon>  -->\n\n        <!--  Aceptar\n\n                </button>--\n\n    </ion-item> -->\n\n    <!--</ion-list>-->\n\n    <!--<ion-list *ngIf="reservaciones.length == 0 && reservacionesAcep.length == 0">\n\n        <-- <ion-item>\n\n            <ion-avatar item-start>\n\n                <img src="../assets/imgs/icons/book.png">\n\n            </ion-avatar>\n\n            Por el momento no hay reservaciones disponibles.\n\n        </ion-item> -->\n\n    <!--  <ion-card>\n\n            <div text-center>\n\n                <img text-center class="adv" src="../assets/imgs/icons/advertencia.png" />\n\n            </div>\n\n            <ion-card-content>\n\n                <ion-card-title text-center>\n\n                    Aviso\n\n                </ion-card-title>\n\n                <p>\n\n                    Por el momento no hay reservaciones.\n\n                </p>\n\n            </ion-card-content>\n\n        </ion-card>\n\n    </ion-list>-->\n\n    <ion-fab right bottom>\n\n        <button *ngIf="menu != \'menu\'" ion-fab color="dark" (click)="scan()">\n\n            <ion-icon ios="ios-barcode" md="md-barcode"></ion-icon>\n\n        </button>\n\n    </ion-fab>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-monitear-reserv\admin-monitear-reserv.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_2__providers_monitoreo_reservas_monitoreo_reservas__["a" /* MonitoreoReservasProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], AdminMonitearReservPage);
    return AdminMonitearReservPage;
}());

//# sourceMappingURL=admin-monitear-reserv.js.map

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CuponesSucursalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__agregar_cupones_agregar_cupones__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__detalle_cupon_detalle_cupon__ = __webpack_require__(222);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CuponesSucursalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CuponesSucursalPage = /** @class */ (function () {
    function CuponesSucursalPage(navCtrl, navParams, afs) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afs = afs;
        //cargar info de las sucursales
        this.afs.collection('sucursales').valueChanges().subscribe(function (s) {
            _this.sucursales = s;
        });
        //cargar info de los cupones
        //this.afs.collection('cupones').valueChanges().subscribe( c => {
        //this.cupones = c;
        //});
    }
    CuponesSucursalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CuponesSucursalPage');
    };
    CuponesSucursalPage.prototype.agregarCupon = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__agregar_cupones_agregar_cupones__["a" /* AgregarCuponesPage */]);
    };
    CuponesSucursalPage.prototype.cuponesDisponibles = function (idSucursal) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__detalle_cupon_detalle_cupon__["a" /* DetalleCuponPage */], { idSucursal: idSucursal });
    };
    CuponesSucursalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cupones-sucursal',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\cupones-sucursal\cupones-sucursal.html"*/'\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Cupones</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content class="fondo_admin">\n\n    <ion-card-header class="marg" color="light">\n\n      <button (click)="agregarCupon()" text-align ion-button clear small color="light" icon-left item-end>\n\n        <ion-icon name="add" class="colorIcon" style="zoom:1.1;"></ion-icon>\n\n         Agregar cupones a una sucursal\n\n      </button>\n\n    </ion-card-header>\n\n    <ion-list clxass="card_atras" *ngFor="let sucursal of sucursales">\n\n        <ion-item>\n\n           <ion-thumbnail item-start>\n\n             <img [src]="sucursal.photoURL">\n\n             <h2 style="text-align: center">{{sucursal.displayName}}</h2>\n\n           </ion-thumbnail>\n\n           <ion-badge (click)="cuponesDisponibles(sucursal.uid)" text-align color="naranja" item-end>\n\n              Ver cupones disponibles\n\n           </ion-badge>\n\n           <!--<div *ngFor="let cupon of cupones">-->\n\n            <!-- <div *ngIf="cupon.idSucursal == sucursal.uid">-->\n\n                <!--<div *ngIf="cupon.estatus == \'Activo\'">-->\n\n                <!--  <ion-badge color="naranja" item-end>\n\n                    Cupón de descuento {{ cupon.valorCupon | currency }}\n\n                    <br>\n\n                    Código {{ cupon.codigoCupon }}\n\n                    <br>\n\n                    <ion-badge color="light"> Disponibles {{ cupon.numCupones }}</ion-badge>\n\n                    <br>\n\n                    <ion-badge color="dark">{{ cupon.estatus }}</ion-badge>\n\n                  </ion-badge>-->\n\n              <!--  </div>-->\n\n            <!-- </div>-->\n\n           <!--</div>-->\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\cupones-sucursal\cupones-sucursal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], CuponesSucursalPage);
    return CuponesSucursalPage;
}());

//# sourceMappingURL=cupones-sucursal.js.map

/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CiudadEstablecimientoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__agregar_ciudad_agregar_ciudad__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__admin_menu_reservacion_admin_menu_reservacion__ = __webpack_require__(123);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CiudadEstablecimientoPage = /** @class */ (function () {
    function CiudadEstablecimientoPage(navCtrl, navParams, afs) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afs = afs;
    }
    CiudadEstablecimientoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CiudadEstablecimientoPage');
        this.cargarCiudadesData();
    };
    //funcion cargar info de establecimientos
    CiudadEstablecimientoPage.prototype.cargarCiudadesData = function () {
        var _this = this;
        this.afs.collection('ciudades').valueChanges().subscribe(function (ciudades) {
            _this.ciudades = ciudades;
        });
    };
    CiudadEstablecimientoPage.prototype.agregarCiudad = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__agregar_ciudad_agregar_ciudad__["a" /* AgregarCiudadPage */]);
    };
    CiudadEstablecimientoPage.prototype.goBack = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__admin_menu_reservacion_admin_menu_reservacion__["a" /* AdminMenuReservacionPage */]);
    };
    CiudadEstablecimientoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-ciudad-establecimiento',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\ciudad-establecimiento\ciudad-establecimiento.html"*/'\n\n<ion-header class="esquina">\n\n    <ion-navbar>\n\n       <ion-row >\n\n         <ion-col>\n\n            <button ion-fab class="colorfab" (click)="goBack()"><ion-icon name="arrow-back" class="colorI" style="zoom:1.0;"></ion-icon></button>\n\n         </ion-col>\n\n         <ion-col>\n\n             <ion-title class="texto-alineado"><h6>Ciudades registradas</h6></ion-title>\n\n         </ion-col>\n\n         <ion-col>\n\n         </ion-col>\n\n       </ion-row>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content class="fondo_admin">\n\n    <ion-card-header class="marg" color="light">\n\n      <button (click)="agregarCiudad()" text-align ion-button clear small color="light" icon-left item-end>\n\n          <img class="iconMas" src="../assets/imgs/icons/mas.png" item-start>\n\n         Agregar Ciudad\n\n      </button>\n\n    </ion-card-header>\n\n    <ion-list class="card_atras" *ngFor="let ciud of ciudades">\n\n      <ion-item>\n\n          <img class="icon" src="../assets/imgs/icons/antro.png" item-start>\n\n           <a class="blanco_2">{{ ciud.ciudad }}</a>\n\n          <ion-badge color="naranja" item-end>Agregada</ion-badge>\n\n      </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\ciudad-establecimiento\ciudad-establecimiento.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], CiudadEstablecimientoPage);
    return CiudadEstablecimientoPage;
}());

//# sourceMappingURL=ciudad-establecimiento.js.map

/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToastProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ToastProvider = /** @class */ (function () {
    function ToastProvider(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ToastProvider.prototype.show = function (message, duration) {
        if (duration === void 0) { duration = 3000; }
        return this.toastCtrl
            .create({
            message: message,
            duration: duration,
        })
            .present();
    };
    ToastProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */]])
    ], ToastProvider);
    return ToastProvider;
}());

//# sourceMappingURL=toast.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushNotiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_onesignal__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_user__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PushNotiProvider = /** @class */ (function () {
    function PushNotiProvider(oneSignal, platform, _providerUser, afireauth) {
        this.oneSignal = oneSignal;
        this.platform = platform;
        this._providerUser = _providerUser;
        this.afireauth = afireauth;
        console.log("Hello PushNotiProvider Provider");
    }
    PushNotiProvider.prototype.init_push_noti = function () {
        var _this = this;
        if (this.platform.is("cordova")) {
            this.oneSignal.startInit("de05ee4f-03c8-4ff4-8ca9-c80c97c5c0d9", "853477386824");
            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
            this.oneSignal.handleNotificationReceived().subscribe(function () {
                // do something when notification is received
            });
            this.oneSignal.handleNotificationOpened().subscribe(function () {
                // do something when a notification is opened
            });
            this.oneSignal.getIds().then(function (data) {
                // alert('Data :' + JSON.stringify(data));
                var uidUser = localStorage.getItem("uid");
                var playerID = data.userId;
                _this._providerUser.idOneSignal(uidUser, playerID);
                localStorage.setItem("playerID", playerID);
            });
            this.oneSignal.endInit();
        }
        else {
            console.log("*** OneSignal no funciona en web ***");
        }
    };
    PushNotiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["AngularFireAuth"]])
    ], PushNotiProvider);
    return PushNotiProvider;
}());

//# sourceMappingURL=push-noti.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminCartaImageEditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_carga_archivo_carta_carga_archivo__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminCartaImageEditPage = /** @class */ (function () {
    function AdminCartaImageEditPage(navCtrl, navParams, imagePicker, caPr) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imagePicker = imagePicker;
        this.caPr = caPr;
        this.imagenPreview = "";
        this.imagen64 = "";
        this.carta = { key: null };
        this.carta.key = navParams.get('key');
        this.cartaUid = navParams.get('uid');
    }
    AdminCartaImageEditPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminCartaImageEditPage');
        console.log('KEY:', this.carta.key);
    };
    AdminCartaImageEditPage.prototype.seleccionar_foto = function () {
        var _this = this;
        var opciones = {
            quality: 70,
            outputType: 1,
            maximumImagesCount: 1
        };
        this.imagePicker.getPictures(opciones).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                //console.log('Image URI: ' + results[i]);
                _this.imagenPreview = 'data:image/jpeg;base64,' + results[i];
                _this.imagen64 = results[i];
            }
        }, function (err) {
            console.log("Error en selector", JSON.stringify(err));
        });
    };
    AdminCartaImageEditPage.prototype.editarImagen = function (key) {
        var _this = this;
        var data = {
            img: this.imagen64,
            key: key
        };
        this.caPr.cargar_imagen_firebase_carta(data, this.cartaUid)
            .then(function () { return _this.navCtrl.pop; });
    };
    AdminCartaImageEditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-carta-image-edit',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-carta-image-edit\admin-carta-image-edit.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Editar Imagen Carta</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-list>\n\n        <ion-item *ngIf="imagenPreview">\n\n            <img [src]="imagenPreview">\n\n        </ion-item>\n\n    </ion-list>\n\n    <ion-grid>\n\n        <ion-row>\n\n            <ion-col>\n\n                <button color="dark" ion-button block icon-left (click)="seleccionar_foto()">\n\n      <ion-icon name="photos"></ion-icon>\n\n        Seleccionar\n\n  </button>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n        <ion-buttons end>\n\n            <button color="dark" ion-button [disabled]="imagenPreview.length <= 1" (click)="editarImagen(carta.key)">\n\n        Editar imagen\n\n      </button>\n\n        </ion-buttons>\n\n    </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-carta-image-edit\admin-carta-image-edit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__["a" /* ImagePicker */], __WEBPACK_IMPORTED_MODULE_3__providers_carga_archivo_carta_carga_archivo__["a" /* CargaArchivoCartaProvider */]])
    ], AdminCartaImageEditPage);
    return AdminCartaImageEditPage;
}());

//# sourceMappingURL=admin-carta-image-edit.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminCartaHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__admin_carta_edit_admin_carta_edit__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_toast_toast__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__admin_carta_subir_admin_evento_subir__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_carga_archivo_carta_carga_archivo__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { AdminCartaAddPage } from "../admin-carta-add/admin-carta-add";
//import { CartaItem } from '../../providers/carta-add/CartaItem';

// import { Observable } from 'rxjs/Observable';
//import { CartaAddProvider } from '../../providers/carta-add/carta-add';







var AdminCartaHomePage = /** @class */ (function () {
    //hayMas:boolean= true;
    //cartaList$: Observable<CartaItem[]>;
    //cartaList: CartaItem[];
    function AdminCartaHomePage(navCtrl, navParams, _cap, 
        //private PrCa: CartaAddProvider,
        DB, actionSheet, toastCtrl, authProvider, afs, firebase) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._cap = _cap;
        this.DB = DB;
        this.actionSheet = actionSheet;
        this.toastCtrl = toastCtrl;
        this.authProvider = authProvider;
        this.afs = afs;
        this.firebase = firebase;
        this.cartas = [];
        this.sucursal = this.firebase.auth.currentUser;
        if (this.sucursal != null) {
            this.uid = this.sucursal.uid;
            //Cuando es un usuario se saca el id de la sucursal ala que pertenece
            this.afs.collection('users', function (ref) { return ref.where('uid', '==', _this.sucursal.uid); }).valueChanges().subscribe(function (data) {
                _this.sucursales = data;
                _this.sucursales.forEach(function (element) {
                    var uidSucursal = element.uidSucursal;
                    //  console.log('sucursal del user', uidSucursal );
                    // Listado de las cartas por sucursal
                    _this.afs.collection('cartas', function (ref) { return ref.where('uidSucursal', '==', uidSucursal); })
                        .valueChanges().subscribe(function (c) {
                        _this.cartas = c;
                        console.log('cartas', c);
                    });
                });
            });
            // Listado de las cartas por sucursal
            this.afs.collection('cartas', function (ref) { return ref.where('uidSucursal', '==', _this.uid); })
                .valueChanges().subscribe(function (c) {
                _this.cartas = c;
                console.log('cartas', c);
            });
        }
        // this.cartas = DB.list('bebidas').valueChanges();
    }
    //Go to the admin-carta-subir
    AdminCartaHomePage.prototype.goCartaAdd = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__admin_carta_subir_admin_evento_subir__["a" /* AdminCartaSubirPage */]);
    };
    AdminCartaHomePage.prototype.selectCartaItem = function (cartaItem) {
        var _this = this;
        /* Display an action that gives the user the following options
          1.Edit
          2. Delete
          3. Cancel
        */
        this.actionSheet.create({
            title: "" + cartaItem.titulo,
            buttons: [
                {
                    text: 'Editar',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__admin_carta_edit_admin_carta_edit__["a" /* AdminCartaEditPage */], _this._cap.selectedProduct = Object.assign({}, cartaItem));
                        //this.navParams.get("cartaItem: cartaItem");
                        //console.log(cartaItem);
                    }
                },
                {
                    text: 'Delete',
                    role: 'destructive',
                    handler: function () {
                        //Delete the currente CartaItem
                        if (confirm('¿Estás seguro de eliminar este registro?')) {
                            _this._cap.deleteCarta(cartaItem.key, cartaItem.img, cartaItem.uid);
                            console.log('uidDele', cartaItem.uid);
                            _this.toastCtrl.show(cartaItem.titulo + " deleted");
                        }
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log("El usuario cancelo.");
                    }
                }
            ]
        }).present();
    };
    AdminCartaHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-carta-home',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-carta-home\admin-carta-home.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Carta CRUD</ion-title>\n\n\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="fondo_admin" padding>\n\n    <ion-fab left bottom>\n\n        <button ion-fab color="light"><ion-icon name="arrow-dropright"></ion-icon></button>\n\n        <ion-fab-list side="right">\n\n            <button ion-fab (click)="goCartaAdd()">\n\n          <ion-icon name="add"></ion-icon>\n\n          </button>\n\n        </ion-fab-list>\n\n    </ion-fab>\n\n    <ion-list>\n\n        <ion-list-header>\n\n            Menú<br> Listado de bebidas\n\n        </ion-list-header>\n\n        <ion-item *ngFor="let cartaItem of cartas" (click)="selectCartaItem(cartaItem)">\n\n            <ion-avatar item-start>\n\n                <img [src]="cartaItem.img">\n\n            </ion-avatar>\n\n            <h2>{{cartaItem.titulo}}</h2>\n\n            <p>{{cartaItem.categoria}}</p>\n\n        </ion-item>\n\n    </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-carta-home\admin-carta-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6__providers_carga_archivo_carta_carga_archivo__["a" /* CargaArchivoCartaProvider */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_toast_toast__["a" /* ToastProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_7__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_9_angularfire2_auth__["AngularFireAuth"]])
    ], AdminCartaHomePage);
    return AdminCartaHomePage;
}());

//# sourceMappingURL=admin-carta-home.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminCartaEditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_carga_archivo_carta_carga_archivo__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__admin_carta_image_edit_admin_carta_image_edit__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { NgForm } from '@angular/forms';


var AdminCartaEditPage = /** @class */ (function () {
    function AdminCartaEditPage(navCtrl, navParams, crPr) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.crPr = crPr;
        this.cartaItem = {
            titulo: this.crPr.selectedProduct.titulo,
            precio: this.crPr.selectedProduct.precio,
            categoria: this.crPr.selectedProduct.categoria,
            nota: this.crPr.selectedProduct.nota,
            img: this.crPr.selectedProduct.img
        };
        // $key: string;
        this.data = {};
        this.crPr.selectedProduct.key;
        this.crPr.selectedProduct.titulo;
        this.crPr.selectedProduct.categoria;
        this.crPr.selectedProduct.precio;
        console.log(this.crPr.selectedProduct.titulo);
    }
    AdminCartaEditPage.prototype.ionViewDidLoad = function () {
        this.cartaItem = this.navParams.get('cartaItem');
    };
    AdminCartaEditPage.prototype.SaveCartaItem = function (cartaItem) {
        console.log(cartaItem);
        this.data = {
            titulo: this.crPr.selectedProduct.titulo,
            precio: this.crPr.selectedProduct.precio,
            categoria: this.crPr.selectedProduct.categoria,
            nota: this.crPr.selectedProduct.nota,
            KEY: this.crPr.selectedProduct.key
        };
        var cartaUId = this.crPr.selectedProduct.uid;
        console.log(this.data);
        this.crPr.updateCarta(this.data, cartaUId);
        // .then(ref =>{
        //   this.toastCtrl.show(`${this.data.name} Edited!`)
        //  //this.navCtrl.setRoot('AdminCartaHomePage', {$key: ref.$key});
        this.navCtrl.pop();
        //})
        //console.log(this.crPr.selectedProduct.$key);
        //console.log(this.crPr.selectedProduct.$key);
    };
    AdminCartaEditPage.prototype.editCartaImg = function (key, uid) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__admin_carta_image_edit_admin_carta_image_edit__["a" /* AdminCartaImageEditPage */], { key: key, uid: uid });
    };
    AdminCartaEditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-carta-edit',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-carta-edit\admin-carta-edit.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title> {{crPr.selectedProduct.titulo }} </ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content class="fondo_admin" padding>\n\n    <ion-list>\n\n        <ion-list-header>\n\n            Editar: {{ crPr.selectedProduct.titulo }}\n\n        </ion-list-header>\n\n        <ion-input type="hidden" [(ngModel)]="crPr.selectedProduct.key"></ion-input>\n\n        <ion-input type="hidden" [(ngModel)]="crPr.selectedProduct.uid"></ion-input>\n\n        <ion-item>\n\n            <ion-label floating>Nombre</ion-label>\n\n            <ion-input type="text" [(ngModel)]="crPr.selectedProduct.titulo"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label floating>Categoria</ion-label>\n\n            <ion-input type="text" [(ngModel)]="crPr.selectedProduct.categoria"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label floating>Precio</ion-label>\n\n            <ion-input type="number" [(ngModel)]="crPr.selectedProduct.precio"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="fond">\n\n            <ion-label floating>Nota</ion-label>\n\n            <ion-textarea class="fond" placeholder="Descripción" [(ngModel)]="crPr.selectedProduct.nota" name="nota" autocomplete="on" autocorrect="on"></ion-textarea>\n\n        </ion-item>\n\n        <ion-item>\n\n            <button (click)="editCartaImg(crPr.selectedProduct.key, crPr.selectedProduct.uid )"><img [src]="crPr.selectedProduct.img" alt=""></button>\n\n        </ion-item>\n\n        <ion-row>\n\n            <ion-col>\n\n                <button class="md-trash" color="dark" ion-button block (click)="SaveCartaItem()">Guardar edición</button>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-carta-edit\admin-carta-edit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_carga_archivo_carta_carga_archivo__["a" /* CargaArchivoCartaProvider */]])
    ], AdminCartaEditPage);
    return AdminCartaEditPage;
}());

//# sourceMappingURL=admin-carta-edit.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminEventoEditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_carga_archivo_carga_archivo__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__admin_evento_image_edit_admin_evento_image_edit__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminEventoEditPage = /** @class */ (function () {
    function AdminEventoEditPage(navCtrl, navParams, _cap) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._cap = _cap;
        this.data = {};
    }
    AdminEventoEditPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminEventoEditPage');
    };
    AdminEventoEditPage.prototype.saveEventoItem = function (eventoItem) {
        console.log(eventoItem);
        this.data = {
            titulo: this._cap.selectedEventoItem.titulo,
            fecha: this._cap.selectedEventoItem.fecha,
            hora: this._cap.selectedEventoItem.hora,
            hora_fin: this._cap.selectedEventoItem.hora_fin,
            categoria: this._cap.selectedEventoItem.categoria,
            lugar: this._cap.selectedEventoItem.lugar,
            obs: this._cap.selectedEventoItem.obs,
            img: this._cap.selectedEventoItem.img,
            KEY: this._cap.selectedEventoItem.key
        };
        console.log(this.data);
        this._cap.updateEvento(this.data, this._cap.selectedEventoItem.uid);
        console.log(this._cap.selectedEventoItem.uid);
        this.navCtrl.pop();
    };
    AdminEventoEditPage.prototype.editEventoImg = function (key, uid) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__admin_evento_image_edit_admin_evento_image_edit__["a" /* AdminEventoImageEditPage */], { key: key, uid: uid });
        // this.navCtrl.push(AdminEventoImageEditPage, {uid:uid});
    };
    AdminEventoEditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-evento-edit',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-evento-edit\admin-evento-edit.html"*/'<!-- <ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title> {{ _cap.selectedEventoItem.titulo }} </ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header> -->\n\n\n\n<ion-header>\n\n\n\n    <ion-navbar class="borde">\n\n        <ion-row>\n\n            <ion-col col-1 align="center">\n\n                <img src="./assets/content/line2.png" class="imgLi" alt="">\n\n            </ion-col>\n\n            <ion-col col-8>\n\n                <div class="AdminEvento">{{ _cap.selectedEventoItem.titulo }}</div>\n\n                <div class="sucursal">Sucursal</div>\n\n            </ion-col>\n\n            <ion-col>\n\n                <div align=\'end\'>\n\n                    <img src="./assets/content/corona.png" style="width: 40%;" alt="">\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n    <ion-row>\n\n        <ion-col col-1></ion-col>\n\n        <ion-col>\n\n            <p class="lugar">\n\n                EDICIÓN DE EVENTO\n\n                <br>\n\n                <img src="./assets/content/line-hori.png" style="width: 40%;" alt="">\n\n            </p>\n\n            <p class="descrip">\n\n                Para mejorar la experiencia del usuario Ghest , le recomendamos subir imagenes en formato PNG\n\n            </p>\n\n        </ion-col>\n\n        <ion-col col-1></ion-col>\n\n    </ion-row>\n\n\n\n    <br>\n\n\n\n    <ion-row>\n\n        <ion-col></ion-col>\n\n        <ion-col>\n\n            <button style="background-color: transparent;" (click)="editEventoImg(_cap.selectedEventoItem.key, _cap.selectedEventoItem.uid)"><img [src]="_cap.selectedEventoItem.img" class="imgEvento" alt=""></button>\n\n            <img src="./assets/content/Ellipse.png" class="camara" alt="">\n\n            <!-- <div *ngIf="imagenPreview">\n\n                <img [src]="imagenPreview" class="imgEvento">\n\n            </div> -->\n\n            <!-- <img src="./assets/content/1.jpg" class="imgEvento" alt=""> -->\n\n        </ion-col>\n\n        <ion-col></ion-col>\n\n    </ion-row>\n\n\n\n    <ion-list>\n\n        <ion-list-header>\n\n            Editar: {{ _cap.selectedEventoItem.titulo }}\n\n        </ion-list-header>\n\n        <ion-input type="hidden" [(ngModel)]="_cap.selectedEventoItem.uid"></ion-input>\n\n        <ion-input type="hidden" [(ngModel)]="_cap.selectedEventoItem.key"></ion-input>\n\n        <ion-item>\n\n            <ion-label floating>Nombre</ion-label>\n\n            <ion-input type="text" class="letra" [(ngModel)]="_cap.selectedEventoItem.titulo"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Fecha</ion-label>\n\n            <ion-input type="date" class="letra" [(ngModel)]="_cap.selectedEventoItem.fecha"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Hora inicio</ion-label>\n\n            <ion-input type="time" class="letra" [(ngModel)]="_cap.selectedEventoItem.hora"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>Hora fin</ion-label>\n\n            <ion-input type="time" class="letra" [(ngModel)]="_cap.selectedEventoItem.hora_fin"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Categoría</ion-label>\n\n            <ion-select [(ngModel)]="_cap.selectedEventoItem.categoria">\n\n                <ion-option value="Deportivo">Deportivo</ion-option>\n\n                <ion-option value="Cultural">Cultural</ion-option>\n\n                <ion-option value="Académico">Académico</ion-option>\n\n                <ion-option value="Recreativo">Recreativo</ion-option>\n\n                <ion-option value="Social">Social</ion-option>\n\n                <ion-option value="Festival">Festival</ion-option>\n\n                <ion-option value="Homenaje">Homenaje</ion-option>\n\n                <ion-option value="Reconocimiento">Reconocimiento</ion-option>\n\n                <ion-option value="Feria">Feria</ion-option>\n\n                <ion-option value="Cumpleaños">Cumpleaños</ion-option>\n\n                <ion-option value="Familiy">Familiy Day</ion-option>\n\n                <ion-option value="Simposio">Simposio</ion-option>\n\n                <ion-option value="Otro">Otro</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label floating>Lugar</ion-label>\n\n            <ion-input type="text" class="letra" [(ngModel)]="_cap.selectedEventoItem.lugar"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-card class="cardDes">\n\n            <!-- <ion-item class="fond" no-lines>\n\n                <ion-textarea class="fond" placeholder="Descripción" [(ngModel)]="obs" name="obs" autocomplete="on"\n\n                    autocorrect="on"></ion-textarea>\n\n            </ion-item> -->\n\n            <ion-item class="fond">\n\n                <ion-textarea class="fond" placeholder="Descripción" [(ngModel)]="obs" name="obs" autocomplete="on"\n\n                autocorrect="on"></ion-textarea>\n\n            </ion-item>\n\n        </ion-card>\n\n\n\n        \n\n        <!-- <ion-item>\n\n            <button (click)="editEventoImg(_cap.selectedEventoItem.key, _cap.selectedEventoItem.uid)"><img [src]="_cap.selectedEventoItem.img" alt=""></button>\n\n        </ion-item> -->\n\n        <ion-row>\n\n            <ion-col></ion-col>\n\n            <ion-col>\n\n                <button class="md-trash" ion-button (click)="saveEventoItem()">Guardar edición</button>\n\n            </ion-col>\n\n            <ion-col></ion-col>\n\n        </ion-row>\n\n\n\n\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-evento-edit\admin-evento-edit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_carga_archivo_carga_archivo__["a" /* CargaArchivoProvider */]])
    ], AdminEventoEditPage);
    return AdminEventoEditPage;
}());

//# sourceMappingURL=admin-evento-edit.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminEventoImageEditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_carga_archivo_carga_archivo__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { AdminEventoSubirPage } from '../admin-evento-subir/admin-evento-subir';
var AdminEventoImageEditPage = /** @class */ (function () {
    function AdminEventoImageEditPage(navCtrl, navParams, imagePicker, sucProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imagePicker = imagePicker;
        this.sucProv = sucProv;
        this.imagenPreview = "";
        this.imagen64 = "";
        this.evento = { key: null };
        this.eventoKey = navParams.get('key');
        console.log('key', this.evento.key);
        this.eventoUid = navParams.get('uid');
        console.log('uid', this.eventoUid);
    }
    AdminEventoImageEditPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminEventoImageEditPage');
    };
    AdminEventoImageEditPage.prototype.seleccionar_foto = function () {
        var _this = this;
        var opciones = {
            quality: 70,
            outputType: 1,
            maximumImagesCount: 1
        };
        this.imagePicker.getPictures(opciones).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                //console.log('Image URI: ' + results[i]);
                _this.imagenPreview = 'data:image/jpeg;base64,' + results[i];
                _this.imagen64 = results[i];
            }
        }, function (err) {
            console.log("Error en selector", JSON.stringify(err));
        });
    };
    AdminEventoImageEditPage.prototype.editarImagen = function () {
        var _this = this;
        var data = {
            img: this.imagen64
        };
        this.sucProv.cargar_imagen_firebase_evento(data, this.eventoKey, this.eventoUid)
            .then(function () { return _this.navCtrl.pop; });
        console.log('data', data);
    };
    AdminEventoImageEditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-evento-image-edit',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-evento-image-edit\admin-evento-image-edit.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Editar Imagen Evento</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-list>\n\n        <ion-item *ngIf="imagenPreview">\n\n            <img [src]="imagenPreview">\n\n        </ion-item>\n\n    </ion-list>\n\n    <ion-grid>\n\n        <ion-row>\n\n            <ion-col>\n\n                <button color="dark" ion-button block icon-left (click)="seleccionar_foto()">\n\n        <ion-icon name="photos"></ion-icon>\n\n          Seleccionar\n\n    </button>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n        <ion-buttons end>\n\n            <button color="dark" ion-button [disabled]="imagenPreview.length <= 1" (click)="editarImagen(evento.key)">\n\n          Editar imagen\n\n        </button>\n\n        </ion-buttons>\n\n    </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-evento-image-edit\admin-evento-image-edit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__["a" /* ImagePicker */], __WEBPACK_IMPORTED_MODULE_3__providers_carga_archivo_carga_archivo__["a" /* CargaArchivoProvider */]])
    ], AdminEventoImageEditPage);
    return AdminEventoImageEditPage;
}());

//# sourceMappingURL=admin-evento-image-edit.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventoDetallePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_carga_archivo_carga_archivo__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__reservaciones_reservaciones__ = __webpack_require__(91);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { AngularFirestore } from '@angular/fire/firestore';

var EventoDetallePage = /** @class */ (function () {
    function EventoDetallePage(navCtrl, navParams, _cap) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._cap = _cap;
        this.evento = {};
        this.evento.uid = this.navParams.get("uid");
        this.sucursalID = this.navParams.get("sucursalID");
        console.log("key", this.evento.uid);
        console.log("SucursalID", this.sucursalID);
        // _cap.getEvento(this.evento.key)
        //     .valueChanges().subscribe(evento=>{
        //       this.evento = evento;
        //     });
        // this.getDetails(this.evento.key);
    }
    EventoDetallePage.prototype.ionViewDidLoad = function () {
        this.getDetails();
        console.log("ionViewDidLoad EventoDetallePage");
    };
    EventoDetallePage.prototype.getDetails = function () {
        var _this = this;
        this._cap.getEvento(this.evento.uid).then(function (e) {
            _this.evento = e;
            console.log("evento", e);
        });
    };
    EventoDetallePage.prototype.Reservar = function (uid, idSucursal) {
        console.log(idSucursal);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__reservaciones_reservaciones__["a" /* ReservacionesPage */], {
            uid: uid,
            idSucursal: idSucursal
        });
    };
    EventoDetallePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-evento-detalle",template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\evento-detalle\evento-detalle.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Guest Eventos</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="fondo2 cards-bg" padding>\n\n    <ion-card class="atras">\n\n\n\n        <img [src]="evento.img" />\n\n        <ion-card-content class="atras" text-center>\n\n            <ion-card-title>\n\n                {{ evento.titulo }}\n\n            </ion-card-title>\n\n            <p>\n\n                {{evento.obs}}\n\n            </p>\n\n            <p>\n\n                {{evento.fecha}}, {{ evento.hora }}\n\n            </p>\n\n            <p>\n\n                {{evento.categoria}}\n\n            </p>\n\n            <p>\n\n                {{evento.lugar}}\n\n            </p>\n\n        </ion-card-content>\n\n\n\n        <ion-row no-padding>\n\n            <ion-col>\n\n                <button ion-button clear small color="gold" icon-start (click)="Reservar(evento.uid, sucursalID)">\n\n          <ion-icon name=\'custom-corona\'></ion-icon>\n\n          Reservar\n\n        </button>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </ion-card>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\evento-detalle\evento-detalle.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_carga_archivo_carga_archivo__["a" /* CargaArchivoProvider */]])
    ], EventoDetallePage);
    return EventoDetallePage;
}());

//# sourceMappingURL=evento-detalle.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductoDetallePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_carta_carta__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_reservacion_reservacion__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__carta_carta__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductoDetallePage = /** @class */ (function () {
    function ProductoDetallePage(navCtrl, navParams, _providerCarta, _providerReservacion) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._providerCarta = _providerCarta;
        this._providerReservacion = _providerReservacion;
        this.product = {};
        this.pisto = 0;
        this.disableButton = true;
        this.idProducto = navParams.get("idProducto");
        this.idReservacion = navParams.get("idReservacion");
        this.evento = navParams.get("uid");
        this.idSucursal = navParams.get("idSucursal");
    }
    ProductoDetallePage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ProductoDetallePage");
        this.getProduct(this.idProducto);
    };
    ProductoDetallePage.prototype.getProduct = function (idProducto) {
        var _this = this;
        this._providerCarta.getProduct(idProducto).subscribe(function (product) {
            _this.product = product;
            console.log("Detalle producto: ", _this.product.titulo);
        });
    };
    ProductoDetallePage.prototype.increment = function () {
        if (this.pisto < 20) {
            this.pisto++;
        }
    };
    ProductoDetallePage.prototype.decrement = function () {
        if (this.pisto > 0) {
            this.pisto--;
        }
    };
    ProductoDetallePage.prototype.validarBoton = function () {
        if (this.pisto >= 1) {
            this.disableButton = false;
        }
        else {
            this.disableButton = true;
        }
    };
    ProductoDetallePage.prototype.agregar = function () {
        var _this = this;
        var producto = {
            cantidad: this.pisto,
            idProducto: this.idProducto,
            idReservacion: this.idReservacion
        };
        this._providerReservacion
            .addProducto(producto)
            .then(function (respuesta) {
            if (respuesta.success == true) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__carta_carta__["a" /* CartaPage */], {
                    idReservacion: _this.idReservacion,
                    uid: _this.evento,
                    idSucursal: _this.idSucursal
                });
            }
            else {
            }
        })
            .catch();
    };
    ProductoDetallePage.prototype.goBack = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__carta_carta__["a" /* CartaPage */], {
            idReservacion: this.idReservacion,
            uid: this.evento,
            idSucursal: this.idSucursal
        });
    };
    ProductoDetallePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-producto-detalle",template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\producto-detalle\producto-detalle.html"*/'<ion-header>\n\n    <ion-navbar hideBackButton>\n\n        <div text-center>\n\n            <ion-title>{{product.titulo}}</ion-title>\n\n        </div>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="card-background-page">\n\n    <ion-card>\n\n        <img [src]="product.img" />\n\n        <div class="card-title">\n\n            <ion-badge id="color-nota" item-end id="badge-titulo">\n\n                <div text-wrap>\n\n                    {{product.nota}}\n\n                </div>\n\n            </ion-badge>\n\n        </div>\n\n        <div class="card-subtitle">\n\n            <ion-badge color="dark" item-end id="badge-stitulo">{{product.precio | currency}}\n\n            </ion-badge>\n\n        </div>\n\n    </ion-card>\n\n    <ion-grid id="pisto">\n\n        <ion-row>\n\n            <ion-col col-4>\n\n                <div text-right>\n\n                    <img src="assets/imgs/icons/-.png" id="mas_menos" (click)="decrement(); validarBoton();">\n\n                </div>\n\n            </ion-col>\n\n            <ion-col col-4 id="caja">\n\n                <div class="caja" text-center>\n\n                    <span>{{pisto}}</span>\n\n                </div>\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <div text-rigth>\n\n                    <img src="assets/imgs/icons/+.png" id="mas_menos" (click)="increment(); validarBoton();">\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <div text-center>\n\n                    <button ion-button round block color="dark" (click)="(agregar())" [disabled]="disableButton">Agregar</button>\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n</ion-content>\n\n\n\n<ion-footer no-border>\n\n    <ion-toolbar>\n\n        <ion-grid>\n\n            <ion-row>\n\n                <ion-col col-12>\n\n                    <div text-center>\n\n                        <button ion-button round block color="dark" (click)="(goBack())">Atras</button>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-grid>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\producto-detalle\producto-detalle.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_carta_carta__["a" /* CartaProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_reservacion_reservacion__["a" /* ReservacionProvider */]])
    ], ProductoDetallePage);
    return ProductoDetallePage;
}());

//# sourceMappingURL=producto-detalle.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminUsersListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_users_guest_admin_users_guest__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__admin_user_user_admin_user_user__ = __webpack_require__(209);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminUsersListPage = /** @class */ (function () {
    function AdminUsersListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AdminUsersListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminUsersListPage');
    };
    AdminUsersListPage.prototype.goEmpleados = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__admin_users_guest_admin_users_guest__["a" /* AdminUsersGuestPage */]);
    };
    AdminUsersListPage.prototype.goUsers = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__admin_user_user_admin_user_user__["a" /* AdminUserUserPage */]);
    };
    AdminUsersListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-users-list',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-users-list\admin-users-list.html"*/'<!--\n\n  Generated template for the AdminUsersListPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Administración de usuarios</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="fondo_admin">\n\n    <div class="centrar">\n\n        <ion-row>\n\n            <ion-col class="text-center">\n\n                <img height="150" width="200" src="./assets/imgs/home/Logo.png">\n\n            </ion-col>\n\n        </ion-row>\n\n    </div>\n\n    <ion-list class="atras">\n\n        <button class="atras" ion-item (click)="goEmpleados()" icon-start>\n\n          <ion-icon item-start><img class="home" src="../assets/imgs/icons/corona.png"> </ion-icon>\n\n          Administración de empleados\n\n        </button>\n\n\n\n        <button class="atras" ion-item (click)="goUsers()" icon-start>\n\n          <ion-icon item-start><img class="home" src="../assets/imgs/icons/admin-user.png"> </ion-icon>\n\n          Administración de usuarios\n\n        </button>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-users-list\admin-users-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], AdminUsersListPage);
    return AdminUsersListPage;
}());

//# sourceMappingURL=admin-users-list.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminUsersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminUsersPage = /** @class */ (function () {
    function AdminUsersPage(navCtrl, loadingCtrl, toastCtrl, viewCtrl, userProvider, afs, firebase) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.userProvider = userProvider;
        this.afs = afs;
        this.firebase = firebase;
        this.credentials = {
            name: '',
            email: '',
            password: '',
            type: ''
        };
        this.sucursal = this.firebase.auth.currentUser;
        if (this.sucursal != null) {
            this.uidSucursal = this.sucursal.uid;
            //Cuando es un usuario se saca el id de la sucursal ala que pertenece
            this.afs.collection('users', function (ref) { return ref.where('uid', '==', _this.sucursal.uid); }).valueChanges().subscribe(function (data) {
                _this.sucursales = data;
                _this.sucursales.forEach(function (element) {
                    var tipoUser = element.type;
                    var uidSucursal = element.uidSucursal;
                    if (tipoUser == 'coordinacion' || tipoUser == 'rp' || tipoUser == 'capitan_mesero') {
                        _this.uidSucursal = uidSucursal;
                    }
                });
            });
        }
    }
    AdminUsersPage.prototype.register = function () {
        var toaster = this.toastCtrl.create({
            duration: 3000,
            position: 'bottom'
        });
        if (this.credentials.email == '' || this.credentials.password == '' || this.credentials.name == '' || this.credentials.type == '') {
            toaster.setMessage('Todos los campos son requeridos');
            toaster.present();
        }
        else if (this.credentials.password.length < 7) {
            toaster.setMessage('La contraseña no es sufucientemente larga, intenta con más de 7 caracteres');
            toaster.present();
        }
        else {
            var loader = this.loadingCtrl.create({
                content: 'Por favor, espere'
            });
            // loader.present();
            this.userProvider.newRegister(this.credentials, this.uidSucursal);
            // this.navCtrl.pop();newRegister
            this.cerrar_modal();
        }
        //this.navCtrl.push(AdminUsersGuestPage);
    };
    AdminUsersPage.prototype.cerrar_modal = function () {
        this.viewCtrl.dismiss();
    };
    AdminUsersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-users',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-users\admin-users.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button class="bt" ion-button (click)="cerrar_modal()">\n\n                Cerrar\n\n        </button>\n\n        <ion-title>Alta de empleados</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="fondo_admin" padding>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-label floating>Nombre</ion-label>\n\n            <ion-input type="text" [(ngModel)]="credentials.name"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>Correo</ion-label>\n\n            <ion-input type="email" [(ngModel)]="credentials.email"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>Contraseña</ion-label>\n\n            <ion-input type="password" [(ngModel)]="credentials.password"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="fond">\n\n            <ion-label>Tipo de usuario</ion-label>\n\n            <ion-select [(ngModel)]="credentials.type" class="fond">\n\n                <!-- <ion-option value="a">Administrador</ion-option> -->\n\n                <!--<ion-option value="e">Empleado</ion-option>-->\n\n                <ion-option value="coordinacion">Coordinación</ion-option>\n\n                <ion-option value="rp">RP</ion-option>\n\n                <ion-option value="capitan_mesero">Capitán meseros</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-item text-center>\n\n            <button color="dark" ion-button round style="min-width: 200px; padding: 10px 16px" (click)="register()">Register</button>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-users\admin-users.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_4__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["AngularFireAuth"]])
    ], AdminUsersPage);
    return AdminUsersPage;
}());

//# sourceMappingURL=admin-users.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminUserUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_database__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__admin_user_detail_admin_user_detail__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_usuario_usuario__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import firebase from 'firebase';

var AdminUserUserPage = /** @class */ (function () {
    function AdminUserUserPage(navCtrl, navParams, DB, actionSheet, _up, afs) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DB = DB;
        this.actionSheet = actionSheet;
        this._up = _up;
        this.afs = afs;
        this.afs.collection('users').valueChanges().subscribe(function (u) {
            _this.users = u;
        });
        // this.users = this.DB.list('users').valueChanges();
    }
    AdminUserUserPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminUserUserPage');
    };
    AdminUserUserPage.prototype.selectUsuario = function (uid) {
        var _this = this;
        this.actionSheet.create({
            title: 'Acciones',
            buttons: [
                {
                    text: 'Ver más',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__admin_user_detail_admin_user_detail__["a" /* AdminUserDetailPage */], { uid: uid });
                    }
                },
                // {          
                //   text:'Inhabilitar cuenta',
                //   role: 'destructive',
                //   handler:()=>{
                //     if(confirm('¿Estas seguro de desactivar a este usuario?')){
                //       this._up.inhabilitar(uid);
                //       console.log('uid usuario', uid);
                //       console.log('Se inhabilito');
                //   } 
                // }
                // },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log("Cancelo");
                    }
                }
            ]
        }).present();
    };
    AdminUserUserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-user-user',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-user-user\admin-user-user.html"*/'<!--\n\n  Generated template for the AdminUserUserPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Usuarios</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="fondo_admin">\n\n    <ion-card-header color="light">Lista de usuarios</ion-card-header>\n\n    <div *ngFor="let user of users">\n\n\n\n        <ion-list *ngIf="user.type == \'u\' " class="list_blanco_transp">\n\n            <ion-item (click)="selectUsuario(user.uid)">\n\n                <ion-thumbnail item-start>\n\n                    <img [src]="user.photoURL">\n\n                </ion-thumbnail>\n\n                <h6>\n\n                    Nombre\n\n                    <p class="p">{{ user.displayName }}</p>\n\n                </h6>\n\n                <ion-badge *ngIf="user.active == false " class="fon" item-end>Inhabilitado</ion-badge>\n\n                <h6>\n\n                    ID\n\n                    <p class="p">{{ user.uid }}</p>\n\n                </h6>\n\n            </ion-item>\n\n        </ion-list>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-user-user\admin-user-user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_usuario_usuario__["a" /* UsuarioProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], AdminUserUserPage);
    return AdminUserUserPage;
}());

//# sourceMappingURL=admin-user-user.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminUserDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AdminUserDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdminUserDetailPage = /** @class */ (function () {
    function AdminUserDetailPage(navCtrl, navParams, _cap) {
        //  this.user.uid = navParams.get('uid');
        //  _cap.getUser(this.user.uid)
        //   .valueChanges().subscribe(user=>{
        //     this.user = user;
        //   });
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._cap = _cap;
        //user: any={uid: null, nombre: null, email: null, imagen: null,  provider: null, active: null, phoneNumber: null}
        this.user = {};
        this.getUser(navParams.get('uid'));
    }
    AdminUserDetailPage.prototype.getUser = function (idx) {
        var _this = this;
        var array = {};
        this._cap.getUser1(idx).subscribe(function (users) {
            _this.user = users;
            console.log('users', users);
            users.forEach(function (value) {
                console.log(value.active);
                array = { uid: value.uid, displayName: value.displayName, email: value.email, photoURL: value.photoURL, provider: value.provider, active: value.active, phoneNumber: value.phoneNumber };
            });
            _this.user = array;
            console.log(_this.user);
        });
    };
    AdminUserDetailPage.prototype.habilitar_user = function (uid, status) {
        console.log(status);
        if (status == true) {
            if (confirm('¿Estas seguro de habilitar este usuario?')) {
                this._cap.habilitar(uid, status);
                console.log('Se habilito');
            }
        }
        else {
            if (confirm('¿Estas seguro de deshabilitar este usuario?')) {
                this._cap.habilitar(uid, status);
                console.log('Se deshabilito');
            }
        }
    };
    AdminUserDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminUserDetailPage');
    };
    AdminUserDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-user-detail',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-user-detail\admin-user-detail.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Usuario detalle </ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <!-- <h3 text-center class="blanco"> Información del usuario </h3> -->\n\n\n\n\n\n    <ion-list no-lines class="margen_2">\n\n        <ion-item *ngIf="user.active == true">\n\n            <h4>Estado: </h4>\n\n            <ion-badge class="fon" item-end (click)="habilitar_user(user.uid, false)" >Activo</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="user.active == false">\n\n            <h4>Estado: </h4>\n\n            <ion-badge (click)="habilitar_user(user.uid, true)" class="fon2" item-end>Inhabilitado</ion-badge>\n\n\n\n        </ion-item>\n\n        <ion-item text-center>\n\n            <!--Imagen del usuario-->\n\n            <div class="">\n\n                <img class="imagen_perfil" [src]="user.photoURL">\n\n            </div>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <h4>Usuario tipo:</h4>\n\n            <!--Filtro de bandera de usuario-->\n\n            <div class="filt">\n\n                <img class="cate" src="../assets/imgs/icons/gold_guest.png" item-start>\n\n            </div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <h4>ID: </h4>\n\n            {{user.uid}}\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/profile.png" item-start>\n\n            <div class="margen3">\n\n                <h4> Nombre:</h4> {{user.displayName}}</div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/correo.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Correo:</h4>{{ user.email}}</div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/cumple.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Cumpleaños:</h4>xxxx-xx-xx</div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/telefono.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Teléfono:</h4> {{user.phoneNumber }} </div>\n\n        </ion-item>\n\n        <ion-item *ngIf="user.provider == \'facebook\'">\n\n            <div class="margen3">\n\n                <h4>Sesión:</h4>Facebook</div>\n\n            <img class="icon" src="../assets/imgs/icons/facebook.png" item-start>\n\n        </ion-item>\n\n        <ion-item *ngIf="user.provider == \'google\'">\n\n            <div class="margen3">\n\n                <h4>Sesión:</h4>Google</div>\n\n            <img class="icon" src="../assets/imgs/icons/google.png" item-start>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <div class="margen3">\n\n                <h4>Tarjetas</h4>\n\n            </div>\n\n            <img class="icon" src="../assets/imgs/icons/tarjeta.png" item-start>\n\n            <ion-title>**** 5425</ion-title>\n\n            <ion-title>**** 6420</ion-title>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-user-detail\admin-user-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__["a" /* UsuarioProvider */]])
    ], AdminUserDetailPage);
    return AdminUserDetailPage;
}());

//# sourceMappingURL=admin-user-detail.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminSucursalSubirPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminSucursalSubirPage = /** @class */ (function () {
    function AdminSucursalSubirPage(navCtrl, navParams, loadingCtrl, toastCtrl, viewCtrl, ProSuc, afs) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.ProSuc = ProSuc;
        this.afs = afs;
        this.credentials = {
            sucursal: '',
            nombrecontacto: '',
            direccion: '',
            email: '',
            password: '',
            status: 'activo',
            tipo: '',
            ciudad: '',
            telefono: ''
        };
    }
    AdminSucursalSubirPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminSucursalSubirPage');
        this.cargarCiudades();
    };
    //funcion cargar info de las sucursales
    AdminSucursalSubirPage.prototype.cargarCiudades = function () {
        var _this = this;
        this.afs.collection('ciudades').valueChanges().subscribe(function (ciu) {
            _this.ciudades = ciu;
        });
    };
    AdminSucursalSubirPage.prototype.registrar = function () {
        console.log('datos ciudad', this.credentials);
        var toaster = this.toastCtrl.create({
            duration: 3000,
            position: 'bottom'
        });
        if (this.credentials.email == '' || this.credentials.password == '' || this.credentials.name == '' || this.credentials.sucursal == '' || this.credentials.direccion == '' || this.credentials.tipo == '' || this.credentials.ciudad == '') {
            toaster.setMessage('Todos los campos son requeridos');
            toaster.present();
        }
        else if (this.credentials.password.length < 7) {
            toaster.setMessage('La contraseña no es sufucientemente larga, intenta con más de 7 caracteres');
            toaster.present();
        }
        else {
            var loader = this.loadingCtrl.create({
                content: 'Por favor, espere'
            });
            // loader.present();
            this.ProSuc.newRegister(this.credentials);
            // this.navCtrl.pop();
            this.cerrar_modal();
        }
    };
    AdminSucursalSubirPage.prototype.cerrar_modal = function () {
        this.viewCtrl.dismiss();
    };
    AdminSucursalSubirPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-sucursal-subir',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-subir\admin-sucursal-subir.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button class="bt" ion-button (click)="cerrar_modal()">\n\n                Cerrar\n\n        </button>\n\n        <ion-title>Alta de sucursales</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="fondo_admin" padding>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-label floating>Nombre del contacto</ion-label>\n\n            <ion-input type="text" [(ngModel)]="credentials.nombrecontacto"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label floating>Nombre sucursal</ion-label>\n\n            <ion-input type="text" [(ngModel)]="credentials.sucursal"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label floating>Teléfono</ion-label>\n\n            <ion-input type="phone" [(ngModel)]="credentials.telefono"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Dirección</ion-label>\n\n            <ion-input type="text" [(ngModel)]="credentials.direccion" placeholder="Colonia, Calle, Número"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label floating>Correo</ion-label>\n\n            <ion-input type="email" [(ngModel)]="credentials.email"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>Contraseña</ion-label>\n\n            <ion-input type="password" [(ngModel)]="credentials.password"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="fond">\n\n            <ion-label>Tipo de sucursal</ion-label>\n\n            <ion-select [(ngModel)]="credentials.tipo" class="fond">\n\n                <ion-option value="bar">Bar</ion-option>\n\n                <ion-option value="antro">Antro</ion-option>\n\n                <ion-option value="restaurante">Restaurante</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-item class="fond">\n\n         <ion-label>Ciudad</ion-label>\n\n           <ion-select [(ngModel)]="credentials.ciudad" class="fond">\n\n              <div>\n\n                <ion-option *ngFor="let ciud of ciudades" [value]="ciud.ciudad">{{ ciud.ciudad }}</ion-option>\n\n              </div>\n\n         </ion-select>\n\n        </ion-item>\n\n        <ion-item text-center>\n\n            <button color="dark" ion-button round style="min-width: 200px; padding: 10px 16px" (click)="registrar()">Registrar</button>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-subir\admin-sucursal-subir.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], AdminSucursalSubirPage);
    return AdminSucursalSubirPage;
}());

//# sourceMappingURL=admin-sucursal-subir.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminSucursalEditperfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { MapsAPILoader, MouseEvent } from '@agm/core';
var AdminSucursalEditperfilPage = /** @class */ (function () {
    function AdminSucursalEditperfilPage(navCtrl, navParams, sucProv, toastCtrl, Fiauth, 
        // private mapsAPILoader: MapsAPILoader,
        // private ngZone: NgZone
        zone, platform, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sucProv = sucProv;
        this.toastCtrl = toastCtrl;
        this.Fiauth = Fiauth;
        this.zone = zone;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.sucursalItem = {};
        this.data = {};
    }
    AdminSucursalEditperfilPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminSucursalEditperfilPage');
        console.log('SUCURSALITEM', this.sucProv.selectedSucursalItem);
        this.sucursalItem = this.navParams.get('sucursalItem');
        // this.servicioID = this.navParams.get("servicioID");    
    };
    AdminSucursalEditperfilPage.prototype.savePerfil = function (sucursalItem) {
        console.log(sucursalItem);
        this.data = {
            contacto: this.sucProv.selectedSucursalItem.contacto,
            direccion: this.sucProv.selectedSucursalItem.direccion,
            displayName: this.sucProv.selectedSucursalItem.displayName,
            email: this.sucProv.selectedSucursalItem.email,
            tel: this.sucProv.selectedSucursalItem.tel,
            tipo: this.sucProv.selectedSucursalItem.tipo,
            uid: this.sucProv.selectedSucursalItem.uid,
            horas: this.sucProv.selectedSucursalItem.horas,
            codigoEtiqueta: this.sucProv.selectedSucursalItem.codigoEtiqueta,
            descripcion: this.sucProv.selectedSucursalItem.descripcion,
            estacionamiento: this.sucProv.selectedSucursalItem.estacionamiento,
        };
        console.log(this.data);
        this.sucProv.updateProfile(this.data);
        this.navCtrl.pop();
    };
    AdminSucursalEditperfilPage.prototype.changePass = function (email) {
        var auth = this.Fiauth.auth;
        email;
        // Email sent.
        if (confirm('¿Estás seguro de que quieres restablecer la contraseña?')) {
            auth.sendPasswordResetEmail(email);
            this.mostrar_toast('Se ha enviado un correo para el cambio/restablecimiento de contraseña a ' + email);
            // console.log('Correo enviado a '+ email);
        }
    };
    AdminSucursalEditperfilPage.prototype.mostrar_toast = function (mensaje) {
        var toast = this.toastCtrl.create({
            message: mensaje,
            duration: 3000
        }).present();
    };
    AdminSucursalEditperfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-sucursal-editperfil',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-editperfil\admin-sucursal-editperfil.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Editar Perfil</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="fondo_admin" padding>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-label floating>Nombre del contacto</ion-label>\n\n            <ion-input type="text" [(ngModel)]="sucProv.selectedSucursalItem.contacto"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label floating>Nombre sucursal</ion-label>\n\n            <ion-input type="text" [(ngModel)]="sucProv.selectedSucursalItem.displayName"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label floating>Teléfono</ion-label>\n\n            <ion-input type="phone" [(ngModel)]="sucProv.selectedSucursalItem.tel"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>Dirección</ion-label>\n\n            <ion-input type="text" [(ngModel)]="sucProv.selectedSucursalItem.direccion"></ion-input>\n\n        </ion-item>\n\n\n\n        <!-- <ion-item>\n\n            <ion-label floating>Email</ion-label>\n\n            <ion-input type="email" [(ngModel)]="credentials.email"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>Contraseña</ion-label>\n\n            <ion-input type="password" [(ngModel)]="credentials.password"></ion-input>\n\n        </ion-item> -->\n\n\n\n        <ion-item class="fond">\n\n            <ion-label>Tipo de sucursal</ion-label>\n\n            <ion-select [(ngModel)]="sucProv.selectedSucursalItem.tipo" class="fond">\n\n                <ion-option value="bar">Bar</ion-option>\n\n                <ion-option value="antro">Antro</ion-option>\n\n                <ion-option value="restaurante">Restaurante</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-row style="background-color: #000000; color:#fff;" text-center>\n\n            <ion-label>Información para el usuario</ion-label>\n\n        </ion-row>\n\n        <ion-item>\n\n            <ion-textarea class="fond" placeholder="Descriipción del horario de su sucursal." [(ngModel)]="sucProv.selectedSucursalItem.horas" name="horas" autocomplete="on" autocorrect="on"></ion-textarea>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-textarea class="fond" placeholder="Descripción de servicio de estacionamiento" [(ngModel)]="sucProv.selectedSucursalItem.estacionamiento" name="estacionamiento" autocomplete="on" autocorrect="on"></ion-textarea>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-textarea class="fond" placeholder="Descripción de código de etiqueta" [(ngModel)]="sucProv.selectedSucursalItem.codigoEtiqueta" name="etiqueta" autocomplete="on" autocorrect="on"></ion-textarea>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-textarea class="fond" placeholder="Descripción de la sucursal" [(ngModel)]="sucProv.selectedSucursalItem.descripcion" name="descripcion" autocomplete="on" autocorrect="on"></ion-textarea>\n\n        </ion-item>\n\n\n\n    </ion-list>\n\n    <ion-row>\n\n        <ion-col>\n\n            <button class="md-trash" color="dark" ion-button block (click)="savePerfil()">Guardar edición</button>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col>\n\n            <button class="md-trash" color="dark" ion-button block (click)="changePass(sucProv.selectedSucursalItem.email)">Restablecer/Cambiar Contraseña</button>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-editperfil\admin-sucursal-editperfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], AdminSucursalEditperfilPage);
    return AdminSucursalEditperfilPage;
}());

//# sourceMappingURL=admin-sucursal-editperfil.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminSucursalEditperfilImagenPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminSucursalEditperfilImagenPage = /** @class */ (function () {
    function AdminSucursalEditperfilImagenPage(navCtrl, navParams, imagePicker, sucProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imagePicker = imagePicker;
        this.sucProv = sucProv;
        this.imagenPreview = "";
        this.imagen64 = "";
        this.sucursal = { uid: null };
        this.sucursal.uid = navParams.get('uid');
        console.log(this.sucursal.uid);
    }
    AdminSucursalEditperfilImagenPage.prototype.seleccionar_foto = function () {
        var _this = this;
        var opciones = {
            quality: 70,
            outputType: 1,
            maximumImagesCount: 1
        };
        this.imagePicker.getPictures(opciones).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                //console.log('Image URI: ' + results[i]);
                _this.imagenPreview = 'data:image/jpeg;base64,' + results[i];
                _this.imagen64 = results[i];
            }
        }, function (err) {
            console.log("Error en selector", JSON.stringify(err));
        });
    };
    AdminSucursalEditperfilImagenPage.prototype.editarImagen = function (uid) {
        var _this = this;
        var archivo = {
            photoURL: this.imagen64,
            uid: uid,
        };
        this.sucProv.cargar_imagen_firebase(archivo)
            .then(function () { return _this.navCtrl.pop(); });
    };
    AdminSucursalEditperfilImagenPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminSucursalEditperfilImagenPage');
    };
    AdminSucursalEditperfilImagenPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-sucursal-editperfil-imagen',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-editperfil-imagen\admin-sucursal-editperfil-imagen.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Editar Imagen</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <ion-list>\n\n        <ion-item *ngIf="imagenPreview">\n\n            <img [src]="imagenPreview">\n\n        </ion-item>\n\n    </ion-list>\n\n    <ion-grid>\n\n        <ion-row>\n\n            <ion-col>\n\n                <button color="dark" ion-button block icon-left (click)="seleccionar_foto()">\n\n        <ion-icon name="photos"></ion-icon>\n\n          Seleccionar\n\n    </button>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n        <ion-buttons end>\n\n            <button color="dark" ion-button [disabled]="imagenPreview.length <= 1" (click)="editarImagen(sucursal.uid)">\n\n          Editar imagen\n\n        </button>\n\n        </ion-buttons>\n\n    </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-editperfil-imagen\admin-sucursal-editperfil-imagen.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__["a" /* ImagePicker */],
            __WEBPACK_IMPORTED_MODULE_3__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */]])
    ], AdminSucursalEditperfilImagenPage);
    return AdminSucursalEditperfilImagenPage;
}());

//# sourceMappingURL=admin-sucursal-editperfil-imagen.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminSucursalCroquisPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { AdminSucursalCroquisPage } from '../admin-sucursal-croquis/admin-sucursal-croquis';
var AdminSucursalCroquisPage = /** @class */ (function () {
    function AdminSucursalCroquisPage(navCtrl, navParams, SucProv, authProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.SucProv = SucProv;
        this.authProvider = authProvider;
        this.sucursal = { uid: null, contacto: null, direccion: null, displayName: null, email: null, photoURL: null, status: null, tel: null, tipo: null };
        this.sucursal.uid = navParams.get('idSucursal');
        console.log("Este es el id: ", this.sucursal.uid);
        SucProv.getSucursal(this.sucursal.uid).subscribe(function (sucursal) {
            _this.sucursal = sucursal;
        });
    }
    AdminSucursalCroquisPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminSucursalCroquisPage');
    };
    AdminSucursalCroquisPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-sucursal-croquis',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-croquis\admin-sucursal-croquis.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-title>Arquitectura de la Sucursal</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="fondo_admin">\n\n    <ion-row>\n\n        <ion-col text-center>\n\n            <div class="">\n\n                <img class="imagen_croquis" [src]="sucursal.croquis">\n\n                <ion-badge (click)="goEditPerfilImagen(sucursal.uid)" class="edit_pho">\n\n                    <ion-icon name="md-brush" ios="ios-outline" md="md-brush"></ion-icon>\n\n                </ion-badge>\n\n            </div>\n\n        </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-list>\n\n        <ion-item-sliding>\n\n            <ion-item>\n\n                Hola\n\n            </ion-item>\n\n            <!-- <ion-item-options side="right">\n\n\n\n            </ion-item-options> -->\n\n            <ion-item-options side="right">\n\n                <button ion-button color="primary">\n\n                  <ion-icon name="mail"></ion-icon>\n\n                  Email\n\n                </button>\n\n                <button ion-button color="dark">\n\n                        <ion-icon name="text"></ion-icon>\n\n                        Text\n\n                      </button>\n\n                <button ion-button color="secondary">\n\n                        <ion-icon name="call"></ion-icon>\n\n                        Call\n\n                </button>\n\n            </ion-item-options>\n\n        </ion-item-sliding>\n\n    </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-croquis\admin-sucursal-croquis.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */]])
    ], AdminSucursalCroquisPage);
    return AdminSucursalCroquisPage;
}());

//# sourceMappingURL=admin-sucursal-croquis.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImagencroquisPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_carga_croquis_carga_croquis__ = __webpack_require__(462);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ImagencroquisPage = /** @class */ (function () {
    function ImagencroquisPage(navCtrl, navParams, imagePicker, cap) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imagePicker = imagePicker;
        this.cap = cap;
        this.imagenPreview = "";
        this.idSucursal = navParams.get('idSucursal');
    }
    ImagencroquisPage.prototype.seleccionaFoto = function () {
        var _this = this;
        var opciones = {
            quality: 70,
            outputType: 1,
            maximumImagesCount: 1
        };
        this.imagePicker.getPictures(opciones).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                //console.log('Image URI: ' + results[i]);
                _this.imagenPreview = 'data:image/jpeg;base64,' + results[i];
                _this.imagen64 = results[i];
            }
        }, function (err) {
            console.log("Error en el selector", JSON.stringify(err));
        });
    };
    ImagencroquisPage.prototype.subirFoto = function () {
        var archivo = {
            plano: this.imagen64,
            key: this.idSucursal
        };
        this.cap.cargarImagen(archivo);
        // this.cerrarModal();
    };
    ImagencroquisPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-imagencroquis',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\imagencroquis\imagencroquis.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <!-- <ion-title>imagencroquis</ion-title> -->\n\n        <ion-buttons>\n\n            <button ion-button (click)="cerrarModal()">\n\n              Cerrar\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="fondo_admin" padding>\n\n    <ion-item text-center *ngIf="imagenPreview">\n\n        <img [src]="imagenPreview">\n\n        <!-- <img src="{{usuario.photo}}?type=large&width=720&height=720" style="width:60%;border-radius:50%;"> -->\n\n    </ion-item>\n\n\n\n    <ion-grid>\n\n        <ion-row>\n\n            <ion-col>\n\n                <button ion-button block icon-left color="dark" (click)="seleccionaFoto()">\n\n              <ion-icon name="photos"></ion-icon>\n\n              &nbsp;&nbsp;Seleccionar\n\n            </button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n    <ion-buttons end>\n\n        <button color="dark" ion-button [disabled]="imagenPreview.length <= 1" (click)="subirFoto()">\n\n              Actualizar\n\n            </button>\n\n    </ion-buttons>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\imagencroquis\imagencroquis.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__["a" /* ImagePicker */],
            __WEBPACK_IMPORTED_MODULE_3__providers_carga_croquis_carga_croquis__["a" /* CargaCroquisProvider */]])
    ], ImagencroquisPage);
    return ImagencroquisPage;
}());

//# sourceMappingURL=imagencroquis.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CorteVentaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_reservacion_reservacion__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CorteVentaPage = /** @class */ (function () {
    function CorteVentaPage(navCtrl, navParams, sucProv, 
        //public toastCtrl: ToastController,
        //public Fiauth: AngularFireAuth,
        // private mapsAPILoader: MapsAPILoader,
        // private ngZone: NgZone
        //public zone: NgZone,
        //public platform: Platform,
        alertCtrl, _reservaciones, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sucProv = sucProv;
        this.alertCtrl = alertCtrl;
        this._reservaciones = _reservaciones;
        this.formBuilder = formBuilder;
        this.sucursalItem = {};
        this.data = {};
        this.productos = [];
        this.ocultar1 = false;
        this.myForm = this.createMyForm();
    }
    CorteVentaPage.prototype.accion1 = function () {
        this.ocultar1 = !this.ocultar1;
    };
    CorteVentaPage.prototype.createMyForm = function () {
        return this.formBuilder.group({
            FechaInicio: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            FechaFin: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]
        });
    };
    CorteVentaPage.prototype.saveData = function () {
        console.log(this.myForm.value);
        console.log('Fecha Inicio', this.myForm.value.FechaInicio);
        console.log('Fecha Fin', this.myForm.value.FechaFin);
        this.fechaI = this.myForm.value.FechaInicio;
        this.fechaF = this.myForm.value.FechaFin;
        var fech = __WEBPACK_IMPORTED_MODULE_5_moment__(this.fechaI).format("x");
        console.log(fech);
        this.getReservaciones(this.sucProv.selectedSucursalItem.uid, this.fechaI, this.fechaF);
        this.contador = 0;
    };
    CorteVentaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CorteVentaPage');
        console.log('SUCURSALITEMM', this.sucProv.selectedSucursalItem);
        //this.sucursalItem = this.navParams.get('sucursalItem');    
        // this.servicioID = this.navParams.get("servicioID");    
        console.log('idSucursal1', this.sucProv.selectedSucursalItem.uid);
        console.log('nombreSucursal', this.sucProv.selectedSucursalItem.displayName);
        this.nombreSucursal = this.sucProv.selectedSucursalItem.displayName;
        this.idSucursal = this.sucProv.selectedSucursalItem.uid;
        this.getIdLast(this.idSucursal);
        this.contador = 0;
        this.sumatoria = 0;
        //this.getReservaciones(this.sucProv.selectedSucursalItem.uid);
    };
    CorteVentaPage.prototype.getReservaciones = function (idx, fechaI, fechaF) {
        var _this = this;
        console.log('llamar provider', idx);
        this._reservaciones.getReservaciones(idx, fechaI, fechaF).subscribe(function (res) {
            console.log("Este es el resultado de reservaciones: ", res);
            var suma = 0;
            var propina = 0;
            res.forEach(function (value) {
                console.log('Propina', value.propina);
                if (value.totalReservacion != null) {
                    suma = suma + parseFloat(value.totalReservacion);
                }
                if (value.propina != null) {
                    propina = propina + parseFloat(value.totalReservacion) * parseFloat(value.propina);
                }
                console.log('suma', suma);
                console.log('propinaTotal: ', propina);
            });
            _this.fechaI = fechaI;
            _this.fechaF = fechaF;
            _this.sumatoria = suma;
            _this.comision1 = suma * 0.056;
            _this.comision2 = suma - _this.comision1;
            _this.propinaTotal = propina;
            _this.reservaciones = res;
            console.log('comision1', _this.comision1);
            console.log('comision2', _this.comision2);
        });
    };
    CorteVentaPage.prototype.getIdLast = function (idx) {
        var _this = this;
        var x = 0;
        this._reservaciones.getIdLast(idx).subscribe(function (res) {
            console.log("Este es el resultado de corte: ", res);
            res.forEach(function (value) {
                console.log('ultimo folio', value.folio);
                x = value.folio + 1;
            });
            _this.nuevoFolio = x;
            console.log('nuevo folio: ', _this.nuevoFolio);
        });
    };
    CorteVentaPage.prototype.guardarCorte = function () {
        console.log('***** F_Guardar_Corte');
        // FechaI, FechaF, comisionGuest(5.6%), comisionSucursal(94.4%), sumaReservaioners, Sucursal, propina 
        this._reservaciones.addCorte(this.fechaI, this.fechaF, this.comision1, this.comision2, this.sumatoria, this.idSucursal, this.propinaTotal, this.nuevoFolio);
        this.presentAlert();
    };
    CorteVentaPage.prototype.suma = function (x) {
        //this.contador=this.contador+1;
        //console.log(this.contador);
        console.log('idReservacion funcion', x);
    };
    CorteVentaPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Corte guardado',
            subTitle: '',
            buttons: ['Aceptar']
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], CorteVentaPage.prototype, "content", void 0);
    CorteVentaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-corte-venta',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\corte-venta\corte-venta.html"*/'\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>corte-venta</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <form [formGroup]="myForm" (ngSubmit)="saveData()">\n\n        <ion-list>\n\n            <ion-item>\n\n                <ion-label>Fecha Inicio</ion-label>\n\n                <ion-datetime formControlName="FechaInicio" displayFormat="DD/MM/YYYY" min="2018" max="2050-10-31" doneText=Aceptar cancelText=Cancelar> </ion-datetime>\n\n            </ion-item>\n\n            <ion-item>\n\n                <ion-label>Fecha Fin</ion-label>\n\n                <ion-datetime formControlName="FechaFin" displayFormat="DD/MM/YYYY" min="2018" max="2050-10-31" doneText=Aceptar cancelText=Cancelar></ion-datetime>\n\n            </ion-item>            \n\n        </ion-list>\n\n\n\n        <div padding>\n\n            <button ion-button  color="naranja" block type="submit" [disabled]="!myForm.valid" >Generar Corte</button>\n\n          </div>\n\n    </form>\n\n\n\n   \n\n   <ion-card class="card_atras" *ngIf="reservaciones != null ">\n\n    <ion-card-content class="card_atras"  >\n\n        <ion-card-title> \n\n            <h5 class="blanco_1"> Resumen Corte {{ nombreSucursal }}</h5>\n\n                                     \n\n        </ion-card-title>\n\n\n\n        \n\n        <ion-item class="card_atras" >\n\n            <a class="blanco_2"> {{ nombreSucursal }}</a>\n\n            <h4 class="blanco_1"  ></h4>\n\n            <ion-badge class="negro" item-end> {{ comision2 | currency}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item class="card_atras" >\n\n                <a class="blanco_2"> Guest (5.6%)</a>\n\n                <h4 class="blanco_1"  ></h4>\n\n                <ion-badge class="negro" item-end> {{ comision1 | currency}}</ion-badge>\n\n        </ion-item>\n\n\n\n        \n\n        <ion-item class="card_atras" >\n\n            <a class="blanco_2"> Propina</a>\n\n            <h4 class="blanco_1"  ></h4>\n\n            <ion-badge class="negro" item-end> {{ propinaTotal | currency }}</ion-badge>\n\n        </ion-item>\n\n\n\n        <ion-item class="card_atras" >\n\n                <a class="blanco_2">Total</a>\n\n                <h4 class="blanco_1"  ></h4>\n\n                <ion-badge class="negro" item-end> {{ sumatoria | currency }} </ion-badge>\n\n            </ion-item>\n\n\n\n            <ion-item class="card_atras" >\n\n                <a class="blanco_2">Total + propina</a>\n\n                <h4 class="blanco_1"  ></h4>\n\n                <ion-badge class="negro" item-end> {{ sumatoria + propinaTotal | currency }} </ion-badge>\n\n            </ion-item>\n\n\n\n        <ion-item class="card_atras" >\n\n            <a  (click)="accion1()" > Ver detalle </a>\n\n        </ion-item>\n\n\n\n        <button ion-button color="naranja" block (click)="guardarCorte()">Guardar</button>\n\n    </ion-card-content>\n\n   </ion-card>\n\n   \n\n    <ion-card class="card_atras" *ngFor="let reserva of reservaciones" >\n\n\n\n        <ion-card-content class="card_atras"  *ngIf="ocultar1">\n\n            <ion-card-title> \n\n                <h4 class="blanco_2" > Folio: {{ reserva.folio }} </h4>\n\n            </ion-card-title>\n\n\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/calendario.png" item-start>\n\n                <p class="blanco_2">Fecha de reservación</p>\n\n                <ion-badge class="negro" item-end>{{ reserva.fechaR }} </ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/time.png" item-start>\n\n                <a class="blanco_2">Estatus</a>\n\n                <ion-badge class="negro" item-end> {{ reserva.estatus }}  </ion-badge>\n\n            </ion-item>\n\n\n\n            <ion-item class="card_atras" >\n\n                    <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                    <a class="blanco_2">Total</a>\n\n                    <h4 class="blanco_1"  ></h4>\n\n                    <ion-badge class="negro" item-end> {{ reserva.totalReservacion | currency}} </ion-badge>\n\n            </ion-item>\n\n\n\n            <ion-item class="card_atras" *ngIf="reserva.propina != null ">\n\n                <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                <a class="blanco_2">Propina {{ reserva.propina * 100}} % </a>\n\n                <ion-badge class="negro" item-end> {{ reserva.propina * reserva.totalReservacion | currency }} </ion-badge>\n\n            </ion-item>\n\n\n\n            <ion-item class="card_atras" *ngIf="reserva.propina != null " >\n\n                <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                <a class="blanco_2">Total + propina</a>\n\n                <ion-badge  class="negro" item-end> {{ reserva.propina * reserva.totalReservacion +  reserva.totalReservacion | currency }} </ion-badge>\n\n            </ion-item>\n\n\n\n        </ion-card-content>\n\n        \n\n    </ion-card>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\corte-venta\corte-venta.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_reservacion_reservacion__["a" /* ReservacionProvider */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
    ], CorteVentaPage);
    return CorteVentaPage;
}());

//# sourceMappingURL=corte-venta.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdministrarReservacionesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_gestion_reservaciones_gestion_reservaciones__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_reservacion_reservacion__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AdministrarReservacionesPage = /** @class */ (function () {
    function AdministrarReservacionesPage(navCtrl, navParams, modalCtrl, _providerReserva, afs, _gestionReser) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this._providerReserva = _providerReserva;
        this.afs = afs;
        this._gestionReser = _gestionReser;
        this.fecha = new Date();
        this.res = false;
        this.area = {};
        this.zona = {};
        this.reserv = {};
        this.estatus = true;
        this.estatus2 = true;
    }
    AdministrarReservacionesPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.uidSucursal = this.navParams.get("uidSucursal");
        this.getReservaciones(this.uidSucursal);
        this.getUsuarios();
        this.getMesas2(this.uidSucursal);
        this.getImgSucursal(this.uidSucursal);
        //Cuando es un usuario se saca el id de la sucursal ala que pertenece
        this.afs.collection('users', function (ref) { return ref.where('uid', '==', _this.uidSucursal); }).valueChanges().subscribe(function (data) {
            _this.sucursales2 = data;
            _this.sucursales2.forEach(function (element) {
                _this.uidSucursal2 = element.uidSucursal;
                _this.getReservaciones(_this.uidSucursal2);
                _this.getMesas2(_this.uidSucursal2);
                _this.getImgSucursal(_this.uidSucursal2);
            });
        });
    };
    AdministrarReservacionesPage.prototype.getCompartidas = function (idR) {
        var _this = this;
        console.log('compartidas ts: ' + idR);
        //let idR= "EOAolnpwc9GRuQt8zLpf";
        this._gestionReser.getCompartidas(idR).subscribe(function (reserv) {
            _this.compartidas = reserv;
            console.log("Compartidas de reeseva: ", _this.compartidas);
        });
        this.getUsuarios();
    };
    AdministrarReservacionesPage.prototype.getReservaciones = function (idSucursal) {
        var _this = this;
        var date = __WEBPACK_IMPORTED_MODULE_5_moment___default()().format("YYYY-MM-DD");
        var fecha = __WEBPACK_IMPORTED_MODULE_5_moment___default()(date).format('x');
        console.log("Fecha numeros: ", fecha);
        console.log("Fecha: ", date);
        this._gestionReser.getReservaciones(idSucursal, fecha).subscribe(function (reserv) {
            _this.reservaciones = reserv;
            console.log("Estas son las reservacionesXs: ", _this.reservaciones);
        });
    };
    AdministrarReservacionesPage.prototype.getUsuarios = function () {
        var _this = this;
        this._gestionReser.getUsuarios().subscribe(function (users) {
            _this.usuarios = users;
            console.log("Estos son los usuarios: ", _this.usuarios);
        });
    };
    AdministrarReservacionesPage.prototype.Agendar = function () {
        alert('Si entra');
    };
    AdministrarReservacionesPage.prototype.modStatus_cancelacion = function (idReserv, idSucursal) {
        console.log("moStatus_cancelacion", idReserv);
        console.log(idSucursal);
        var modal = this.modalCtrl.create("Modalstatus_cancelacionPage", {
            idReserv: idReserv,
            idSucursal: idSucursal,
        });
        modal.present();
    };
    AdministrarReservacionesPage.prototype.consultaReservacion = function (idReser) {
        var _this = this;
        this.estatus = false;
        this.estatus2 = true;
        this._gestionReser.getReservacion(idReser).subscribe(function (res) {
            _this.reserv = res;
            console.log("Esta es la reservación: ", _this.reserv);
            var idArea = _this.reserv.idArea;
            _this._gestionReser.getArea(idArea).subscribe(function (area) {
                _this.area = area;
                console.log("Estos son las areas: ", _this.area);
            });
            var idZona = _this.reserv.idZona;
            _this._gestionReser.getZona(idZona).subscribe(function (zona) {
                _this.zona = zona;
                console.log("Estos son las zonas: ", _this.zona);
            });
            _this.getMesas(idZona);
            _this.consultaHistorial(_this.reserv.idUsuario);
            _this.getProductos(idReser);
        });
        this.getCompartidas(idReser);
    };
    AdministrarReservacionesPage.prototype.verCroquis = function () {
        this.estatus2 = false;
        this.estatus = true;
        //console.log("administrar-reservacion.ts id->",idReservacion);
    };
    AdministrarReservacionesPage.prototype.consultaHistorial = function (idUsuario) {
        var _this = this;
        this._gestionReser.getHistorial(idUsuario).subscribe(function (history) {
            _this.historial = history;
            if (_this.historial.length != 0) {
                _this.res = true;
            }
            else {
                _this.res = false;
            }
            console.log("Estas son las reservaciones completas: ", _this.historial);
        });
        this.consultaSucursales();
    };
    AdministrarReservacionesPage.prototype.consultaSucursales = function () {
        var _this = this;
        this._gestionReser.getSucursales().subscribe(function (sucursales) {
            _this.sucursales = sucursales;
            console.log("Estas son las sucursales: ", _this.sucursales);
        });
    };
    AdministrarReservacionesPage.prototype.modZonaMesa = function (idReserv, idSucursal, idArea, idZona) {
        var modal = this.modalCtrl.create("ModiareazonaPage", {
            idReserv: idReserv,
            idSucursal: idSucursal,
            idArea: idArea,
            idZona: idZona
        });
        modal.present();
    };
    AdministrarReservacionesPage.prototype.modStatus = function (idReserv, idSucursal) {
        console.log("moStatus", idReserv);
        console.log(idSucursal);
        var modal = this.modalCtrl.create("ModalstatusPage", {
            idReserv: idReserv,
            idSucursal: idSucursal,
        });
        modal.present();
    };
    AdministrarReservacionesPage.prototype.asignarMesa = function (idReserv, idZona) {
        var modal = this.modalCtrl.create("ModalmesasPage", {
            idReserv: idReserv,
            idZona: idZona
        });
        modal.present();
    };
    AdministrarReservacionesPage.prototype.Aceptar = function (idReserv) {
        var _this = this;
        //console.log("Este es el id: ", idReserv);
        //this._gestionReser.aceptarReservacion(idReserv);
        //Obtener el estatus de la reservacion si es creada normal o CreadaCompartida
        this._gestionReser.getEstatusReser(idReserv).subscribe(function (reser) {
            _this.estatusReser = reser;
            _this.estatusReser.forEach(function (data) {
                if (data.estatus == 'Creando') {
                    console.log("Este es el id creada: ", idReserv);
                    _this._gestionReser.aceptarReservacion(idReserv);
                }
                if (data.estatus == 'CreadaCompartida') {
                    console.log("Este es el id compartida: ", idReserv);
                    console.log("ejecutocompartida: ", idReserv);
                    _this._gestionReser.aceptarReservacionCompartida(idReserv);
                }
            });
        });
    };
    AdministrarReservacionesPage.prototype.Cancelar = function (idReserv) {
        console.log("Este es el id: ", idReserv);
        this._gestionReser.cancelarReservacion(idReserv);
    };
    AdministrarReservacionesPage.prototype.getMesas = function (idZona) {
        var _this = this;
        this._gestionReser.getMesas(idZona).subscribe(function (mesas) {
            _this.mesas = mesas;
            console.log("mesas JAJA: ", _this.mesas);
        });
    };
    AdministrarReservacionesPage.prototype.getMesas2 = function (idSucursal) {
        var _this = this;
        console.log("getMesas2sucursal", idSucursal);
        this._providerReserva.getMesas2(idSucursal).subscribe(function (mesas2) {
            console.log("Mesas2", mesas2);
            _this.mesas2 = mesas2;
        });
    };
    AdministrarReservacionesPage.prototype.getProductos = function (idReserv) {
        var _this = this;
        var total = 0;
        console.log("getProductos id reserva", idReserv);
        this._providerReserva.getProductos(idReserv).subscribe(function (productos) {
            console.log("productos: ", productos);
            _this.productos = productos;
            productos.forEach(function (value) {
                console.log('foreachIonci' + value.total);
                total = total + value.total;
            });
            console.log('totalreserva: ' + total);
            _this.totalReserv = total;
        });
    };
    AdministrarReservacionesPage.prototype.getImgSucursal = function (uid) {
        var _this = this;
        this._providerReserva.getImagenSucursal(uid).subscribe(function (res) {
            _this.imagenList = res;
            console.log("imagenes123", _this.imagenList);
            res.forEach(function (value) {
                //console.log("imagenId",value.myId);
                console.log("forech urlImg", value.imagenes);
                //this.imgSucursal=value.imagenes;
                //console.log("Url", this.imgSucursal);
            });
        });
    };
    AdministrarReservacionesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-administrar-reservaciones',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\administrar-reservaciones\administrar-reservaciones.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Gestion de reservaciones</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="fondo_admin" padding>\n\n    <ion-card class="card">\n\n        <ion-card-header class="header">\n\n            <ion-item-divider color="light">Reservaciones</ion-item-divider>\n\n        </ion-card-header>\n\n        <div *ngFor="let reservacion of reservaciones">\n\n            <div *ngFor="let user of usuarios">\n\n                <div *ngIf="reservacion.idUsuario==user.uid && reservacion.estatus == \'Creando\'" class="espacio">\n\n                    <ion-row >\n\n                        <ion-col col-2 text-center>\n\n                            <ion-icon name="ios-book" style="background-color: gold;"></ion-icon>\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            {{user.displayName}} <br>{{reservacion.hora}}\n\n                        </ion-col>\n\n                        <ion-col col-2 text-center>\n\n                            <!-- {{reservacion.fechaR}} -->\n\n                            {{ reservacion.fechaR | date : \'dd/MM/yyyy\'}}\n\n                        </ion-col>\n\n                        <ion-col col-1 text-center>\n\n                            {{reservacion.numPersonas}}\n\n                        </ion-col>\n\n                        <ion-col col-1 text-center (click)="consultaReservacion(reservacion.idReservacion)">\n\n                            <ion-icon name="arrow-round-forward"></ion-icon>\n\n                        </ion-col>\n\n                        <!-- <ion-col col-2 text-center (click)="adminReservacion(reservacion.idReservacion)"> -->\n\n                        <!-- <ion-col col-2 text-center (click)="verCroquis(reservacion.idReservacion, reservacion.idSucursal)">\n\n                                <button class="btn">Croquis</button>\n\n                        </ion-col> -->\n\n                    </ion-row>\n\n                </div>\n\n                <div *ngIf="reservacion.idUsuario==user.uid && reservacion.estatus == \'CreadaCompartida\'" class="espacio">\n\n                    <ion-row >\n\n                        <ion-col col-2 text-center>\n\n                            <ion-icon name="ios-book" style="background-color: gold;"></ion-icon>\n\n                        </ion-col>\n\n                        <ion-col>\n\n                            {{user.displayName}} <br>{{reservacion.hora}}\n\n                        </ion-col>\n\n                        <ion-col col-2 text-center>\n\n                            <!-- {{reservacion.fechaR}} -->\n\n                            {{ reservacion.fechaR | date : \'dd/MM/yyyy\'}}\n\n                        </ion-col>\n\n                        <ion-col col-1 text-center>\n\n                            {{reservacion.numPersonas}}\n\n                        </ion-col>\n\n                        <ion-col col-1 text-center (click)="consultaReservacion(reservacion.idReservacion)">\n\n                            <ion-icon name="arrow-round-forward"></ion-icon>\n\n                        </ion-col>\n\n                        <!-- <ion-col col-2 text-center (click)="adminReservacion(reservacion.idReservacion)"> -->\n\n                        <!-- <ion-col col-2 text-center (click)="verCroquis(reservacion.idReservacion, reservacion.idSucursal)">\n\n                                <button class="btn">Croquis</button>\n\n                        </ion-col> -->\n\n                    </ion-row>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </ion-card>\n\n    <div class="panelReserv" [hidden]="estatus2==true">\n\n            <!-- <div *ngFor="let usuario of usuarios">\n\n                <div >\n\n                    <img id="imgS" src="https://firebasestorage.googleapis.com/v0/b/guestreservation-8b24b.appspot.com/o/croquis%2FJDJgepeCbM4Kp7XB8i2A?alt=media&token=21db3258-e871-4fb7-ab88-6e07fd2cd7fa" alt="your image" />\n\n                </div>\n\n            </div> -->\n\n\n\n            <div *ngFor="let img of imagenList; ">\n\n                <img id="imgS" src="{{img.imagenes}}" alt="your image" />\n\n                <!-- {{img.imagenes}} -->\n\n            </div>\n\n\n\n    </div>\n\n\n\n    <div class="panelReserv" [hidden]="estatus==true">\n\n        <div *ngFor="let usuario of usuarios">\n\n            <div *ngIf="usuario.uid == reserv.idUsuario">\n\n                <ion-row class="title">\n\n                    {{usuario.displayName}}\n\n                </ion-row>\n\n                <!-- <ion-row>\n\n                    <ion-col col-2 text-center (click)="verCroquis()">\n\n                        <button class="btn">Croquis</button>\n\n                    </ion-col>\n\n\n\n                </ion-row> -->\n\n                <ion-row class="row1">\n\n                    <ion-col col-3>\n\n                        <ion-icon ios="ios-clock" md="md-clock" class="icono"></ion-icon>&emsp;{{reserv.hora}}\n\n                    </ion-col>\n\n                    <ion-col col-3>\n\n                        <ion-icon ios="ios-people" md="md-people" class="icono"></ion-icon>&emsp;{{reserv.numPersonas}}\n\n                    </ion-col>\n\n                    <ion-col text-right class="pad">\n\n                        <ion-icon ios="ios-call" md="md-call" class="icono"></ion-icon>&emsp;{{usuario.phoneNumber}}\n\n                    </ion-col>\n\n                </ion-row>\n\n                <br>\n\n                <br>\n\n\n\n                <!-- <ion-row class="row1">\n\n                    <ion-col>\n\n                        <h4>Mesa:</h4>\n\n                            <ion-icon ios="ios-menu" md="md-menu" class="icono"></ion-icon>&emsp; 1\n\n                            {{reserv.numMesa}}\n\n                    </ion-col>\n\n                </ion-row> -->\n\n                <br><br>\n\n\n\n                <ion-row class="row1">\n\n                    <!--<ion-col text-center (click)="modZonaMesa(reserv.uid, reserv.idSucursal, reserv.idArea, reserv.idZona)">\n\n                        <h3>Area: </h3>\n\n                        <ion-icon ios="ios-menu" md="md-menu" class="icono"></ion-icon>&emsp;{{area.nombre}}\n\n                    </ion-col>-->\n\n                    <ion-col text-center>\n\n                        <h3>Zona: </h3>\n\n                        <ion-icon ios="ios-apps" md="md-apps" class="icono"></ion-icon>&emsp;{{zona.nombre}}\n\n                    </ion-col>\n\n\n\n                    <ion-col text-center>\n\n                        <h3>Total: </h3>\n\n                        <ion-icon ios="ios-apps" md="md-apps" class="icono"></ion-icon>&emsp; {{ totalReserv }}\n\n                    </ion-col>\n\n                    \n\n                </ion-row>\n\n\n\n                <br>\n\n                <br>\n\n                <ion-row class="row1">\n\n                    <ion-col (click)="modStatus(reserv.idReservacion, reserv.idSucursal)">\n\n                        <h4>Estado de la reservación: </h4>\n\n                        <ion-icon ios="ios-book" md="md-book" class="icono"></ion-icon>&emsp;{{reserv.estatus}}\n\n                    </ion-col>\n\n\n\n                    <ion-col >                    \n\n                        <img class="icon" height="40px" width="40px" src="../assets/imgs/icons/mesa.png" item-start>\n\n                            {{ reserv.numMesa }}\n\n                    </ion-col>\n\n                </ion-row>\n\n                <br><br>\n\n\n\n                <ion-row class="row1">\n\n                    <ion-col text-center (click)="modStatus_cancelacion(reserv.idReservacion, reserv.idSucursal)">\n\n                    <!-- <ion-col text-center (click)="Cancelar(reserv.uid)"> -->\n\n                        <!-- <ion-icon ios="ios-close-circle" md="md-close-circle" class="icono"></ion-icon>\n\n                        Cancelar Reservación -->\n\n                        <button ion-button color="danger" outline>Cancelar Reservación</button>\n\n                    </ion-col>\n\n                    <ion-col text-center (click)="Aceptar(reserv.uid)">\n\n                        <!-- <ion-icon ios="ios-checkmark-circle" md="md-checkmark-circle" class="icono"></ion-icon>\n\n                        Aceptar Reservación -->\n\n                        <button ion-button color="secondary" outline>Aceptar Reservación</button>\n\n                    </ion-col>\n\n                </ion-row>\n\n                <br><br>\n\n\n\n                <ion-row class="title2">\n\n                    <ion-col text-center>\n\n                        <ion-item-divider color="light">Compartida con</ion-item-divider>\n\n                    </ion-col>\n\n                </ion-row>\n\n\n\n                <div *ngFor="let compartida of compartidas" >\n\n                    <div *ngFor="let user of usuarios">\n\n                            <div *ngIf="compartida.idUsuario==user.uid" class="espacio">\n\n                            <ion-row >\n\n                                <ion-col col-2 text-center>\n\n                                    <ion-icon name="ios-person" style="background-color: gold;"></ion-icon>\n\n                                </ion-col>\n\n                                <ion-col col-3 text-center>\n\n                                    {{user.displayName}}\n\n                                </ion-col>\n\n                                <ion-col col-2 text-center >\n\n                                   $ {{compartida.totalDividido}}\n\n                                </ion-col>\n\n                            </ion-row>\n\n                        \n\n                            </div>\n\n                    </div>\n\n                </div>\n\n                <br><br>\n\n\n\n\n\n\n\n                \n\n                <ion-row class="title2">\n\n                    <ion-col text-center>\n\n                        <ion-item-divider color="light">Historial de Reservaciones</ion-item-divider>\n\n                    </ion-col>\n\n                </ion-row>\n\n                <div *ngIf="res==true">\n\n                    <ion-grid class="title3">\n\n                        <ion-row>\n\n                            <ion-col text-center>\n\n                                Establecimiento\n\n                            </ion-col>\n\n                            <ion-col text-center>\n\n                                Fecha\n\n                            </ion-col>\n\n                            <ion-col text-center>\n\n                                Hora\n\n                            </ion-col>\n\n                            <ion-col text-center>\n\n                                Puntuación\n\n                            </ion-col>\n\n                        </ion-row>\n\n                        <ion-row class="row1" *ngFor="let history of historial">\n\n                            <ion-col text-center>\n\n                                <div *ngFor="let sucursal of sucursales">\n\n                                    <div *ngIf="sucursal.$key==history.idSucursal">\n\n                                        {{sucursal.displayName}}\n\n                                    </div>\n\n                                </div>\n\n                            </ion-col>\n\n                            <ion-col text-center>\n\n                                {{history.fechaR | date : \'dd/MM/yyyy\'}}\n\n                            </ion-col>\n\n                            <ion-col text-center>\n\n                                {{history.hora}}\n\n                            </ion-col>\n\n                            <ion-col text-center>\n\n                                {{history.puntuacion}} 5\n\n                            </ion-col>\n\n                        </ion-row>\n\n                    </ion-grid>\n\n                </div>\n\n                <div *ngIf="res==false">\n\n                    <ion-row class="row1">\n\n                        <ion-col text-center class="col1">\n\n                            El cliente no tiene reservaciones previas\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\administrar-reservaciones\administrar-reservaciones.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_reservacion_reservacion__["a" /* ReservacionProvider */],
            __WEBPACK_IMPORTED_MODULE_4__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_2__providers_gestion_reservaciones_gestion_reservaciones__["a" /* GestionReservacionesProvider */]])
    ], AdministrarReservacionesPage);
    return AdministrarReservacionesPage;
}());

//# sourceMappingURL=administrar-reservaciones.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GestionReservacionesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase_app__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GestionReservacionesProvider = /** @class */ (function () {
    function GestionReservacionesProvider(aFS, alertCtrl) {
        this.aFS = aFS;
        this.alertCtrl = alertCtrl;
        this.db = __WEBPACK_IMPORTED_MODULE_2_firebase_app__["firestore"]();
        this.items = [];
    }
    GestionReservacionesProvider.prototype.getReservaciones = function (idx, fecha) {
        this.servicios = this.aFS.collection("reservaciones", function (ref) {
            //  ref.where("idSucursal", "==", idx).orderBy("fechaR_","asc"));
            return ref.where("idSucursal", "==", idx)
                .orderBy("fechaR_", "asc");
        }
        //.where("estatus", "array-contains", 'Creando')
        );
        this._servicios = this.servicios.valueChanges();
        return (this._servicios = this.servicios.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    GestionReservacionesProvider.prototype.getHistorial = function (idx) {
        this.servicios = this.aFS.collection("reservaciones", function (ref) {
            return ref.where("idUsuario", "==", idx).where("estatus", "==", "Completado").orderBy("fechaR_", "desc");
        });
        this._servicios = this.servicios.valueChanges();
        return (this._servicios = this.servicios.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    GestionReservacionesProvider.prototype.getCompartidas = function (idx) {
        //let idx ="UOhor7ujhflQTOzje5D3";
        this.servicios = this.aFS.collection("compartidas", function (ref) {
            return ref.where("idReservacion", "==", idx);
        });
        this._servicios = this.servicios.valueChanges();
        console.log('compartidas');
        return (this._servicios = this.servicios.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    GestionReservacionesProvider.prototype.getSucursales = function () {
        this.servicios = this.aFS.collection("sucursales");
        this._servicios = this.servicios.valueChanges();
        return (this._servicios = this.servicios.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    GestionReservacionesProvider.prototype.getUsuarios = function () {
        this.usuarios = this.aFS.collection("users", function (ref) {
            return ref.where("type", "==", "u");
        });
        this._usuarios = this.usuarios.valueChanges();
        return (this._usuarios = this.usuarios.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    GestionReservacionesProvider.prototype.getReservacion = function (idServicio) {
        this.reservacionDoc = this.aFS.doc("reservaciones/" + idServicio);
        return (this.reservacion = this.reservacionDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    GestionReservacionesProvider.prototype.getArea = function (idArea) {
        this.reservacionDoc = this.aFS.doc("areas/" + idArea);
        // this.pedidoDoc = this.afs.collection<Servicios>('servicios').doc(`/${idPedido}`).collection<Pedidos>('pedidos');
        return (this.reservacion = this.reservacionDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    GestionReservacionesProvider.prototype.getZona = function (idZona) {
        this.reservacionDoc = this.aFS.doc("zonas/" + idZona);
        // this.pedidoDoc = this.afs.collection<Servicios>('servicios').doc(`/${idPedido}`).collection<Pedidos>('pedidos');
        return (this.reservacion = this.reservacionDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    GestionReservacionesProvider.prototype.getMesas = function (idx) {
        this.mesasCollection = this.aFS.collection("mesas", function (ref) {
            return ref
                .where("uidZona", "==", idx).orderBy("mesa");
        });
        this.mesas = this.mesasCollection.valueChanges();
        return (this.mesas = this.mesasCollection
            .snapshotChanges()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.id = action.payload.doc.id;
                return data;
            });
        })));
    };
    //obtener estatus de la reservacion para saber si es creada normal o creada compartida
    GestionReservacionesProvider.prototype.getEstatusReser = function (idx) {
        this.estatusCollection = this.aFS.collection("reservaciones", function (ref) {
            return ref
                .where("idReservacion", "==", idx);
        });
        this.estatus = this.estatusCollection.valueChanges();
        return (this.estatus = this.estatusCollection
            .snapshotChanges()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.id = action.payload.doc.id;
                return data;
            });
        })));
    };
    GestionReservacionesProvider.prototype.getMesaReservas = function (idMesa, reserva) {
        // alert("Esta es la mesa: "+idMesa);
        // alert("Esta es la fecha enviada: "+ reserva.fecha);
        this.horasCollection = this.aFS.collection("reservaciones_mesas", function (ref) {
            return ref
                .where("idMesa", "==", idMesa)
                .where("fecha", "==", reserva.fecha)
                .orderBy("hora", "asc");
        });
        this._horas = this.horasCollection.valueChanges();
        return (this._horas = this.horasCollection
            .snapshotChanges()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.id = action.payload.doc.id;
                return data;
            });
        })));
    };
    // consultarMesas() {
    //   this.mesasCollection = this.aFS.collection("mesas");
    //   this.mesas = this.mesasCollection.valueChanges();
    //   return (this.mesas = this.mesasCollection
    //     .snapshotChanges()
    //     .pipe(
    //       map(changes => {
    //         return changes.map(action => {
    //           const data = action.payload.doc.data();
    //           data.id = action.payload.doc.id;
    //           return data;
    //         });
    //       })
    //     ));
    // }
    GestionReservacionesProvider.prototype.actualizaEstatus = function (idx, estatus) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("mesas")
                .doc(idx)
                .update({
                estatus: estatus
            })
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GestionReservacionesProvider.prototype.getOneReservación = function (idx) {
        this.Reservacion = this.aFS.doc("reservaciones/" + idx);
        return (this._Reservacion = this.Reservacion.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    GestionReservacionesProvider.prototype.mesaReservacion = function (reserva) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.aFS
                .collection("reservaciones_mesas")
                .add({
                idReservacion: reserva.idReservacion,
                hora: reserva.hora,
                fecha: reserva.fecha,
                idMesa: reserva.idMesa,
                no_mesa: reserva.noMesa
            })
                .then(function (reserva) {
                console.log("Inserción exitosa: ", reserva);
                resolve({ success: true, idReservacion: reserva.id });
                _this.alertCtrl
                    .create({
                    title: "Se reservo la mesa correctamente correctamente",
                    buttons: ["Aceptar"]
                })
                    .present();
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GestionReservacionesProvider.prototype.Cancelar = function () {
        var mesas = JSON.parse(localStorage.getItem('mesas'));
        if (mesas != null) {
            this.items = JSON.parse(localStorage.getItem('mesas'));
            var contador = 1;
            var _loop_1 = function (item) {
                console.log("Contador: ", contador);
                this_1.db
                    .collection("mesas")
                    .doc(item)
                    .update({
                    estatus: "libre"
                })
                    .then(function () {
                    console.log("Si lo hizo para: ", item);
                })
                    .catch(function (err) {
                    console.log("Error no lo hizo para: ", item);
                });
                contador++;
            };
            var this_1 = this;
            for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
                var item = _a[_i];
                _loop_1(item);
            }
        }
        localStorage.removeItem("mesas");
    };
    GestionReservacionesProvider.prototype.Aceptar = function (idx, mesas) {
        var _this = this;
        localStorage.removeItem("ids");
        localStorage.removeItem("mesas");
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("reservaciones")
                .doc(idx)
                .update({
                mesas: mesas
            })
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GestionReservacionesProvider.prototype.aceptarReservacion = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("reservaciones")
                .doc(idx)
                .update({
                estatus: "Aceptado"
            })
                .then(function () {
                resolve(true);
                _this.alertCtrl
                    .create({
                    title: "Has reservado con éxito",
                    buttons: ["Aceptar"]
                })
                    .present();
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GestionReservacionesProvider.prototype.aceptarReservacionCompartida = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("reservaciones")
                .doc(idx)
                .update({
                estatus: "AceptadoCompartida"
            })
                .then(function () {
                resolve(true);
                _this.alertCtrl
                    .create({
                    title: "Has reservado con éxito",
                    buttons: ["Aceptar"]
                })
                    .present();
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GestionReservacionesProvider.prototype.cancelarReservacion = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("reservaciones")
                .doc(idx)
                .update({
                estatus: "Cancelado"
            })
                .then(function () {
                resolve(true);
                _this.alertCtrl
                    .create({
                    title: "Has cancelado la reservación",
                    buttons: ["Aceptar"]
                })
                    .present();
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GestionReservacionesProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* AlertController */]])
    ], GestionReservacionesProvider);
    return GestionReservacionesProvider;
}());

//# sourceMappingURL=gestion-reservaciones.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminLeeQrPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_monitoreo_reservas_monitoreo_reservas__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminLeeQrPage = /** @class */ (function () {
    function AdminLeeQrPage(navCtrl, navParams, afs, servMon, user, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afs = afs;
        this.servMon = servMon;
        this.user = user;
        this.alertCtrl = alertCtrl;
        this.reservacion = {};
        this.cliente = {};
        this.area = {};
        this.zona = {};
        this.evento = {};
        this.infotarjeta = {};
        //this.idReservacion = this.navParams.get('idReservacion');
        //console.log('id', this.idReservacion);
        //alert('reservacionacep'+ this.idReservacion);
        //console.log('id', this.idReservacion);
        this.datosQrRecibidos = this.navParams.get('datosQr');
        var dataCode = JSON.parse(this.datosQrRecibidos);
        this.idReservacion2 = dataCode.idReservacion;
        this.idCompartir = dataCode.idCompartir;
        //alert(this.idCompartir);
        //Cambiar el estatus a pagado cuando ya se escanea y se verifico el pago
        //si la reservacion es normal cambiar el estatus principal a pagando.
        if (this.idCompartir == undefined) {
            this.afs.collection('reservaciones').doc(this.idReservacion2).update({
                estatus: 'Pagando'
            });
            //alert('Reservación pagada: Acceso permitido');
            var alerta = this.alertCtrl.create({
                //  message:
                //  "Reservación pagada: Acesso permitido",
                buttons: [
                    {
                        text: "Reservación pagada: Acesso permitido! Aceptar",
                        handler: function () {
                            console.log("Buy clicked");
                        }
                    }
                ]
            });
            alerta.present();
        }
        //Cambiar el estatus a pagado cuando ya se escanea y se verifico el pago
        //si la reservacion es compartida cambiar el estatus en tabla compartida de cada persona que comparte.
        if (this.idCompartir != undefined) {
            //Cambiar el estatus a pagado cuando ya se escanea y se verifico el pago
            this.afs.collection('compartidas').doc(this.idCompartir).update({
                estatus_escaneo: 'OK'
            });
            //alert('Reservación pagada: Acceso permitido');
            var alerta = this.alertCtrl.create({
                // message:
                //   "Reservación pagada: Acesso permitido",
                buttons: [
                    {
                        text: "Reservación pagada: Acesso permitido! Aceptar",
                        handler: function () {
                            console.log("Buy clicked");
                        }
                    }
                ]
            });
            alerta.present();
        }
        //this.montoReservacion=dataCode.total;
        //this.idTarjeta=dataCode.tarjeta;
        // Datos de la reservacion
        this.servMon.getReservacion(this.idReservacion2).subscribe(function (reserv) {
            _this.reservacion = reserv;
            // Extraemos uid del usuario
            var uid = _this.reservacion.idUsuario;
            _this.infoClient(uid);
            // Extraemos el id del area
            var uidArea = _this.reservacion.idArea;
            _this.infoArea(uidArea);
            // Extraemos el id de la zona
            var uidZona = _this.reservacion.idZona;
            _this.infoZona(uidZona);
            // Enviamos el idReservacion a reservaciones mesas
            _this.getAllTables(_this.idReservacion2);
            // Extraemos el id del evento
            var uidEvento = _this.reservacion.idevento;
            _this.infoEvento(uidEvento);
            // Enviamos el idReservacion a productos
            _this.getProducts(_this.idReservacion2);
        });
        //this.servMon.getDatosTarjeta(this.idTarjeta).subscribe( tarjeta =>{
        //  this.infotarjeta = tarjeta;
        //  this.numTarjeta = this.infotarjeta.numTarjeta;
        //  this.mesExpiracion = this.infotarjeta.mesExpiracion;
        //  this.anioExpiracion = this.infotarjeta.anioExpiracion;
        //  this.cvc = this.infotarjeta.cvc;
        //console.log('datos de la tarjeta y monto');
        //console.log(this.idTarjeta,this.numTarjeta,  this.mesExpiracion,this.anioExpiracion,this.cvc,this.montoReservacion);
        //mandar datos de la tarjeta
        //this.servMon.cambiaPagando(this.idReservacion2,this.numTarjeta,this.mesExpiracion,this.anioExpiracion,this.cvc,this.montoReservacion);
        //});
    }
    AdminLeeQrPage.prototype.ionViewDidLoad = function () {
    };
    AdminLeeQrPage.prototype.infoClient = function (uid) {
        var _this = this;
        this.user.getUserAdmins(uid).subscribe(function (c) {
            _this.cliente = c;
            console.log('cliente', _this.cliente);
        });
    };
    AdminLeeQrPage.prototype.infoArea = function (uid) {
        var _this = this;
        this.servMon.getArea(uid).subscribe(function (a) {
            _this.area = a;
            console.log('area', _this.area);
        });
    };
    AdminLeeQrPage.prototype.infoZona = function (uid) {
        var _this = this;
        this.servMon.getZona(uid).subscribe(function (a) {
            _this.zona = a;
            console.log('zona', _this.zona);
        });
    };
    AdminLeeQrPage.prototype.getAllTables = function (id) {
        var _this = this;
        this.servMon.getAllMesas(id).then(function (m) {
            _this.mesas = m;
            console.log('mesas', _this.mesas);
        });
    };
    AdminLeeQrPage.prototype.infoEvento = function (uid) {
        var _this = this;
        this.servMon.getEvento(uid).subscribe(function (e) {
            _this.evento = e;
            console.log('evento', e);
        });
    };
    AdminLeeQrPage.prototype.getProducts = function (uid) {
        var _this = this;
        this.servMon.getAllProductos(uid).then(function (p) {
            _this.productos = p;
            console.log('carta', p);
        });
    };
    AdminLeeQrPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-lee-qr',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-lee-qr\admin-lee-qr.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Detalle de la reservación</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="fondo_admin">\n\n\n\n    <div text-center>\n\n        <h2 style="color: white;"> Reservación</h2>\n\n    </div>\n\n\n\n    <ion-card class="fon">\n\n        <ion-card-header class="fon">\n\n            Datos del cliente\n\n        </ion-card-header>\n\n        <ion-card-content class="fon">\n\n            <ion-list class="fon">\n\n                <ion-item class="fon">\n\n                    <ion-avatar item-start>\n\n                        <img [src]="cliente?.photoURL">\n\n                    </ion-avatar>\n\n                    <h1>{{ cliente.displayName }}</h1>\n\n                    <h2>{{ cliente.email }} </h2>\n\n                    <p>{{ cliente.phoneNumber }} </p>\n\n                </ion-item>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card class="fon">\n\n        <ion-card-header class="fon">\n\n            Reservación\n\n        </ion-card-header>\n\n        <ion-card-content>\n\n            <ion-list class="fon">\n\n                <ion-item class="fon">\n\n                    <ion-avatar item-start>\n\n                        <img src="../assets/imgs/icons/book.png">\n\n                    </ion-avatar>\n\n                    <h2>{{ reservacion.fechaR_ | date: \'fullDate\' }}</h2>\n\n                    <h2>{{ reservacion.hora }} </h2>\n\n                </ion-item>\n\n                <ion-card-header class="fon">Detalle de Área</ion-card-header>\n\n                <ion-item class="fon">\n\n                    <ion-avatar item-start>\n\n                        <img src="../assets/imgs/icons/admin-historial.png">\n\n                    </ion-avatar>\n\n                    <ion-grid>\n\n                        <!--<ion-row>\n\n                            <ion-col>\n\n                                <div><b>Área:</b></div>\n\n                            </ion-col>\n\n                            <ion-col>\n\n                                <div>\n\n                                    <p>{{ area.nombre }} </p>\n\n                                </div>\n\n                            </ion-col>\n\n                        </ion-row>-->\n\n                        <ion-row>\n\n                            <ion-col>\n\n                                <div> <b>Zona:</b> </div>\n\n                            </ion-col>\n\n                            <ion-col>\n\n                                <div>\n\n                                    <p>{{ zona.nombre }} </p>\n\n                                </div>\n\n                            </ion-col>\n\n                        </ion-row>\n\n                        <ion-row>\n\n                            <ion-col>\n\n                                <div> <b>Mesa:</b> </div>\n\n                            </ion-col>\n\n                            <ion-col>\n\n                                <div>\n\n                                    <p *ngFor="let mesa of mesas"> {{ mesa.no_mesa }} , </p>\n\n                                </div>\n\n                            </ion-col>\n\n                        </ion-row>\n\n                        <ion-row>\n\n                            <ion-col>\n\n                                <div> <b>No. personas:</b> </div>\n\n                            </ion-col>\n\n                            <ion-col>\n\n                                <div>\n\n                                    <p> {{ reservacion.numPersonas }} </p>\n\n                                </div>\n\n                            </ion-col>\n\n                        </ion-row>\n\n                    </ion-grid>\n\n                </ion-item>\n\n                <ng-container *ngIf="evento">\n\n                    <ion-card-header class="fon">Evento</ion-card-header>\n\n                    <ion-item class="fon">\n\n                        <ion-avatar item-start>\n\n                            <img src="../assets/imgs/icons/admin-evento.png">\n\n                        </ion-avatar>\n\n                        <p>{{ evento.titulo }}</p>\n\n                        <p>{{ evento.categoria }} </p>\n\n                        <p>{{ evento.hora }} </p>\n\n                    </ion-item>\n\n                </ng-container>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card class="fon">\n\n        <ion-card-header class="fon">Carta</ion-card-header>\n\n        <ion-card-content>\n\n            <ion-list class="fon">\n\n                <ion-item class="fon" *ngFor="let product of productos">\n\n                    <ion-avatar item-start>\n\n                        <img [src]="product?.img">\n\n                    </ion-avatar>\n\n                    <h2>{{ product.producto }} </h2>\n\n                    <b>{{ product.cantidad }} </b>\n\n                </ion-item>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-lee-qr\admin-lee-qr.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_3__providers_monitoreo_reservas_monitoreo_reservas__["a" /* MonitoreoReservasProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], AdminLeeQrPage);
    return AdminLeeQrPage;
}());

//# sourceMappingURL=admin-lee-qr.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminHistorialReservacionesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_monitoreo_reservas_monitoreo_reservas__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__admin_reservacion_detalle_admin_reservacion_detalle__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminHistorialReservacionesPage = /** @class */ (function () {
    function AdminHistorialReservacionesPage(navCtrl, navParams, afs, monRes) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afs = afs;
        this.monRes = monRes;
        this.reservaciones = [];
    }
    AdminHistorialReservacionesPage.prototype.ionViewDidLoad = function () {
        this.getAllReservacione();
        this.getClientes();
    };
    AdminHistorialReservacionesPage.prototype.getAllReservacione = function () {
        var _this = this;
        var id = localStorage.getItem('uidSucursal');
        //Cuando es un usuario se saca el id de la sucursal ala que pertenece
        this.afs.collection('users', function (ref) { return ref.where('uid', '==', id); }).valueChanges().subscribe(function (data) {
            _this.sucursales = data;
            _this.sucursales.forEach(function (element) {
                var uidSucursal = element.uidSucursal;
                _this.monRes.getReservacionesPagando(uidSucursal).subscribe(function (reser) {
                    _this.reservaciones = reser;
                    console.log('reservaciones', reser);
                });
            });
        });
        this.monRes.getReservacionesPagando(id).subscribe(function (reser) {
            _this.reservaciones = reser;
            console.log('reservaciones', reser);
        });
    };
    AdminHistorialReservacionesPage.prototype.getClientes = function () {
        var _this = this;
        this.monRes.getAllClientes("users").then(function (c) {
            _this.clientes = c;
            console.log("Estos son los clientes: ", _this.clientes);
        });
    };
    AdminHistorialReservacionesPage.prototype.goDetalle = function (idReservacion) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__admin_reservacion_detalle_admin_reservacion_detalle__["a" /* AdminReservacionDetallePage */], { idReservacion: idReservacion });
    };
    AdminHistorialReservacionesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-historial-reservaciones',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-historial-reservaciones\admin-historial-reservaciones.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Historial de reservaciones</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="list-avatar-page fondo_admin">\n\n    <ion-list *ngIf="reservaciones.length != 0">\n\n        <ion-item *ngFor="let reservacion of reservaciones">\n\n            <ion-avatar item-start>\n\n                <img src="../assets/imgs/icons/book.png">\n\n            </ion-avatar>\n\n            <ng-container *ngFor="let cliente of clientes">\n\n                <h2 *ngIf="cliente.uid == reservacion.idUsuario">{{ cliente.displayName }} </h2>\n\n            </ng-container>\n\n            <!--<p>{{ reservacion.estatus }} </p>-->\n\n            <p>Folio: {{ reservacion.folio}} </p>\n\n            <p *ngIf="reservacion.estatus == \'Pagando\'"> Pagada</p>\n\n            <p>{{ reservacion.fechaR }} </p>\n\n            <ion-note item-end>{{ reservacion.hora }} </ion-note>\n\n            <button ion-button color="dark" round (click)="goDetalle(reservacion.idReservacion)">\n\n                 Detalle\n\n            </button>\n\n        </ion-item>\n\n    </ion-list>\n\n    <ion-list *ngIf="reservaciones.length == 0">\n\n        <ion-card>\n\n            <div text-center>\n\n                <img text-center class="adv" src="../assets/imgs/icons/advertencia.png" />\n\n            </div>\n\n            <ion-card-content>\n\n                <ion-card-title text-center>\n\n                    Aviso\n\n                </ion-card-title>\n\n                <p>\n\n                    Por el momento no hay reservaciones.\n\n                </p>\n\n            </ion-card-content>\n\n        </ion-card>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-historial-reservaciones\admin-historial-reservaciones.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_2__providers_monitoreo_reservas_monitoreo_reservas__["a" /* MonitoreoReservasProvider */]])
    ], AdminHistorialReservacionesPage);
    return AdminHistorialReservacionesPage;
}());

//# sourceMappingURL=admin-historial-reservaciones.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgregarCuponesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cupones_sucursal_cupones_sucursal__ = __webpack_require__(127);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AgregarCuponesPage = /** @class */ (function () {
    function AgregarCuponesPage(navCtrl, navParams, fb, afs, sucursalProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.afs = afs;
        this.sucursalProv = sucursalProv;
        //validar que los inputs del formulario no esten vacios
        this.myForm = this.fb.group({
            sucursal: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            valorCupon: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            numCupones: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            condicion: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]],
            fechaExp: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]]
        });
    }
    AgregarCuponesPage.prototype.ionViewDidLoad = function () {
        //cargar fecha actual
        this.fechaActual = new Date().toJSON().split("T")[0];
        console.log('ionViewDidLoad AgregarCuponesPage', this.fechaActual);
        //ejecutar funcion para cargar info de las Sucursales en cuanto se cargue pagina
        this.cargarSucursales();
    };
    //funcion cargar info de las sucursales
    AgregarCuponesPage.prototype.cargarSucursales = function () {
        var _this = this;
        this.afs.collection('sucursales').valueChanges().subscribe(function (s) {
            _this.sucursales = s;
        });
    };
    //funcion para registrar nuevos cupones
    AgregarCuponesPage.prototype.cuponAdd = function () {
        var a = '100000';
        var codigoCupon = Math.round(Math.random() * (999999 - 100000) + parseInt(a));
        //console.log("codigo",num);
        this.sucursalProv.agregarCupon(codigoCupon, this.sucursal, this.valorCupon, this.numCupones, this.condicion, this.fechaExp, this.fechaActual);
        //this.navCtrl.setRoot(CuponesSucursalPage);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__cupones_sucursal_cupones_sucursal__["a" /* CuponesSucursalPage */]);
    };
    AgregarCuponesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-agregar-cupones',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\agregar-cupones\agregar-cupones.html"*/'\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Agregar cupones</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <form [formGroup]="myForm" novalidate class="">\n\n   <ion-list>\n\n       <ion-item>\n\n        <ion-label class="blanco_1">Sucursal</ion-label>\n\n          <ion-select [(ngModel)]="sucursal" multiple="false" cancelText="Cancelar" okText="Aceptar" name="sucursal" formControlName="sucursal">\n\n             <div>\n\n               <ion-option *ngFor="let sucur of sucursales" [value]="sucur.uid">{{ sucur.displayName }}</ion-option>\n\n             </div>\n\n        </ion-select>\n\n       </ion-item>\n\n       <br>\n\n       <ion-item>\n\n         <ion-label class="blanco_1">Valor del cupón (MXN)</ion-label>\n\n         <ion-input type="number" [(ngModel)]="valorCupon" name="valorCupon" formControlName="valorCupon"></ion-input>\n\n       </ion-item>\n\n       <br>\n\n       <ion-item>\n\n         <ion-label class="blanco_1">Cupones a generar</ion-label>\n\n         <ion-input type="number" [(ngModel)]="numCupones" name="numCupones" formControlName="numCupones"></ion-input>\n\n       </ion-item>\n\n       <br>\n\n       <ion-item>\n\n         <ion-label class="blanco_1">Condición</ion-label>\n\n         <ion-select [(ngModel)]="condicion" multiple="false" cancelText="Cancelar" okText="Aceptar" name="condicion" formControlName="condicion">\n\n             <div>\n\n               <ion-option value="10reservaciones">Usuario + de 10 reservaciones</ion-option>\n\n               <ion-option value="20reservaciones">Usuario + de 20 reservaciones</ion-option>\n\n             </div>\n\n         </ion-select>\n\n       </ion-item>\n\n       <br>\n\n       <ion-item>\n\n         <ion-label class="blanco_1">Fecha de expiración</ion-label>\n\n         <ion-datetime item-end displayFormat="DD/MM/YYYY" [(ngModel)]="fechaExp" name="fechaExp" formControlName="fechaExp" min={{fechaActual}} doneText=Aceptar cancelText=Cancelar></ion-datetime>\n\n       </ion-item>\n\n       <br>\n\n       <ion-item>\n\n       </ion-item>\n\n\n\n    </ion-list>\n\n  </form>\n\n  <ion-footer no-border>\n\n      <ion-toolbar>\n\n          <ion-grid>\n\n              <ion-row>\n\n                <ion-col col-6>\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                      <div text-center>\n\n                          <button ion-button block round color="dark" (click)="cuponAdd()" [disabled]="!myForm.valid">Generar</button>\n\n                      </div>\n\n                </ion-col>\n\n              </ion-row>\n\n          </ion-grid>\n\n      </ion-toolbar>\n\n  </ion-footer>\n\n </ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\agregar-cupones\agregar-cupones.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_4__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */]])
    ], AgregarCuponesPage);
    return AgregarCuponesPage;
}());

//# sourceMappingURL=agregar-cupones.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetalleCuponPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the DetalleCuponPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetalleCuponPage = /** @class */ (function () {
    function DetalleCuponPage(navCtrl, navParams, afs) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afs = afs;
        //recibe parametro de la reservacion
        this.idSucursal = this.navParams.get("idSucursal");
        //obtener los cupones de la  sucursal seleccionada
        this.afs.collection('cupones', function (ref) { return ref.where('idSucursal', '==', _this.idSucursal); }).valueChanges().subscribe(function (c) {
            _this.cupones = c;
        });
        //info de las sucrsales
        this.afs.collection('sucursales').valueChanges().subscribe(function (s) {
            _this.sucursales = s;
        });
    }
    DetalleCuponPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetalleCuponPage');
    };
    DetalleCuponPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detalle-cupon',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\detalle-cupon\detalle-cupon.html"*/'\n\n<ion-header>\n\n  <ion-navbar>\n\n    <div *ngFor="let sucursal of sucursales">\n\n     <ion-title *ngIf="sucursal.uid == idSucursal">Cupones de {{sucursal.displayName}}</ion-title>\n\n    </div>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content class="fondo_admin">\n\n    <ion-list clxass="card_atras">\n\n        <ion-item>\n\n          <ion-thumbnail item-start>\n\n          </ion-thumbnail>\n\n          <div *ngFor="let cupon of cupones">\n\n              <div *ngIf="cupon.estatus == \'Activo\'">\n\n                <ion-badge color="naranja" item-end>\n\n                    Cupón de descuento {{ cupon.valorCupon | currency }}\n\n                    <br>\n\n                    Código {{ cupon.codigoCupon }}\n\n                    <br>\n\n                    <ion-badge color="light"> Disponibles {{ cupon.numCupones }}</ion-badge>\n\n                    <br>\n\n                    <ion-badge color="dark">{{ cupon.estatus }}</ion-badge>\n\n                  </ion-badge>\n\n              </div>\n\n          </div>\n\n        </ion-item>\n\n      </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\detalle-cupon\detalle-cupon.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], DetalleCuponPage);
    return DetalleCuponPage;
}());

//# sourceMappingURL=detalle-cupon.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgregarCiudadPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ciudad_establecimiento_ciudad_establecimiento__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AgregarCiudadPage = /** @class */ (function () {
    function AgregarCiudadPage(navCtrl, navParams, alertCtrl, fb, sucursalProv, afs) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.fb = fb;
        this.sucursalProv = sucursalProv;
        this.afs = afs;
        //validar que los inputs del formulario no esten vacios
        this.myForm = this.fb.group({
            ciudad: ["", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]]
        });
    }
    //funcion para registrar nuevos cupones
    AgregarCiudadPage.prototype.ciudadAdd = function () {
        var _this = this;
        console.log('lego ciudad', this.ciudad);
        this.afs.collection('ciudades', function (ref) { return ref.where('ciudad', '==', _this.ciudad); }).valueChanges().subscribe(function (data) {
            _this.resultadoCiudad = data;
            ///console.log("total ciudades", this.resultadoCiudad.length);
            if (_this.resultadoCiudad.length == 0) {
                _this.sucursalProv.agregarCiudad(_this.ciudad);
                //this.navCtrl.setRoot(CiudadEstablecimientoPage);
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__ciudad_establecimiento_ciudad_establecimiento__["a" /* CiudadEstablecimientoPage */]);
            }
        });
    };
    AgregarCiudadPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-agregar-ciudad',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\agregar-ciudad\agregar-ciudad.html"*/'\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Agregar Ciudad</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <form [formGroup]="myForm" novalidate class="">\n\n   <ion-list>\n\n     <ion-item>\n\n       <ion-label class="blanco_1">Ciudad</ion-label>\n\n       <ion-select [(ngModel)]="ciudad" multiple="false" cancelText="Cancelar" okText="Aceptar" name="ciudad" formControlName="ciudad">\n\n           <div>\n\n             <ion-option value="León">León</ion-option>\n\n             <ion-option value="Aguascalientes">Aguascalientes</ion-option>2\n\n             <ion-option value="Mexicali">Mexicali</ion-option>\n\n             <ion-option value="Tijuana">Tijuana</ion-option>\n\n             <ion-option value="Campeche">Campeche</ion-option>\n\n             <ion-option value="Saltillo">Saltillo</ion-option>\n\n             <ion-option value="Manzanillo">Manzanillo</ion-option>\n\n             <ion-option value="Chihuahua">Chihuahua</ion-option>\n\n             <ion-option value="Durango">Durango</ion-option>\n\n             <ion-option value="Celaya">Celaya</ion-option>\n\n             <ion-option value="Guanajuato">Guanajuato</ion-option>\n\n             <ion-option value="Irapuato">Irapuato</ion-option>\n\n             <ion-option value="Salamanca">Salamanca</ion-option>\n\n             <ion-option value="Acapulco">Acapulco</ion-option>\n\n             <ion-option value="Pachuca">Pachuca</ion-option>\n\n             <ion-option value="Guadalajara">Guadalajara</ion-option>\n\n             <ion-option value="Toluca">Toluca</ion-option>\n\n             <ion-option value="Morelia">Morelia</ion-option>\n\n             <ion-option value="Uruapan">Uruapan</ion-option>\n\n             <ion-option value="Cuernavaca">Cuernavaca</ion-option>\n\n             <ion-option value="Tepic">Tepic</ion-option>\n\n             <ion-option value="Monterrey">Monterrey</ion-option>\n\n             <ion-option value="Oaxaca">Oaxaca</ion-option>\n\n             <ion-option value="Querétaro">Querétaro</ion-option>\n\n             <ion-option value="Cancún">Cancún</ion-option>\n\n             <ion-option value="Chetumal">Chetumal</ion-option>\n\n             <ion-option value="Culiacán">Culiacán</ion-option>\n\n             <ion-option value="Mazatlán">Mazatlán</ion-option>\n\n             <ion-option value="Hermosillo">Hermosillo</ion-option>\n\n             <ion-option value="Villahermosa">Villahermosa</ion-option>\n\n             <ion-option value="Matamoros">Matamoros</ion-option>\n\n             <ion-option value="Veracruz">Veracruz</ion-option>\n\n             <ion-option value="Mérida">Mérida</ion-option>\n\n             <ion-option value="Puebla">Puebla</ion-option>\n\n           </div>\n\n       </ion-select>\n\n     </ion-item>\n\n     <br>\n\n     <ion-item>\n\n     </ion-item>\n\n    </ion-list>\n\n  </form>\n\n  <ion-footer no-border>\n\n      <ion-toolbar>\n\n          <ion-grid>\n\n              <ion-row>\n\n                <ion-col col-6>\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                      <div text-center>\n\n                          <button ion-button block round color="dark" (click)="ciudadAdd()" [disabled]="!myForm.valid">Guardar</button>\n\n                      </div>\n\n                </ion-col>\n\n              </ion-row>\n\n          </ion-grid>\n\n      </ion-toolbar>\n\n  </ion-footer>\n\n </ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\agregar-ciudad\agregar-ciudad.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], AgregarCiudadPage);
    return AgregarCiudadPage;
}());

//# sourceMappingURL=agregar-ciudad.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CuentasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_reservacion_reservacion__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CuentasPage = /** @class */ (function () {
    function CuentasPage(navCtrl, navParams, sucProv, 
        //public toastCtrl: ToastController,
        //public Fiauth: AngularFireAuth,
        // private mapsAPILoader: MapsAPILoader,
        // private ngZone: NgZone
        //public zone: NgZone,
        //public platform: Platform,
        alertCtrl, _reservaciones, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sucProv = sucProv;
        this.alertCtrl = alertCtrl;
        this._reservaciones = _reservaciones;
        this.formBuilder = formBuilder;
        this.sucursalItem = {};
        this.data = {};
        this.productos = [];
        this.ocultar1 = false;
        this.myForm = this.createMyForm();
    }
    CuentasPage.prototype.accion1 = function () {
        this.ocultar1 = !this.ocultar1;
    };
    CuentasPage.prototype.createMyForm = function () {
        return this.formBuilder.group({
            FechaInicio: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            FechaFin: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]
        });
    };
    CuentasPage.prototype.ionViewDidLoad = function () {
        this.idSucursal = this.navParams.get("uidSucursal");
        console.log('id sucursal', this.idSucursal);
        // this.nombreSucursal=this.sucProv.selectedSucursalItem.displayName;
        // this.idSucursal=this.sucProv.selectedSucursalItem.uid;
        this.contador = 0;
        this.sumatoria = 0;
        // this.getReservaciones(this.idSucursal);
    };
    CuentasPage.prototype.saveData = function () {
        console.log(this.myForm.value);
        console.log('Fecha Inicio', this.myForm.value.FechaInicio);
        console.log('Fecha Fin', this.myForm.value.FechaFin);
        this.fechaI = this.myForm.value.FechaInicio;
        this.fechaF = this.myForm.value.FechaFin;
        var fech = __WEBPACK_IMPORTED_MODULE_5_moment__(this.fechaI).format("x");
        console.log(fech);
        this.getReservaciones(this.idSucursal, this.fechaI, this.fechaF);
        this.contador = 0;
    };
    CuentasPage.prototype.getReservaciones = function (idx, fechaI, fechaF) {
        var _this = this;
        console.log('llamar provider', idx);
        this._reservaciones.getReservaciones(idx, fechaI, fechaF).subscribe(function (res) {
            console.log("Este es el resultado de reservaciones: ", res);
            var suma = 0;
            res.forEach(function (value) {
                //console.log('totalResercva', value.totalReservacion);
                if (value.totalReservacion != null) {
                    suma = suma + parseFloat(value.totalReservacion);
                }
                console.log('suma', suma);
            });
            _this.fechaI = fechaI;
            _this.fechaF = fechaF;
            _this.sumatoria = suma;
            _this.comision1 = suma * 0.056;
            _this.comision2 = suma - _this.comision1;
            _this.propina = suma * .05;
            _this.reservaciones = res;
            console.log('comision1', _this.comision1);
            console.log('comision2', _this.comision2);
        });
    };
    // guardarCorte(){
    //   console.log('***** F_Guardar_Corte');
    //   console.log('comision1',this.comision1);
    //   console.log('comision2',this.comision2);
    //   console.log('sumatoria',this.sumatoria);
    //   console.log('fechaI',this.fechaI);
    //   console.log('fechaF',this.fechaF);
    //   this._reservaciones.addCorte(this.fechaI, this.fechaF, this.comision1, this.comision2, this.sumatoria, this.idSucursal);
    // }
    CuentasPage.prototype.suma = function (x) {
        //this.contador=this.contador+1;
        //console.log(this.contador);
        console.log('idReservacion funcion', x);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], CuentasPage.prototype, "content", void 0);
    CuentasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cuentas',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\cuentas\cuentas.html"*/'\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Cuentas</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <form [formGroup]="myForm" (ngSubmit)="saveData()">\n\n        <ion-list>\n\n            <ion-item>\n\n                <ion-label>Fecha Inicio</ion-label>\n\n                <ion-datetime formControlName="FechaInicio" displayFormat="DD/MM/YYYY" min="2018" max="2050-10-31" doneText=Aceptar cancelText=Cancelar> </ion-datetime>\n\n            </ion-item>\n\n            <ion-item>\n\n                <ion-label>Fecha Fin</ion-label>\n\n                <ion-datetime formControlName="FechaFin" displayFormat="DD/MM/YYYY" min="2018" max="2050-10-31" doneText=Aceptar cancelText=Cancelar></ion-datetime>\n\n            </ion-item>            \n\n        </ion-list>\n\n\n\n        <div padding>\n\n            <button ion-button  color="naranja" block type="submit" [disabled]="!myForm.valid" >Generar</button>\n\n          </div>\n\n    </form>\n\n\n\n   \n\n   <ion-card class="card_atras" *ngIf="reservaciones != null ">\n\n    <ion-card-content class="card_atras"  >\n\n        <ion-card-title> \n\n            <h5 class="blanco_1"> Resumen</h5>\n\n                                     \n\n        </ion-card-title>\n\n\n\n        \n\n        <ion-item class="card_atras" >\n\n            <a class="blanco_2">Total</a>\n\n            <h4 class="blanco_1"  ></h4>\n\n            <ion-badge class="negro" item-end> {{ comision2 | currency}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item class="card_atras" >\n\n                <a class="blanco_2"> Guest (5.6%)</a>\n\n                <h4 class="blanco_1"  ></h4>\n\n                <ion-badge class="negro" item-end> {{ comision1 | currency}}</ion-badge>\n\n        </ion-item>\n\n\n\n        \n\n        <ion-item class="card_atras" >\n\n            <a class="blanco_2"> Propina</a>\n\n            <h4 class="blanco_1"  ></h4>\n\n            <ion-badge class="negro" item-end> {{ propina | currency}}</ion-badge>\n\n        </ion-item>\n\n\n\n        <ion-item class="card_atras" >\n\n                <a class="blanco_2">Total</a>\n\n                <h4 class="blanco_1"  ></h4>\n\n                <ion-badge class="negro" item-end> {{ sumatoria | currency }} </ion-badge>\n\n            </ion-item>\n\n\n\n        <ion-item class="card_atras" >\n\n            <a  (click)="accion1()" > Ver detalle </a>\n\n        </ion-item>\n\n\n\n        <!-- <button ion-button color="naranja" block (click)="guardarCorte()">Guardar</button> -->\n\n    </ion-card-content>\n\n   </ion-card>\n\n   \n\n    <ion-card class="card_atras" *ngFor="let reserva of reservaciones" >\n\n\n\n        <ion-card-content class="card_atras"  *ngIf="ocultar1">\n\n            <ion-card-title> \n\n            <p class="blanco_2">Reservacion: {{ reserva.folio}}</p>\n\n            </ion-card-title>\n\n\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/calendario.png" item-start>\n\n                <p class="blanco_2">Fecha de reservación</p>\n\n                <ion-badge class="negro" item-end>{{ reserva.fechaR }} </ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/time.png" item-start>\n\n                <a class="blanco_2">Estatus</a>\n\n                <ion-badge class="negro" item-end> {{ reserva.estatus }}  </ion-badge>\n\n            </ion-item>\n\n\n\n            <ion-item class="card_atras" >\n\n                    <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                    <a class="blanco_2">Total</a>\n\n                    <h4 class="blanco_1"  ></h4>\n\n                    <ion-badge class="negro" item-end> {{ reserva.totalReservacion | currency}} </ion-badge>\n\n            </ion-item>\n\n\n\n        </ion-card-content>\n\n        \n\n    </ion-card>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\cuentas\cuentas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_reservacion_reservacion__["a" /* ReservacionProvider */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
    ], CuentasPage);
    return CuentasPage;
}());

//# sourceMappingURL=cuentas.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminPerfilEmpleadoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(69);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminPerfilEmpleadoPage = /** @class */ (function () {
    function AdminPerfilEmpleadoPage(navCtrl, navParams, authProvider, Fiauth, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.authProvider = authProvider;
        this.Fiauth = Fiauth;
        this.toastCtrl = toastCtrl;
        this._userEmpleado = {};
        this.uid = this.navParams.get('uid');
        console.log('info empleado perfil', this.uid);
        this.authProvider.getUserAdmins(this.uid).subscribe(function (s) {
            _this._userEmpleado = s;
            console.log('empleado info', _this._userEmpleado);
        });
    }
    AdminPerfilEmpleadoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminPerfilEmpleadoPage');
    };
    AdminPerfilEmpleadoPage.prototype.changePass = function (email) {
        var auth = this.Fiauth.auth;
        email;
        // Email sent.
        if (confirm('¿Estás seguro de que quieres restablecer la contraseña?')) {
            auth.sendPasswordResetEmail(email);
            this.mostrar_toast('Se ha enviado un correo para el cambio/restablecimiento de contraseña a ' + email);
            // console.log('Correo enviado a '+ email);
        }
    };
    AdminPerfilEmpleadoPage.prototype.mostrar_toast = function (mensaje) {
        var toast = this.toastCtrl.create({
            message: mensaje,
            duration: 3000
        }).present();
    };
    AdminPerfilEmpleadoPage.prototype.logout = function () {
        this.authProvider.logout();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
    };
    AdminPerfilEmpleadoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-perfil-empleado',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-perfil-empleado\admin-perfil-empleado.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Perfil</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-list>\n\n        <ion-list-header>\n\n            Mis datos\n\n        </ion-list-header>\n\n        <ion-item>{{_userEmpleado.displayName}}</ion-item>\n\n        <ion-item>{{_userEmpleado.correo}} </ion-item>\n\n        <ion-item *ngIf="_userEmpleado.active == \'true\'">Estado:\n\n            <ion-badge item-end color="sec">Activo</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="_userEmpleado.active == \'false\'">Estado:\n\n            <ion-badge item-end color="red">No Activo</ion-badge>\n\n        </ion-item>\n\n    </ion-list>\n\n    <ion-row>\n\n        <ion-col>\n\n            <button class="md-trash" color="dark" ion-button block (click)="changePass(_userEmpleado.correo)">Restablecer/Cambiar Contraseña</button>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <!--<ion-col>\n\n            <button class="md-trash" color="dark" ion-button block (click)="logout()">Cerrar sesión</button>\n\n        </ion-col>-->\n\n    </ion-row>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-perfil-empleado\admin-perfil-empleado.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */]])
    ], AdminPerfilEmpleadoPage);
    return AdminPerfilEmpleadoPage;
}());

//# sourceMappingURL=admin-perfil-empleado.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminReservacionesCursoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_monitoreo_reservas_monitoreo_reservas__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__admin_reservacion_detalle_admin_reservacion_detalle__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminReservacionesCursoPage = /** @class */ (function () {
    function AdminReservacionesCursoPage(navCtrl, navParams, afs, monRes) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afs = afs;
        this.monRes = monRes;
        this.reservaciones = [];
    }
    AdminReservacionesCursoPage.prototype.ionViewDidLoad = function () {
        this.getAllReservacione();
        this.getClientes();
        this.getEvento();
    };
    AdminReservacionesCursoPage.prototype.getAllReservacione = function () {
        var _this = this;
        var dateObj = new Date();
        var anio = dateObj.getFullYear().toString();
        var mes = dateObj.getMonth().toString();
        var dia = dateObj.getDate();
        var mesArray = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        if (dia >= 1 && dia <= 9) {
            var diaCero = '0' + dia;
            this.formatoFecha = anio + '-' + mesArray[mes] + '-' + diaCero;
        }
        else {
            this.formatoFecha = anio + '-' + mesArray[mes] + '-' + dia;
        }
        console.log("fecha actual");
        console.log(this.formatoFecha);
        var id = localStorage.getItem('uidSucursal');
        //Cuando es un usuario se saca el id de la sucursal ala que pertenece
        this.afs.collection('users', function (ref) { return ref.where('uid', '==', id); }).valueChanges().subscribe(function (data) {
            _this.sucursales = data;
            _this.sucursales.forEach(function (element) {
                var uidSucursal = element.uidSucursal;
                _this.monRes.getReservacionesCurso(uidSucursal, _this.formatoFecha).subscribe(function (reser) {
                    _this.reservaciones = reser;
                });
            });
        });
        this.monRes.getReservacionesCurso(id, this.formatoFecha).subscribe(function (reser) {
            _this.reservaciones = reser;
            console.log('reservaciones', reser);
        });
    };
    AdminReservacionesCursoPage.prototype.getClientes = function () {
        var _this = this;
        this.monRes.getAllClientes("users").then(function (c) {
            _this.clientes = c;
            console.log("Estos son los clientes: ", _this.clientes);
        });
    };
    AdminReservacionesCursoPage.prototype.goDetalle = function (idReservacion) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__admin_reservacion_detalle_admin_reservacion_detalle__["a" /* AdminReservacionDetallePage */], { idReservacion: idReservacion });
    };
    AdminReservacionesCursoPage.prototype.getEvento = function () {
        var _this = this;
        this.afs.collection("evento").valueChanges().subscribe(function (data12) {
            _this.infoEvento = data12;
            console.log("evento", _this.infoEvento);
        });
    };
    AdminReservacionesCursoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-reservaciones-curso',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-reservaciones-curso\admin-reservaciones-curso.html"*/'<ion-header>\n\n\n\n    <ion-navbar class="borde">\n\n        <ion-row>\n\n            <ion-col col-1 align="center">\n\n                <img src="./assets/content/line2.png" class="imgLi" alt="">\n\n            </ion-col>\n\n            <ion-col col-8>\n\n                <div class="AdminEvento">Reservaciones en curso</div>\n\n                <div class="sucursal">Sucursal</div>\n\n            </ion-col>\n\n            <ion-col>\n\n                <div align=\'end\'>\n\n                    <img src="./assets/content/corona.png" style="width: 40%;" alt="">\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <br><br><br><br>\n\n    <!-- <ion-searchbar [animated]="true" (ionInput)="getItems($event)"></ion-searchbar> -->\n\n\n\n    <ion-row>\n\n        <ion-col col-1></ion-col>\n\n        <ion-col>\n\n            <ion-card class="descripcion">\n\n                <br>\n\n                <ion-list *ngIf="reservaciones.length != 0">\n\n                    <ion-item *ngFor="let reservacion of reservaciones" class="descripcion" no-lines>\n\n                        \n\n                        <ion-row>\n\n                            <ion-col col-3>\n\n                                <div *ngFor="let event of infoEvento">\n\n                                    <div *ngIf="event.uid == reservacion.idevento">\n\n                                        <img [src]="event.img" class="imgEvent">\n\n                                        <p class="tituloE">{{event.titulo}}</p>\n\n                                    </div>\n\n                                </div>\n\n                            </ion-col>\n\n                            <ion-col col-1>\n\n                                <img src="./assets/content/line3.png" class="imgL" alt="">\n\n                            </ion-col>\n\n                            <ion-col col-6>\n\n                                <ng-container *ngFor="let cliente of clientes">\n\n                                    <h2 *ngIf="cliente.uid == reservacion.idUsuario">{{ cliente.displayName }} </h2>\n\n                                </ng-container>\n\n                                <p class="fechaR">Fecha {{ reservacion.fechaR}} </p>\n\n                                <button ion-button class="btnDetalle" round (click)="goDetalle(reservacion.idReservacion)">\n\n                                    Detalle\n\n                                </button>\n\n                            </ion-col>\n\n                            <ion-col col-2>\n\n                                <p class="hora">{{ reservacion.hora }} </p>\n\n                                <p style="margin-top: 5px;"></p>\n\n                                <div align="center"><img src="./assets/content/Ellipse55.png" class="imgElli" alt=""></div>\n\n                                <p style="margin-top: 5px;"></p>\n\n                                <!-- <p class="status">STATUS </p> -->\n\n                                <p class="aceptado" *ngIf="reservacion.estatus == \'Pagando\'">Pagada </p>\n\n                            </ion-col>\n\n                        </ion-row>\n\n                        <!-- <ion-avatar item-start>\n\n                            <img src="../assets/imgs/icons/book.png">\n\n                        </ion-avatar>\n\n                        <ng-container *ngFor="let cliente of clientes">\n\n                            <h2 *ngIf="cliente.uid == reservacion.idUsuario">{{ cliente.displayName }} </h2>\n\n                        </ng-container>\n\n                        <p>Folio: {{ reservacion.folio}} </p>\n\n                        <p *ngIf="reservacion.estatus == \'Pagando\'"> Pagada </p>\n\n                        <p>{{ reservacion.fechaR}} </p>\n\n                        <ion-note item-end>{{ reservacion.hora }} </ion-note>\n\n                        <button ion-button color="dark" round (click)="goDetalle(reservacion.idReservacion)">\n\n                            Detalle\n\n                        </button> -->\n\n                    </ion-item>\n\n                </ion-list>\n\n            </ion-card>\n\n        </ion-col>\n\n        <ion-col col-1></ion-col>\n\n    </ion-row>\n\n\n\n    \n\n\n\n    <div *ngIf="reservaciones.length == 0">\n\n        <br><br><br><br><br><br><br><br><br><br><br><br>\n\n        <div>\n\n            <div align="center">\n\n                <img text-center class="adv2" src="./assets/content/advertencia.png" />\n\n            </div>\n\n            <p class="aviso">\n\n                Aviso\n\n            </p>\n\n            <p class="momento">\n\n                Por el momento no hay reservaciones.\n\n            </p>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-reservaciones-curso\admin-reservaciones-curso.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_2__providers_monitoreo_reservas_monitoreo_reservas__["a" /* MonitoreoReservasProvider */]])
    ], AdminReservacionesCursoPage);
    return AdminReservacionesCursoPage;
}());

//# sourceMappingURL=admin-reservaciones-curso.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the HistorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HistorialPage = /** @class */ (function () {
    function HistorialPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HistorialPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HistorialPage');
    };
    HistorialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-historial',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\historial\historial.html"*/'<!--\n\n  Generated template for the HistorialPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Historial</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n    <ion-card class="card_title">\n\n        <ion-card-header class="card_title" text-center>\n\n            Historial de las reservaciones\n\n        </ion-card-header>\n\n    </ion-card>\n\n    <ion-card class="card_atras">\n\n        <ion-card-content class="card_atras">\n\n            <ion-card-title>\n\n                <h4 class="blanco_1">Reservación 1</h4>\n\n            </ion-card-title>\n\n            <p class="blanco_2">\n\n                Alfonso Martínez\n\n            </p>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/calendario.png" item-start>\n\n                <p class="blanco_2">Fecha de reservación</p>\n\n                <ion-badge class="negro" item-end>Viernes 9 Julio 2018 </ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                <a class="blanco_2">Consumo total</a>\n\n                <ion-badge class="negro" item-end>$5000.00</ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/mas.png" item-start>\n\n                <a class="blanco_2">Más detalles</a>\n\n                <ion-badge item-end></ion-badge>\n\n            </ion-item>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card class="card_atras">\n\n        <ion-card-content class="card_atras">\n\n            <ion-card-title>\n\n                <h4 class="blanco_1">Reservación 2</h4>\n\n            </ion-card-title>\n\n            <p class="blanco_2">\n\n                Alfonso Martínez\n\n            </p>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/calendario.png" item-start>\n\n                <p class="blanco_2">Fecha de reservación</p>\n\n                <ion-badge class="negro" item-end>Viernes 9 Julio 2018 </ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                <a class="blanco_2">Consumo total</a>\n\n                <ion-badge class="negro" item-end>$5000.00</ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/mas.png" item-start>\n\n                <a class="blanco_2">Más detalles</a>\n\n                <ion-badge item-end></ion-badge>\n\n            </ion-item>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card class="card_atras">\n\n        <ion-card-content class="card_atras">\n\n            <ion-card-title>\n\n                <h4 class="blanco_1">Reservación 3</h4>\n\n            </ion-card-title>\n\n            <p class="blanco_2">\n\n                Alfonso Martínez\n\n            </p>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/calendario.png" item-start>\n\n                <p class="blanco_2">Fecha de reservación</p>\n\n                <ion-badge class="negro" item-end>Viernes 9 Julio 2018 </ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                <a class="blanco_2">Consumo total</a>\n\n                <ion-badge class="negro" item-end>$5000.00</ion-badge>\n\n            </ion-item>\n\n            <ion-item class="card_atras">\n\n                <img class="icon" src="../assets/imgs/icons/mas.png" item-start>\n\n                <a class="blanco_2">Más detalles</a>\n\n                <ion-badge item-end></ion-badge>\n\n            </ion-item>\n\n        </ion-card-content>\n\n    </ion-card>\n\n\n\n    <!-- <ion-card class="card_atras">\n\n<ion-card-content class="card_atras">\n\n  <ion-card-title class="blanco_1">\n\n  Reservación 2\n\n    </ion-card-title>\n\n  <p class="blanco_2">\n\n  Alfonso Martínez\n\n  </p>\n\n<ion-item class="card_atras">\n\n<ion-icon name="md-calendar" item-start></ion-icon>\n\n<h4 class="blanco_2">Fecha de reservación</h4>\n\n<ion-badge  class="negro" item-end>Sabado 29 Julio 2018 </ion-badge>\n\n</ion-item>\n\n<ion-item class="card_atras">\n\n<ion-icon name="md-cash" item-start></ion-icon>\n\n<a>Consumo total</a>\n\n<ion-badge  class="negro" item-end>$5036.00</ion-badge>\n\n</ion-item>\n\n<ion-item class="card_atras">\n\n<ion-icon name="md-add" item-start></ion-icon>\n\n<a>Más detalles</a>\n\n<ion-badge item-end></ion-badge>\n\n</ion-item>\n\n\n\n</ion-card-content>\n\n</ion-card>\n\n\n\n<ion-card class="card_atras">\n\n<ion-card-content class="card_atras">\n\n  <ion-card-title class="blanco_1">\n\n  Reservación 3\n\n    </ion-card-title>\n\n  <p class="blanco_2">\n\n  Alfonso Martínez\n\n  </p>\n\n<ion-item class="card_atras">\n\n<ion-icon name="md-calendar" item-start></ion-icon>\n\n<h4 class="blanco_2">Fecha de reservación</h4>\n\n<ion-badge  class="negro" item-end>Sabado 31 Julio 2018 </ion-badge>\n\n</ion-item>\n\n<ion-item class="card_atras">\n\n<ion-icon name="md-cash" item-start></ion-icon>\n\n<a>Consumo total</a>\n\n<ion-badge  class="negro" item-end>$4000.00</ion-badge>\n\n</ion-item>\n\n<ion-item class="card_atras">\n\n<ion-icon name="md-add" item-start></ion-icon>\n\n<a>Más detalles</a>\n\n<ion-badge item-end></ion-badge>\n\n</ion-item>\n\n\n\n</ion-card-content>\n\n</ion-card> -->\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\historial\historial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], HistorialPage);
    return HistorialPage;
}());

//# sourceMappingURL=historial.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MisReservacionesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_monitoreo_reservas_monitoreo_reservas__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(178);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MisReservacionesPage = /** @class */ (function () {
    function MisReservacionesPage(navCtrl, navParams, monRes, barcodeScanner) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.monRes = monRes;
        this.barcodeScanner = barcodeScanner;
        this.encodText = '';
        this.encodeData = {};
        this.scannedData = {};
    }
    MisReservacionesPage.prototype.ionViewDidLoad = function () {
        this.getAllReservacione();
        this.getClientes();
    };
    MisReservacionesPage.prototype.getAllReservacione = function () {
        var _this = this;
        var id = 'fc5oP4WuwVWbrIhcF4mr6tzWKqA3';
        this.monRes.getMisreservaciones(id).then(function (r) {
            _this.reservaciones = r;
            console.log("Reservaciones: ", _this.reservaciones);
        });
    };
    MisReservacionesPage.prototype.getClientes = function () {
        var _this = this;
        this.monRes.getAllClientes("users").then(function (c) {
            _this.clientes = c;
            console.log("Estos son los clientes: ", _this.clientes);
        });
    };
    MisReservacionesPage.prototype.scan = function () {
        var _this = this;
        this.options = {
            prompt: 'putos todos'
        };
        this.barcodeScanner.scan(this.options).then(function (data) {
            _this.scannedData = data;
        }, function (err) {
            console.log('Error', err);
        });
    };
    MisReservacionesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mis-reservaciones',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\mis-reservaciones\mis-reservaciones.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n          </button>\n\n        <ion-title>Reservaciones en Curso</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="list-avatar-page">\n\n    <!-- <ion-searchbar [animated]="true" (ionInput)="getItems($event)"></ion-searchbar> -->\n\n    <ion-list>\n\n        <!-- <ion-list-header>Reservaciones Pendientes</ion-list-header> -->\n\n        <div *ngFor="let reservacion of reservaciones">\n\n            <ion-item *ngIf="reservacion.estatus != \'Terminado\' && reservacion.estatus!= \'EnProceso\'">\n\n                <ion-avatar item-start>\n\n                    <img src="../assets/imgs/icons/book.png">\n\n                </ion-avatar>\n\n                <ng-container *ngFor="let cliente of clientes">\n\n                    <h2 *ngIf="cliente.uid == reservacion.idUsuario">{{ cliente.displayName }} </h2>\n\n                </ng-container>\n\n                <p>{{ reservacion.estatus }} </p>\n\n                <p>{{ reservacion.fechaR_ | date : \'dd/MM/yyyy\'}} </p>\n\n                <ion-note item-end>{{ reservacion.hora }} </ion-note>\n\n                <!-- <button ion-button color="dark" round>\n\n              <ion-icon ios="ios-more" md="md-more"></ion-icon>\n\n            </button> -->\n\n                <!-- <button ion-button color="dark" round>\n\n                <ion-icon ios="ios-checkmark-circle-outline" md="md-checkmark-circle-outline"></ion-icon>\n\n              </button> -->\n\n            </ion-item>\n\n        </div>\n\n    </ion-list>\n\n    <!-- <ion-fab right bottom>\n\n        <button ion-fab color="dark" (click)="scan()"><ion-icon ios="ios-barcode" md="md-barcode"></ion-icon></button>\n\n    </ion-fab> -->\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\mis-reservaciones\mis-reservaciones.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_monitoreo_reservas_monitoreo_reservas__["a" /* MonitoreoReservasProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], MisReservacionesPage);
    return MisReservacionesPage;
}());

//# sourceMappingURL=mis-reservaciones.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NosotrosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the NosotrosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NosotrosPage = /** @class */ (function () {
    function NosotrosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NosotrosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NosotrosPage');
    };
    NosotrosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-nosotros',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\nosotros\nosotros.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Nosotros</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-slides  pager>\n\n\n\n    <ion-slide class="slide_1">\n\n    </ion-slide>\n\n\n\n    <ion-slide class="atras" >\n\n        <img class="slide_2" src="../assets/imgs/nosotros/2.png">\n\n        <p class="texto">Queremos que disfrutes de la noche sin inconvenientes, siendo tu la estrella,\n\nno más mesas dadas en los peores lugares, ni reservaciones pérdidas por no llegar a tiempo.\n\n<br><br>GUEST te brinda exclusividad, escoge el lugar que tu quieras con el presupuesto que tengas\n\nsin que pierdas tu mesa por no llegar a tiempo, paga todo desde nuestra app y solo llega a disfrutar!\n\n<br><br>Encuentra antros de tu ciudad, eventos próximos, los covers, dj\'s, costo de botellas y mucho más...\n\n    </ion-slide>\n\n\n\n  </ion-slides>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\nosotros\nosotros.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], NosotrosPage);
    return NosotrosPage;
}());

//# sourceMappingURL=nosotros.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_usuario_usuario__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, usuarioProv, afAuth) {
        this.navCtrl = navCtrl;
        this.usuarioProv = usuarioProv;
        this.afAuth = afAuth;
        this.user = {};
        console.log(this.usuarioProv.usuario);
        this.user = this.usuarioProv.usuario;
        this.afAuth.authState.subscribe(function (user) {
            console.log('AFAUTH!!');
            console.log(JSON.stringify(user));
        });
    }
    PerfilPage.prototype.salir = function () {
        var _this = this;
        this.afAuth.auth.signOut().then(function (res) {
            _this.usuarioProv.usuario = {};
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
        });
    };
    PerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-perfil',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\perfil\perfil.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Perfil</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <h3 text-center class="blanco"> Información del usuario </h3>\n\n    <ion-grid class="margen">\n\n        <ion-row>\n\n            <ion-col>\n\n                <img src="./assets/imgs/Logoblanco.png">\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n\n\n    <ion-list no-lines class="margen_2">\n\n        <ion-item text-center>\n\n            <!--Filtro de bandera de usuario-->\n\n            <div class="filt">\n\n                <img class="cate" src="../assets/imgs/icons/gold_guest.png" item-start>\n\n            </div>\n\n            <!--Imagen del usuario-->\n\n            <div class="">\n\n                <img class="imagen_perfil" [src]="user.imagen">\n\n            </div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/profile.png" item-start>\n\n            <div class="margen3">\n\n                <h4> Nombre:</h4> {{user.nombre}}</div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/correo.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Correo:</h4>{{ user.email}}</div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/cumple.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Cumpleaños:</h4>xxxx-xx-xx</div>\n\n        </ion-item>\n\n        <ion-item>\n\n            <img class="icon" src="../assets/imgs/icons/telefono.png" item-start>\n\n            <div class="margen3">\n\n                <h4>Teléfono:</h4> {{ user.phone }} </div>\n\n        </ion-item>\n\n        <ion-item *ngIf="user.provider == \'facebook\'">\n\n            <div class="margen3">\n\n                <h4>Sesión:</h4>Facebook</div>\n\n            <img class="icon" src="../assets/imgs/icons/facebook.png" item-start>\n\n        </ion-item>\n\n        <ion-item *ngIf="user.provider == \'google\'">\n\n            <div class="margen3">\n\n                <h4>Sesión:</h4>Google</div>\n\n            <img class="icon" src="../assets/imgs/icons/google.png" item-start>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <div class="margen3">\n\n                <h4>Tarjetas</h4>\n\n            </div>\n\n            <img class="icon" src="../assets/imgs/icons/tarjeta.png" item-start>\n\n            <ion-title>**** 5425</ion-title>\n\n            <ion-title>**** 6420</ion-title>\n\n        </ion-item>\n\n        <ion-item>\n\n            <button ion-button (click)="salir()" color="dark" block>\n\n        Cerrar Sesión\n\n      </button>\n\n        </ion-item>\n\n\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\perfil\perfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_usuario_usuario__["a" /* UsuarioProvider */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["AngularFireAuth"]])
    ], PerfilPage);
    return PerfilPage;
}());

//# sourceMappingURL=perfil.js.map

/***/ }),

/***/ 269:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 269;

/***/ }),

/***/ 29:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SucursalAltaProvider; });
/* unused harmony export Credenciales */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_operators__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import firebase from 'firebase'




var SucursalAltaProvider = /** @class */ (function () {
    function SucursalAltaProvider(afireauth, afiredatabase, toastCtrl, alertCtrl, loadinCtl, afs) {
        this.afireauth = afireauth;
        this.afiredatabase = afiredatabase;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.loadinCtl = loadinCtl;
        this.afs = afs;
        this.db = __WEBPACK_IMPORTED_MODULE_3_firebase__["firestore"]();
        this.data = {};
        this.sucursal = {};
        this.imagenes = [];
        this.selectedSucursalItem = new Credenciales();
        this.firedata = __WEBPACK_IMPORTED_MODULE_3_firebase__["database"]().ref("/sucursales");
    }
    SucursalAltaProvider.prototype.newRegister = function (newsucursal) {
        var _this = this;
        //var config = {apiKey: "AIzaSyBixlCb21nNbPSurY-Pvqu3hZB80Icl9Pk",
        //authDomain: "guestreservation-8b24b.firebaseapp.com",
        //databaseURL: "https://guestreservation-8b24b.firebaseio.com"};
        //var secondaryApp = firebase.initializeApp(config, "Secondary");
        this.afireauth.auth.createUserWithEmailAndPassword(newsucursal.email, newsucursal.password).then(function (firebaseUser) {
            _this.uid = firebaseUser.user.uid;
            console.log("User " + _this.uid + " created successfully!");
            _this.register(newsucursal, _this.uid);
        });
    };
    SucursalAltaProvider.prototype.register = function (newsucursal, uid) {
        this.afs
            .collection("sucursales").doc(uid)
            .set({
            uid: uid,
            displayName: newsucursal.sucursal,
            contacto: newsucursal.nombrecontacto,
            direccion: newsucursal.direccion,
            photoURL: "../assets/imgs/icons/home.png",
            email: newsucursal.email,
            password: newsucursal.password,
            status: newsucursal.status,
            tipo: newsucursal.tipo,
            type: 'a',
            tel: newsucursal.telefono,
            ciudad: newsucursal.ciudad,
            sucursal: true
        });
    };
    SucursalAltaProvider.prototype.cargarSucursal = function (sucursal, contacto, direccion, imagen, email, tel, ciudad, uid) {
        this.sucursal.displayName = sucursal;
        this.sucursal.contacto = contacto;
        this.sucursal.direccion = direccion;
        this.sucursal.imagen = imagen;
        this.sucursal.email = email;
        this.sucursal.tel = tel;
        this.sucursal.ciudad = ciudad;
        this.sucursal.uid = uid;
    };
    SucursalAltaProvider.prototype.getSucursal = function (uid) {
        // return this.afiredatabase.object("sucursales/" + uid);
        console.log("uid", uid);
        this.sucursalDoc = this.afs.collection("sucursales", function (ref) {
            return ref.where("uid", "==", uid);
        });
        this._sucursal = this.sucursalDoc.valueChanges();
        return (this._sucursal = this.sucursalDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    SucursalAltaProvider.prototype.getOneSucursal = function (idx) {
        this.Sucursal = this.afs.doc("sucursales/" + idx);
        return (this._Sucursal = this.Sucursal.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    SucursalAltaProvider.prototype.updateProfile = function (data) {
        console.log(data.uid);
        this.afs
            .collection("sucursales")
            .doc(data.uid)
            .update(data);
        // this.afiredatabase.database.ref("sucursales/" + data.uid).update(data);
    };
    SucursalAltaProvider.prototype.cargar_imagen_firebase = function (archivo) {
        var _this = this;
        var promesa = new Promise(function (resolve, reject) {
            _this.mostrar_toast("Cargando..");
            var storeRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["storage"]().ref();
            var photoURL = new Date().valueOf().toString();
            var uploadTask = storeRef
                .child("sucursales/" + photoURL + ".jpg")
                .putString(archivo.photoURL, "base64", { contentType: "image/jpeg" });
            uploadTask.on(__WEBPACK_IMPORTED_MODULE_3_firebase__["storage"].TaskEvent.STATE_CHANGED, function () { }, //saber el % cuantos se han subido
            function (//saber el % cuantos se han subido
                error) {
                //manejo
                console.log("Error en la carga");
                console.log(JSON.stringify(error));
                _this.mostrar_toast(JSON.stringify(error));
                reject();
            }, function () {
                // TODO BIEN!
                console.log("Archivo subido");
                _this.mostrar_toast("Imagen cargada correctamente");
                // let url = uploadTask.snapshot.downloadURL;
                // this.crear_post( archivo.titulo, url, nombreArchivo );
                // resolve();
                uploadTask.snapshot.ref
                    .getDownloadURL()
                    .then(function (urlImage) {
                    _this.crear_post(archivo.uid, urlImage);
                    _this.mostrar_toast("URL" + urlImage);
                })
                    .catch(function (error) {
                    console.log(error);
                });
                resolve();
            });
        });
        return promesa;
    };
    SucursalAltaProvider.prototype.crear_post = function (uid, url) {
        var sucursal = {
            uid: uid,
            photoURL: url
        };
        console.log(JSON.stringify(sucursal));
        this.afs
            .collection("sucursales")
            .doc(uid)
            .update(sucursal);
        // this.afiredatabase.object(`sucursales/`+uid).update(sucursal);
        this.imagenes.push(sucursal);
        this.mostrar_toast("Imagen actualizada");
    };
    SucursalAltaProvider.prototype.agregarArea = function (area, idSucursal) {
        var _this = this;
        this.db
            .collection("areas")
            .add({
            uidSucursal: idSucursal,
            nombre: area
        }).then(function (docRef) {
            _this.updateAreaIDX(docRef.id);
            _this.alertCtrl
                .create({
                title: "Se agregó correctamente",
                buttons: ["Aceptar"]
            })
                .present();
        })
            .catch(function (error) {
            console.error("Error adding document: ", JSON.stringify(error));
        });
    };
    SucursalAltaProvider.prototype.updateAreaIDX = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("areas")
                .doc(idx)
                .update({
                uid: idx
            })
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    SucursalAltaProvider.prototype.agregarZona = function (zona, consumoMin, idArea, idSucursal) {
        var _this = this;
        this.db
            .collection("zonas")
            .add({
            uidArea: idArea,
            nombre: zona,
            consumo: Number(consumoMin),
            uidSucursal: idSucursal
        }).then(function (docRef) {
            _this.updateZonaIDX(docRef.id);
            _this.alertCtrl
                .create({
                title: "Se agregó correctamente",
                buttons: ["Aceptar"]
            })
                .present();
        })
            .catch(function (error) {
            console.error("Error adding document: ", JSON.stringify(error));
        });
    };
    SucursalAltaProvider.prototype.updateZonaIDX = function (idx) {
        var _this = this;
        console.log("Esta es la dirección: ", idx);
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("zonas")
                .doc(idx)
                .update({
                uid: idx
            })
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    SucursalAltaProvider.prototype.verificarConsecutivo = function (idSucursal, idArea, idZona, numMesas, numPersonas) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.db
                .collection("mesas")
                .where("uidZona", "==", idZona)
                .get()
                .then(function (querySnapshot) {
                var arr = [];
                querySnapshot.forEach(function (doc) {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    obj.$key = doc.id;
                    // console.log(obj);
                    arr.push(obj);
                });
                if (arr.length > 0) {
                    // console.log("Document data:", arr);
                    var numero = arr.length;
                    // console.log("Este es el tamaño del arreglo: ",numero);
                    _this.agregarMesa(idSucursal, idArea, idZona, numMesas, numPersonas, numero);
                    resolve(arr);
                }
                else {
                    var numero1 = 0;
                    _this.agregarMesa(idSucursal, idArea, idZona, numMesas, numPersonas, numero1);
                    // console.log("No such document!");
                    resolve(null);
                }
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    SucursalAltaProvider.prototype.agregarMesa = function (idSucursal, idArea, idZona, numMesas, numPersonas, numero) {
        console.log("Numero recibido: ", numero);
        console.log("Sucur recibido: ", idSucursal);
        console.log("area recibido: ", idArea);
        console.log("Zona recibido: ", idZona);
        console.log("mesas recibido: ", numMesas);
        console.log("PErsonas recibido: ", numPersonas);
        for (var i = 1; i <= numMesas; i++) {
            // this.name  = this.Data.name
            // this.age= this.Data.age
            console.log("Este es el valor de I: ", i);
            this.db.collection("mesas").add({
                uidArea: idArea,
                uidZona: idZona,
                uidSucursal: idSucursal,
                mesa: numero + i,
                numPersonas: Number(numPersonas),
                estatus: "libre"
            });
        }
    };
    SucursalAltaProvider.prototype.eliminarZona = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("zonas")
                .doc(idx)
                .delete()
                .then(function () {
                _this.alertCtrl
                    .create({
                    title: "La zona se eliminó correctamente",
                    buttons: ["Aceptar"]
                })
                    .present();
                resolve(true);
            })
                .catch(function (err) {
                _this.alertCtrl
                    .create({
                    title: "Error al eliminar la zona. Intente de nuevo",
                    buttons: ["Aceptar"]
                })
                    .present();
                reject(err);
            });
        });
        return promise;
    };
    SucursalAltaProvider.prototype.eliminarArea = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("areas")
                .doc(idx)
                .delete()
                .then(function () {
                _this.alertCtrl
                    .create({
                    title: "El área se eliminó correctamente",
                    buttons: ["Aceptar"]
                })
                    .present();
                resolve(true);
            })
                .catch(function (err) {
                _this.alertCtrl
                    .create({
                    title: "Error al eliminar el área. Intente de nuevo",
                    buttons: ["Aceptar"]
                })
                    .present();
                reject(err);
            });
        });
        return promise;
    };
    SucursalAltaProvider.prototype.eliminarSucursal = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("sucursales")
                .doc(idx)
                .delete()
                .then(function () {
                _this.alertCtrl
                    .create({
                    title: "La sucursal se eliminó correctamente",
                    buttons: ["Aceptar"]
                })
                    .present();
                resolve(true);
            })
                .catch(function (err) {
                _this.alertCtrl
                    .create({
                    title: "Error al eliminar sucursal. Intente de nuevo",
                    buttons: ["Aceptar"]
                })
                    .present();
                reject(err);
            });
        });
        return promise;
    };
    SucursalAltaProvider.prototype.modificarZona = function (zona, consumoMin, idZona) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("zonas")
                .doc(idZona)
                .update({
                nombre: zona,
                consumo: consumoMin
            })
                .then(function () {
                resolve(true);
                _this.alertCtrl
                    .create({
                    title: "La zona se actualizó correctamente",
                    buttons: ["Aceptar"]
                })
                    .present();
            })
                .catch(function (err) {
                reject(err);
                _this.alertCtrl
                    .create({
                    title: "Error al actualizar la zona. Intente de nuevo",
                    buttons: ["Aceptar"]
                })
                    .present();
            });
        });
        return promise;
    };
    SucursalAltaProvider.prototype.modificarArea = function (area, idArea) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("areas")
                .doc(idArea)
                .update({
                nombre: area
            })
                .then(function () {
                resolve(true);
                _this.alertCtrl
                    .create({
                    title: "El área se actualizó correctamente",
                    buttons: ["Aceptar"]
                })
                    .present();
            })
                .catch(function (err) {
                reject(err);
                _this.alertCtrl
                    .create({
                    title: "Error al actualizar el área. Intente de nuevo",
                    buttons: ["Aceptar"]
                })
                    .present();
            });
        });
        return promise;
    };
    SucursalAltaProvider.prototype.mostrar_toast = function (mensaje) {
        var toast = this.toastCtrl
            .create({
            message: mensaje,
            duration: 3000
        })
            .present();
    };
    SucursalAltaProvider.prototype.getAreas = function (idx) {
        this.areasCollection = this.afs.collection("areas", function (ref) {
            return ref
                .where("uidSucursal", "==", idx);
        });
        this.areas = this.areasCollection.valueChanges();
        return (this.areas = this.areasCollection
            .snapshotChanges()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.id = action.payload.doc.id;
                return data;
            });
        })));
    };
    SucursalAltaProvider.prototype.getZonas = function (idx) {
        this.zonasCollection = this.afs.collection("zonas", function (ref) {
            return ref
                .where("uidSucursal", "==", idx);
        });
        this.zonas = this.zonasCollection.valueChanges();
        return (this.zonas = this.zonasCollection
            .snapshotChanges()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.id = action.payload.doc.id;
                return data;
            });
        })));
    };
    SucursalAltaProvider.prototype.getArea = function (idx) {
        this.Sucursal = this.afs.doc("areas/" + idx);
        return (this._Sucursal = this.Sucursal.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    SucursalAltaProvider.prototype.getZona = function (idx) {
        this.Sucursal = this.afs.doc("zonas/" + idx);
        return (this._Sucursal = this.Sucursal.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    SucursalAltaProvider.prototype.getMesas = function (idx) {
        this.areasCollection = this.afs.collection("mesas", function (ref) {
            return ref
                .where("uidZona", "==", idx).orderBy("mesa");
        });
        this.areas = this.areasCollection.valueChanges();
        return (this.areas = this.areasCollection
            .snapshotChanges()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.id = action.payload.doc.id;
                return data;
            });
        })));
    };
    SucursalAltaProvider.prototype.eliminarMesa = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("mesas")
                .doc(idx)
                .delete()
                .then(function () {
                _this.alertCtrl
                    .create({
                    title: "La mesa se eliminó correctamente",
                    buttons: ["Aceptar"]
                })
                    .present();
                resolve(true);
            })
                .catch(function (err) {
                _this.alertCtrl
                    .create({
                    title: "Error al eliminar la mesa. Intente de nuevo",
                    buttons: ["Aceptar"]
                })
                    .present();
                reject(err);
            });
        });
        return promise;
    };
    SucursalAltaProvider.prototype.modificarMesa = function (numMesa, numPersonas, idMesa) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("mesas")
                .doc(idMesa)
                .update({
                mesa: numMesa,
                numPersonas: numPersonas
            })
                .then(function () {
                resolve(true);
                _this.alertCtrl
                    .create({
                    title: "La mesa se actualizó correctamente",
                    buttons: ["Aceptar"]
                })
                    .present();
            })
                .catch(function (err) {
                reject(err);
                _this.alertCtrl
                    .create({
                    title: "Error al actualizar la mesa. Intente de nuevo",
                    buttons: ["Aceptar"]
                })
                    .present();
            });
        });
        return promise;
    };
    ///CUPONES///
    SucursalAltaProvider.prototype.agregarCupon = function (codigoCupon, sucursal, valorCupon, numCupones, condicion, fechaExp, fechaActual) {
        var _this = this;
        console.log("Datos en provider de cupon: ", codigoCupon, sucursal, valorCupon, numCupones, fechaExp, fechaActual);
        this.db.collection("cupones").add({
            codigoCupon: codigoCupon,
            idSucursal: sucursal,
            valorCupon: valorCupon,
            numCupones: numCupones,
            condicion: condicion,
            fechaExpiracion: fechaExp,
            fechaRegistro: fechaActual,
            estatus: "Activo"
        }).then(function (docRef) {
            _this.updateCuponIDX(docRef.id);
        })
            .catch(function (error) {
            console.error("Error adding document: ", JSON.stringify(error));
        });
    };
    //agrgar el ID del registro
    SucursalAltaProvider.prototype.updateCuponIDX = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("cupones")
                .doc(idx)
                .update({
                uid: idx
            })
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ///CIUDADES///
    SucursalAltaProvider.prototype.agregarCiudad = function (ciudad) {
        var _this = this;
        this.db.collection("ciudades").add({
            ciudad: ciudad
        }).then(function (docRef) {
            _this.updateCiudadIDX(docRef.id);
        })
            .catch(function (error) {
            console.error("Error adding document: ", JSON.stringify(error));
        });
    };
    //agrgar el ID del registro
    SucursalAltaProvider.prototype.updateCiudadIDX = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("ciudades")
                .doc(idx)
                .update({
                uid: idx
            })
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ///Establecimientos//
    SucursalAltaProvider.prototype.agregarEstablecimiento = function (establecimiento, ciudad) {
        var _this = this;
        this.db.collection("establecimientos").add({
            establecimiento: establecimiento,
            idCiudad: ciudad
        }).then(function (docRef) {
            _this.updateEstablecimientoIDX(docRef.id);
        })
            .catch(function (error) {
            console.error("Error adding document: ", JSON.stringify(error));
        });
    };
    //agrgar el ID del registro
    SucursalAltaProvider.prototype.updateEstablecimientoIDX = function (idx) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("establecimientos")
                .doc(idx)
                .update({
                uid: idx
            })
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    SucursalAltaProvider.prototype.getImagenSucursal = function (identificador) {
        console.log('Servicio getImagenSucursal inicio', identificador);
        this.Croquis = this.afs.collection("croquis_img", function (ref) { return ref.where('idSucursal', '==', identificador).orderBy('fecha_creado', 'asc'); });
        this._Croquis = this.Croquis.valueChanges();
        return (this._Croquis = this.Croquis.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
        //console.log('Servicio getImagenSucursal Fin', identificador);
    };
    SucursalAltaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], SucursalAltaProvider);
    return SucursalAltaProvider;
}());

var Credenciales = /** @class */ (function () {
    function Credenciales() {
    }
    return Credenciales;
}());

//# sourceMappingURL=sucursal-alta.js.map

/***/ }),

/***/ 311:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/admin-carta-edit/admin-carta-edit.module": [
		728,
		54
	],
	"../pages/admin-carta-home/admin-carta-home.module": [
		727,
		53
	],
	"../pages/admin-carta-image-edit/admin-carta-image-edit.module": [
		726,
		52
	],
	"../pages/admin-de-mon-user/admin-de-mon-user.module": [
		731,
		51
	],
	"../pages/admin-evento-edit/admin-evento-edit.module": [
		729,
		50
	],
	"../pages/admin-evento-image-edit/admin-evento-image-edit.module": [
		730,
		49
	],
	"../pages/admin-historial-reservaciones/admin-historial-reservaciones.module": [
		743,
		48
	],
	"../pages/admin-home/admin-home.module": [
		778,
		47
	],
	"../pages/admin-lee-qr/admin-lee-qr.module": [
		734,
		46
	],
	"../pages/admin-login/admin-login.module": [
		732,
		45
	],
	"../pages/admin-menu-reservacion/admin-menu-reservacion.module": [
		733,
		44
	],
	"../pages/admin-monitear-reserv/admin-monitear-reserv.module": [
		735,
		43
	],
	"../pages/admin-perfil-empleado/admin-perfil-empleado.module": [
		737,
		42
	],
	"../pages/admin-reservacion-detalle/admin-reservacion-detalle.module": [
		736,
		41
	],
	"../pages/admin-reservaciones-curso/admin-reservaciones-curso.module": [
		738,
		40
	],
	"../pages/admin-sucursal-croquis/admin-sucursal-croquis.module": [
		739,
		39
	],
	"../pages/admin-sucursal-editperfil-imagen/admin-sucursal-editperfil-imagen.module": [
		746,
		38
	],
	"../pages/admin-sucursal-editperfil/admin-sucursal-editperfil.module": [
		740,
		1
	],
	"../pages/admin-sucursal-list/admin-sucursal-list.module": [
		741,
		37
	],
	"../pages/admin-sucursal-perfil/admin-sucursal-perfil.module": [
		779,
		36
	],
	"../pages/admin-sucursal-subir/admin-sucursal-subir.module": [
		742,
		35
	],
	"../pages/admin-user-detail/admin-user-detail.module": [
		744,
		34
	],
	"../pages/admin-user-user/admin-user-user.module": [
		745,
		33
	],
	"../pages/admin-users-guest/admin-users-guest.module": [
		747,
		32
	],
	"../pages/admin-users-list/admin-users-list.module": [
		748,
		31
	],
	"../pages/admin-users/admin-users.module": [
		749,
		30
	],
	"../pages/administrar-reservaciones/administrar-reservaciones.module": [
		750,
		29
	],
	"../pages/adminmesas/adminmesas.module": [
		753,
		7
	],
	"../pages/agregar-ciudad/agregar-ciudad.module": [
		751,
		28
	],
	"../pages/agregar-cupones/agregar-cupones.module": [
		752,
		27
	],
	"../pages/carta/carta.module": [
		756,
		26
	],
	"../pages/ciudad-establecimiento/ciudad-establecimiento.module": [
		754,
		25
	],
	"../pages/corte-historial/corte-historial.module": [
		755,
		24
	],
	"../pages/corte-venta/corte-venta.module": [
		757,
		23
	],
	"../pages/cuentas/cuentas.module": [
		758,
		22
	],
	"../pages/cupones-sucursal/cupones-sucursal.module": [
		759,
		21
	],
	"../pages/detalle-cupon/detalle-cupon.module": [
		760,
		20
	],
	"../pages/evento-detalle/evento-detalle.module": [
		763,
		19
	],
	"../pages/eventos/eventos.module": [
		764,
		18
	],
	"../pages/generarqr/generarqr.module": [
		761,
		6
	],
	"../pages/historial/historial.module": [
		762,
		17
	],
	"../pages/imagencroquis/imagencroquis.module": [
		769,
		16
	],
	"../pages/login/login.module": [
		765,
		15
	],
	"../pages/mis-reservaciones/mis-reservaciones.module": [
		766,
		14
	],
	"../pages/modalmesas/modalmesas.module": [
		767,
		5
	],
	"../pages/modalstatus/modalstatus.module": [
		768,
		4
	],
	"../pages/modalstatus_cancelacion/modalstatus_cancelacion.module": [
		773,
		3
	],
	"../pages/modiareazona/modiareazona.module": [
		772,
		2
	],
	"../pages/nosotros/nosotros.module": [
		770,
		13
	],
	"../pages/perfil/perfil.module": [
		771,
		12
	],
	"../pages/producto-detalle/producto-detalle.module": [
		775,
		11
	],
	"../pages/reservacion-1/reservacion-1.module": [
		774,
		10
	],
	"../pages/reservaciones/reservaciones.module": [
		780,
		0
	],
	"../pages/resumen/resumen.module": [
		776,
		9
	],
	"../pages/tabs/tabs.module": [
		777,
		8
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 311;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminCartaSubirPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_carga_archivo_carta_carga_archivo__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { Camera, CameraOptions } from '@ionic-native/camera';




var AdminCartaSubirPage = /** @class */ (function () {
    function AdminCartaSubirPage(viewCtrl, 
        //private camera: Camera,
        imagePicker, _cap, firebase, navParams, afs) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.imagePicker = imagePicker;
        this._cap = _cap;
        this.firebase = firebase;
        this.navParams = navParams;
        this.afs = afs;
        this.titulo = "";
        this.categoria = "";
        this.precio = "";
        this.nota = "";
        this.imagenPreview = "";
        this.imagen64 = "";
        // ricibiendo parametro del uid de la sucursal
        this.sucursal = this.firebase.auth.currentUser;
        if (this.sucursal != null) {
            this.uid = this.sucursal.uid;
            //Cuando es un usuario se saca el id de la sucursal ala que pertenece
            this.afs.collection('users', function (ref) { return ref.where('uid', '==', _this.sucursal.uid); }).valueChanges().subscribe(function (data) {
                _this.sucursales = data;
                _this.sucursales.forEach(function (element) {
                    var tipoUser = element.type;
                    var uidSucursal = element.uidSucursal;
                    if (tipoUser == 'coordinacion' || tipoUser == 'rp' || tipoUser == 'capitan_mesero') {
                        _this.uid = uidSucursal;
                    }
                });
            });
        }
    }
    AdminCartaSubirPage.prototype.cerrar_modal = function () {
        this.viewCtrl.dismiss();
    };
    //   mostrar_camara(){
    //     const options: CameraOptions = {
    //   quality: 100,
    //   destinationType: this.camera.DestinationType.FILE_URI,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    //
    // this.camera.getPicture(options).then((imageData) => {
    //  // imageData is either a base64 encoded string or a file URI
    //  // If it's base64 (DATA_URL):
    //  this.imagenPreview = 'data:image/jpeg;base64,' + imageData;
    //  this.imagen64 = imageData;
    // }, (err) => {
    //  // Handle error
    //  console.log( "Error en la camara", JSON.stringify(err));
    // });
    //   }
    AdminCartaSubirPage.prototype.seleccionar_foto = function () {
        var _this = this;
        var opciones = {
            quality: 70,
            outputType: 1,
            maximumImagesCount: 1
        };
        this.imagePicker.getPictures(opciones).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                //console.log('Image URI: ' + results[i]);
                _this.imagenPreview = 'data:image/jpeg;base64,' + results[i];
                _this.imagen64 = results[i];
            }
        }, function (err) {
            console.log("Error en selector", JSON.stringify(err));
        });
    };
    AdminCartaSubirPage.prototype.crear_post = function () {
        var _this = this;
        var archivo = {
            img: this.imagen64,
            titulo: this.titulo,
            categoria: this.categoria,
            precio: this.precio,
            nota: this.nota,
            uidSucursal: this.uid
        };
        this._cap.cargar_imagen_firebase(archivo)
            .then(function () { return _this.cerrar_modal(); });
    };
    AdminCartaSubirPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-evento-subir',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-carta-subir\admin-evento-subir.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-buttons>\n\n        </ion-buttons>\n\n        <ion-title>{{ titulo | placeHolder:\'Crear nueva bebida\' }}</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content class="fondo_admin" padding>\n\n    <div class="warning">\n\n        <div style="text-align: center">\n\n\n\n            <img class="icon" height="43px" width="43px" src="../assets/imgs/icons/corona.png" item-start><br> Importante!!\n\n        </div>\n\n        <br> Para mejor la experiencia del usuario Guest, le recomendamos subir imágenes en formato PNG.\n\n    </div>\n\n    <br><br>\n\n    <ion-list>\n\n\n\n        <ion-item>\n\n            <ion-label floating>Titulo</ion-label>\n\n            <ion-input type="text" [(ngModel)]="titulo"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>Categoria</ion-label>\n\n            <ion-input type="text" [(ngModel)]="categoria"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>Precio</ion-label>\n\n            <ion-input type="number" [(ngModel)]="precio"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-textarea class="fond" placeholder="Descripción" [(ngModel)]="nota" name="nota" autocomplete="on" autocorrect="on"></ion-textarea>\n\n        </ion-item>\n\n        <ion-item *ngIf="imagenPreview">\n\n            <img [src]="imagenPreview">\n\n        </ion-item>\n\n    </ion-list>\n\n    <ion-grid>\n\n        <ion-row>\n\n            <ion-col>\n\n                <button color="dark" ion-button block icon-left (click)="seleccionar_foto()">\n\n            <ion-icon name="photos"></ion-icon>\n\n              Seleccionar\n\n        </button>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n        <ion-buttons end>\n\n            <button color="dark" ion-button [disabled]="titulo.length <= 1 || imagenPreview.length <= 1" (click)="crear_post()">\n\n              Crear post\n\n            </button>\n\n        </ion-buttons>\n\n        <!--\n\n\n\n        <ion-row>\n\n            <ion-col>\n\n                <button ion-button block icon-left (click)="mostrar_camara()">\n\n            <ion-icon name="camera"></ion-icon>\n\n              Camara\n\n        </button>\n\n            </ion-col>\n\n\n\n        </ion-row> -->\n\n    </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-carta-subir\admin-evento-subir.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__["a" /* ImagePicker */],
            __WEBPACK_IMPORTED_MODULE_3__providers_carga_archivo_carta_carga_archivo__["a" /* CargaArchivoCartaProvider */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], AdminCartaSubirPage);
    return AdminCartaSubirPage;
}());

//# sourceMappingURL=admin-evento-subir.js.map

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\home\home.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Guest Reservations</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-slides class="slide">\n\n\n\n        <ion-slide>\n\n            <img src="../assets/imgs/home/Hart.png" />\n\n        </ion-slide>\n\n\n\n        <ion-slide>\n\n            <img src="../assets/imgs/home/monroe.png" />\n\n        </ion-slide>\n\n\n\n        <ion-slide>\n\n            <img src="../assets/imgs/home/santa.png" />\n\n        </ion-slide>\n\n\n\n    </ion-slides><br>\n\n\n\n    <b class="blanco">Populares en Guest Reservations</b>\n\n\n\n    <br>\n\n    <ion-list class="list">\n\n        <ion-item-sliding>\n\n            <ion-item>\n\n                <ion-avatar item-start>\n\n                    <img src="../assets/imgs/places/santa.png">\n\n                </ion-avatar>\n\n                <h2>La Santa</h2>\n\n            </ion-item>\n\n            <ion-item-options side="left">\n\n                <button ion-button color="secondary">\n\n        <ion-icon name="call"></ion-icon>\n\n        Call\n\n      </button>\n\n            </ion-item-options>\n\n            <ion-item-options side="right">\n\n                <button ion-button color="primary">\n\n        <ion-icon name="mail"></ion-icon>\n\n        Email\n\n      </button>\n\n            </ion-item-options>\n\n        </ion-item-sliding>\n\n        <ion-item-sliding>\n\n            <ion-item>\n\n                <ion-avatar item-start>\n\n                    <img src="../assets/imgs/places/monroe.jpg">\n\n                </ion-avatar>\n\n                <h2>Monroe</h2>\n\n            </ion-item>\n\n            <ion-item-options side="left">\n\n                <button ion-button color="secondary">\n\n        <ion-icon name="call"></ion-icon>\n\n        Call\n\n      </button>\n\n            </ion-item-options>\n\n            <ion-item-options side="right">\n\n                <button ion-button color="primary">\n\n        <ion-icon name="mail"></ion-icon>\n\n        Email\n\n      </button>\n\n            </ion-item-options>\n\n        </ion-item-sliding>\n\n        <ion-item-sliding>\n\n            <ion-item>\n\n                <ion-avatar item-start>\n\n                    <img src="../assets/imgs/places/hart.png">\n\n                </ion-avatar>\n\n                <h2>Hart</h2>\n\n            </ion-item>\n\n            <ion-item-options side="left">\n\n                <button ion-button color="secondary">\n\n        <ion-icon name="call"></ion-icon>\n\n        Call\n\n      </button>\n\n            </ion-item-options>\n\n            <ion-item-options side="right">\n\n                <button ion-button color="primary">\n\n        <ion-icon name="mail"></ion-icon>\n\n        Email\n\n      </button>\n\n            </ion-item-options>\n\n        </ion-item-sliding>\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthProvider = /** @class */ (function () {
    function AuthProvider(angFireAuth, afs) {
        this.angFireAuth = angFireAuth;
        this.afs = afs;
    }
    AuthProvider.prototype.login = function (credentials) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.angFireAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password)
                .then(function (user) {
                localStorage.setItem("isLogin", "true");
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.logout = function () {
        localStorage.removeItem("isLogin");
    };
    AuthProvider.prototype.getUserBd = function (uid) {
        this.sucursalDoc = this.afs.doc("sucursales/" + uid);
        return this.sucursal = this.sucursalDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        }));
    };
    AuthProvider.prototype.getUserAdmins = function (uid) {
        this.adminDoc = this.afs.doc("users/" + uid);
        return this.admin = this.adminDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        }));
    };
    AuthProvider.prototype.getUserAdminsSucursal = function (uidSucursal) {
        this.adminUsDoc = this.afs.doc("sucursales/" + uidSucursal);
        return this.adminUs = this.adminUsDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        }));
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CartaProvider = /** @class */ (function () {
    function CartaProvider(afdb) {
        this.afdb = afdb;
        console.log("Hello CartaProvider Provider");
    }
    CartaProvider.prototype.getProduct = function (idProducto) {
        this.producto = this.afdb.doc("cartas/" + idProducto);
        return (this._producto = this.producto.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    CartaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], CartaProvider);
    return CartaProvider;
}());

//# sourceMappingURL=carta.js.map

/***/ }),

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminEventoHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_evento_subir_admin_evento_subir__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_carga_archivo_carga_archivo__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_toast_toast__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_admin_evento_edit_admin_evento_edit__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var AdminEventoHomePage = /** @class */ (function () {
    function AdminEventoHomePage(modalctrl, navCtrl, actionSheet, toastCtrl, _cap, afDB, afs, firebase) {
        var _this = this;
        this.modalctrl = modalctrl;
        this.navCtrl = navCtrl;
        this.actionSheet = actionSheet;
        this.toastCtrl = toastCtrl;
        this._cap = _cap;
        this.afDB = afDB;
        this.afs = afs;
        this.firebase = firebase;
        this.sucursal = this.firebase.auth.currentUser;
        console.log("sucursal acual");
        console.log(this.sucursal);
        console.log(this.sucursal.uid);
        if (this.sucursal != null) {
            this.uid = this.sucursal.uid;
            //Cuando es un usuario se saca el id de la sucursala ala que pertenece
            this.afs.collection('users', function (ref) { return ref.where('uid', '==', _this.uid); }).valueChanges().subscribe(function (data) {
                _this.sucursales = data;
                _this.sucursales.forEach(function (element) {
                    var uidSucursal = element.uidSucursal;
                    console.log("sucursal acual del user");
                    console.log(uidSucursal);
                    _this.eventos = _this.afs.collection('evento', function (ref) { return ref.where('uidSucursal', '==', uidSucursal); })
                        .valueChanges();
                });
            });
            this.eventos = this.afs.collection('evento', function (ref) { return ref.where('uidSucursal', '==', _this.uid); })
                .valueChanges();
        }
        // this.eventos = afDB.list('evento').valueChanges();
    }
    // doInfinite(infiniteScroll) {
    //   console.log('Begin async operation');
    //   this._cap.cargar_imagenes().then(
    //     (hayMas:boolean)=>{
    //       console.log(hayMas);
    //       this.hayMas = hayMas;
    //       infiniteScroll.complete();
    //     }
    //   );
    // }
    AdminEventoHomePage.prototype.mostrar_modal = function () {
        var modal = this.modalctrl.create(__WEBPACK_IMPORTED_MODULE_2__admin_evento_subir_admin_evento_subir__["a" /* AdminEventoSubirPage */]);
        modal.present();
    };
    AdminEventoHomePage.prototype.selectEventoItem = function (eventoItem) {
        var _this = this;
        /* Display an action that gives the user the following options
          1.Edit
          2. Delete
          3. Cancel
        */
        this.actionSheet.create({
            title: "" + eventoItem.titulo,
            buttons: [
                {
                    text: 'Editar',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_admin_evento_edit_admin_evento_edit__["a" /* AdminEventoEditPage */], _this._cap.selectedEventoItem = Object.assign({}, eventoItem));
                        console.log(eventoItem);
                    }
                },
                {
                    text: 'Delete',
                    role: 'destructive',
                    handler: function () {
                        //Delete the currente CartaItem
                        if (confirm('¿Estás seguro de eliminar este registro?')) {
                            _this._cap.deleteEvento(eventoItem.key, eventoItem.img, eventoItem.uid);
                            _this.toastCtrl.show(eventoItem.titulo + " deleted");
                        }
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log("El usuario cancelo.");
                    }
                }
            ]
        }).present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('myNav'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */])
    ], AdminEventoHomePage.prototype, "nav", void 0);
    AdminEventoHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-evento-home',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-evento-home\admin-evento-home.html"*/'<ion-header>\n\n\n\n    <ion-navbar class="borde">\n\n        <ion-row>\n\n            <ion-col col-1 align="center">\n\n                <img src="./assets/content/line2.png" class="imgLi" alt="">\n\n            </ion-col>\n\n            <ion-col col-8>\n\n                <div class="AdminEvento">Administrar de Eventos</div>\n\n                <div class="sucursal">Sucursal</div>\n\n            </ion-col>\n\n            <ion-col>\n\n                <div align=\'end\'>\n\n                    <img src="./assets/content/corona.png" style="width: 40%;" alt="">\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n    <ion-row>\n\n        <ion-col col-1></ion-col>\n\n        <ion-col>\n\n            <p class="lugar">\n\n                Eventos\n\n                <br>\n\n                <img src="./assets/content/line-hori.png" style="width: 40%;" alt="">\n\n            </p>\n\n            <p class="descrip">\n\n                Administración de Eventos\n\n            </p>\n\n        </ion-col>\n\n        <ion-col col-1></ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n        <ion-col col-1></ion-col>\n\n        <ion-col>\n\n            <ion-card class="cuadro">\n\n                EVENTOS\n\n            </ion-card>\n\n            <ion-card class="descripcion">\n\n                <ion-item *ngFor="let evento of eventos | async" (click)=" selectEventoItem(evento)" no-line>\n\n                    <ion-avatar item-start>\n\n                        <img [src]="evento.img">\n\n                    </ion-avatar>\n\n                    \n\n                    <h2>\n\n                        {{evento.titulo}}</h2>\n\n                    <p>{{evento.categoria}}</p>\n\n                </ion-item>\n\n            </ion-card>\n\n        </ion-col>\n\n        <ion-col col-1></ion-col>\n\n    </ion-row>\n\n\n\n    <!-- <ion-list>\n\n\n\n        <ion-list-header>\n\n            Administración de eventos\n\n        </ion-list-header>\n\n        <ion-item *ngFor="let evento of eventos | async" (click)=" selectEventoItem(evento)">\n\n            <ion-avatar item-start>\n\n                <img [src]="evento.img">\n\n            </ion-avatar>\n\n            <h2>{{evento.titulo}}</h2>\n\n            <p>{{evento.categoria}}</p>\n\n        </ion-item>\n\n    </ion-list> -->\n\n    <!-- <ion-fab left bottom>\n\n        <button ion-fab color="light">\n\n            <ion-icon name="arrow-dropright"></ion-icon>\n\n        </button>\n\n        <ion-fab-list side="right">\n\n            <button ion-fab (click)="mostrar_modal()">\n\n                <ion-icon name="add"></ion-icon>\n\n            </button>\n\n        </ion-fab-list>\n\n\n\n    </ion-fab> -->\n\n\n\n</ion-content>\n\n<ion-footer no-border>\n\n    <ion-grid>\n\n        <ion-row>\n\n            <ion-col col-3>\n\n            </ion-col>\n\n            <ion-col col-6>\n\n                <div text-center>\n\n                    <button ion-button round block class="crear" (click)="mostrar_modal()">Crear Evento</button>\n\n                </div>\n\n            </ion-col>\n\n            <ion-col col-3>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-evento-home\admin-evento-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_toast_toast__["a" /* ToastProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_carga_archivo_carga_archivo__["a" /* CargaArchivoProvider */],
            __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_7__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__["AngularFireAuth"]])
    ], AdminEventoHomePage);
    return AdminEventoHomePage;
}());

//# sourceMappingURL=admin-evento-home.js.map

/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminEventoSubirPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_carga_archivo_carga_archivo__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_user__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { Camera, CameraOptions } from '@ionic-native/camera';





var AdminEventoSubirPage = /** @class */ (function () {
    function AdminEventoSubirPage(viewCtrl, 
        //private camera: Camera,
        imagePicker, _cap, firebase, afs, platform, _provderUser) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.imagePicker = imagePicker;
        this._cap = _cap;
        this.firebase = firebase;
        this.afs = afs;
        this.platform = platform;
        this._provderUser = _provderUser;
        this.titulo = "";
        this.fecha = "";
        this.hora = "";
        this.hora_fin = "";
        this.categoria = "";
        this.lugar = "";
        this.obs = "";
        this.imagenPreview = "";
        this.imagen64 = "";
        // ricibiendo parametro del uid de la sucursal
        this.sucursal = this.firebase.auth.currentUser;
        if (this.sucursal != null) {
            this.uid = this.sucursal.uid;
            //Cuando es un usuario se saca el id de la sucursal ala que pertenece
            this.afs.collection('users', function (ref) { return ref.where('uid', '==', _this.sucursal.uid); }).valueChanges().subscribe(function (data) {
                _this.sucursales = data;
                _this.sucursales.forEach(function (element) {
                    var tipoUser = element.type;
                    var uidSucursal = element.uidSucursal;
                    if (tipoUser == 'coordinacion' || tipoUser == 'rp' || tipoUser == 'capitan_mesero') {
                        _this.uid = uidSucursal;
                        //Cuando es un usuario se saca el id de la sucursal ala que pertenece
                        _this.afs.collection('sucursales', function (ref) { return ref.where('uid', '==', _this.uid); }).valueChanges().subscribe(function (data2) {
                            _this.sucursales2 = data2;
                            _this.sucursales2.forEach(function (element2) {
                                _this.ciudadSucursal = element2.ciudad;
                                console.log('ciudad sucursal', _this.ciudadSucursal);
                            });
                        });
                    }
                });
            });
        }
        //obtener la ciudad de la sucursal para insertarla en el Evento
        this.afs.collection('sucursales', function (ref) { return ref.where('uid', '==', _this.uid); }).valueChanges().subscribe(function (data2) {
            _this.sucursales2 = data2;
            _this.sucursales2.forEach(function (element2) {
                _this.ciudadSucursal = element2.ciudad;
                console.log('ciudad sucursal', _this.ciudadSucursal);
            });
        });
    }
    AdminEventoSubirPage.prototype.ionViewDidLoad = function () {
    };
    AdminEventoSubirPage.prototype.cerrar_modal = function () {
        this.viewCtrl.dismiss();
    };
    //   mostrar_camara(){
    //     const options: CameraOptions = {
    //   quality: 100,
    //   destinationType: this.camera.DestinationType.FILE_URI,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    //
    // this.camera.getPicture(options).then((imageData) => {
    //  // imageData is either a base64 encoded string or a file URI
    //  // If it's base64 (DATA_URL):
    //  this.imagenPreview = 'data:image/jpeg;base64,' + imageData;
    //  this.imagen64 = imageData;
    // }, (err) => {
    //  // Handle error
    //  console.log( "Error en la camara", JSON.stringify(err));
    // });
    //   }
    AdminEventoSubirPage.prototype.seleccionar_foto = function () {
        var _this = this;
        var opciones = {
            quality: 70,
            outputType: 1,
            maximumImagesCount: 1
        };
        this.imagePicker.getPictures(opciones).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                //console.log('Image URI: ' + results[i]);
                _this.imagenPreview = 'data:image/jpeg;base64,' + results[i];
                _this.imagen64 = results[i];
            }
        }, function (err) {
            console.log("Error en selector", JSON.stringify(err));
        });
    };
    AdminEventoSubirPage.prototype.crear_post = function (ciudad) {
        var _this = this;
        var archivo = {
            img: this.imagen64,
            titulo: this.titulo,
            fecha: this.fecha,
            hora: this.hora,
            hora_fin: this.hora_fin,
            categoria: this.categoria,
            lugar: this.lugar,
            obs: this.obs,
            uidSucursal: this.uid,
            ciudad: ciudad
        };
        this._cap.cargar_imagen_firebase(archivo)
            .then(function () {
            _this.cerrar_modal();
            _this.getUsersPusNoti();
        });
    };
    AdminEventoSubirPage.prototype.getUsersPusNoti = function () {
        var _this = this;
        this._provderUser.getAllUsers().subscribe(function (users) {
            _this.users = users;
            console.log('Estos son los usuarios: ', _this.users);
            _this.users.forEach(function (user) {
                console.log("PlayerID: ", user.playerID);
                if (_this.platform.is("cordova")) {
                    var noti = {
                        app_id: "de05ee4f-03c8-4ff4-8ca9-c80c97c5c0d9",
                        include_player_ids: [user.playerID],
                        data: { foo: "bar" },
                        contents: {
                            en: " Nuevo Evento "
                        }
                    };
                    window["plugins"].OneSignal.postNotification(noti, function (successResponse) {
                        console.log("Notification Post Success:", successResponse);
                    }, function (failedResponse) {
                        console.log("Notification Post Failed: ", failedResponse);
                    });
                }
                else {
                    console.log("Solo funciona en dispositivos");
                }
            });
        });
    };
    AdminEventoSubirPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-evento-subir',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-evento-subir\admin-evento-subir.html"*/'<!-- <ion-header>\n\n\n\n    <ion-navbar>\n\n        <ion-buttons>\n\n            <button ion-button (click)="cerrar_modal()">\n\n              Cerrar\n\n            </button>\n\n        </ion-buttons>\n\n        <ion-title>{{ titulo | placeHolder:\'Crear nuevo evento\' }}</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header> -->\n\n\n\n<ion-header>\n\n\n\n    <ion-navbar class="borde">\n\n        <ion-row>\n\n            <ion-col col-2>\n\n                <div class="cerrar" (click)="cerrar_modal()">\n\n                    Cerrar\n\n                </div>\n\n            </ion-col>\n\n            <ion-col col-1 align="center">\n\n                <img src="./assets/content/line2.png" class="imgLi" alt="">\n\n            </ion-col>\n\n            <ion-col col-8>\n\n                <div class="AdminEvento">{{ titulo | placeHolder:\'Crear nuevo evento\' }}</div>\n\n                <div class="sucursal">Sucursal</div>\n\n            </ion-col>\n\n            <ion-col col-1>\n\n                <div>\n\n                    <img src="./assets/content/corona.png" style="width: 120%;" alt="">\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n    <ion-row>\n\n        <ion-col col-1></ion-col>\n\n        <ion-col>\n\n            <p class="lugar">\n\n                CREAR NUEVA EVENTO\n\n                <br>\n\n                <img src="./assets/content/line-hori.png" style="width: 40%;" alt="">\n\n            </p>\n\n            <p class="descrip">\n\n                Para mejorar la experiencia del usuario Ghest , le recomendamos subir imagenes en formato PNG\n\n            </p>\n\n        </ion-col>\n\n        <ion-col col-1></ion-col>\n\n    </ion-row>\n\n\n\n    <br>\n\n\n\n    <ion-row>\n\n        <ion-col></ion-col>\n\n        <ion-col>\n\n            <div *ngIf="imagenPreview">\n\n                <img [src]="imagenPreview" class="imgEvento">\n\n            </div>\n\n            <!-- <img src="./assets/content/1.jpg" class="imgEvento" alt=""> -->\n\n        </ion-col>\n\n        <ion-col></ion-col>\n\n    </ion-row>\n\n\n\n    <ion-list>\n\n\n\n        <ion-item no-lines>\n\n            <ion-label floating>Titulo</ion-label>\n\n            <ion-input type="text" class="letra" [(ngModel)]="titulo"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>Fecha </ion-label>\n\n            <ion-input type="date" class="letra" [(ngModel)]="fecha"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>Hora inicio</ion-label>\n\n            <ion-input type="time" class="letra" [(ngModel)]="hora"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>Hora fin</ion-label>\n\n            <ion-input type="time" class="letra" [(ngModel)]="hora_fin"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>Categoría <ion-icon name="md-arrow-dropdown" style="color: #FD9530;"></ion-icon>\n\n            </ion-label>\n\n            <ion-select [(ngModel)]="categoria">\n\n                <ion-option value="Deportivo">Deportivo</ion-option>\n\n                <ion-option value="Cultural">Cultural</ion-option>\n\n                <ion-option value="Académico">Académico</ion-option>\n\n                <ion-option value="Recreativo">Recreativo</ion-option>\n\n                <ion-option value="Social">Social</ion-option>\n\n                <ion-option value="Festival">Festival</ion-option>\n\n                <ion-option value="Homenaje">Homenaje</ion-option>\n\n                <ion-option value="Reconocimiento">Reconocimiento</ion-option>\n\n                <ion-option value="Feria">Feria</ion-option>\n\n                <ion-option value="Cumpleaños">Cumpleaños</ion-option>\n\n                <ion-option value="Familiy">Familiy Day</ion-option>\n\n                <ion-option value="Simposio">Simposio</ion-option>\n\n                <ion-option value="Otro">Otro</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label>Lugar</ion-label>\n\n            <ion-input type="text" class="letra" [(ngModel)]="lugar"></ion-input>\n\n        </ion-item>\n\n        <ion-card class="cardDes">\n\n            <ion-item class="fond" no-lines>\n\n                <ion-textarea class="fond" placeholder="Descripción" [(ngModel)]="obs" name="obs" autocomplete="on"\n\n                    autocorrect="on"></ion-textarea>\n\n            </ion-item>\n\n        </ion-card>\n\n        <!-- <ion-item *ngIf="imagenPreview">\n\n            <img [src]="imagenPreview">\n\n        </ion-item> -->\n\n    </ion-list>\n\n    <ion-grid>\n\n        <ion-row>\n\n            <ion-col>\n\n                <button class="btnimg" ion-button block icon-left (click)="seleccionar_foto()">\n\n                    <ion-icon name="md-reverse-camera"></ion-icon>\n\n                    Seleccionar\n\n                </button>\n\n\n\n                <br>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n            <ion-col></ion-col>\n\n            <ion-col>\n\n                <button class="btnimgs" ion-button [disabled]="titulo.length <= 1 || imagenPreview.length <= 1"\n\n                    (click)="crear_post(ciudadSucursal)">\n\n                    Crear post\n\n                </button>\n\n            </ion-col>\n\n            <ion-col></ion-col>\n\n        </ion-row>\n\n        <!-- <ion-buttons end>\n\n            <--|| imagenPreview.length <= 1--\n\n            <button color="dark" ion-button [disabled]="titulo.length <= 1 || imagenPreview.length <= 1"\n\n                (click)="crear_post(ciudadSucursal)">\n\n                Crear post\n\n            </button> \n\n        </ion-buttons>-->\n\n        <!-- <ion-row>\n\n            <ion-col>\n\n                <button ion-button block icon-left (click)="mostrar_camara()">\n\n            <ion-icon name="camera"></ion-icon>\n\n              Camara\n\n        </button>\n\n            </ion-col>\n\n\n\n        </ion-row> -->\n\n    </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-evento-subir\admin-evento-subir.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_image_picker__["a" /* ImagePicker */],
            __WEBPACK_IMPORTED_MODULE_3__providers_carga_archivo_carga_archivo__["a" /* CargaArchivoProvider */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_5__providers_user_user__["a" /* UserProvider */]])
    ], AdminEventoSubirPage);
    return AdminEventoSubirPage;
}());

//# sourceMappingURL=admin-evento-subir.js.map

/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CargaCroquisProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_database__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CargaCroquisProvider = /** @class */ (function () {
    function CargaCroquisProvider(toastCtrl, afDB) {
        this.toastCtrl = toastCtrl;
        this.afDB = afDB;
        this.db = __WEBPACK_IMPORTED_MODULE_3_firebase__["firestore"]();
        console.log('Hello CargaCroquisProvider Provider');
    }
    CargaCroquisProvider.prototype.cargarImagen = function (archivo) {
        var _this = this;
        var promesa = new Promise(function (resolve, reject) {
            _this.mostrarToast('Cargando...');
            var storeRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["storage"]().ref();
            var nombreArchivo = new Date().valueOf().toString();
            var uploadTask = storeRef.child("croquis/" + nombreArchivo)
                .putString(archivo.plano, 'base64', { contentType: 'image/jpeg' });
            uploadTask.on(__WEBPACK_IMPORTED_MODULE_3_firebase__["storage"].TaskEvent.STATE_CHANGED, function () { }, //Saber el % de Mbs se han subido
            function (error) {
                //Manejo de error
                console.log("Error en la carga");
                console.log(JSON.stringify(error));
                _this.mostrarToast(JSON.stringify(error));
                reject();
            }, function () {
                // TODO bien
                console.log("Archivo subido");
                _this.mostrarToast('El croquis se agrego correctamente');
                //let url = uploadTask.snapshot.ref.getDownloadURL();
                //Inician mis pruebas
                uploadTask.snapshot.ref.getDownloadURL().then(function (urlImage) {
                    _this.actualizaImagen(archivo.key, urlImage);
                    // this.mostrarToast('URL:' + urlImage);
                }).catch(function (error) {
                    console.log(error);
                });
                resolve();
            });
        });
        return promesa;
    };
    // private actualizaImagen(key, url: string) {
    //   let croquis: ArchivoSubir = {      
    //     plano: url
    //   };
    //   console.log(JSON.stringify(croquis));
    //   this.afDB.object('sucursales/'+key+'/arquitectura/').update(croquis);
    //   this.afDB.object('su')
    //   // this.afDB.object('users/'+this.uid+'/perfil/').update(perfil);
    //   this.mostrarToast('Imagen actualizada');
    // }
    CargaCroquisProvider.prototype.actualizaImagen = function (key, url) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.db
                .collection("sucursales")
                .doc(key)
                .update({
                plano: url
            })
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    CargaCroquisProvider.prototype.mostrarToast = function (mensaje) {
        this.toastCtrl.create({
            message: mensaje,
            duration: 2000
        }).present();
    };
    CargaCroquisProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_database__["AngularFireDatabase"]])
    ], CargaCroquisProvider);
    return CargaCroquisProvider;
}());

//# sourceMappingURL=carga-croquis.js.map

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResumenProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ResumenProvider = /** @class */ (function () {
    function ResumenProvider(af) {
        this.af = af;
        console.log("Hello ResumenProvider Provider");
    }
    ResumenProvider.prototype.gerReservacion = function (idx) {
        this.reservacion = this.af.doc("reservaciones/" + idx);
        // this.pedidoDoc = this.afs.collection<Servicios>('servicios').doc(`/${idPedido}`).collection<Pedidos>('pedidos');
        return (this._reservacion = this.reservacion.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    ResumenProvider.prototype.getSucursal = function (idx) {
        this.sucursales = this.af.doc("sucursales/" + idx);
        // this.pedidoDoc = this.afs.collection<Servicios>('servicios').doc(`/${idPedido}`).collection<Pedidos>('pedidos');
        return (this._sucursales = this.sucursales.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    ResumenProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], ResumenProvider);
    return ResumenProvider;
}());

//# sourceMappingURL=resumen.js.map

/***/ }),

/***/ 505:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminDeMonUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminDeMonUserPage = /** @class */ (function () {
    function AdminDeMonUserPage(navCtrl, navParams, afs) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afs = afs;
        this.user = {};
        this.terminados = [];
        this.cancelados = [];
        this.idUser = this.navParams.get('idUser');
        console.log('idUser', this.idUser);
        this.afs.collection('users').doc(this.idUser).valueChanges().subscribe(function (user) {
            _this.user = user;
            console.log('user doc', _this.user);
        });
        this.getTerminados(this.idUser);
        this.getCancelados(this.idUser);
    }
    AdminDeMonUserPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminDeMonUserPage');
    };
    AdminDeMonUserPage.prototype.getTerminados = function (id) {
        var _this = this;
        this.afs.collection('reservaciones', function (ref) { return ref
            .where('idUsuario', '==', id)
            .where('estatus', '==', 'Terminados'); })
            .valueChanges().subscribe(function (ter) {
            _this.terminados = ter;
            console.log('terminados', ter);
        });
    };
    AdminDeMonUserPage.prototype.getCancelados = function (id) {
        var _this = this;
        this.afs.collection('reservaciones', function (ref) { return ref
            .where('idUsuario', '==', id)
            .where('estatus', '==', 'Cancelados'); })
            .valueChanges().subscribe(function (ter) {
            _this.cancelados = ter;
            console.log('cancelados', ter);
        });
    };
    AdminDeMonUserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-de-mon-user',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-de-mon-user\admin-de-mon-user.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Detalle usuario</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="cards-bg fondo_admin">\n\n    <div text-center>\n\n        <ion-row>\n\n            <ion-col class="text-center">\n\n                <img style="border-radius:65px;" height="150" width="150" [src]="this.user.photoURL" />\n\n            </ion-col>\n\n        </ion-row>\n\n    </div>\n\n    <ion-card>\n\n\n\n        <!-- <img style="border-radius:65px;" height="150" width="150" src="../assets/imgs/icons/book.png" /> -->\n\n\n\n        <ion-card-content>\n\n            <ion-card-title text-center>\n\n                {{ this.user.displayName }}\n\n            </ion-card-title>\n\n            <p>\n\n                <!-- <img class="cate" src="../assets/imgs/icons/gold_guest.png" /> -->\n\n            </p>\n\n        </ion-card-content>\n\n\n\n        <ion-item>\n\n            <ion-icon ios="ios-close-circle" md="md-close-circle" item-start style="color: #EA2027"></ion-icon>\n\n            Reservaciones canceladas\n\n            <ion-badge item-end>{{ cancelados.length }} </ion-badge>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-icon ios="ios-checkmark-circle-outline" md="md-checkmark-circle-outline" item-start style="color: #4cd137"></ion-icon>\n\n            Reservaciones\n\n            <ion-badge item-end>{{ terminados.length }} </ion-badge>\n\n        </ion-item>\n\n\n\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-de-mon-user\admin-de-mon-user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], AdminDeMonUserPage);
    return AdminDeMonUserPage;
}());

//# sourceMappingURL=admin-de-mon-user.js.map

/***/ }),

/***/ 506:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResumenPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_resumen_resumen__ = __webpack_require__(463);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ResumenPage = /** @class */ (function () {
    function ResumenPage(navCtrl, navParams, _resumenProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._resumenProvider = _resumenProvider;
        this.r = {};
    }
    ResumenPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResumenPage');
        this.loadNavParams();
        this.getReservacion();
    };
    ResumenPage.prototype.loadNavParams = function () {
        this.idSucursal = this.navParams.get("idSucursal");
        this.idReservacion = this.navParams.get("idReservacion");
        this.uidEvento = this.navParams.get('uid');
    };
    ResumenPage.prototype.getReservacion = function () {
        var _this = this;
        this._resumenProvider.gerReservacion(this.idReservacion).subscribe(function (reservacion) {
            _this.r = reservacion;
            console.log('Reserva: ', reservacion);
            var month = __WEBPACK_IMPORTED_MODULE_2_moment__(Number(reservacion.fechaR_));
            month.locale("es");
            console.log("Mes", month.format("MMM"));
            console.log("Día", month.format("DD"));
            console.log("Día", month.format("dddd"));
            _this.mes = month.format("MMM");
            _this.diasN = month.format("DD");
            _this.dias = month.format("dddd");
        });
    };
    ResumenPage.prototype.getSucursal = function () {
        this._resumenProvider.getSucursal(this.idSucursal).subscribe(function (sucursal) {
            console.log('Sucursal', sucursal);
        });
    };
    ResumenPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-resumen',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\resumen\resumen.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>resumen</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <ion-card class="_card">\n\n\n\n        <ion-card-content>\n\n            <ion-grid>\n\n                <ion-row>\n\n                    <ion-col col-3>\n\n                        <div text-center text-uppercase class="text_color">\n\n                            {{mes}}\n\n                        </div>\n\n                        <div text-center class="text_color">\n\n                            <b>{{diasN}}</b>\n\n                        </div>\n\n                        <div text-center class="text_color">\n\n                            {{dias}}\n\n                        </div>\n\n                    </ion-col>\n\n                    <ion-col col-9>\n\n                        <div class="sucursal">\n\n                            Aromas\n\n                        </div>\n\n                        <br>\n\n                        <div class="">\n\n                            Reservación para {{r.numPersonas}} personas\n\n                        </div>\n\n                        <br>\n\n                        <div text-uppercase>{{r.hora}}</div>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-grid>\n\n        </ion-card-content>\n\n\n\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\resumen\resumen.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_resumen_resumen__["a" /* ResumenProvider */]])
    ], ResumenPage);
    return ResumenPage;
}());

//# sourceMappingURL=resumen.js.map

/***/ }),

/***/ 507:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(508);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(640);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MonitoreoReservasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase_app__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_stripe__ = __webpack_require__(177);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MonitoreoReservasProvider = /** @class */ (function () {
    function MonitoreoReservasProvider(afs, loadinCtl, http, stripe) {
        this.afs = afs;
        this.loadinCtl = loadinCtl;
        this.http = http;
        this.stripe = stripe;
        this.db = __WEBPACK_IMPORTED_MODULE_1_firebase_app__["firestore"]();
        console.log('Hello MonitoreoReservasProvider Provider');
    }
    MonitoreoReservasProvider.prototype.getReservaciones = function (idSucursal, fechaActual) {
        this.reservacionesCollection = this.afs.collection("reservaciones", function (ref) {
            return ref.where("idSucursal", "==", idSucursal)
                .where("estatus", '==', 'Aceptado')
                .where("fechaR", '>=', fechaActual);
        });
        this.reservaciones = this.reservacionesCollection.valueChanges();
        return (this.reservaciones = this.reservacionesCollection.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    MonitoreoReservasProvider.prototype.getReservacionesAcepCom = function (idSucursal, fechaActual) {
        this.reservacionesAcepComCollection = this.afs.collection("reservaciones", function (ref) {
            return ref.where("idSucursal", "==", idSucursal)
                .where("estatus", '==', 'AceptadoCompartida')
                .where("fechaR", '>=', fechaActual);
        });
        this.reservacionesAcepCom = this.reservacionesAcepComCollection.valueChanges();
        return (this.reservacionesAcepCom = this.reservacionesAcepComCollection.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    MonitoreoReservasProvider.prototype.getReservacionesCurso = function (idSucursal, fechaActual) {
        this.reservacionesCursoCollection = this.afs.collection("reservaciones", function (ref) {
            return ref.where("idSucursal", "==", idSucursal)
                .where("estatus", '==', 'Pagando')
                .where("fechaR", '==', fechaActual);
        });
        this.reservacionesCurso = this.reservacionesCursoCollection.valueChanges();
        return (this.reservacionesCurso = this.reservacionesCursoCollection.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    MonitoreoReservasProvider.prototype.getReservacionesDetalle = function (idReservacion) {
        this.reservacionesCollection = this.afs.collection("reservaciones", function (ref) {
            return ref.where("idReservacion", "==", idReservacion)
                .orderBy('fechaR_', "asc");
        });
        this.reservaciones = this.reservacionesCollection.valueChanges();
        return (this.reservaciones = this.reservacionesCollection.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    MonitoreoReservasProvider.prototype.getReserCom = function (idx) {
        // return this.afiredatabase.object("sucursales/" + uid);
        console.log("idReservacion", idx);
        this.reservaInfo2 = this.afs.collection("compartidas", function (ref) {
            return ref.where("idReservacion", "==", idx);
        });
        this._reservaInfo2 = this.reservaInfo2.valueChanges();
        return (this._reservaInfo2 = this.reservaInfo2.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    MonitoreoReservasProvider.prototype.getInfo = function (idx) {
        // return this.afiredatabase.object("sucursales/" + uid);
        console.log("idReservacion", idx);
        this.reservaInfo3 = this.afs.collection("reservaciones", function (ref) {
            return ref.where("idReservacion", "==", idx);
        });
        this._reservaInfo3 = this.reservaInfo3.valueChanges();
        return (this._reservaInfo3 = this.reservaInfo3.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    MonitoreoReservasProvider.prototype.getReservacionesPagando = function (idSucursal) {
        this.reservacionesCollection = this.afs.collection("reservaciones", function (ref) {
            return ref.where("idSucursal", "==", idSucursal)
                .where("estatus", '==', 'Pagando')
                .orderBy('fechaR_', "asc");
        });
        this.reservaciones = this.reservacionesCollection.valueChanges();
        return (this.reservaciones = this.reservacionesCollection.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    MonitoreoReservasProvider.prototype.getAllClientes = function (collection) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.db
                .collection(collection)
                .where("type", "==", "u")
                .get()
                .then(function (querySnapshot) {
                var arr = [];
                querySnapshot.forEach(function (doc) {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    obj.$key = doc.id;
                    console.log(obj);
                    arr.push(obj);
                });
                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                }
                else {
                    console.log("No such document!");
                    resolve(null);
                }
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    //Sacr el total de productos de una reservacion
    MonitoreoReservasProvider.prototype.getProductos = function (idx) {
        this.areas = this.afs.collection("productos", function (ref) {
            return ref.where("idReservacion", "==", idx);
        });
        this._areas = this.areas.valueChanges();
        return (this._areas = this.areas.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    MonitoreoReservasProvider.prototype.getMisreservaciones = function (idUsuario) {
        var _this = this;
        // this.afs.collection('reservaciones', ref => ref.where('idSucursal', '==', idSucursal)
        return new Promise(function (resolve, reject) {
            _this.db
                .collection('reservaciones')
                .orderBy('fechaR_', "asc")
                .get()
                .then(function (querySnapshot) {
                var arr = [];
                querySnapshot.forEach(function (doc) {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    obj.$key = doc.id;
                    console.log(obj);
                    arr.push(obj);
                });
                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                }
                else {
                    console.log("No such document!");
                    resolve(null);
                }
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    MonitoreoReservasProvider.prototype.getReservacion = function (idReservacion) {
        console.log(idReservacion);
        this.reservacionDoc = this.afs.doc("reservaciones/" + idReservacion);
        return this.reservacion = this.reservacionDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.idReservacion = action.payload.id;
                return data;
            }
        }));
    };
    MonitoreoReservasProvider.prototype.getArea = function (idArea) {
        console.log(idArea);
        this.areaDoc = this.afs.doc("areas/" + idArea);
        return this.area = this.areaDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.idReservacion = action.payload.id;
                return data;
            }
        }));
    };
    MonitoreoReservasProvider.prototype.getZona = function (idZona) {
        console.log(idZona);
        this.zonaDoc = this.afs.doc("zonas/" + idZona);
        return this.area = this.zonaDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.idReservacion = action.payload.id;
                return data;
            }
        }));
    };
    MonitoreoReservasProvider.prototype.getAllMesas = function (idReserv) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.db
                .collection('reservaciones_mesas')
                .where("idReservacion", "==", idReserv)
                .get()
                .then(function (querySnapshot) {
                var arr = [];
                querySnapshot.forEach(function (doc) {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    obj.$key = doc.id;
                    console.log(obj);
                    arr.push(obj);
                });
                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                }
                else {
                    console.log("No such document!");
                    resolve(null);
                }
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    MonitoreoReservasProvider.prototype.getEvento = function (idEvento) {
        console.log(idEvento);
        this.eventoDoc = this.afs.doc("evento/" + idEvento);
        return this.area = this.eventoDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.idReservacion = action.payload.id;
                return data;
            }
        }));
    };
    MonitoreoReservasProvider.prototype.getAllProductos = function (idReserv) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.db
                .collection('productos')
                .where("idReservacion", "==", idReserv)
                .get()
                .then(function (querySnapshot) {
                var arr = [];
                querySnapshot.forEach(function (doc) {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    obj.$key = doc.id;
                    console.log(obj);
                    arr.push(obj);
                });
                if (arr.length > 0) {
                    console.log("Document data:", arr);
                    resolve(arr);
                }
                else {
                    console.log("No such document!");
                    resolve(null);
                }
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    MonitoreoReservasProvider.prototype.getDatosTarjeta = function (idTarjeta) {
        console.log(idTarjeta);
        this.tarjetaDoc = this.afs.doc("tarjetas/" + idTarjeta);
        return this.tarjeta = this.tarjetaDoc.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.idTarjeta = action.payload.id;
                return data;
            }
        }));
    };
    MonitoreoReservasProvider.prototype.cambiaPagando = function (uidRerservacion, numTarjeta, mesExpiracion, anioExpiracion, cvc, montoReservacion) {
        var _this = this;
        console.log('llegaron a pagar', uidRerservacion, numTarjeta, mesExpiracion, anioExpiracion, cvc, montoReservacion);
        // Poppup de carga para procesar el metodo
        var loading = this.loadinCtl.create({
            spinner: "bubbles",
            content: "Procesando pago."
        }); //
        loading.present();
        /* Cambiando estatus a la reservación  */
        this.afs.collection('reservaciones').doc(uidRerservacion).update({
            estatus: 'Pagando'
        });
        alert('Estatus actualizado');
        //
        this.stripe.setPublishableKey('pk_test_TWx1xbw2HExTUYjy2Hz44koG00nFNYC3J4');
        //
        var card = {
            number: '4242424242424242',
            expMonth: 12,
            expYear: 2020,
            cvc: '220' //cvc
        };
        this.stripe.createCardToken(card)
            .then(function (token) {
            var headers = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Headers */]({
                "Content-Type": "application/json"
            });
            var options = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["d" /* RequestOptions */]({ headers: headers });
            var url = "https://proyectosinternos.com/guest_pagos/stripe_config.php";
            var data = JSON.stringify({
                cardToken: token.id,
                amount: '5000',
                accion: 'stripe_prueba'
            });
            _this.http.post(url, data, options).subscribe(function (res) {
                console.log('Este es el mensaje', JSON.stringify(res));
                if (res.json().status == "succeeded") {
                    var title = "¡ Pago con exito !";
                    alert('Pago con exito!');
                    setTimeout(function () {
                        loading.dismiss();
                    }, 3000);
                }
            });
        })
            .catch(function (error) {
            alert('error!');
            alert(error);
            setTimeout(function () {
                loading.dismiss();
            }, 3000);
        });
    };
    MonitoreoReservasProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_stripe__["a" /* Stripe */]])
    ], MonitoreoReservasProvider);
    return MonitoreoReservasProvider;
}());

//# sourceMappingURL=monitoreo-reservas.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReservacionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReservacionProvider = /** @class */ (function () {
    function ReservacionProvider(af) {
        this.af = af;
        console.log("Hello ReservacionProvider Provider");
    }
    ReservacionProvider.prototype.getAreas = function (idx) {
        // return this.afiredatabase.object("sucursales/" + uid);
        console.log("idSucursal", idx);
        this.areas = this.af.collection("areas", function (ref) {
            return ref.where("uidSucursal", "==", idx);
        });
        this._areas = this.areas.valueChanges();
        return (this._areas = this.areas.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    ReservacionProvider.prototype.getZonas = function (idx, area) {
        // return this.afiredatabase.object("sucursales/" + uid);
        console.log("idSucursal", idx);
        this.areas = this.af.collection("zonas", function (ref) {
            return ref.where("uidSucursal", "==", idx).where("uidArea", "==", area);
        });
        this._areas = this.areas.valueChanges();
        return (this._areas = this.areas.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    ReservacionProvider.prototype.getMesas = function (idx, area, zona) {
        // return this.afiredatabase.object("sucursales/" + uid);
        console.log("idSucursal", idx);
        this.areas = this.af.collection("mesas", function (ref) {
            return ref
                .where("uidSucursal", "==", idx)
                .where("uidArea", "==", area)
                .where("uidZona", "==", zona);
        });
        this._areas = this.areas.valueChanges();
        return (this._areas = this.areas.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    ReservacionProvider.prototype.getMesas2 = function (idx) {
        // return this.afiredatabase.object("sucursales/" + uid);
        console.log("getMesas2 idSucursal", idx);
        this.areas = this.af.collection("mesas", function (ref) {
            return ref
                .where("uidSucursal", "==", idx).orderBy("noMesa", "asc");
        });
        this._areas = this.areas.valueChanges();
        return (this._areas = this.areas.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    ReservacionProvider.prototype.getImagenSucursal = function (identificador) {
        console.log('Servicio getImagenSucursal inicio', identificador);
        this.areas = this.af.collection("croquis_img", function (ref) { return ref.where('idSucursal', '==', identificador).orderBy('fecha_creado', 'asc'); });
        this._areas = this.areas.valueChanges();
        return (this._areas = this.areas.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
        //console.log('Servicio getImagenSucursal Fin', identificador);
    };
    ReservacionProvider.prototype.saveReservacion = function (reservacion) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var fecha = __WEBPACK_IMPORTED_MODULE_3_moment__(reservacion.fecha).format("x");
            var hora = __WEBPACK_IMPORTED_MODULE_3_moment__(reservacion.hora, "HH:mm").format("hh:mm a");
            var idUsuario = localStorage.getItem("uid");
            console.log("Evento: ", reservacion.idevento);
            _this.af
                .collection("reservaciones")
                .add({
                numPersonas: reservacion.numPersonas,
                hora: hora,
                fechaR: reservacion.fecha,
                fechaR_: fecha,
                estatus: "Creando",
                idArea: reservacion.area,
                idZona: reservacion.zona,
                idSucursal: reservacion.idSucursal,
                idevento: reservacion.idevento,
                idUsuario: idUsuario
            })
                .then(function (reserva) {
                console.log("Reservación exitosa: ", reserva.id);
                _this.updateReservaId(reserva.id);
                resolve({ success: true, idReservacion: reserva.id });
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ReservacionProvider.prototype.updateReservacion = function (idx, reservacion) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var fecha = __WEBPACK_IMPORTED_MODULE_3_moment__(reservacion.fecha).format("x");
            var hora = __WEBPACK_IMPORTED_MODULE_3_moment__(reservacion.hora, "HH:mm").format("hh:mm a");
            var idUsuario = localStorage.getItem("uid");
            console.log("Evento: ", reservacion.idevento);
            _this.af
                .collection("reservaciones")
                .doc(idx)
                .update({
                numPersonas: reservacion.numPersonas,
                hora: hora,
                fechaR: reservacion.fecha,
                fechaR_: fecha,
                estatus: "Creando",
                idArea: reservacion.area,
                idZona: reservacion.zona,
                idSucursal: reservacion.idSucursal,
                idevento: reservacion.idevento,
                idUsuario: idUsuario
            })
                .then(function (reserva) {
                console.log("Reservación actualizada: ", JSON.stringify(reserva));
                resolve({ success: true });
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ReservacionProvider.prototype.updateStatus = function (id, info) {
        var _this = this;
        console.log("updateStatusServicio", id);
        console.log("Status: ", info.status);
        console.log("Mesa: ", info.mesa);
        return new Promise(function (resolve, reject) {
            //const idUsuario = localStorage.getItem("uid");
            _this.af
                .collection("reservaciones")
                .doc(id)
                .update({
                estatus: info.status,
                numMesa: info.mesa
            })
                .then(function (reserva) {
                console.log("Reservación actualizad: ", JSON.stringify(reserva));
                resolve({ success: true });
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ReservacionProvider.prototype.addProducto = function (producto) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log("Evento: ");
            _this.af
                .collection("productos")
                .add({
                cantidad: producto.cantidad,
                idProducto: producto.idProducto,
                idReservacion: producto.idReservacion
            })
                .then(function (reserva) {
                console.log("Producto exitoso: ", reserva.id);
                _this.updateReservaId(reserva.id);
                resolve({ success: true, idReservacion: reserva.id });
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ReservacionProvider.prototype.cancelarStatus = function (id, info) {
        var _this = this;
        console.log("cancelarReservacion", id);
        console.log(info);
        console.log("Status: ", info.status);
        console.log("Motivo: ", info.motivo);
        console.log("Otro: ", info.otro);
        return new Promise(function (resolve, reject) {
            //const idUsuario = localStorage.getItem("uid");
            _this.af
                .collection("reservaciones")
                .doc(id)
                .update({
                estatus: info.status,
                motivoCancelacion: info.motivo
            })
                .then(function (reserva) {
                console.log("Reservación cancelada: ", JSON.stringify(reserva));
                resolve({ success: true });
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ReservacionProvider.prototype.addCorte = function (fechaI, fechaF, comision1, comision2, suma, idSucursal, propina, folio) {
        var _this = this;
        console.log('****Provider add****');
        console.log('fechaI', fechaI);
        console.log('fechaF', fechaF);
        console.log('comision1', comision1);
        console.log('comision2', comision2);
        console.log('suma', suma);
        console.log('idSucursal', idSucursal);
        return new Promise(function (resolve, reject) {
            _this.af
                .collection("corte")
                .add({
                fecha_Inicio: fechaI,
                fecha_Fin: fechaF,
                comision1: comision1,
                comision2: comision2,
                totalCorte: suma,
                idSucursal: idSucursal,
                propina: propina,
                folio: folio
            })
                .then(function (reserva) {
                console.log('corteExitoso:', idSucursal);
                resolve({ success: true, idSucursal: idSucursal });
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ReservacionProvider.prototype.getCortes = function (idx) {
        //return this.afiredatabase.object("sucursales/" + uid);
        console.log("idSucursal provider", idx);
        this.areas = this.af.collection("corte", function (ref) {
            return ref.where("idSucursal", "==", idx);
        });
        this._areas = this.areas.valueChanges();
        return (this._areas = this.areas.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    ReservacionProvider.prototype.delete_corte = function (idCorte) {
        this.af
            .collection("corte")
            .doc(idCorte)
            .delete()
            .then(function () {
        })
            .catch(function (error) {
        });
    };
    ReservacionProvider.prototype.updateReservaId = function (ID) {
        this.af
            .collection("reservaciones")
            .doc(ID)
            .update({
            idReservacion: ID
        })
            .then(function () { })
            .catch(function () { });
    };
    ReservacionProvider.prototype.getReservacion = function (idx) {
        this.reservacion = this.af.doc("reservaciones/" + idx);
        // this.pedidoDoc = this.afs.collection<Servicios>('servicios').doc(`/${idPedido}`).collection<Pedidos>('pedidos');
        return (this._reservacion = this.reservacion.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    ReservacionProvider.prototype.getReservaciones = function (idx, fecha1, fecha2) {
        // return this.afiredatabase.object("sucursales/" + uid);
        //let idUsuario = idx;
        this.fechaI = fecha1;
        this.fechaF = fecha2;
        var fechI = __WEBPACK_IMPORTED_MODULE_3_moment__(this.fechaI).format("x");
        var fechF = __WEBPACK_IMPORTED_MODULE_3_moment__(this.fechaF).format("x");
        console.log('fechaIProvider', fechI);
        console.log('fechaFProvider', fechF);
        console.log("idsucursal", idx);
        this.reservaciones = this.af.collection("reservaciones", function (ref) {
            //      ref.where("idSucursal", "==", idx).where("estatus", "==", "Creando")
            return ref.where("idSucursal", "==", idx).where("estatus", "==", "Completado")
                .where("fechaR_", ">=", fechI)
                .where("fechaR_", "<=", fechF);
        });
        this._reservaciones = this.reservaciones.valueChanges();
        return (this._reservaciones = this.reservaciones.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                // console.log("que es", data.$key);
                return data;
            });
        })));
    };
    ReservacionProvider.prototype.getIdLast = function (idx) {
        this.num = this.af.collection("corte", function (ref) {
            return ref.where("idSucursal", "==", idx).orderBy('folio', 'desc').limit(1);
        });
        this._num = this.num.valueChanges();
        return (this._num = this.num.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    ReservacionProvider.prototype.getProductos = function (idx) {
        //return this.afiredatabase.object("sucursales/" + uid);
        console.log("idSucursal", idx);
        this.areas = this.af.collection("productos", function (ref) {
            return ref.where("idReservacion", "==", idx);
        });
        this._areas = this.areas.valueChanges();
        return (this._areas = this.areas.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    ReservacionProvider.prototype.getZona = function (idx) {
        this.zona = this.af.doc("zonas/" + idx);
        // this.pedidoDoc = this.afs.collection<Servicios>('servicios').doc(`/${idPedido}`).collection<Pedidos>('pedidos');
        return (this._zona = this.zona.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    ReservacionProvider.prototype.deleteReservacion = function (idReservacion) {
        this.af
            .collection("reservaciones")
            .doc(idReservacion)
            .delete()
            .then(function () {
            // console.log("Document successfully deleted!");
        })
            .catch(function (error) {
            // console.error("Error removing document: ", error);
        });
        var pedidosProductServ = this.af.collection("productos", function (ref) {
            return ref.where("idReservacion", "==", idReservacion);
        });
        pedidosProductServ.get().subscribe(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                doc.ref.delete();
            });
        });
    };
    ReservacionProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], ReservacionProvider);
    return ReservacionProvider;
}());

//# sourceMappingURL=reservacion.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CargaArchivoProvider; });
/* unused harmony export Credenciales */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(695);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ArchivoSubir__ = __webpack_require__(697);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CargaArchivoProvider = /** @class */ (function () {
    function CargaArchivoProvider(toastCtrl, 
        //public navCtrl: NavController,
        afDB, afs) {
        // this.cargar_ultimo_key()
        //     .subscribe(()=>this.cargar_imagenes());
        this.toastCtrl = toastCtrl;
        this.afDB = afDB;
        this.afs = afs;
        this.selectedEventoItem = new __WEBPACK_IMPORTED_MODULE_5__ArchivoSubir__["a" /* ArchivoSubir */]();
        this.db = __WEBPACK_IMPORTED_MODULE_3_firebase__["firestore"]();
        this.imagenes = [];
    }
    // private cargar_ultimo_key(){
    //   return this.afDB.list('/evento', ref=> ref.orderByKey().limitToLast(1))
    //             .valueChanges()
    //             .map( (evento:any) => {
    //               this.lastKey = evento[0].key;
    //               this.imagenes.push( evento[0]);
    //             });
    // }
    // cargar_imagenes(){
    //   return new Promise ((resolve, reject)=>{
    //     this.afDB.list('/evento',
    //       ref=> ref.limitToLast(8)
    //                 .orderByKey()
    //                 .endAt(this.lastKey)
    //               ).valueChanges()
    //                .subscribe((eventos:any)=>{
    //                  eventos.pop();
    //               if( eventos.length == 0){
    //                 console.log('Ya no hay más registros');
    //                 resolve(false);
    //                 return;
    //               }
    //               this.lastKey = eventos[0].key;
    //               for( let i = eventos.length-1; i>=0; i--){
    //                 let evento = eventos[i];
    //                 this.imagenes.push(evento);
    //               }
    //               resolve(true);
    //             });
    //   });
    // }
    CargaArchivoProvider.prototype.cargar_imagen_firebase = function (archivo) {
        var _this = this;
        var promesa = new Promise(function (resolve, reject) {
            _this.mostrar_toast('Cargando..');
            var storeRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["storage"]().ref();
            var nombreArchivo = new Date().valueOf().toString();
            var uploadTask = storeRef.child("evento/" + nombreArchivo + ".jpg")
                .putString(archivo.img, 'base64', { contentType: 'image/jpeg' });
            uploadTask.on(__WEBPACK_IMPORTED_MODULE_3_firebase__["storage"].TaskEvent.STATE_CHANGED, function () { }, //saber el % cuantos se han subido
            function (error) {
                //manejo
                console.log("Error en la carga");
                console.log(JSON.stringify(error));
                _this.mostrar_toast(JSON.stringify(error));
                reject();
            }, function () {
                // TODO BIEN!
                console.log('Archivo subido');
                _this.mostrar_toast('Imagen cargada correctamente');
                // let url = uploadTask.snapshot.downloadURL;
                // this.crear_post( archivo.titulo, url, nombreArchivo );
                // resolve();
                uploadTask.snapshot.ref.getDownloadURL().then(function (url) {
                    console.log('nombreArchivo', nombreArchivo);
                    console.log('url', url);
                    console.log('file.titulo', archivo.titulo);
                    _this.crear_post(archivo.titulo, archivo.fecha, archivo.hora, archivo.hora_fin, archivo.categoria, archivo.lugar, archivo.obs, url, nombreArchivo, archivo.uidSucursal, archivo.ciudad);
                    resolve();
                });
            });
        });
        return promesa;
    };
    CargaArchivoProvider.prototype.crear_post = function (titulo, fecha, hora, hora_fin, categoria, lugar, obs, url, nombreArchivo, uidSucursal, ciudad) {
        var _this = this;
        var evento = {
            img: url,
            titulo: titulo,
            fecha: fecha,
            hora: hora,
            hora_fin: hora_fin,
            categoria: categoria,
            lugar: lugar,
            obs: obs,
            key: nombreArchivo,
            uidSucursal: uidSucursal,
            ciudad: ciudad
        };
        console.log(JSON.stringify(evento));
        //this.afDB.list('/evento').push(evento);
        this.afs.collection('evento').add(evento).then(function (ref) {
            console.log('id', ref.id);
            var eventoUid = ref.id;
            _this.afs.collection('evento').doc(eventoUid).update({
                uid: eventoUid,
            });
        });
        // this.afDB.object(`evento/${ nombreArchivo }`).update(evento);
        this.imagenes.push(evento);
        this.mostrar_toast('Evento grabado a BD');
    };
    CargaArchivoProvider.prototype.getEvento = function (uid) {
        var _this = this;
        console.log('uid', uid);
        return new Promise(function (resolve, reject) {
            _this.db
                .collection("evento")
                .doc(uid)
                .get()
                .then(function (doc) {
                if (!doc.exists) {
                    console.log("No such document!");
                    resolve(null);
                }
                else {
                    console.log("Document data:", doc.data());
                    resolve(doc.data());
                }
            })
                .catch(function (err) {
                console.log("Error getting document", err);
                reject(err);
            });
        });
        // return this.afDB.object('evento/'+id);
    };
    CargaArchivoProvider.prototype.deleteEvento = function (key, img, uid) {
        var _this = this;
        console.log('key', key);
        console.log('uid delte', uid);
        var storeRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["storage"]().ref();
        var desertRef = storeRef.child("evento/" + img + ".jpg");
        console.log(img);
        // Delete the file
        desertRef.delete().then(function () {
            this.mostrar_toast('Finally');
            this.afs.collection('evento').doc(key).delete();
        }).catch(function (error) {
            // Uh-oh, an error occurred!
        });
        // this.afDB.database.ref('evento/'+key).remove();
        var promise = new Promise(function (resolve, reject) {
            console.log('data delete uid', uid);
            _this.db
                .collection("evento")
                .doc(uid)
                .delete()
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    CargaArchivoProvider.prototype.updateEvento = function (data, uid) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            console.log('data uid', uid);
            _this.db
                .collection("evento")
                .doc(uid)
                .update(data)
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
        // this.afDB.database.ref('evento/'+data.KEY).update(data);
    };
    CargaArchivoProvider.prototype.mostrar_toast = function (mensaje) {
        var toast = this.toastCtrl.create({
            message: mensaje,
            duration: 3000
        }).present();
    };
    // Edicion de imagen
    CargaArchivoProvider.prototype.cargar_imagen_firebase_evento = function (archivo, key, uid) {
        var _this = this;
        var promesa = new Promise(function (resolve, reject) {
            _this.mostrar_toast('Cargando..');
            var storeRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["storage"]().ref();
            var img = new Date().valueOf().toString();
            var uploadTask = storeRef.child("evento/" + img + ".jpg")
                .putString(archivo.img, 'base64', { contentType: 'image/jpeg' });
            uploadTask.on(__WEBPACK_IMPORTED_MODULE_3_firebase__["storage"].TaskEvent.STATE_CHANGED, function () { }, //saber el % cuantos se han subido
            function (error) {
                //manejo
                console.log("Error en la carga");
                console.log(JSON.stringify(error));
                _this.mostrar_toast(JSON.stringify(error));
                reject();
            }, function () {
                // TODO BIEN!
                console.log('Archivo subido');
                _this.mostrar_toast('Imagen cargada correctamente');
                // let url = uploadTask.snapshot.downloadURL;
                // this.crear_post( archivo.titulo, url, nombreArchivo );
                // resolve();
                uploadTask.snapshot.ref.getDownloadURL().then(function (urlImage) {
                    _this.crear_post_edev(urlImage, key, uid);
                    _this.mostrar_toast('URL' + urlImage);
                }).catch(function (error) {
                    console.log(error);
                });
                resolve();
            });
        });
        return promesa;
    };
    CargaArchivoProvider.prototype.crear_post_edev = function (url, key, uid) {
        var _this = this;
        var evento = {
            img: url,
            key: key
        };
        console.log(JSON.stringify(evento));
        var promise = new Promise(function (resolve, reject) {
            console.log('data key', key);
            console.log('data uid', uid);
            _this.db
                .collection("evento")
                .doc(uid)
                .update(evento)
                .then(function () {
                resolve(true);
                _this.mostrar_toast('Imagen actualizada');
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
        // this.afDB.object(`evento/`+key).update(evento);
        // this.imagenes.push(sucursal);
    };
    CargaArchivoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], CargaArchivoProvider);
    return CargaArchivoProvider;
}());

var Credenciales = /** @class */ (function () {
    function Credenciales() {
    }
    return Credenciales;
}());

//# sourceMappingURL=carga-archivo.js.map

/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_fire_database__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import firebase from 'firebase';


var UsuarioProvider = /** @class */ (function () {
    function UsuarioProvider(afDB, afireauth, afs) {
        this.afDB = afDB;
        this.afireauth = afireauth;
        this.afs = afs;
        this.data = {};
        this.usuario = {};
    }
    UsuarioProvider.prototype.cargarUsuario = function (nombre, email, imagen, uid, phone, provider) {
        this.usuario.nombre = nombre;
        this.usuario.email = email;
        this.usuario.imagen = imagen;
        this.usuario.uid = uid;
        this.usuario.provider = provider;
        this.usuario.phone = phone;
    };
    UsuarioProvider.prototype.getUser = function (uid) {
        return this.afDB.object('users/' + uid);
    };
    UsuarioProvider.prototype.inhabilitar = function (uid) {
        console.log(uid);
        this.data = {
            active: false
        };
        this.afs.collection('users').doc(uid).update(this.data);
        // this.afDB.database.ref('users/'+ uid).update(this.data);
    };
    UsuarioProvider.prototype.habilitar = function (uid, status) {
        console.log(uid);
        this.data = {
            active: status
        };
        this.afs.collection('users').doc(uid).update(this.data);
        //  this.afDB.database.ref('users/'+ uid).update(this.data);
    };
    UsuarioProvider.prototype.getUser1 = function (idx) {
        // return this.afiredatabase.object("sucursales/" + uid);
        console.log("user", idx);
        this.areas = this.afs.collection("users", function (ref) {
            return ref.where("uid", "==", idx);
        });
        this._areas = this.areas.valueChanges();
        return (this._areas = this.areas.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    UsuarioProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_fire_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], UsuarioProvider);
    return UsuarioProvider;
}());

//# sourceMappingURL=usuario.js.map

/***/ }),

/***/ 640:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(503);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(504);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(717);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_perfil_perfil__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_eventos_eventos__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_nosotros_nosotros__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_carta_carta__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_historial_historial__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_reservacion_1_reservacion_1__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_tabs_tabs__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_evento_detalle_evento_detalle__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_reservaciones_reservaciones__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_producto_detalle_producto_detalle__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_resumen_resumen__ = __webpack_require__(506);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_admin_login_admin_login__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_admin_home_admin_home__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_admin_users_admin_users__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_admin_evento_home_admin_evento_home__ = __webpack_require__(460);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_admin_evento_subir_admin_evento_subir__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_admin_evento_edit_admin_evento_edit__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_admin_carta_home_admin_carta_home__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_admin_carta_edit_admin_carta_edit__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_admin_users_list_admin_users_list__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_admin_user_user_admin_user_user__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_admin_carta_subir_admin_evento_subir__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_admin_users_guest_admin_users_guest__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_admin_user_detail_admin_user_detail__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_admin_menu_reservacion_admin_menu_reservacion__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_admin_sucursal_list_admin_sucursal_list__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_admin_sucursal_subir_admin_sucursal_subir__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_admin_sucursal_perfil_admin_sucursal_perfil__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_admin_sucursal_editperfil_admin_sucursal_editperfil__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_admin_sucursal_editperfil_imagen_admin_sucursal_editperfil_imagen__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_admin_evento_image_edit_admin_evento_image_edit__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_admin_carta_image_edit_admin_carta_image_edit__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_admin_sucursal_croquis_admin_sucursal_croquis__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_imagencroquis_imagencroquis__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_admin_perfil_empleado_admin_perfil_empleado__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_administrar_reservaciones_administrar_reservaciones__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_admin_lee_qr_admin_lee_qr__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_corte_venta_corte_venta__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_corte_historial_corte_historial__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_cupones_sucursal_cupones_sucursal__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_agregar_cupones_agregar_cupones__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_ciudad_establecimiento_ciudad_establecimiento__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_agregar_ciudad_agregar_ciudad__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_admin_historial_reservaciones_admin_historial_reservaciones__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_admin_reservacion_detalle_admin_reservacion_detalle__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_admin_reservaciones_curso_admin_reservaciones_curso__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_detalle_cupon_detalle_cupon__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_cuentas_cuentas__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pipes_pipes_module__ = __webpack_require__(718);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57_angularfire2__ = __webpack_require__(722);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57_angularfire2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_57_angularfire2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_58_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_59_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__providers_usuario_usuario__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__providers_carga_archivo_carga_archivo__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__providers_carta_add_carta_add__ = __webpack_require__(723);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__providers_toast_toast__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__providers_carga_archivo_carta_carga_archivo__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__providers_sucursal_alta_sucursal_alta__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__providers_carga_croquis_carga_croquis__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__providers_push_noti_push_noti__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__providers_reservacion_reservacion__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__ionic_native_google_plus__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__providers_auth_auth__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__providers_user_user__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__ionic_native_image_picker__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__ionic_native_social_sharing__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_74__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75_firebase_app__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_75_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_76__ionic_native_onesignal__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_77__providers_carta_carta__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_78__angular_http__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_79__providers_gestion_reservaciones_gestion_reservaciones__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_80__pages_admin_monitear_reserv_admin_monitear_reserv__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_81__providers_monitoreo_reservas_monitoreo_reservas__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_82__pages_admin_de_mon_user_admin_de_mon_user__ = __webpack_require__(505);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_83__providers_resumen_resumen__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_84__pages_mis_reservaciones_mis_reservaciones__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_85__ionic_native_barcode_scanner__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_86__ionic_native_stripe__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_87__providers_payment_payment__ = __webpack_require__(725);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















//administrador





































//Pipes

//import { AuthService } from '../providers/auth-service';
//Firebase



//providers









//Plugins
// import { Facebook } from '@ionic-native/facebook';



//import { Camera, CameraOptions } from '@ionic-native/camera';














var firebaseConfig = {
    apiKey: "AIzaSyBixlCb21nNbPSurY-Pvqu3hZB80Icl9Pk",
    authDomain: "guestreservation-8b24b.firebaseapp.com",
    databaseURL: "https://guestreservation-8b24b.firebaseio.com",
    projectId: "guestreservation-8b24b",
    storageBucket: "guestreservation-8b24b.appspot.com",
    messagingSenderId: "853477386824"
};


__WEBPACK_IMPORTED_MODULE_75_firebase_app__["initializeApp"](firebaseConfig);
//var secondaryConnection = firebase.initializeApp(firebaseConfig);
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_eventos_eventos__["a" /* EventosPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_reservacion_1_reservacion_1__["a" /* Reservacion_1Page */],
                __WEBPACK_IMPORTED_MODULE_10__pages_nosotros_nosotros__["a" /* NosotrosPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_carta_carta__["a" /* CartaPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_historial_historial__["a" /* HistorialPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_producto_detalle_producto_detalle__["a" /* ProductoDetallePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_resumen_resumen__["a" /* ResumenPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_admin_login_admin_login__["a" /* AdminLoginPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_admin_home_admin_home__["a" /* AdminHomePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_admin_users_admin_users__["a" /* AdminUsersPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_admin_evento_home_admin_evento_home__["a" /* AdminEventoHomePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_admin_evento_subir_admin_evento_subir__["a" /* AdminEventoSubirPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_admin_carta_home_admin_carta_home__["a" /* AdminCartaHomePage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_admin_carta_subir_admin_evento_subir__["a" /* AdminCartaSubirPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_admin_carta_edit_admin_carta_edit__["a" /* AdminCartaEditPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_admin_evento_edit_admin_evento_edit__["a" /* AdminEventoEditPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_admin_users_list_admin_users_list__["a" /* AdminUsersListPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_admin_users_guest_admin_users_guest__["a" /* AdminUsersGuestPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_admin_user_user_admin_user_user__["a" /* AdminUserUserPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_admin_user_detail_admin_user_detail__["a" /* AdminUserDetailPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_admin_menu_reservacion_admin_menu_reservacion__["a" /* AdminMenuReservacionPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_admin_sucursal_list_admin_sucursal_list__["a" /* AdminSucursalListPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_admin_sucursal_subir_admin_sucursal_subir__["a" /* AdminSucursalSubirPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_evento_detalle_evento_detalle__["a" /* EventoDetallePage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_admin_sucursal_perfil_admin_sucursal_perfil__["a" /* AdminSucursalPerfilPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_admin_sucursal_editperfil_admin_sucursal_editperfil__["a" /* AdminSucursalEditperfilPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_admin_sucursal_editperfil_imagen_admin_sucursal_editperfil_imagen__["a" /* AdminSucursalEditperfilImagenPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_admin_evento_image_edit_admin_evento_image_edit__["a" /* AdminEventoImageEditPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_admin_carta_image_edit_admin_carta_image_edit__["a" /* AdminCartaImageEditPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_admin_sucursal_croquis_admin_sucursal_croquis__["a" /* AdminSucursalCroquisPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_imagencroquis_imagencroquis__["a" /* ImagencroquisPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_reservaciones_reservaciones__["a" /* ReservacionesPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_admin_perfil_empleado_admin_perfil_empleado__["a" /* AdminPerfilEmpleadoPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_administrar_reservaciones_administrar_reservaciones__["a" /* AdministrarReservacionesPage */],
                __WEBPACK_IMPORTED_MODULE_80__pages_admin_monitear_reserv_admin_monitear_reserv__["a" /* AdminMonitearReservPage */],
                __WEBPACK_IMPORTED_MODULE_82__pages_admin_de_mon_user_admin_de_mon_user__["a" /* AdminDeMonUserPage */],
                __WEBPACK_IMPORTED_MODULE_84__pages_mis_reservaciones_mis_reservaciones__["a" /* MisReservacionesPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_admin_lee_qr_admin_lee_qr__["a" /* AdminLeeQrPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_corte_venta_corte_venta__["a" /* CorteVentaPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_corte_historial_corte_historial__["a" /* CorteHistorialPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_cupones_sucursal_cupones_sucursal__["a" /* CuponesSucursalPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_agregar_cupones_agregar_cupones__["a" /* AgregarCuponesPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_ciudad_establecimiento_ciudad_establecimiento__["a" /* CiudadEstablecimientoPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_agregar_ciudad_agregar_ciudad__["a" /* AgregarCiudadPage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_admin_historial_reservaciones_admin_historial_reservaciones__["a" /* AdminHistorialReservacionesPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_admin_reservacion_detalle_admin_reservacion_detalle__["a" /* AdminReservacionDetallePage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_admin_reservaciones_curso_admin_reservaciones_curso__["a" /* AdminReservacionesCursoPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_detalle_cupon_detalle_cupon__["a" /* DetalleCuponPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_cuentas_cuentas__["a" /* CuentasPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {
                    platforms: {
                        ios: {
                            backButtonText: ''
                        }
                    }
                }, {
                    links: [
                        { loadChildren: '../pages/admin-carta-image-edit/admin-carta-image-edit.module#AdminCartaImageEditPageModule', name: 'AdminCartaImageEditPage', segment: 'admin-carta-image-edit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-carta-home/admin-carta-home.module#AdminCartaHomePageModule', name: 'AdminCartaHomePage', segment: 'admin-carta-home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-carta-edit/admin-carta-edit.module#AdminCartaEditPageModule', name: 'AdminCartaEditPage', segment: 'admin-carta-edit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-evento-edit/admin-evento-edit.module#AdminEventoEditPageModule', name: 'AdminEventoEditPage', segment: 'admin-evento-edit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-evento-image-edit/admin-evento-image-edit.module#AdminEventoImageEditPageModule', name: 'AdminEventoImageEditPage', segment: 'admin-evento-image-edit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-de-mon-user/admin-de-mon-user.module#AdminDeMonUserPageModule', name: 'AdminDeMonUserPage', segment: 'admin-de-mon-user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-login/admin-login.module#AdminLoginPageModule', name: 'AdminLoginPage', segment: 'admin-login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-menu-reservacion/admin-menu-reservacion.module#AdminMenuReservacionPageModule', name: 'AdminMenuReservacionPage', segment: 'admin-menu-reservacion', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-lee-qr/admin-lee-qr.module#AdminLeeQrPageModule', name: 'AdminLeeQrPage', segment: 'admin-lee-qr', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-monitear-reserv/admin-monitear-reserv.module#AdminMonitearReservPageModule', name: 'AdminMonitearReservPage', segment: 'admin-monitear-reserv', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-reservacion-detalle/admin-reservacion-detalle.module#AdminReservacionDetallePageModule', name: 'AdminReservacionDetallePage', segment: 'admin-reservacion-detalle', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-perfil-empleado/admin-perfil-empleado.module#AdminPerfilEmpleadoPageModule', name: 'AdminPerfilEmpleadoPage', segment: 'admin-perfil-empleado', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-reservaciones-curso/admin-reservaciones-curso.module#AdminReservacionesCursoPageModule', name: 'AdminReservacionesCursoPage', segment: 'admin-reservaciones-curso', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-sucursal-croquis/admin-sucursal-croquis.module#AdminSucursalCroquisPageModule', name: 'AdminSucursalCroquisPage', segment: 'admin-sucursal-croquis', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-sucursal-editperfil/admin-sucursal-editperfil.module#AdminSucursalEditperfilPageModule', name: 'AdminSucursalEditperfilPage', segment: 'admin-sucursal-editperfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-sucursal-list/admin-sucursal-list.module#AdminSucursalListPageModule', name: 'AdminSucursalListPage', segment: 'admin-sucursal-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-sucursal-subir/admin-sucursal-subir.module#AdminSucursalSubirPageModule', name: 'AdminSucursalSubirPage', segment: 'admin-sucursal-subir', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-historial-reservaciones/admin-historial-reservaciones.module#AdminHistorialReservacionesPageModule', name: 'AdminHistorialReservacionesPage', segment: 'admin-historial-reservaciones', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-user-detail/admin-user-detail.module#AdminUserDetailPageModule', name: 'AdminUserDetailPage', segment: 'admin-user-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-user-user/admin-user-user.module#AdminUserUserPageModule', name: 'AdminUserUserPage', segment: 'admin-user-user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-sucursal-editperfil-imagen/admin-sucursal-editperfil-imagen.module#AdminSucursalEditperfilImagenPageModule', name: 'AdminSucursalEditperfilImagenPage', segment: 'admin-sucursal-editperfil-imagen', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-users-guest/admin-users-guest.module#AdminUsersGuestPageModule', name: 'AdminUsersGuestPage', segment: 'admin-users-guest', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-users-list/admin-users-list.module#AdminUsersListPageModule', name: 'AdminUsersListPage', segment: 'admin-users-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-users/admin-users.module#AdminUsersPageModule', name: 'AdminUsersPage', segment: 'admin-users', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/administrar-reservaciones/administrar-reservaciones.module#AdministrarReservacionesPageModule', name: 'AdministrarReservacionesPage', segment: 'administrar-reservaciones', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/agregar-ciudad/agregar-ciudad.module#AgregarCiudadPageModule', name: 'AgregarCiudadPage', segment: 'agregar-ciudad', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/agregar-cupones/agregar-cupones.module#AgregarCuponesPageModule', name: 'AgregarCuponesPage', segment: 'agregar-cupones', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/adminmesas/adminmesas.module#AdminmesasPageModule', name: 'AdminmesasPage', segment: 'adminmesas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ciudad-establecimiento/ciudad-establecimiento.module#CiudadEstablecimientoPageModule', name: 'CiudadEstablecimientoPage', segment: 'ciudad-establecimiento', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/corte-historial/corte-historial.module#CorteHistorialPageModule', name: 'CorteHistorialPage', segment: 'corte-historial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/carta/carta.module#CartaPageModule', name: 'CartaPage', segment: 'carta', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/corte-venta/corte-venta.module#CorteVentaPageModule', name: 'CorteVentaPage', segment: 'corte-venta', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cuentas/cuentas.module#CorteVentaPageModule', name: 'CuentasPage', segment: 'cuentas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cupones-sucursal/cupones-sucursal.module#CuponesSucursalPageModule', name: 'CuponesSucursalPage', segment: 'cupones-sucursal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detalle-cupon/detalle-cupon.module#DetalleCuponPageModule', name: 'DetalleCuponPage', segment: 'detalle-cupon', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/generarqr/generarqr.module#GenerarqrPageModule', name: 'GenerarqrPage', segment: 'generarqr', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/historial/historial.module#HistorialPageModule', name: 'HistorialPage', segment: 'historial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/evento-detalle/evento-detalle.module#EventoDetallePageModule', name: 'EventoDetallePage', segment: 'evento-detalle', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eventos/eventos.module#EventosPageModule', name: 'EventosPage', segment: 'eventos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mis-reservaciones/mis-reservaciones.module#MisReservacionesPageModule', name: 'MisReservacionesPage', segment: 'mis-reservaciones', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modalmesas/modalmesas.module#ModalmesasPageModule', name: 'ModalmesasPage', segment: 'modalmesas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modalstatus/modalstatus.module#ModalstatusPageModule', name: 'ModalstatusPage', segment: 'modalstatus', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/imagencroquis/imagencroquis.module#ImagencroquisPageModule', name: 'ImagencroquisPage', segment: 'imagencroquis', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nosotros/nosotros.module#NosotrosPageModule', name: 'NosotrosPage', segment: 'nosotros', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/perfil/perfil.module#PerfilPageModule', name: 'PerfilPage', segment: 'perfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modiareazona/modiareazona.module#ModiareazonaPageModule', name: 'ModiareazonaPage', segment: 'modiareazona', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modalstatus_cancelacion/modalstatus_cancelacion.module#Modalstatus_cancelacionPageModule', name: 'Modalstatus_cancelacionPage', segment: 'modalstatus_cancelacion', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reservacion-1/reservacion-1.module#Reservacion_1PageModule', name: 'Reservacion_1Page', segment: 'reservacion-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/producto-detalle/producto-detalle.module#ProductoDetallePageModule', name: 'ProductoDetallePage', segment: 'producto-detalle', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/resumen/resumen.module#ResumenPageModule', name: 'ResumenPage', segment: 'resumen', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-home/admin-home.module#AdminHomePageModule', name: 'AdminHomePage', segment: 'admin-home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-sucursal-perfil/admin-sucursal-perfil.module#AdminSucursalPerfilPageModule', name: 'AdminSucursalPerfilPage', segment: 'admin-sucursal-perfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reservaciones/reservaciones.module#ReservacionesPageModule', name: 'ReservacionesPage', segment: 'reservaciones', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_57_angularfire2__["AngularFireModule"].initializeApp(firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_58_angularfire2_database__["AngularFireDatabaseModule"],
                __WEBPACK_IMPORTED_MODULE_59_angularfire2_auth__["AngularFireAuthModule"],
                __WEBPACK_IMPORTED_MODULE_56__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_78__angular_http__["c" /* HttpModule */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_eventos_eventos__["a" /* EventosPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_reservacion_1_reservacion_1__["a" /* Reservacion_1Page */],
                __WEBPACK_IMPORTED_MODULE_10__pages_nosotros_nosotros__["a" /* NosotrosPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_carta_carta__["a" /* CartaPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_historial_historial__["a" /* HistorialPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_producto_detalle_producto_detalle__["a" /* ProductoDetallePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_resumen_resumen__["a" /* ResumenPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_admin_login_admin_login__["a" /* AdminLoginPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_admin_home_admin_home__["a" /* AdminHomePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_admin_users_admin_users__["a" /* AdminUsersPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_admin_evento_home_admin_evento_home__["a" /* AdminEventoHomePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_admin_evento_subir_admin_evento_subir__["a" /* AdminEventoSubirPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_admin_evento_edit_admin_evento_edit__["a" /* AdminEventoEditPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_admin_carta_home_admin_carta_home__["a" /* AdminCartaHomePage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_admin_carta_edit_admin_carta_edit__["a" /* AdminCartaEditPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_admin_carta_subir_admin_evento_subir__["a" /* AdminCartaSubirPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_admin_users_list_admin_users_list__["a" /* AdminUsersListPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_admin_users_guest_admin_users_guest__["a" /* AdminUsersGuestPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_admin_user_user_admin_user_user__["a" /* AdminUserUserPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_admin_user_detail_admin_user_detail__["a" /* AdminUserDetailPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_admin_menu_reservacion_admin_menu_reservacion__["a" /* AdminMenuReservacionPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_admin_sucursal_list_admin_sucursal_list__["a" /* AdminSucursalListPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_admin_sucursal_subir_admin_sucursal_subir__["a" /* AdminSucursalSubirPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_evento_detalle_evento_detalle__["a" /* EventoDetallePage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_admin_sucursal_perfil_admin_sucursal_perfil__["a" /* AdminSucursalPerfilPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_admin_sucursal_editperfil_admin_sucursal_editperfil__["a" /* AdminSucursalEditperfilPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_admin_sucursal_editperfil_imagen_admin_sucursal_editperfil_imagen__["a" /* AdminSucursalEditperfilImagenPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_admin_evento_image_edit_admin_evento_image_edit__["a" /* AdminEventoImageEditPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_admin_carta_image_edit_admin_carta_image_edit__["a" /* AdminCartaImageEditPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_admin_sucursal_croquis_admin_sucursal_croquis__["a" /* AdminSucursalCroquisPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_imagencroquis_imagencroquis__["a" /* ImagencroquisPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_reservaciones_reservaciones__["a" /* ReservacionesPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_admin_perfil_empleado_admin_perfil_empleado__["a" /* AdminPerfilEmpleadoPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_reservaciones_reservaciones__["a" /* ReservacionesPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_administrar_reservaciones_administrar_reservaciones__["a" /* AdministrarReservacionesPage */],
                __WEBPACK_IMPORTED_MODULE_80__pages_admin_monitear_reserv_admin_monitear_reserv__["a" /* AdminMonitearReservPage */],
                __WEBPACK_IMPORTED_MODULE_82__pages_admin_de_mon_user_admin_de_mon_user__["a" /* AdminDeMonUserPage */],
                __WEBPACK_IMPORTED_MODULE_84__pages_mis_reservaciones_mis_reservaciones__["a" /* MisReservacionesPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_admin_lee_qr_admin_lee_qr__["a" /* AdminLeeQrPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_corte_venta_corte_venta__["a" /* CorteVentaPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_corte_historial_corte_historial__["a" /* CorteHistorialPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_cupones_sucursal_cupones_sucursal__["a" /* CuponesSucursalPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_agregar_cupones_agregar_cupones__["a" /* AgregarCuponesPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_ciudad_establecimiento_ciudad_establecimiento__["a" /* CiudadEstablecimientoPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_agregar_ciudad_agregar_ciudad__["a" /* AgregarCiudadPage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_admin_historial_reservaciones_admin_historial_reservaciones__["a" /* AdminHistorialReservacionesPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_admin_reservacion_detalle_admin_reservacion_detalle__["a" /* AdminReservacionDetallePage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_admin_reservaciones_curso_admin_reservaciones_curso__["a" /* AdminReservacionesCursoPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_detalle_cupon_detalle_cupon__["a" /* DetalleCuponPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_cuentas_cuentas__["a" /* CuentasPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_58_angularfire2_database__["AngularFireDatabase"],
                __WEBPACK_IMPORTED_MODULE_85__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_60__providers_usuario_usuario__["a" /* UsuarioProvider */],
                // Facebook,
                __WEBPACK_IMPORTED_MODULE_69__ionic_native_google_plus__["a" /* GooglePlus */],
                __WEBPACK_IMPORTED_MODULE_70__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_71__providers_user_user__["a" /* UserProvider */],
                //Camera,
                __WEBPACK_IMPORTED_MODULE_72__ionic_native_image_picker__["a" /* ImagePicker */],
                __WEBPACK_IMPORTED_MODULE_73__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_61__providers_carga_archivo_carga_archivo__["a" /* CargaArchivoProvider */],
                __WEBPACK_IMPORTED_MODULE_62__providers_carta_add_carta_add__["a" /* CartaAddProvider */],
                __WEBPACK_IMPORTED_MODULE_63__providers_toast_toast__["a" /* ToastProvider */],
                __WEBPACK_IMPORTED_MODULE_64__providers_carga_archivo_carta_carga_archivo__["a" /* CargaArchivoCartaProvider */],
                __WEBPACK_IMPORTED_MODULE_65__providers_sucursal_alta_sucursal_alta__["a" /* SucursalAltaProvider */],
                __WEBPACK_IMPORTED_MODULE_66__providers_carga_croquis_carga_croquis__["a" /* CargaCroquisProvider */],
                __WEBPACK_IMPORTED_MODULE_74__angular_fire_firestore__["a" /* AngularFirestore */],
                __WEBPACK_IMPORTED_MODULE_67__providers_push_noti_push_noti__["a" /* PushNotiProvider */],
                //AuthService,
                __WEBPACK_IMPORTED_MODULE_76__ionic_native_onesignal__["a" /* OneSignal */],
                __WEBPACK_IMPORTED_MODULE_68__providers_reservacion_reservacion__["a" /* ReservacionProvider */],
                __WEBPACK_IMPORTED_MODULE_79__providers_gestion_reservaciones_gestion_reservaciones__["a" /* GestionReservacionesProvider */],
                __WEBPACK_IMPORTED_MODULE_77__providers_carta_carta__["a" /* CartaProvider */],
                __WEBPACK_IMPORTED_MODULE_81__providers_monitoreo_reservas_monitoreo_reservas__["a" /* MonitoreoReservasProvider */],
                __WEBPACK_IMPORTED_MODULE_83__providers_resumen_resumen__["a" /* ResumenProvider */],
                __WEBPACK_IMPORTED_MODULE_86__ionic_native_stripe__["a" /* Stripe */],
                __WEBPACK_IMPORTED_MODULE_87__providers_payment_payment__["a" /* PaymentProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminLoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_admin_home_admin_home__ = __webpack_require__(118);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminLoginPage = /** @class */ (function () {
    function AdminLoginPage(navCtrl, loadingCtrl, authProvider) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.authProvider = authProvider;
        this.credentials = {
            email: '',
            password: ''
        };
    }
    AdminLoginPage.prototype.ionViewDidLoad = function () {
        if (localStorage.getItem("isLogin") == "true") {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_admin_home_admin_home__["a" /* AdminHomePage */]);
        }
    };
    AdminLoginPage.prototype.login = function () {
        var _this = this;
        this.authProvider.login(this.credentials).then(function (res) {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_admin_home_admin_home__["a" /* AdminHomePage */]);
            console.log(_this.credentials.email);
            console.log('Res', res);
        }).catch(function (err) {
            alert(err.message);
        });
    };
    AdminLoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-login',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-login\admin-login.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title></ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-row>\n\n    <ion-col class="top-20">\n\n      <img src="./assets/imgs/Logoblanco.png">\n\n    </ion-col>\n\n  </ion-row>\n\n    <ion-list class="atras">\n\n        <ion-item class="atras">\n\n            <ion-label floating>Email</ion-label>\n\n            <ion-input type="email" [(ngModel)] = "credentials.email"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="atras">\n\n            <ion-label floating>Password</ion-label>\n\n            <ion-input type="password" [(ngModel)] = "credentials.password"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="atras" text-center>\n\n            <button class="blanco" ion-button round style="min-width: 200px; padding: 10px 16px" (click) = "login()" >Login</button>\n\n        </ion-item>\n\n        <!-- <ion-item text-center>\n\n            <p tappable (click) = "register()"> Dont\'t have account? Register </p>\n\n        </ion-item> -->\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-login\admin-login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */]])
    ], AdminLoginPage);
    return AdminLoginPage;
}());

//# sourceMappingURL=admin-login.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArchivoSubir; });
var ArchivoSubir = /** @class */ (function () {
    function ArchivoSubir() {
    }
    return ArchivoSubir;
}());

//# sourceMappingURL=ArchivoSubir.js.map

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase_app__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase_database__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_usuario_usuario__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_plus__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_admin_login_admin_login__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__eventos_eventos__ = __webpack_require__(120);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// import { Facebook } from '@ionic-native/facebook';


// import { AngularFireDatabase } from 'angularfire2/database';


var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, afAuth, usuarioProv, 
        // private fb: Facebook,
        googlePlus, platform, 
        // public afiredatabase: AngularFireDatabase,
        afs) {
        this.navCtrl = navCtrl;
        this.afAuth = afAuth;
        this.usuarioProv = usuarioProv;
        this.googlePlus = googlePlus;
        this.platform = platform;
        this.afs = afs;
        this.pageLogin = "admin-login";
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        //  if (localStorage.getItem("isLogin") == "true") {
        //      this.navCtrl.setRoot(TabsPage);
        //  }
    };
    LoginPage.prototype.signInWithFacebook_ = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__eventos_eventos__["a" /* EventosPage */]);
    };
    LoginPage.prototype.signInGoogle = function () {
        var _this = this;
        this.googlePlus
            .login({
            webClientId: "853477386824-kt4bl5ccfs8hgfm255i3384fhb6e50jq.apps.googleusercontent.com",
            offline: true
        })
            .then(function (res) {
            __WEBPACK_IMPORTED_MODULE_3_firebase_app__["auth"]()
                .signInWithCredential(__WEBPACK_IMPORTED_MODULE_3_firebase_app__["auth"].GoogleAuthProvider.credential(res.idToken))
                .then(function (user) {
                _this.us = user.user;
                console.log(JSON.stringify(user));
                console.log(res.idToken);
                _this.usuarioProv.cargarUsuario(_this.us.displayName, _this.us.email, _this.us.photoURL, _this.us.uid, _this.us.phoneNumber, "google");
                _this.afs
                    .collection("users")
                    .doc(_this.usuarioProv.usuario.uid)
                    .set({
                    uid: _this.usuarioProv.usuario.uid,
                    displayName: _this.us.displayName,
                    email: _this.us.email,
                    photoURL: _this.us.photoURL,
                    provider: "google",
                    phoneNumber: _this.us.phoneNumber,
                    type: "u"
                });
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */]);
            })
                .catch(function (error) {
                return console.log("Firebase failure: " + JSON.stringify(error));
            });
        })
            .catch(function (err) { return console.error("Error: ", err); });
    };
    // signInWithFacebook() {
    //   if (this.platform.is('cordova')) {
    //     this.fb.login(['email', 'public_profile']).then(res => {
    //      const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
    //      firebase.auth().signInWithCredential(facebookCredential)
    //      .then(user => {
    //       this.us = user.user;
    //       localStorage.setItem("uid", this.us.uid);
    //       alert(JSON.stringify(this.us.uid));
    //        console.log(res);
    //        this.usuarioProv.cargarUsuario(
    //          this.us.displayName,
    //          this.us.email,
    //          this.us.photoURL,
    //          this.us.uid,
    //          this.us.phoneNumber,
    //          'facebook'         
    //        );
    //        this.afs.collection('users').doc(this.usuarioProv.usuario.uid).set({
    //          uid: this.usuarioProv.usuario.uid,
    //          displayName: this.us.displayName,
    //          email: this.us.email,
    //          photoURL: this.us.photoURL,
    //          phoneNumber: this.us.phoneNumber,
    //          provider: 'facebook',
    //          type: 'u'
    //        });
    //        this.navCtrl.setRoot(TabsPage);
    //       }).catch(e => alert('Error de autenticación' + JSON.stringify(e)));
    //     })
    //   }else{
    //     //Escritorio
    //     this.afAuth.auth
    //     .signInWithPopup(new firebase.auth.FacebookAuthProvider())
    //     .then(res => {
    //       console.log(res);
    //       let user = res.user;
    //       console.log('Datos User: ', user);
    //       localStorage.setItem('uid', user.uid);
    //       this.usuarioProv.cargarUsuario(
    //         user.displayName,
    //         user.email,
    //         user.photoURL,
    //         user.uid,
    //         user.phoneNumber,
    //         'facebook'
    //       );
    //       if( this.usuarioProv.usuario.uid ){
    //         this.afs.collection('users').doc(this.usuarioProv.usuario.uid).set({
    //           uid: this.usuarioProv.usuario.uid,
    //           displayName: user.displayName,
    //           email: user.email,
    //           photoURL: user.photoURL,
    //           phoneNumber: user.phoneNumber,
    //           provider: 'facebook',
    //           type: 'u'
    //         });
    //       }else {
    //         this.afs.collection('users').add({
    //           uid: this.usuarioProv.usuario.uid,
    //           displayName: user.displayName,
    //           email: user.email,
    //           photoURL: user.photoURL,
    //           phoneNumber: user.phoneNumber,
    //           provider: 'facebook',
    //           type: 'u'
    //         });
    //       }
    //       this.navCtrl.setRoot(TabsPage);
    //     });
    //   }
    // }
    LoginPage.prototype.signIn = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */]);
    };
    LoginPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_admin_login_admin_login__["a" /* AdminLoginPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-login",template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\login\login.html"*/'<ion-content padding text-center>\n\n    <ion-grid>\n\n        <ion-row class="esquina">\n\n            <ion-col><button class="esquina" (click)="goLogin()" icon-left block>\n\n   <img src="./assets/imgs/icons/corona.png" class="ima"></button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n    <ion-grid class="margen">\n\n        <ion-row>\n\n            <ion-col class="top-20">\n\n                <img src="./assets/imgs/Logoblanco.png">\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n\n\n        <!-- <ion-row>\n\n            <ion-col>\n\n                <button class="blanco" (click)="signInGoogle()" ion-button icon-left block>\n\n                <ion-icon name="logo-google"></ion-icon>\n\n                Google\n\n              </button>\n\n            </ion-col>\n\n\n\n        </ion-row> -->\n\n        <ion-row>\n\n            <ion-col>\n\n\n\n                <button class="blanco" (click)="signInWithFacebook_()" ion-button icon-left block>\n\n                  <ion-icon name="logo-facebook"></ion-icon>\n\n                  Facebook\n\n                </button> -->\n\n\n\n                <button class="blanco" (click)="signIn()" ion-button icon-left block>\n\n                    <ion-icon name="logo-facebook"></ion-icon>\n\n                    Facebook\n\n                  </button>\n\n\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </ion-grid>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_5__providers_usuario_usuario__["a" /* UsuarioProvider */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_plus__["a" /* GooglePlus */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_9__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 697:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArchivoSubir; });
var ArchivoSubir = /** @class */ (function () {
    function ArchivoSubir() {
    }
    return ArchivoSubir;
}());

//# sourceMappingURL=ArchivoSubir.js.map

/***/ }),

/***/ 699:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 329,
	"./af.js": 329,
	"./ar": 330,
	"./ar-dz": 331,
	"./ar-dz.js": 331,
	"./ar-kw": 332,
	"./ar-kw.js": 332,
	"./ar-ly": 333,
	"./ar-ly.js": 333,
	"./ar-ma": 334,
	"./ar-ma.js": 334,
	"./ar-sa": 335,
	"./ar-sa.js": 335,
	"./ar-tn": 336,
	"./ar-tn.js": 336,
	"./ar.js": 330,
	"./az": 337,
	"./az.js": 337,
	"./be": 338,
	"./be.js": 338,
	"./bg": 339,
	"./bg.js": 339,
	"./bm": 340,
	"./bm.js": 340,
	"./bn": 341,
	"./bn.js": 341,
	"./bo": 342,
	"./bo.js": 342,
	"./br": 343,
	"./br.js": 343,
	"./bs": 344,
	"./bs.js": 344,
	"./ca": 345,
	"./ca.js": 345,
	"./cs": 346,
	"./cs.js": 346,
	"./cv": 347,
	"./cv.js": 347,
	"./cy": 348,
	"./cy.js": 348,
	"./da": 349,
	"./da.js": 349,
	"./de": 350,
	"./de-at": 351,
	"./de-at.js": 351,
	"./de-ch": 352,
	"./de-ch.js": 352,
	"./de.js": 350,
	"./dv": 353,
	"./dv.js": 353,
	"./el": 354,
	"./el.js": 354,
	"./en-SG": 355,
	"./en-SG.js": 355,
	"./en-au": 356,
	"./en-au.js": 356,
	"./en-ca": 357,
	"./en-ca.js": 357,
	"./en-gb": 358,
	"./en-gb.js": 358,
	"./en-ie": 359,
	"./en-ie.js": 359,
	"./en-il": 360,
	"./en-il.js": 360,
	"./en-nz": 361,
	"./en-nz.js": 361,
	"./eo": 362,
	"./eo.js": 362,
	"./es": 363,
	"./es-do": 364,
	"./es-do.js": 364,
	"./es-us": 365,
	"./es-us.js": 365,
	"./es.js": 363,
	"./et": 366,
	"./et.js": 366,
	"./eu": 367,
	"./eu.js": 367,
	"./fa": 368,
	"./fa.js": 368,
	"./fi": 369,
	"./fi.js": 369,
	"./fo": 370,
	"./fo.js": 370,
	"./fr": 371,
	"./fr-ca": 372,
	"./fr-ca.js": 372,
	"./fr-ch": 373,
	"./fr-ch.js": 373,
	"./fr.js": 371,
	"./fy": 374,
	"./fy.js": 374,
	"./ga": 375,
	"./ga.js": 375,
	"./gd": 376,
	"./gd.js": 376,
	"./gl": 377,
	"./gl.js": 377,
	"./gom-latn": 378,
	"./gom-latn.js": 378,
	"./gu": 379,
	"./gu.js": 379,
	"./he": 380,
	"./he.js": 380,
	"./hi": 381,
	"./hi.js": 381,
	"./hr": 382,
	"./hr.js": 382,
	"./hu": 383,
	"./hu.js": 383,
	"./hy-am": 384,
	"./hy-am.js": 384,
	"./id": 385,
	"./id.js": 385,
	"./is": 386,
	"./is.js": 386,
	"./it": 387,
	"./it-ch": 388,
	"./it-ch.js": 388,
	"./it.js": 387,
	"./ja": 389,
	"./ja.js": 389,
	"./jv": 390,
	"./jv.js": 390,
	"./ka": 391,
	"./ka.js": 391,
	"./kk": 392,
	"./kk.js": 392,
	"./km": 393,
	"./km.js": 393,
	"./kn": 394,
	"./kn.js": 394,
	"./ko": 395,
	"./ko.js": 395,
	"./ku": 396,
	"./ku.js": 396,
	"./ky": 397,
	"./ky.js": 397,
	"./lb": 398,
	"./lb.js": 398,
	"./lo": 399,
	"./lo.js": 399,
	"./lt": 400,
	"./lt.js": 400,
	"./lv": 401,
	"./lv.js": 401,
	"./me": 402,
	"./me.js": 402,
	"./mi": 403,
	"./mi.js": 403,
	"./mk": 404,
	"./mk.js": 404,
	"./ml": 405,
	"./ml.js": 405,
	"./mn": 406,
	"./mn.js": 406,
	"./mr": 407,
	"./mr.js": 407,
	"./ms": 408,
	"./ms-my": 409,
	"./ms-my.js": 409,
	"./ms.js": 408,
	"./mt": 410,
	"./mt.js": 410,
	"./my": 411,
	"./my.js": 411,
	"./nb": 412,
	"./nb.js": 412,
	"./ne": 413,
	"./ne.js": 413,
	"./nl": 414,
	"./nl-be": 415,
	"./nl-be.js": 415,
	"./nl.js": 414,
	"./nn": 416,
	"./nn.js": 416,
	"./pa-in": 417,
	"./pa-in.js": 417,
	"./pl": 418,
	"./pl.js": 418,
	"./pt": 419,
	"./pt-br": 420,
	"./pt-br.js": 420,
	"./pt.js": 419,
	"./ro": 421,
	"./ro.js": 421,
	"./ru": 422,
	"./ru.js": 422,
	"./sd": 423,
	"./sd.js": 423,
	"./se": 424,
	"./se.js": 424,
	"./si": 425,
	"./si.js": 425,
	"./sk": 426,
	"./sk.js": 426,
	"./sl": 427,
	"./sl.js": 427,
	"./sq": 428,
	"./sq.js": 428,
	"./sr": 429,
	"./sr-cyrl": 430,
	"./sr-cyrl.js": 430,
	"./sr.js": 429,
	"./ss": 431,
	"./ss.js": 431,
	"./sv": 432,
	"./sv.js": 432,
	"./sw": 433,
	"./sw.js": 433,
	"./ta": 434,
	"./ta.js": 434,
	"./te": 435,
	"./te.js": 435,
	"./tet": 436,
	"./tet.js": 436,
	"./tg": 437,
	"./tg.js": 437,
	"./th": 438,
	"./th.js": 438,
	"./tl-ph": 439,
	"./tl-ph.js": 439,
	"./tlh": 440,
	"./tlh.js": 440,
	"./tr": 441,
	"./tr.js": 441,
	"./tzl": 442,
	"./tzl.js": 442,
	"./tzm": 443,
	"./tzm-latn": 444,
	"./tzm-latn.js": 444,
	"./tzm.js": 443,
	"./ug-cn": 445,
	"./ug-cn.js": 445,
	"./uk": 446,
	"./uk.js": 446,
	"./ur": 447,
	"./ur.js": 447,
	"./uz": 448,
	"./uz-latn": 449,
	"./uz-latn.js": 449,
	"./uz.js": 448,
	"./vi": 450,
	"./vi.js": 450,
	"./x-pseudo": 451,
	"./x-pseudo.js": 451,
	"./yo": 452,
	"./yo.js": 452,
	"./zh-cn": 453,
	"./zh-cn.js": 453,
	"./zh-hk": 454,
	"./zh-hk.js": 454,
	"./zh-tw": 455,
	"./zh-tw.js": 455
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 699;

/***/ }),

/***/ 717:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(504);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(503);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_admin_login_admin_login__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_nosotros_nosotros__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_carta_carta__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_reservacion_1_reservacion_1__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_perfil_perfil__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_historial_historial__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_usuario_usuario__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_mis_reservaciones_mis_reservaciones__ = __webpack_require__(228);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, menuCtrl, usuarioProv, afAuth) {
        var _this = this;
        this.menuCtrl = menuCtrl;
        this.usuarioProv = usuarioProv;
        this.afAuth = afAuth;
        this.user = {};
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_admin_login_admin_login__["a" /* AdminLoginPage */];
        this.home = __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */];
        this.nosotros = __WEBPACK_IMPORTED_MODULE_6__pages_nosotros_nosotros__["a" /* NosotrosPage */];
        this.carta = __WEBPACK_IMPORTED_MODULE_7__pages_carta_carta__["a" /* CartaPage */];
        this.perfil = __WEBPACK_IMPORTED_MODULE_9__pages_perfil_perfil__["a" /* PerfilPage */];
        this.historial = __WEBPACK_IMPORTED_MODULE_10__pages_historial_historial__["a" /* HistorialPage */];
        this.reservacion = __WEBPACK_IMPORTED_MODULE_8__pages_reservacion_1_reservacion_1__["a" /* Reservacion_1Page */];
        this.reservaciones = __WEBPACK_IMPORTED_MODULE_13__pages_mis_reservaciones_mis_reservaciones__["a" /* MisReservacionesPage */];
        console.log('esta es la info del usuario');
        console.log(this.usuarioProv.usuario.nombre);
        this.user = this.usuarioProv.usuario;
        platform.ready().then(function () {
            if (localStorage.getItem("isLogin") == "false") {
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_admin_login_admin_login__["a" /* AdminLoginPage */]);
            }
            // else if (localStorage.getItem("reservacion") == "true") {
            //   this.nav.setRoot(ResumenPage, {
            //     idReservacion: localStorage.getItem("idReservacion"),
            //     idSucursal: localStorage.getItem("idSucursal"),
            //     uid: localStorage.getItem("uidEvento")
            //   });
            // }
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    //Menu de la aplicacion
    MyApp.prototype.irHome = function (home) {
        console.log(home);
        this.rootPage = home;
        this.menuCtrl.close();
    };
    MyApp.prototype.irNosotros = function (nosotros) {
        console.log(nosotros);
        this.rootPage = nosotros;
        this.menuCtrl.close();
    };
    MyApp.prototype.irPerfil = function (perfil) {
        console.log(perfil);
        this.rootPage = perfil;
        this.menuCtrl.close();
    };
    MyApp.prototype.irCarta = function (carta) {
        console.log(carta);
        this.rootPage = carta;
        this.menuCtrl.close();
    };
    MyApp.prototype.irHistorial = function (historial) {
        console.log(historial);
        this.rootPage = historial;
        this.menuCtrl.close();
    };
    MyApp.prototype.irReservacion = function (reservacion) {
        console.log(reservacion);
        this.rootPage = reservacion;
        this.menuCtrl.close();
    };
    MyApp.prototype.irMisreservaciones = function (reservaciones) {
        console.log(reservaciones);
        this.rootPage = reservaciones;
        this.menuCtrl.close();
    };
    MyApp.prototype.irLogin = function (rootPage) {
        console.log(rootPage);
        this.rootPage = rootPage;
        this.menuCtrl.close();
    };
    MyApp.prototype.salir = function () {
        //this.afAuth.auth.signOut().then(res => {
        //this.usuarioProv.usuario = {};
        localStorage.setItem("isLogin", 'false');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_admin_login_admin_login__["a" /* AdminLoginPage */]);
        this.menuCtrl.close();
        //});
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\app\app.html"*/'<ion-menu [content]="content">\n\n  <ion-content class="menu-content" padding>\n\n\n\n    <ion-row>\n\n      <ion-col col-3></ion-col>\n\n      <ion-col align="center">\n\n        <!-- <h4>Menú</h4> -->\n\n        <br><br><br>\n\n        <div>\n\n          <div class="margen3_app">\n\n            <!--<img class="imagen_perfil" [src]="user.imagen">-->\n\n            <img class="imagen_perfil" src="../assets/content/user.png">\n\n          </div>\n\n          <br><br><br>\n\n          <div class="margen2_app">\n\n            <h6>{{user.nombre}}Administrador</h6>\n\n          </div>\n\n          <!--<div class="margen1_app">\n\n                <img class="cate" src="../assets/imgs/icons/gold_guest.png">\n\n            </div>-->\n\n        </div>\n\n      </ion-col>\n\n      <ion-col col-3></ion-col>\n\n    </ion-row>\n\n\n\n    <!-- <div class="borde"></div> -->\n\n    <div class="margen4_app">\n\n      <ion-list class="menu-list">\n\n        <!--<button ion-item (click)="irHome( home )">\n\n                    <img class="home" src="../assets/imgs/icons/home.png">  Home\n\n                  </button>-->\n\n        <!-- <button ion-item (click)="irNosotros( nosotros )">\n\n                  <img class="home" src="../assets/imgs/icons/corona.png">  Nosotros\n\n                  </button> -->\n\n        <!--<button ion-item (click)="irReservacion( reservacion )">\n\n                    <img class="home" src="../assets/imgs/icons/time.png"> Reservaciones\n\n                  </button>\n\n                <button ion-item (click)="irMisreservaciones( reservaciones )">\n\n                        <img class="home" src="../assets/imgs/icons/sucursal.png">  Mis Reservaciones\n\n               </button>\n\n                <button ion-item (click)="irCarta( carta )">\n\n                    <img class="home" src="../assets/imgs/icons/champagne.png">  Carta Móvil\n\n                  </button>\n\n                <button ion-item (click)="irPerfil( perfil )">\n\n                    <img class="home" src="../assets/imgs/icons/profile.png"> Perfil\n\n                  </button>\n\n                <button ion-item (click)="irHistorial( historial )">\n\n                    <img class="home" src="../assets/imgs/icons/book.png">  Historial\n\n                  </button>-->\n\n        <ion-footer>\n\n\n\n          <!-- <button style="text-align: right" ion-item (click)="salir()">\n\n            <img class="home" src="../assets/imgs/icons/turn-off.png"> Cerrar Sesión\n\n          </button> -->\n\n          <button\n\n            style="border-radius: 10px;color:#ffff;width:50px;width:130px;height:30px;margin-left:50px;margin-top:30px"\n\n            block (click)="salir()">\n\n            <img src="../assets/imgs/icons/icono negro-03.png" style="width:20px" alt="">\n\n            <font color="black"> Salir </font>\n\n          </button>\n\n        </ion-footer>\n\n\n\n      </ion-list>\n\n    </div>\n\n  </ion-content>\n\n</ion-menu>\n\n\n\n\n\n\n\n<ion-nav [root]="rootPage" #content></ion-nav>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_12__providers_usuario_usuario__["a" /* UsuarioProvider */],
            __WEBPACK_IMPORTED_MODULE_11_angularfire2_auth__["AngularFireAuth"]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 718:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__place_holder_place_holder__ = __webpack_require__(719);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pipes_categoria_pipes_categoria__ = __webpack_require__(720);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_filter_evento_pipes_filter_evento__ = __webpack_require__(721);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__place_holder_place_holder__["a" /* PlaceHolderPipe */],
                __WEBPACK_IMPORTED_MODULE_2__pipes_categoria_pipes_categoria__["a" /* PipesCategoriaPipe */],
                __WEBPACK_IMPORTED_MODULE_3__pipes_filter_evento_pipes_filter_evento__["a" /* PipesFilterEventoPipe */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__place_holder_place_holder__["a" /* PlaceHolderPipe */],
                __WEBPACK_IMPORTED_MODULE_2__pipes_categoria_pipes_categoria__["a" /* PipesCategoriaPipe */],
                __WEBPACK_IMPORTED_MODULE_3__pipes_filter_evento_pipes_filter_evento__["a" /* PipesFilterEventoPipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 719:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaceHolderPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Generated class for the PlaceHolderPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var PlaceHolderPipe = /** @class */ (function () {
    function PlaceHolderPipe() {
    }
    PlaceHolderPipe.prototype.transform = function (value, defecto) {
        if (defecto === void 0) { defecto = "Sin texto"; }
        return (value) ? value : defecto;
    };
    PlaceHolderPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'placeHolder',
        })
    ], PlaceHolderPipe);
    return PlaceHolderPipe;
}());

//# sourceMappingURL=place-holder.js.map

/***/ }),

/***/ 720:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesCategoriaPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Generated class for the PipesCategoriaPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var PipesCategoriaPipe = /** @class */ (function () {
    function PipesCategoriaPipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    //   transform(items: any[], filter: Object): any {
    //     if (!items || !filter) {
    //         return items;
    //     }
    //     // filter items array, items which match and return true will be
    //     // kept, false will be filtered out
    //     return items.filter(item => item.title.indexOf(filter.title) !== -1);
    // }
    PipesCategoriaPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (!arg)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var sucursal = value_1[_i];
            if (sucursal.tipo.indexOf(arg) > -1) {
                resultPosts.push(sucursal);
            }
        }
        return resultPosts;
    };
    PipesCategoriaPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'pipesCategoria',
        })
    ], PipesCategoriaPipe);
    return PipesCategoriaPipe;
}());

//# sourceMappingURL=pipes-categoria.js.map

/***/ }),

/***/ 721:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesFilterEventoPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Generated class for the PipesFilterEventoPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var PipesFilterEventoPipe = /** @class */ (function () {
    function PipesFilterEventoPipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    PipesFilterEventoPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (!arg)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var evento = value_1[_i];
            if (evento.categoria.indexOf(arg) > -1) {
                resultPosts.push(evento);
            }
        }
        return resultPosts;
    };
    PipesFilterEventoPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'pipesFilterEvento',
        })
    ], PipesFilterEventoPipe);
    return PipesFilterEventoPipe;
}());

//# sourceMappingURL=pipes-filter-evento.js.map

/***/ }),

/***/ 723:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartaAddProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__CartaItem__ = __webpack_require__(724);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { ToastController } from 'ionic-angular';
//import { Observable } from 'rxjs/Observable';
var CartaAddProvider = /** @class */ (function () {
    function CartaAddProvider(DB) {
        this.DB = DB;
        this.cartaList = this.DB.list('bebidas');
        //cartaList:  AngularFireList<any>;
        //public cartaList: Observable<CartaItem>
        this.imagenes = [];
        this.selectedProduct = new __WEBPACK_IMPORTED_MODULE_2__CartaItem__["a" /* CartaItem */]();
    }
    CartaAddProvider.prototype.getCarta = function () {
        //return this.cartaList = this.DB.list('bebidas');
    };
    // addCarta(cartaItem: CartaItem ){
    //   return this.cartaList.push(cartaItem);
    // }
    CartaAddProvider.prototype.getEvento = function (id) {
        return this.DB.object('carta/' + id);
    };
    CartaAddProvider.prototype.updateCarta = function (data) {
        console.log(data);
        // this.cartaList.update(cartaItem.$key,{
        //   name: cartaItem.name,
        //   categoria: cartaItem.categoria,
        //   precio: cartaItem.precio
        // });
        return this.cartaList.update(data.$key, data);
    };
    CartaAddProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["AngularFireDatabase"]])
    ], CartaAddProvider);
    return CartaAddProvider;
}());

//# sourceMappingURL=carta-add.js.map

/***/ }),

/***/ 724:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartaItem; });
var CartaItem = /** @class */ (function () {
    function CartaItem() {
    }
    return CartaItem;
}());

//# sourceMappingURL=CartaItem.js.map

/***/ }),

/***/ 725:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_stripe__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PaymentProvider = /** @class */ (function () {
    function PaymentProvider(stripe, loadinCtl, alertCtrl, afs, http) {
        this.stripe = stripe;
        this.loadinCtl = loadinCtl;
        this.alertCtrl = alertCtrl;
        this.afs = afs;
        this.http = http;
        console.log('Hello PaymentProvider Provider');
    }
    PaymentProvider.prototype.payment = function (uidRerservacion) {
        var _this = this;
        // Poppup de carga para procesar el metodo
        var loading = this.loadinCtl.create({
            spinner: "bubbles",
            content: "Cambiando estatus de reservación."
        });
        loading.present();
        /* Cambiando estatus a la reservación  */
        this.afs.collection('reservaciones').doc(uidRerservacion).update({
            estatus: 'Pagando'
        });
        alert('Estatus actualizado');
        //
        this.stripe.setPublishableKey('pk_test_3PsctNfFB5NuLWNc89rvH9gC00a0GVfm9K');
        // 
        var card = {
            number: '4242424242424242',
            expMonth: 12,
            expYear: 2020,
            cvc: '220'
        };
        this.stripe.createCardToken(card)
            .then(function (token) {
            var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Headers */]({
                "Content-Type": "application/json"
            });
            var options = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["d" /* RequestOptions */]({ headers: headers });
            var url = "https://guest-7dfab.firebaseapp.com/pago";
            var data = JSON.stringify({
                cardToken: token.id,
                amount: '500',
                clave: 'clave'
            });
            _this.http.post(url, data, options).subscribe(function (res) {
                if (res.json().status == "succeeded") {
                    var title = "¡ Pago con exito !";
                    setTimeout(function () {
                        loading.dismiss();
                    }, 3000);
                }
            });
        })
            .catch(function (error) { return console.error(error); });
    };
    PaymentProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_stripe__["a" /* Stripe */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */]])
    ], PaymentProvider);
    return PaymentProvider;
}());

//# sourceMappingURL=payment.js.map

/***/ }),

/***/ 86:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CargaArchivoCartaProvider; });
/* unused harmony export CartaSubir */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ArchivoSubir__ = __webpack_require__(688);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import   "rxjs/add/operator/map";
//import { Observable } from 'rxjs/Observable';
// import { Credenciales } from '../usuario/usuario';

var CargaArchivoCartaProvider = /** @class */ (function () {
    //lastKey: string = null;  
    //private cartaList: Observable<ArchivoSubir>;
    //private cartaList = this.afDB.list<ArchivoSubir>('bebidas');  
    function CargaArchivoCartaProvider(toastCtrl, afDB, afs) {
        // this.cargar_ultimo_key()
        //     .subscribe(()=>this.cargar_imagenes());
        this.toastCtrl = toastCtrl;
        this.afDB = afDB;
        this.afs = afs;
        this.selectedProduct = new __WEBPACK_IMPORTED_MODULE_4__ArchivoSubir__["a" /* ArchivoSubir */]();
        this.db = __WEBPACK_IMPORTED_MODULE_3_firebase__["firestore"]();
        this.imagenes = [];
    }
    // private cargar_ultimo_key(){
    //   return this.afDB.list('/bebidas', ref=> ref.orderByKey().limitToLast(1))
    //             .valueChanges()
    //             .map( (bebida:any) => {
    //               this.lastKey = bebida[0].key;
    //               this.imagenes.push( bebida[0]);
    //             });
    //}
    // cargar_imagenes(){
    //   return new Promise ((resolve, reject)=>{
    //     this.afDB.list('/bebidas',
    //       ref=> ref.limitToLast(8)
    //                 .orderByKey()
    //                 .endAt(this.lastKey)
    //               ).valueChanges()
    //                .subscribe((bebidas:any)=>{
    //                  bebidas.pop();
    //               if( bebidas.length == 0){
    //                 console.log('Ya no hay más registros');
    //                 resolve(false);
    //                 return;
    //               }
    //               this.lastKey = bebidas[0].key;
    //               for( let i = bebidas.length-1; i>=0; i--){
    //                 let bebida = bebidas[i];
    //                 this.imagenes.push(bebida);
    //               }
    //               resolve(true);
    //             });
    //   });
    // }
    CargaArchivoCartaProvider.prototype.cargar_imagen_firebase = function (archivo) {
        var _this = this;
        var promesa = new Promise(function (resolve, reject) {
            _this.mostrar_toast('Cargando..');
            var storeRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["storage"]().ref();
            var nombreArchivo = new Date().valueOf().toString();
            var uploadTask = storeRef.child("bebidas/" + nombreArchivo + ".jpg")
                .putString(archivo.img, 'base64', { contentType: 'image/jpeg' });
            uploadTask.on(__WEBPACK_IMPORTED_MODULE_3_firebase__["storage"].TaskEvent.STATE_CHANGED, function () { }, //saber el % cuantos se han subido
            function (error) {
                //manejo
                console.log("Error en la carga");
                console.log(JSON.stringify(error));
                _this.mostrar_toast(JSON.stringify(error));
                reject();
            }, function () {
                // TODO BIEN!
                console.log('Archivo subido');
                _this.mostrar_toast('Imagen cargada correctamente');
                // let url = uploadTask.snapshot.downloadURL;
                // this.crear_post( archivo.titulo, url, nombreArchivo );
                // resolve();
                uploadTask.snapshot.ref.getDownloadURL().then(function (url) {
                    console.log('nombreArchivo', nombreArchivo);
                    console.log('url', url);
                    console.log('file.titulo', archivo.titulo);
                    _this.crear_post(archivo.titulo, archivo.categoria, archivo.precio, archivo.nota, url, nombreArchivo, archivo.uidSucursal);
                    resolve();
                });
            });
        });
        return promesa;
    };
    CargaArchivoCartaProvider.prototype.crear_post = function (titulo, categoria, precio, nota, url, nombreArchivo, uidSucursal) {
        var _this = this;
        var bebida = {
            img: url,
            titulo: titulo,
            categoria: categoria,
            precio: precio,
            nota: nota,
            key: nombreArchivo,
            uidSucursal: uidSucursal
        };
        console.log(JSON.stringify(bebida));
        //this.afDB.list('/evento').push(evento);
        this.afs.collection('cartas').add(bebida).then(function (ref) {
            console.log('id', ref.id);
            var cartaUid = ref.id;
            _this.afs.collection('cartas').doc(cartaUid).update({
                uid: cartaUid
            });
        });
        // this.afDB.object(`bebidas/${ nombreArchivo }`).update(bebida);
        this.imagenes.push(bebida);
        this.mostrar_toast('Evento grabado a BD');
    };
    CargaArchivoCartaProvider.prototype.deleteCarta = function (key, img, uid) {
        var _this = this;
        var storeRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["storage"]().ref();
        var desertRef = storeRef.child("bebidas/" + img + ".jpg");
        console.log(img);
        // Delete the file
        desertRef.delete().then(function () {
            this.mostrar_toast('Finally');
        }).catch(function (error) {
            // Uh-oh, an error occurred!
        });
        // this.afDB.database.ref('bebidas/'+key).remove();   
        var promise = new Promise(function (resolve, reject) {
            console.log('data delete uid', uid);
            _this.db
                .collection("cartas")
                .doc(uid)
                .delete()
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    CargaArchivoCartaProvider.prototype.updateCarta = function (data, uid) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            console.log('data uid', uid);
            _this.db
                .collection("cartas")
                .doc(uid)
                .update(data)
                .then(function () {
                resolve(true);
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
        // console.log(data.KEY);
        // this.afDB.database.ref('bebidas/'+data.KEY).update(data);
    };
    CargaArchivoCartaProvider.prototype.mostrar_toast = function (mensaje) {
        var toast = this.toastCtrl.create({
            message: mensaje,
            duration: 3000
        }).present();
    };
    //  public getEvento(id){
    //     return this.afDB.object('bebidas/'+id);
    //  }
    // Edicion de imagen 
    CargaArchivoCartaProvider.prototype.cargar_imagen_firebase_carta = function (archivo, uid) {
        var _this = this;
        var promesa = new Promise(function (resolve, reject) {
            _this.mostrar_toast('Cargando..');
            var storeRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["storage"]().ref();
            var img = new Date().valueOf().toString();
            var uploadTask = storeRef.child("bebidas/" + img + ".jpg")
                .putString(archivo.img, 'base64', { contentType: 'image/jpeg' });
            uploadTask.on(__WEBPACK_IMPORTED_MODULE_3_firebase__["storage"].TaskEvent.STATE_CHANGED, function () { }, //saber el % cuantos se han subido
            function (error) {
                //manejo
                console.log("Error en la carga");
                console.log(JSON.stringify(error));
                _this.mostrar_toast(JSON.stringify(error));
                reject();
            }, function () {
                // TODO BIEN!
                console.log('Archivo subido');
                _this.mostrar_toast('Imagen cargada correctamente');
                // let url = uploadTask.snapshot.downloadURL;
                // this.crear_post( archivo.titulo, url, nombreArchivo );
                // resolve();
                uploadTask.snapshot.ref.getDownloadURL().then(function (urlImage) {
                    _this.crear_post_edev(archivo.key, urlImage, uid);
                    _this.mostrar_toast('URL' + urlImage);
                }).catch(function (error) {
                    console.log(error);
                });
                resolve();
            });
        });
        return promesa;
    };
    CargaArchivoCartaProvider.prototype.crear_post_edev = function (key, url, uid) {
        var _this = this;
        var carta = {
            key: key,
            img: url
        };
        console.log(JSON.stringify(carta));
        var promise = new Promise(function (resolve, reject) {
            console.log('data key', key);
            console.log('data uid', uid);
            _this.db
                .collection("cartas")
                .doc(uid)
                .update(carta)
                .then(function () {
                resolve(true);
                _this.mostrar_toast('Imagen actualizada');
            })
                .catch(function (err) {
                reject(err);
            });
        });
        return promise;
        // this.afDB.object(`bebidas/`+key).update(carta);
        // this.imagenes.push(sucursal);
    };
    CargaArchivoCartaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], CargaArchivoCartaProvider);
    return CargaArchivoCartaProvider;
}());

var CartaSubir = /** @class */ (function () {
    function CartaSubir() {
    }
    return CartaSubir;
}());

//# sourceMappingURL=carga-archivo.js.map

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase_app__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase_firestore__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import firebase from 'firebase';






var UserProvider = /** @class */ (function () {
    function UserProvider(afireauth, afiredatabase, afs, http, toastCtrl) {
        this.afireauth = afireauth;
        this.afiredatabase = afiredatabase;
        this.afs = afs;
        this.http = http;
        this.toastCtrl = toastCtrl;
        // firedata = firebase.database().ref('/users');
        this.db = __WEBPACK_IMPORTED_MODULE_5_firebase_app__["firestore"]();
        console.log("Hello UserProvider Provider");
        afireauth.authState.subscribe(function (user) {
            console.log('user', user);
            if (user) {
                var uidUser = user.uid;
                localStorage.setItem('uid', uidUser);
                console.log("este es el uid de", uidUser);
            }
        });
    }
    UserProvider.prototype.newRegister = function (newuser, uidSucursal) {
        var _this = this;
        //var config = {apiKey: "AIzaSyBixlCb21nNbPSurY-Pvqu3hZB80Icl9Pk",
        //authDomain: "guestreservation-8b24b.firebaseapp.com",
        //databaseURL: "https://guestreservation-8b24b.firebaseio.com"};
        //var secondaryApp = firebase.initializeApp(config, "Secondary");
        this.afireauth.auth.createUserWithEmailAndPassword(newuser.email, newuser.password).then(function (firebaseUser) {
            _this.uid = firebaseUser.user.uid;
            console.log("User " + _this.uid + " created successfully!");
            _this.register(newuser, uidSucursal);
        });
    };
    UserProvider.prototype.register = function (newuser, uidSucursal) {
        this.afs
            .collection("users").doc(this.uid)
            .set({
            uid: this.uid,
            displayName: newuser.name,
            correo: newuser.email,
            type: newuser.type,
            active: "true",
            uidSucursal: uidSucursal,
            photoURL: "../assets/imgs/icons/profile.png",
        });
    };
    UserProvider.prototype.delete_user = function (uid) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        //
        var options = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var url = 'https://guestreservation-8b24b.firebaseapp.com/delete';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('El usuario se elimino de SDK');
        });
        // Eliminamos el usuario de la base de datos
        this.afs.collection('users').doc(uid).delete().then(function () {
            _this.mostrar_toast('Se elimino el empleado.');
            console.log('Se elimino el usuario de la base de datos');
        });
    };
    // Inhabilitar cuenta de empleado
    UserProvider.prototype.inhabilitar_user = function (uid) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        //
        var options = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var url = 'https://guestreservation-8b24b.firebaseapp.com/inhabilitar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('El usuario se inhabilito del SDK');
        });
        // Actualizamos al usuario de la base de datos
        this.afs.collection('users').doc(uid).update({
            active: 'false'
        }).then(function () {
            _this.mostrar_toast('Se inhabilito al usuario.');
        });
    };
    // habilitar cuenta de empleado
    UserProvider.prototype.habilitar_user = function (uid) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        //
        var options = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var url = 'https://guestreservation-8b24b.firebaseapp.com/habilitar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('El usuario se inhabilito del SDK');
        });
        // Actualizamos al usuario de la base de datos
        this.afs.collection('users').doc(uid).update({
            active: 'true'
        }).then(function () {
            _this.mostrar_toast('Se habilito al usuario.');
        });
    };
    UserProvider.prototype.idOneSignal = function (idx, playerID) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.db
                .collection("users")
                .doc(idx)
                .update({
                playerID: playerID
            })
                .then(function () {
                console.log("Document written with ID: ", idx);
                resolve({ success: true });
            })
                .catch(function (error) {
                console.error("Error adding document: ", JSON.stringify(error));
            });
        });
    };
    UserProvider.prototype.getAllUsers = function () {
        this.users = this.afs.collection("users");
        this._users = this.users.valueChanges();
        return (this._users = this.users.snapshotChanges().pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    UserProvider.prototype.mostrar_toast = function (mensaje) {
        var toast = this.toastCtrl.create({
            message: mensaje,
            duration: 3000
        }).present();
    };
    UserProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_7__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["o" /* ToastController */]])
    ], UserProvider);
    return UserProvider;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReservacionesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_carga_archivo_carga_archivo__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_reservacion_reservacion__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__carta_carta__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ReservacionesPage = /** @class */ (function () {
    function ReservacionesPage(navCtrl, navParams, alertCtrl, modalCtrl, afDB, fb, _cap, _providerReserva, afs) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.afDB = afDB;
        this.fb = fb;
        this._cap = _cap;
        this._providerReserva = _providerReserva;
        this.afs = afs;
        this.sucursal = {};
        this.arquitectura = {};
        this.data = {};
        this.ocultar = false;
        this.Sucursal = "reservar";
        this.people = 0;
        this.disabledFecha = false;
        this.idSucursal = this.navParams.get("idSucursal");
        this.evento = this.navParams.get("uid");
        this.idReservacion = this.navParams.get("idReservacion");
        if (this.evento != null) {
            this.evento = this.navParams.get("uid");
        }
        else {
            this.evento = null;
        }
        this.afs
            .collection("sucursales")
            .doc(this.idSucursal)
            .valueChanges()
            .subscribe(function (data) {
            _this.sucursal = data;
            console.log(_this.sucursal);
        });
        this.myForm = this.fb.group({
            hora: [" ", [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]],
            fecha: ["", [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]],
            area: [" ", [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]],
            zona: ["", [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]]
        });
        console.log("Area seleccionada: ", this.data.area);
    }
    ReservacionesPage.prototype.ionViewDidLoad = function () {
        if (this.evento != null) {
            this.getDetails();
            this.disabledFecha = true;
        }
        if (this.idReservacion != null) {
            this.loadReservacion(this.idReservacion);
        }
        this.getAreas(this.idSucursal);
        console.log("ionViewDidLoad EventoDetallePage");
        this.fechaActual = new Date().toJSON().split("T")[0];
        console.log("horaActual Actual: ");
    };
    ReservacionesPage.prototype.goBack = function (idReservacion) {
        var _this = this;
        if (this.idReservacion != null) {
            this.navCtrl.popToRoot().then(function () {
                _this._providerReserva.deleteReservacion(idReservacion);
                localStorage.removeItem("idReservacion");
                localStorage.removeItem("idSucursal");
                localStorage.removeItem("uidEvento");
            });
        }
        else {
            this.navCtrl.popToRoot();
        }
    };
    ReservacionesPage.prototype.getDetails = function () {
        var _this = this;
        this._cap.getEvento(this.evento).then(function (e) {
            _this.data = e;
            console.log("evento", _this.data.plano);
        });
    };
    ReservacionesPage.prototype.loadReservacion = function (idx) {
        var _this = this;
        this._providerReserva.getReservacion(idx).subscribe(function (reservacion) {
            if (reservacion != null) {
                _this.people = reservacion.numPersonas;
                _this.data.hora = reservacion.hora;
                _this.data.area = reservacion.idArea;
                _this._getZonas(_this.data.area);
                _this.data.zona = reservacion.idZona;
            }
        });
    };
    ReservacionesPage.prototype.increment = function () {
        if (this.people < 20) {
            this.people++;
        }
    };
    ReservacionesPage.prototype.decrement = function () {
        if (this.people > 0) {
            this.people--;
        }
    };
    ReservacionesPage.prototype.getAreas = function (idx) {
        var _this = this;
        this._providerReserva.getAreas(idx).subscribe(function (areas) {
            console.log("areas", areas);
            _this.areas = areas;
        });
    };
    ReservacionesPage.prototype.getZonas = function () {
        var _this = this;
        var idx = this.idSucursal;
        var area = this.data.area;
        this._providerReserva.getZonas(idx, area).subscribe(function (zonas) {
            console.log("zona", zonas);
            _this.zonas = zonas;
        });
    };
    ReservacionesPage.prototype._getZonas = function (_area) {
        var _this = this;
        var idx = this.idSucursal;
        this._providerReserva.getZonas(idx, _area).subscribe(function (zonas) {
            console.log("zona", zonas);
            _this.zonas = zonas;
        });
    };
    ReservacionesPage.prototype.alertConsumo = function () {
        var _this = this;
        var zona = this.data.zona;
        this._providerReserva.getZona(zona).subscribe(function (zona) {
            var formatter = new Intl.NumberFormat("en-MX", {
                style: "currency",
                currency: "MXN",
                minimumFractionDigits: 2
            });
            var x = formatter.format(zona.consumo); // "$1,000.00"
            console.log(x);
            var alertMesas = _this.alertCtrl.create({
                message: "<div text-center> Esta zona cuenta con un consumo sugerido de " +
                    "<br><br>" +
                    "<b>" +
                    formatter.format(zona.consumo) +
                    "</b>" +
                    "</div>",
                buttons: [
                    {
                        text: "Aceptar",
                        handler: function () {
                            console.log("Buy clicked");
                        }
                    }
                ]
            });
            alertMesas.present();
        });
    };
    ReservacionesPage.prototype.getMesas = function () {
        var _this = this;
        var idx = this.idSucursal;
        var area = this.data.area;
        var zonas = this.data.zona;
        this._providerReserva.getMesas(idx, area, zonas).subscribe(function (mesas) {
            _this.mesas = mesas;
            console.log("Mesas : ", _this.mesas);
            var _mesas = mesas.length;
            console.log("Mesas : ", _mesas);
            var _personas = _this.mesas.reduce(function (acc, obj) { return acc + obj.numPersonas; }, 0);
            console.log("Personas : ", _personas);
            // this.alertMesas(_mesas, _personas);
        });
    };
    ReservacionesPage.prototype.alertMesas = function (mesas, personas) {
        var alertMesas = this.alertCtrl.create({
            message: "<div text-center> Esta zona cuenta con " +
                "<b>" +
                mesas +
                "</b>" +
                " mesas y con " +
                "<b>" +
                personas +
                "</b>" +
                " lugares disponibles. </div>",
            buttons: [
                {
                    text: "Atras",
                    role: "cancel",
                    handler: function () {
                        console.log("Cancel clicked");
                    }
                },
                {
                    text: "Aceptar",
                    handler: function () {
                        console.log("Buy clicked");
                    }
                }
            ]
        });
        alertMesas.present();
    };
    ReservacionesPage.prototype.reservacionAdd = function () {
        var _this = this;
        var alertMesas = this.alertCtrl.create({
            title: "Carta",
            message: "¿Deseas ordenar de la carta?",
            buttons: [
                {
                    text: "Más Tarde",
                    role: "cancel",
                    handler: function () {
                        console.log("Sin consumo");
                        var info = {
                            numPersonas: _this.people,
                            hora: _this.data.hora,
                            fecha: _this.data.fecha,
                            area: _this.data.area,
                            zona: _this.data.zona,
                            idSucursal: _this.idSucursal,
                            idevento: _this.evento
                        };
                        _this._providerReserva
                            .saveReservacion(info)
                            .then(function (respuesta) {
                            console.log("Respuesta: ", respuesta);
                        });
                    }
                },
                {
                    text: "Aceptar",
                    handler: function () {
                        console.log("Con consumo");
                        var info = {
                            numPersonas: _this.people,
                            hora: _this.data.hora,
                            fecha: _this.data.fecha,
                            area: _this.data.area,
                            zona: _this.data.zona,
                            idSucursal: _this.idSucursal,
                            idevento: _this.evento
                        };
                        _this._providerReserva
                            .saveReservacion(info)
                            .then(function (respuesta) {
                            console.log("Respuesta: ", respuesta);
                            if (respuesta.success == true) {
                                console.log("Success: ", respuesta.success);
                                localStorage.setItem("idReservacion", respuesta.idReservacion);
                                localStorage.setItem("idSucursal", _this.idSucursal);
                                localStorage.setItem("uidEvento", _this.evento);
                                localStorage.setItem('reservacion', 'true');
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__carta_carta__["a" /* CartaPage */], {
                                    idReservacion: respuesta.idReservacion,
                                    idSucursal: _this.idSucursal,
                                    uid: _this.evento
                                });
                            }
                        });
                    }
                }
            ]
        });
        alertMesas.present();
    };
    ReservacionesPage.prototype.reservacionUpdate = function (idReservacion) {
        var _this = this;
        var alertMesas = this.alertCtrl.create({
            title: "Carta",
            message: "¿Deseas ordenar de la carta?",
            buttons: [
                {
                    text: "Declinar",
                    role: "cancel",
                    handler: function () {
                        console.log("Sin consumo");
                        var info = {
                            numPersonas: _this.people,
                            hora: _this.data.hora,
                            fecha: _this.data.fecha,
                            area: _this.data.area,
                            zona: _this.data.zona,
                            idSucursal: _this.idSucursal,
                            idevento: _this.evento
                        };
                        _this._providerReserva
                            .updateReservacion(idReservacion, info)
                            .then(function (respuesta) {
                            console.log("Respuesta: ", respuesta);
                        });
                    }
                },
                {
                    text: "Aceptar",
                    handler: function () {
                        console.log("Con consumo");
                        var info = {
                            numPersonas: _this.people,
                            hora: _this.data.hora,
                            fecha: _this.data.fecha,
                            area: _this.data.area,
                            zona: _this.data.zona,
                            idSucursal: _this.idSucursal,
                            idevento: _this.evento
                        };
                        _this._providerReserva
                            .updateReservacion(idReservacion, info)
                            .then(function (respuesta) {
                            console.log("Respuesta: ", respuesta);
                            if (respuesta.success == true) {
                                console.log("Success: ", respuesta.success);
                                localStorage.setItem("idReservacion", idReservacion);
                                var cartaModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__carta_carta__["a" /* CartaPage */], {
                                    idReservacion: idReservacion
                                });
                                cartaModal.present();
                            }
                        });
                    }
                }
            ]
        });
        alertMesas.present();
    };
    ReservacionesPage.prototype.areaSeleccionada = function () {
        console.log("Esta es el área: " + this.data.area);
    };
    // ionViewDidLoad() {
    //   console.log('ionViewDidLoad ReservacionesPage');
    // }
    ReservacionesPage.prototype.ocultarClic = function () {
        this.ocultar = !this.ocultar;
        this.checkActiveButton();
    };
    ReservacionesPage.prototype.checkActiveButton = function () {
        if (this.ocultar) {
            this.ocultar = true;
        }
        else if (!this.ocultar) {
            this.ocultar = false;
        }
    };
    ReservacionesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-reservaciones",template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\reservaciones\reservaciones.html"*/'<ion-header>\n\n\n\n    <ion-navbar hideBackButton>\n\n        <ion-title style="text-align: center;">Reservación</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <div class="fon bar bar-subheader bar-energized">\n\n        <ion-card class="sin">\n\n            <img class="sin" *ngIf="sucursal.tipo == \'bar\'" src="../assets/imgs/bar.png" />\n\n            <img class="sin" *ngIf="sucursal.tipo == \'antro\'" src="../assets/imgs/antro.png" />\n\n            <!-- <div class="card-title">São Paulo</div>\n\n            <div class="card-subtitle">41 Listings</div> -->\n\n            <ion-col class="card-title" col-4>\n\n                <img class="imgSucursal" [src]="sucursal.photoURL" />\n\n            </ion-col>\n\n            <ion-col class="card-subtitle">\n\n                <p class="blanco_2">{{sucursal.displayName}}</p>\n\n                <p class="blanco_2">{{sucursal.direccion}}</p>\n\n                <div class="card_subtitle">\n\n                    <div *ngIf="sucursal.tipo == \'antro\'"> <img *ngIf="sucursal.tipo == \'antro\' " class="icon" src="../assets/imgs/icons/antro.png" item-start> Antro</div>\n\n                    <div *ngIf="sucursal.tipo == \'bar\'"><img *ngIf="sucursal.tipo == \'bar\' " class="icon" src="../assets/imgs/icons/bar.png" item-start> Bar</div>\n\n                    <div *ngIf="sucursal.tipo == \'restaurante\'"><img *ngIf="sucursal.tipo == \'restaurante\' " class="icon" src="../assets/imgs/icons/restaurante.png" item-start> Restaurante</div>\n\n                </div>\n\n            </ion-col>\n\n        </ion-card>\n\n    </div>\n\n\n\n    <!--Inician las pruebas-->\n\n    <div padding>\n\n        <ion-segment [(ngModel)]="Sucursal">\n\n            <ion-segment-button value="reservar" aria-pressed="true">\n\n                Reservar\n\n            </ion-segment-button>\n\n            <ion-segment-button value="informacion">\n\n                Información\n\n            </ion-segment-button>\n\n        </ion-segment>\n\n    </div>\n\n\n\n    <div [ngSwitch]="Sucursal">\n\n        <ion-list *ngSwitchCase="\'reservar\'">\n\n            <h4 text-center class="blanco_2"> Realiza tu Reservación</h4>\n\n            <form [formGroup]="myForm" novalidate class="card_atras">\n\n                <ion-list>\n\n                    <div *ngIf="evento != null">\n\n                        <ion-item>\n\n                            <ion-grid>\n\n                                <ion-row>\n\n                                    <ion-col col-6>\n\n                                        Reservación para:\n\n                                    </ion-col>\n\n                                    <ion-col col-6>\n\n                                        <div text-rigth class="titulo">\n\n                                            {{data.titulo}}\n\n                                        </div>\n\n                                    </ion-col>\n\n                                </ion-row>\n\n                            </ion-grid>\n\n                        </ion-item>\n\n                    </div>\n\n                    <ion-item>\n\n                        <ion-grid>\n\n                            <ion-row>\n\n                                <ion-col col-4>\n\n                                    <div text-center>\n\n                                        <img src="assets/imgs/icons/admin-user.png" id="people">\n\n                                    </div>\n\n                                </ion-col>\n\n                                <ion-col col-3>\n\n                                    <div text-right>\n\n                                        <img src="assets/imgs/icons/-.png" id="mas_menos" (click)="decrement()">\n\n                                    </div>\n\n                                </ion-col>\n\n                                <ion-col col-2 id="caja">\n\n                                    <div class="caja" text-center>\n\n                                        <span>{{people}}</span>\n\n                                    </div>\n\n                                </ion-col>\n\n                                <ion-col col-3>\n\n                                    <div text-rigth>\n\n                                        <img src="assets/imgs/icons/+.png" id="mas_menos" (click)="increment()">\n\n                                    </div>\n\n                                </ion-col>\n\n                            </ion-row>\n\n                        </ion-grid>\n\n                    </ion-item>\n\n                    <ion-item>\n\n                        <ion-label>\n\n                            <div class="_icons">\n\n                                <img src="assets/imgs/icons/time.png" id="label">\n\n                            </div>\n\n                        </ion-label>\n\n                        <ion-datetime item-end displayFormat="hh:mm a" [(ngModel)]="data.hora" name="hora" formControlName="hora" doneText=Aceptar cancelText=Cancelar></ion-datetime>\n\n                    </ion-item>\n\n                    <ion-item>\n\n                        <ion-label>\n\n                            <div class="_icons">\n\n                                <img src="assets/imgs/icons/calendario.png" id="label">\n\n                            </div>\n\n                        </ion-label>\n\n                        <ion-datetime item-end displayFormat="DD/MM/YYYY" [(ngModel)]="data.fecha" name="fecha" formControlName="fecha" [disabled]="disabledFecha" min={{fechaActual}} doneText=Aceptar cancelText=Cancelar>\n\n                        </ion-datetime>\n\n                    </ion-item>\n\n                    <ion-item>\n\n                        <button ion-button block type="button" color="dark" (click)="ocultarClic()">{{  !ocultar ? \'Mostrar croquis\' : \'Ocultar croquis\'}} </button>\n\n                    </ion-item>\n\n                    <ion-card>\n\n                        <div *ngIf="ocultar">\n\n                            <img class="imagen_croquis" [src]="sucursal.plano">\n\n                        </div>\n\n                    </ion-card>\n\n                    <ion-item>\n\n                        <ion-label>Área</ion-label>\n\n                        <ion-select [(ngModel)]="data.area" multiple="false" cancelText="Cancelar" okText="Aceptar" name="area" formControlName="area" (ionChange)=getZonas()>\n\n                            <div>\n\n                                <ion-option *ngFor="let area of areas" value="{{area.$key}}">{{area.nombre}}</ion-option>\n\n                            </div>\n\n                        </ion-select>\n\n                    </ion-item>\n\n                    <ion-item>\n\n                        <ion-label>Zona</ion-label>\n\n                        <ion-select [(ngModel)]="data.zona" multiple="false" cancelText="Cancelar" okText="Aceptar" name="zona" formControlName="zona" (ionChange)=alertConsumo()>\n\n                            <div>\n\n                                <ion-option *ngFor="let zona of zonas" value="{{zona.$key}}">{{ zona.nombre }}\n\n                                </ion-option>\n\n                            </div>\n\n                        </ion-select>\n\n                    </ion-item>\n\n                </ion-list>\n\n            </form>\n\n        </ion-list>\n\n\n\n        <ion-list *ngSwitchCase="\'informacion\'">\n\n            <ion-list class="card_atras">\n\n                <ion-item>\n\n                    <ion-label>Descripción:</ion-label>\n\n                    <ion-label text-wrap>{{sucursal.descripcion}}</ion-label>\n\n                </ion-item>\n\n                <ion-item>\n\n                    <ion-label>Tipo:</ion-label>\n\n                    <ion-label>{{sucursal.tipo}}</ion-label>\n\n                </ion-item>\n\n                <ion-item>\n\n                    <ion-label>Horario:</ion-label>\n\n                    <ion-label>{{sucursal.horas}}</ion-label>\n\n                </ion-item>\n\n                <ion-item>\n\n                    <ion-label>Estacionamiento:</ion-label>\n\n                    <ion-label text-wrap>{{sucursal.estacionamiento}}</ion-label>\n\n                </ion-item>\n\n                <ion-item>\n\n                    <ion-label>Código de Etiqueta:</ion-label>\n\n                    <ion-label text-wrap>{{sucursal.codigoEtiqueta}}</ion-label>\n\n                </ion-item>\n\n                <!-- <ion-item>\n\n                    <ion-label>Esta es la latitud: {{lat}}</ion-label>\n\n                </ion-item> -->\n\n\n\n            </ion-list>\n\n\n\n\n\n            <!-- <agm-map [zoom]="zoom" [latitude]="lat" [longitude]="lng">\n\n                <agm-marker [latitude]="lat" [longitude]="lng"></agm-marker>\n\n            </agm-map> -->\n\n        </ion-list>\n\n    </div>\n\n</ion-content>\n\n\n\n<ion-footer no-border>\n\n    <ion-toolbar>\n\n        <ion-grid>\n\n            <ion-row>\n\n                <ion-col col-6>\n\n                    <div text-center>\n\n                        <button ion-button round block round color="dark" (click)="(goBack(idReservacion))">Atras</button>\n\n                    </div>\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                    <div text-center>\n\n                        <div *ngIf="idReservacion == null">\n\n                            <button ion-button block round color="dark" (click)="reservacionAdd()" [disabled]="!myForm.valid || people == 0">Continuar</button>\n\n                        </div>\n\n                        <div *ngIf="idReservacion != null">\n\n                            <button ion-button block round color="dark" (click)="reservacionUpdate(idReservacion)" [disabled]="!myForm.valid || people == 0">Continuar</button>\n\n                        </div>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-grid>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\reservaciones\reservaciones.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__providers_carga_archivo_carga_archivo__["a" /* CargaArchivoProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_reservacion_reservacion__["a" /* ReservacionProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], ReservacionesPage);
    return ReservacionesPage;
}());

//# sourceMappingURL=reservaciones.js.map

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__producto_detalle_producto_detalle__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__reservaciones_reservaciones__ = __webpack_require__(91);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//Firebase

// import { Observable } from 'rxjs/Observable';
//import { Observable } from 'rxjs-compat';
//import { map } from 'rxjs-compat/operators';
//import { CargaArchivoCartaProvider } from '../../providers/carga-archivo-carta/carga-archivo';



var CartaPage = /** @class */ (function () {
    function CartaPage(navCtrl, navParam, afDB, afs, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParam = navParam;
        this.afDB = afDB;
        this.afs = afs;
        this.modalCtrl = modalCtrl;
        this.cartas = [];
        this.afs
            .collection("cartas")
            .valueChanges()
            .subscribe(function (c) {
            _this.cartas = c;
            console.log("cartas", c);
        });
        //  this.cartas = afDB.list('bebidas').valueChanges();
        this.idReservacion = localStorage.getItem("idReservacion");
        this.idSucursal = navParam.get("idSucursal");
        this.evento = navParam.get('uid');
    }
    CartaPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad CartaPage");
    };
    CartaPage.prototype.goBack = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__reservaciones_reservaciones__["a" /* ReservacionesPage */], {
            idReservacion: this.idReservacion,
            idSucursal: this.idSucursal,
            uid: this.evento
        });
    };
    CartaPage.prototype.productoDetalle = function (idProducto) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__producto_detalle_producto_detalle__["a" /* ProductoDetallePage */], {
            idProducto: idProducto,
            idReservacion: this.idReservacion,
            uid: this.evento,
            idSucursal: this.idSucursal
        });
    };
    CartaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-carta",template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\carta\carta.html"*/'<ion-header>\n\n\n\n    <ion-navbar hideBackButton>\n\n        <ion-title>Carta Móvil</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <!-- <ion-list *ngFor=\'let task of tasks | async\'>\n\n    <ion-item>{{ task.name }}</ion-item>\n\n</ion-list> -->\n\n    <!--Barra de busqueda-->\n\n    <!-- <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar> -->\n\n    <ion-card class="card_atras">\n\n\n\n        <ion-card-header class="card_atras">\n\n            <p class="blanco_1" text-center> Carta de bebidas </p>\n\n        </ion-card-header>\n\n        <ion-card-content class="card_atras">\n\n            <p class="blanco_2"> Puedes reservar tus bebidas antes y despues de tu reservación. </p>\n\n        </ion-card-content>\n\n\n\n    </ion-card>\n\n    <ion-grid>\n\n        <ion-row>\n\n            <ion-col col-6 *ngFor="let cartaItem of cartas">\n\n                <ion-card class="card_atras">\n\n                    <ion-card-content class="card_atras">\n\n                        <img [src]="cartaItem.img" />\n\n                        <ion-card-title class="blanco_1" text-center>\n\n                            {{cartaItem.titulo}}\n\n                        </ion-card-title>\n\n                        <p class="blanco_2">{{cartaItem.categoria}}</p>\n\n                        <ion-item class="card_atras">\n\n                            <img class="icon" src="../assets/imgs/icons/billete.png" item-start>\n\n                            <ion-badge class="fon" item-end>{{cartaItem.precio | currency}}</ion-badge>\n\n                        </ion-item>\n\n                        <!-- <ion-item class="card_atras">\n\n                        <img class="icon" src="../assets/imgs/icons/admin-historial.png" item-start>\n\n                        <p class="blanco_1"> Nota </p>\n\n                        <ion-badge class="fon" item-end>{{cartaItem.nota}}</ion-badge>\n\n                    </ion-item> -->\n\n                        <ion-item style="text-align: right;" class="card_atras" (click)="productoDetalle(cartaItem.uid)">\n\n                            <img class="icon" src="../assets/imgs/icons/corona.png" item-end>\n\n                            <a class="blanco_2">Reservar</a>\n\n                            <ion-badge item-end></ion-badge>\n\n                        </ion-item>\n\n                    </ion-card-content>\n\n                </ion-card>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n</ion-content>\n\n\n\n<ion-footer no-border>\n\n    <ion-toolbar>\n\n        <ion-grid>\n\n            <ion-row>\n\n                <ion-col col-6>\n\n                    <div text-center>\n\n                        <button ion-button round block color="dark" (click)="(goBack())">Atras</button>\n\n                    </div>\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                    <div text-center>\n\n                        <button ion-button round block color="dark" (click)="(goBack())">Continuar</button>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-grid>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\carta\carta.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], CartaPage);
    return CartaPage;
}());

//# sourceMappingURL=carta.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminSucursalListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_sucursal_subir_admin_sucursal_subir__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__admin_sucursal_perfil_admin_sucursal_perfil__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__admin_home_admin_home__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AdminSucursalListPage = /** @class */ (function () {
    function AdminSucursalListPage(navCtrl, navParams, modalCtrl, Db, afs) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.Db = Db;
        this.afs = afs;
        //recibe parametro de la reservacion
        this.usertipo = this.navParams.get("usertipo");
        console.log("llego a list");
        console.log(this.usertipo);
        this.afs.collection('sucursales').valueChanges().subscribe(function (s) {
            _this.sucursales = s;
        });
        // this.sucursales = this.Db.list('sucursales').valueChanges();
    }
    AdminSucursalListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminSucursalListPage');
    };
    AdminSucursalListPage.prototype.mostrar_modal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__admin_sucursal_subir_admin_sucursal_subir__["a" /* AdminSucursalSubirPage */]);
        modal.present();
    };
    AdminSucursalListPage.prototype.adminSucursal = function (uid) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__admin_sucursal_perfil_admin_sucursal_perfil__["a" /* AdminSucursalPerfilPage */], { 'uid': uid, 'usertipo': this.usertipo });
    };
    AdminSucursalListPage.prototype.goBack = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__admin_home_admin_home__["a" /* AdminHomePage */]);
    };
    AdminSucursalListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-sucursal-list',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-list\admin-sucursal-list.html"*/'\n\n<ion-header>\n\n    <ion-navbar hideBackButton>\n\n       <ion-row >\n\n         <ion-col>\n\n            <ion-icon (click)="(goBack())" name="arrow-back" style="color:#6C6A6A; zoom:1.3;margin-top:6%;margin-left: 20%;"></ion-icon>\n\n         </ion-col>\n\n         <ion-col>\n\n             <ion-title class="texto-alineado">Sucursales</ion-title>\n\n         </ion-col>\n\n         <ion-col>\n\n         </ion-col>\n\n       </ion-row>\n\n    </ion-navbar>\n\n</ion-header>\n\n<!--<ion-header>\n\n  <ion-navbar>\n\n    <ion-icon (click)="(goBack())" name="arrow-back" style="color:#858484; zoom:1.3;margin-top:8%;margin-left: -1%;"></ion-icon>\n\n    <ion-title>Sucursales</ion-title>\n\n  </ion-navbar>\n\n</ion-header>-->\n\n<ion-content class="fondo_admin">\n\n    <ion-card-header class="marg" color="light">Lista de sucursales</ion-card-header>\n\n    <ion-list class="card_atras" *ngFor="let sucursal of sucursales">\n\n        <ion-item (click)="adminSucursal(sucursal.uid)">\n\n            <ion-thumbnail item-start>\n\n                <img [src]="sucursal.photoURL">\n\n            </ion-thumbnail>\n\n            <h2>{{sucursal.displayName}}</h2>\n\n            <p> {{ sucursal.direccion }} </p>\n\n            <p> {{ sucursal.ciudad }} </p>\n\n        </ion-item>\n\n    </ion-list>\n\n    <ion-fab left bottom>\n\n        <button ion-fab color="light"><ion-icon name="paper"></ion-icon></button>\n\n        <ion-fab-list side="right">\n\n            <button ion-fab (click)="mostrar_modal()">\n\n              <ion-icon name="pin"></ion-icon>\n\n              </button>\n\n        </ion-fab-list>\n\n    </ion-fab>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-sucursal-list\admin-sucursal-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], AdminSucursalListPage);
    return AdminSucursalListPage;
}());

//# sourceMappingURL=admin-sucursal-list.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminReservacionDetallePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_monitoreo_reservas_monitoreo_reservas__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminReservacionDetallePage = /** @class */ (function () {
    function AdminReservacionDetallePage(navCtrl, navParams, monRes, afs) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.monRes = monRes;
        this.afs = afs;
        this.reservaciones = {};
        //recibe parametro de la reservacion
        this.idReservacion = this.navParams.get("idReservacion");
        console.log('idReservacion', this.idReservacion);
        //Traer datos de la reservacion seleccionada
        this.afs.collection('reservaciones').doc(this.idReservacion).valueChanges().subscribe(function (reservacion) {
            _this.reservaciones = reservacion;
            console.log('reservacion doc', _this.reservaciones);
        });
        //consultar tabla areas
        this.afs
            .collection("areas")
            .valueChanges()
            .subscribe(function (data) {
            _this.Areas = data;
            console.log('areas doc', _this.Areas);
        });
        //consultar tabla zonas
        this.afs
            .collection("zonas")
            .valueChanges()
            .subscribe(function (data1) {
            _this.Zonas = data1;
            console.log('zonas doc', _this.Zonas);
        });
        //consultar tabla eventos
        this.afs
            .collection("evento")
            .valueChanges()
            .subscribe(function (data2) {
            _this.Eventos = data2;
        });
        //consultar tabla productos
        this.afs
            .collection("productos")
            .valueChanges()
            .subscribe(function (data3) {
            _this.Productos = data3;
            console.log('productos doc', _this.Productos);
        });
        //consultar tabla productos
        this.afs
            .collection("cupones")
            .valueChanges()
            .subscribe(function (data10) {
            _this.Cupones = data10;
            console.log('cupones doc', _this.Cupones);
        });
        //consultar tabla compartidas
        this.afs
            .collection("compartidas")
            .valueChanges()
            .subscribe(function (data11) {
            _this.cuentasCompartidas = data11;
            console.log("compartidas", _this.cuentasCompartidas);
        });
        //consultar tabla users
        this.afs
            .collection("users")
            .valueChanges()
            .subscribe(function (data12) {
            _this.infoUsers = data12;
            console.log("usuarios", _this.infoUsers);
        });
        //ver si la reservacioon existe en las compartidas
        this.monRes.getReserCom(this.idReservacion).subscribe(function (res11) {
            _this.infoReserCom = res11;
            _this.infoReserCom_num = _this.infoReserCom.length;
            console.log("Este es el resultado de compartidas: ", _this.infoReserCom_num);
        });
        //sacar totales con propina y cupon
        this.monRes.getInfo(this.idReservacion).subscribe(function (res2) {
            _this.infoReservacion = res2;
            console.log("Este es el resultado de reservacion: ", _this.infoReservacion);
            if (res2[0].uidCupon == undefined) {
                _this.validarCupon = 'Noexiste';
                // total de general dependiendo los productos que tenga la reservacion
                _this.monRes.getProductos(_this.idReservacion).subscribe(function (productos) {
                    _this.productos_total = productos;
                    console.log('productos todos', _this.productos_total);
                    _this.total_final = _this.productos_total.reduce(function (acc, obj) { return acc + obj.total; }, 0);
                    _this.propinaRe2 = _this.total_final * res2[0].propina;
                    _this.totalPropina = _this.total_final + _this.propinaRe2;
                    console.log('productos total', _this.total_final);
                });
            }
            else {
                _this.validarCupon = 'Existe';
                console.log('validar cupon', _this.validarCupon);
                _this.propinaRe = res2[0].totalReservacion * res2[0].propina;
                var propinaCalculo = res2[0].totalReservacion * res2[0].propina;
                _this.totalPropinaCupon = res2[0].totalReservacion + propinaCalculo;
                console.log('descuenton', res2[0].totalReservacion);
                console.log('propina', res2[0].propina);
                console.log('propina y cupon', _this.totalPropinaCupon);
            }
        });
    }
    AdminReservacionDetallePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin-reservacion-detalle',template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-reservacion-detalle\admin-reservacion-detalle.html"*/'<ion-header>\n\n\n\n  <ion-navbar class="borde">\n\n    <ion-row>\n\n      <ion-col col-1 align="center">\n\n        <img src="./assets/content/line2.png" class="imgLi" alt="">\n\n      </ion-col>\n\n      <ion-col col-8>\n\n        <div class="AdminEvento">Detalle de Reservación</div>\n\n        <div class="sucursal">Sucursal</div>\n\n      </ion-col>\n\n      <ion-col>\n\n        <div align=\'end\'>\n\n          <img src="./assets/content/corona.png" style="width: 40%;" alt="">\n\n        </div>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n\n\n  <br><br>\n\n\n\n  <ion-row>\n\n    <ion-col col-2></ion-col>\n\n    <ion-col col-8>\n\n      <div *ngFor="let user of infoUsers">\n\n        <div *ngIf="user.uid == reservaciones.idUsuario">\n\n          <p class="nombre">{{user.displayName}}</p>\n\n        </div>\n\n      </div>\n\n\n\n      <h6 *ngFor="let even of Eventos">\n\n        <h6 text-center *ngIf="this.reservaciones.idevento == even.uid" class="nombreE">\n\n          {{ even.titulo }}\n\n        </h6>\n\n      </h6>\n\n    </ion-col>\n\n    <ion-col col-2></ion-col>\n\n  </ion-row>\n\n\n\n  <br><br><br>\n\n\n\n  <ion-row>\n\n    <ion-col col-1></ion-col>\n\n    <ion-col align="center">\n\n      <p class="categoria">Categoria</p>\n\n      <div *ngFor="let even of Eventos">\n\n        <p class="categoriaE" text-center *ngIf="this.reservaciones.idevento == even.uid">\n\n          {{ even.categoria }}\n\n        </p>\n\n      </div>\n\n    </ion-col>\n\n\n\n    <ion-col align="center">\n\n      <ion-icon name="person-outline" class="iconoP"></ion-icon>\n\n      <div *ngFor="let even of Eventos">\n\n        <div text-center class="numE" *ngIf="this.reservaciones.idevento == even.uid">\n\n          {{ this.reservaciones.numPersonas }}\n\n        </div>\n\n      </div>\n\n    </ion-col>\n\n\n\n    <ion-col align="center">\n\n      <p class="zona">Zona</p>\n\n      <div *ngFor="let zon of Zonas">\n\n        <div class="zonaE" text-center *ngIf="this.reservaciones.idZona == zon.uid">\n\n          {{ zon.nombre }}\n\n        </div>\n\n      </div>\n\n    </ion-col>\n\n    <ion-col col-1></ion-col>\n\n  </ion-row>\n\n\n\n  <br><br>\n\n\n\n  <div class="Tproducto" text-center>PRODUCTOS DE LA RESERVACIÓN</div>\n\n    <h2 *ngFor="let pro of Productos">\n\n      <h2 *ngIf="this.reservaciones.idReservacion == pro.idReservacion">\n\n        <ion-item>\n\n          <p>Producto</p>\n\n          <ion-badge class="badge" item-end>{{pro.producto}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item>\n\n          <p>Cantidad</p>\n\n          <ion-badge class="badge" item-end>{{pro.cantidad}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item>\n\n          <p>Costo</p>\n\n          <ion-badge class="badge" item-end>{{pro.costo | currency}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item>\n\n          <p>Total</p>\n\n          <ion-badge class="badge" item-end>{{pro.total | currency}}</ion-badge>\n\n        </ion-item>\n\n      </h2>\n\n    </h2>\n\n\n\n\n\n\n\n    <div *ngIf="validarCupon==\'Noexiste\'">\n\n      <ion-item class="cuponD">\n\n        <p>Propina</p>\n\n        <ion-badge color="dark" item-end>{{propinaRe2 | currency}}</ion-badge>\n\n      </ion-item>\n\n      <ion-item class="cuponD">\n\n        <p>TOTAL RESERVACIÓN</p>\n\n        <ion-badge color="dark" item-end>{{totalPropina | currency}}</ion-badge>\n\n      </ion-item>\n\n    </div>\n\n    <div *ngIf="validarCupon==\'Existe\'">\n\n      <h2 *ngFor="let cup of Cupones">\n\n        <h2 *ngIf="this.reservaciones.uidCupon == cup.uid">\n\n          <ion-item class="cuponD">\n\n            <p>Cupon descuento</p>\n\n            <ion-badge color="dark" item-end>{{cup.valorCupon | currency}}</ion-badge>\n\n          </ion-item>\n\n          <ion-item>\n\n            <p>TOTAL</p>\n\n            <ion-badge color="dark" item-end>{{this.reservaciones.totalReservacion | currency}}</ion-badge>\n\n          </ion-item>\n\n          <ion-item>\n\n            <p>Propina</p>\n\n            <ion-badge color="dark" item-end>{{propinaRe | currency}}</ion-badge>\n\n          </ion-item>\n\n          <ion-item>\n\n            <p>TOTAL FINAL</p>\n\n            <ion-badge color="dark" item-end>{{totalPropinaCupon | currency}}</ion-badge>\n\n          </ion-item>\n\n        </h2>\n\n      </h2>\n\n    </div>\n\n\n\n\n\n    <ion-row>\n\n      <ion-col col-2></ion-col>\n\n      <ion-col>\n\n        <ion-card class="card_atras">\n\n          <p *ngIf="infoReserCom_num != 0" text-center class="blanco_2">\n\n            <br>\n\n            COMPARTIDA ENTRE\n\n            <img src="./assets/content/iconoC.png" class="iconoAr" alt="">\n\n            <br>\n\n          </p>\n\n          <div *ngFor="let compartidas of cuentasCompartidas">\n\n            <div *ngIf="this.reservaciones.idReservacion==compartidas.idReservacion">\n\n              <div *ngFor="let users of infoUsers">\n\n                <div *ngIf="compartidas.telefono==users.phoneNumber">\n\n                  <ion-item color="dark">\n\n                    <ion-avatar item-start>\n\n                      <img src="../assets/imgs/icons/profile.png">\n\n                    </ion-avatar>\n\n                    <ion-badge color="dark" item-end>{{users.displayName}}</ion-badge>\n\n                  </ion-item>\n\n                </div>\n\n              </div>\n\n            </div>\n\n          </div>\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col col-2></ion-col>\n\n    </ion-row>\n\n\n\n    \n\n\n\n\n\n\n\n\n\n  <!-- <ion-item>\n\n    <h2 *ngFor="let zon of Zonas">\n\n      <h2 text-center *ngIf="this.reservaciones.idZona == zon.uid">Zona: {{ zon.nombre }} </h2>\n\n    </h2>\n\n    <h2 text-center>Num. Personas: {{ this.reservaciones.numPersonas }} </h2>\n\n    <h2 *ngFor="let even of Eventos">\n\n      <h2 text-center *ngIf="this.reservaciones.idevento == even.uid">\n\n        Evento: {{ even.titulo }}\n\n      </h2>\n\n    </h2>\n\n    <h2 *ngFor="let even of Eventos">\n\n      <h2 text-center *ngIf="this.reservaciones.idevento == even.uid">\n\n        Categoria: {{ even.categoria }}\n\n      </h2>\n\n    </h2>\n\n    <br>\n\n    <h2 text-center>PRODUCTOS DE LA RESERVACIÓN</h2>\n\n    <h2 *ngFor="let pro of Productos">\n\n      <h2 *ngIf="this.reservaciones.idReservacion == pro.idReservacion">\n\n        <ion-item>\n\n          <p>Producto</p>\n\n          <ion-badge color="dark" item-end>{{pro.producto}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item>\n\n          <p>Cantidad</p>\n\n          <ion-badge color="dark" item-end>{{pro.cantidad}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item>\n\n          <p>Costo</p>\n\n          <ion-badge color="dark" item-end>{{pro.costo | currency}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item>\n\n          <p>Total</p>\n\n          <ion-badge color="dark" item-end>{{pro.total | currency}}</ion-badge>\n\n        </ion-item>\n\n      </h2>\n\n    </h2>\n\n  </ion-item> -->\n\n  <!-- <div *ngIf="validarCupon==\'Noexiste\'">\n\n    <ion-item color="dark">\n\n      <p>Propina</p>\n\n      <ion-badge color="light" item-end>{{propinaRe2 | currency}}</ion-badge>\n\n    </ion-item>\n\n    <ion-item color="dark">\n\n      <p>TOTAL RESERVACIÓN</p>\n\n      <ion-badge color="light" item-end>{{totalPropina | currency}}</ion-badge>\n\n    </ion-item>\n\n  </div>\n\n  <div *ngIf="validarCupon==\'Existe\'">\n\n    <h2 *ngFor="let cup of Cupones">\n\n      <h2 *ngIf="this.reservaciones.uidCupon == cup.uid">\n\n        <ion-item color="dark">\n\n          <p>Cupon descuento</p>\n\n          <ion-badge color="light" item-end>{{cup.valorCupon | currency}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item color="dark">\n\n          <p>TOTAL</p>\n\n          <ion-badge color="light" item-end>{{this.reservaciones.totalReservacion | currency}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item color="dark">\n\n          <p>Propina</p>\n\n          <ion-badge color="light" item-end>{{propinaRe | currency}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item color="dark">\n\n          <p>TOTAL FINAL</p>\n\n          <ion-badge color="light" item-end>{{totalPropinaCupon | currency}}</ion-badge>\n\n        </ion-item>\n\n      </h2>\n\n    </h2>\n\n  </div> -->\n\n  <!-- <ion-card class="card_atras">\n\n    <p *ngIf="infoReserCom_num != 0" text-center class="blanco_2">COMPARTIDA ENTRE</p>\n\n    <div *ngFor="let compartidas of cuentasCompartidas">\n\n      <div *ngIf="this.reservaciones.idReservacion==compartidas.idReservacion">\n\n        <div *ngFor="let users of infoUsers">\n\n          <div *ngIf="compartidas.telefono==users.phoneNumber">\n\n            <ion-item color="dark">\n\n              <ion-avatar item-start>\n\n                <img src="../assets/imgs/icons/profile.png">\n\n              </ion-avatar>\n\n              <ion-badge color="light" item-end>{{users.displayName}}</ion-badge>\n\n            </ion-item>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </ion-card> -->\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\admin-reservacion-detalle\admin-reservacion-detalle.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_monitoreo_reservas_monitoreo_reservas__["a" /* MonitoreoReservasProvider */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], AdminReservacionDetallePage);
    return AdminReservacionDetallePage;
}());

//# sourceMappingURL=admin-reservacion-detalle.js.map

/***/ })

},[507]);
//# sourceMappingURL=main.js.map