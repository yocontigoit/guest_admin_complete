webpackJsonp([5],{

/***/ 767:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalmesasPageModule", function() { return ModalmesasPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modalmesas__ = __webpack_require__(813);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ModalmesasPageModule = /** @class */ (function () {
    function ModalmesasPageModule() {
    }
    ModalmesasPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__modalmesas__["a" /* ModalmesasPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__modalmesas__["a" /* ModalmesasPage */]),
            ],
        })
    ], ModalmesasPageModule);
    return ModalmesasPageModule;
}());

//# sourceMappingURL=modalmesas.module.js.map

/***/ }),

/***/ 813:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalmesasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_gestion_reservaciones_gestion_reservaciones__ = __webpack_require__(218);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ModalmesasPage = /** @class */ (function () {
    function ModalmesasPage(navCtrl, alertCtrl, navParams, viewCtrl, reservProv) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.reservProv = reservProv;
        this.items = [];
        this.ids = [];
        this.reservacion = {};
        this.horas = {};
        this.estatus = false;
        this.idReserv = this.navParams.get("idReserv");
        console.log("Id Reserv: ", this.idReserv);
        this.idZona = this.navParams.get("idZona");
        console.log("Zona: ", this.idZona);
    }
    ModalmesasPage.prototype.ionViewDidLoad = function () {
        this.getMesas();
    };
    ModalmesasPage.prototype.getMesas = function () {
        var _this = this;
        this.reservProv.getMesas(this.idZona).subscribe(function (mesas) {
            _this.mesas = mesas;
            var longitud = _this.mesas.length;
            console.log("Esta es la longitud: ", longitud);
            console.log("mesas JAJA: ", _this.mesas);
        });
    };
    ModalmesasPage.prototype.selecMesa = function (idMesa, noMesa) {
        var _this = this;
        this.reservProv.getOneReservación(this.idReserv).subscribe(function (reservacion) {
            _this.reservacion = reservacion;
            console.log("Esta es la reservacion: ", _this.reservacion);
            var reserva = {
                idReservacion: _this.reservacion.idReservacion,
                fecha: _this.reservacion.fechaR_,
                hora: _this.reservacion.hora,
                idMesa: idMesa,
                noMesa: noMesa
            };
            _this.consultaHorarios(idMesa, reserva);
        });
    };
    ModalmesasPage.prototype.consultaHorarios = function (idMesa, reserva) {
        var _this = this;
        var alert = this.alertCtrl.create();
        alert.setTitle("Horarios Reservados");
        this.reservProv.getMesaReservas(idMesa, reserva).subscribe(function (horas) {
            _this.horas = horas;
            // console.log("ESta afuera: ", this.horas);  
            for (var _i = 0, _a = _this.horas; _i < _a.length; _i++) {
                var item = _a[_i];
                _this.hora = item.hora;
                var jaja = '1';
                alert.addInput({
                    // type: 'radio',
                    // label: 'blue',
                    value: _this.hora,
                    checked: true,
                    disabled: true
                });
            }
        });
        var hora1 = "06:00 PM";
        var hora2 = "08:00 PM";
        // alert.setSubTitle(hora1
        // + "<br>" 
        // + hora2);   
        alert.addButton("Cancelar");
        alert.addButton({
            text: "Aceptar",
            handler: function (data) {
                _this.reservProv.mesaReservacion(reserva);
                // this.testRadioOpen = false;
                // this.cancelarMotivo = data;
                // // console.log("Este es el resultado: ",this.cancelarMotivo);
                // let status = "Cancelado";
                // this.pedidosProv.cancelarServicio(
                //   this.servicioID,
                //   status,
                //   this.cancelarMotivo
                // );
                // this.mensajeCancelado(playerID, username, this.cancelarMotivo);
                // localStorage.removeItem("servIniciado");
                // localStorage.removeItem("tipoS");
                // localStorage.removeItem("yendo");
                // localStorage.removeItem("comprando");
                // localStorage.removeItem("comprado");
                // localStorage.removeItem("llevandolo");
                // localStorage.removeItem("enpuerta");
                // localStorage.removeItem("enviando");
                // this.navCtrl.setRoot("HomePage");
            }
        });
        alert.present();
    };
    // asignarMesa(idReserv,idZona){
    //   let modal = this.modalCtrl.create("ModalmesasPage",{  
    //     idReserv: idReserv,   
    //     idZona: idZona
    //   });
    //   modal.present();    
    // } 
    // consultarMesas() {
    //   this.reservProv.consultarMesas().subscribe(mesas => {
    //     this.mesas = mesas;
    //     console.log("Todas las mesas: ", this.mesas);
    //   });
    // }
    // selecMesa(idMesa) {
    //   this.reservProv.getOneReservación(this.idReserv).subscribe(reservacion => {
    //     this.reservacion = reservacion;
    //     console.log("Esta es la reservacion: ", this.reservacion);
    //     let reserva = {
    //       idReservacion: this.reservacion.idReservacion,
    //       hora: this.reservacion.fechaR_,
    //       fecha: this.reservacion.hora,
    //       idMesa: idMesa
    //     };
    //     this.reservProv.mesaReservacion(reserva);
    //   });            
    //   let items = JSON.parse(localStorage.getItem("mesas"));   
    //   if (items != null) {
    //     var res = items.indexOf(idMesa);
    //     if (res == -1) {
    //       let estatus = "ocupada";
    //       this.reservProv.actualizaEstatus(idMesa, estatus);
    //       console.log("No esta la mesa en el arreglo");
    //       let mesas =idMesa;
    //       this.items = JSON.parse(localStorage.getItem('mesas'));                      
    //       this.items.push(mesas);
    //       localStorage.setItem("mesas", JSON.stringify(this.items));
    //     } else {
    //      console.log("Esta en esta posicion del arreglo: ",res);
    //      this.items = JSON.parse(localStorage.getItem('mesas'));
    //       this.items.splice(res, 1);
    //       localStorage.setItem("mesas", JSON.stringify(this.items));
    //       let estatus = "libre";
    //       this.reservProv.actualizaEstatus(idMesa, estatus);
    //     }
    //   } else {  
    //     console.log("Primera vez");
    //     let mesas = idMesa;
    //     this.items.push(mesas);
    //     localStorage.setItem("mesas", JSON.stringify(this.items));
    //     let estatus = "ocupada";
    //     this.reservProv.actualizaEstatus(idMesa, estatus);
    //   }
    // }
    // consultarDatos() {
    //   setTimeout(() => {
    //     this.prueba = JSON.parse(localStorage.getItem("mesas"));
    //     console.log("Prueba: "+this.prueba);
    //   }, 100);
    // }
    ModalmesasPage.prototype.Cerrar = function () {
        // this.reservProv.Cancelar();
        this.cerrar();
    };
    ModalmesasPage.prototype.Aceptar = function () {
        // let mesas = JSON.parse(localStorage.getItem("mesas"));
        // // console.log("Este es el id de la reservacion: ", mesas);
        // this.reservProv.Aceptar(this.idReserv, mesas);
        this.cerrar();
    };
    ModalmesasPage.prototype.cerrar = function () {
        this.viewCtrl.dismiss();
    };
    ModalmesasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-modalmesas",template:/*ion-inline-start:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\modalmesas\modalmesas.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <!-- <ion-title>modalmesas</ion-title> -->\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="fondo2">\n\n    <ion-item-group>\n\n        <ion-row class="grid-3 grid-example">\n\n            <ion-col width-50 class="grid-item" *ngFor="let mesa of mesas">\n\n                <div *ngIf="mesa.estatus==\'libre\'">\n\n                    <div class="grid-item-wrapper">\n\n                        <div class="numPersonas" text-center>\n\n                            {{mesa.numPersonas}}\n\n                        </div>\n\n                        <div class="grid-item-inner" (click)="selecMesa(mesa.id, mesa.mesa)">\n\n                            <ion-icon class="icono"><img src="../assets/imgs/icons/mesa.png" /></ion-icon>\n\n                            <div class="grid-item-inner" class="color1">\n\n                            </div>\n\n                        </div>\n\n                        <div class="numMesas" text-center>\n\n                            {{mesa.mesa}}\n\n                        </div>\n\n                    </div>\n\n                </div>\n\n                <!-- <div *ngIf="mesa.estatus==\'ocupada\'">\n\n                    <div class="numPersonas" text-center>\n\n                        {{mesa.numPersonas}}\n\n                    </div>\n\n                    <div class="grid-item-wrapper1">\n\n                        <div class="grid-item-inner1" (click)="selecMesa(mesa.id)">\n\n                            <ion-icon class="icono"><img src="../assets/imgs/icons/mesa.png" /></ion-icon>\n\n                             <div class="grid-item-inner" class="color1">\n\n                                {{mesa.numPersonas}}\n\n                            </div> \n\n                        </div>\n\n                        <div class="numMesas" text-center>\n\n                            {{mesa.mesa}}\n\n                        </div>\n\n                    </div>\n\n                </div> -->\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-item-group>\n\n\n\n    <div>\n\n        <div *ngIf="estatus==true" class="numMesas">\n\n            Ya hay reservaciones de la mesa\n\n        </div>\n\n        <div *ngIf="estatus == false" class="numMesas">\n\n            No hay reservaciones\n\n        </div>\n\n    </div>\n\n    <br>\n\n    <br>\n\n    <br>\n\n    <br>\n\n\n\n    <ion-row>\n\n        <ion-col text-center (click)="Cerrar()">\n\n            <ion-icon ios="ios-close-circle" md="md-close-circle" class="icono"></ion-icon>\n\n        </ion-col>\n\n        <!-- <ion-col text-center (click)="Aceptar()">\n\n            <ion-icon ios="ios-checkmark-circle" md="md-checkmark-circle" class="icono"></ion-icon>\n\n        </ion-col> -->\n\n    </ion-row>\n\n</ion-content>'/*ion-inline-end:"C:\Users\cvict\Desktop\guest_admin_complete\src\pages\modalmesas\modalmesas.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_gestion_reservaciones_gestion_reservaciones__["a" /* GestionReservacionesProvider */]])
    ], ModalmesasPage);
    return ModalmesasPage;
}());

//# sourceMappingURL=modalmesas.js.map

/***/ })

});
//# sourceMappingURL=5.js.map